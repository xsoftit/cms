<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    protected $permissions = [
        //SEED_HERE
    ];

    public function run()
    {
        foreach ($this->permissions as $name => $dependencies) {
            if (!Permission::where('name', $name)->first()) {
                $newPermission = new Permission([
                    'name' => $name
                ]);
                $newPermission->dependencies = json_encode($dependencies);
                $newPermission->save();
            }
        }
    }
}
