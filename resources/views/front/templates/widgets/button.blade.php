@if($data->get('text') || $data->get('link'))
    <a href="{{$data->get('link')}}"
       class="btn {{$data->get('class')}}">{!! ($data->get('path'))?'<img class="svg-fix" src="'.$data->get('path').'" alt="'.$data->get('alt').'">':'' !!}{!!  $data->get('text') !!}</a>
@endif
