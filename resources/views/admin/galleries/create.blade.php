@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.galleries.title.index')}} | {{__('app.galleries.title.create')}}
@endsection

@section('title')
    {{__('app.galleries.title.create')}}
@endsection

@section('subtitle')
    {{__('app.galleries.subtitle.create')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make(
        [__('app.dashboard.title'),route('admin.dashboard')],
        [__('app.galleries.title.index'),route('admin.galleries.index')],
        [__('app.galleries.title.create')]
    ) !!}
@endsection

@section('page-content')
    @php($languages = LanguageHelper::getAllFront())
    <form action="{{route('admin.galleries.store')}}" method="POST">
        @csrf
        <div class="form-group">
            <label>{{__('app.galleries.fields.name')}}*</label>
            <input type="text" name="name" value="{{old('name')}}" class="form-control" required>
        </div>
        <div class="form-group gallery-list">
            <label>{{__('app.galleries.fields.items')}}</label>
            <div id="addItemRow" class="row">
                <div class="col-12">
                    <button type="button" id="addItem"
                            class="btn btn-primary">{{__('app.galleries.buttons.addItem')}}</button>
                </div>
            </div>
        </div>
        <div class="form-group h-20">
            <input type="submit" class="btn btn-outline-success float-right" value="{{__('app.save')}}">
        </div>
    </form>
    <template id="galleryItemTemplate">
        @include('cms::admin.galleries.partials.item')
    </template>
@endsection

@section('js')
    @parent
    @include('cms::admin.galleries.partials.galleryJs')
@endsection
