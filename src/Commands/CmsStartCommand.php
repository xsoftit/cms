<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Xsoft\Cms\CmsServiceProvider;
use Xsoft\Cms\Helpers\LanguageHelper;
use Xsoft\Cms\Helpers\SourceModel\Field;
use Xsoft\Cms\Helpers\SourceModel\SourceModel;
use Xsoft\Cms\Services\Admin\WidgetService;

class CmsStartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:start {--install} {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $composer;

    public function __construct(Composer $composer)
    {
        $this->composer = $composer;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::queue('storage:link', ['-q' => '']);
        echo 'Storage link created' . PHP_EOL;
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsStorage", '--force' => true]);
        echo 'Storage structure created' . PHP_EOL;
        Artisan::queue('make:cms:directories', ['-q' => '']);
        echo 'CMS directories generated' . PHP_EOL;

        if ($this->option('install')) {
            if ($this->confirm('Installing "CMS" again will reset your routing and MenuHelper. Continue?')) {
                $this->install();
            }
        } elseif ($this->option('update')) {
            $this->update();
        }
        $this->versionUpdate();
    }

    protected function install()
    {
        //Spatie
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "config", "--provider" => 'Spatie\Permission\PermissionServiceProvider', '--force' => true]);
        echo 'Spatie Permissions generated' . PHP_EOL;

        //DataTables
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "datatables", '--force' => true]);
        echo 'DataTables installed' . PHP_EOL;

        //CMS publish
        $this->publish(true);
        $this->adminPanelPublish(true);
        $this->pickTemplate();
        $this->delete();

        //CMS config
        $app = File::get(config_path() . '/app.php');
        $app = str_replace("'locale' => 'en'", "'locale' => 'lang'", $app);
        $app = str_replace("'fallback_locale' => 'en'", "'fallback_locale' => 'default'", $app);
        file_put_contents(config_path() . '/app.php', $app);
        Artisan::queue('config:cache', ['-q' => '']);
        echo 'Config files generated' . PHP_EOL;

        //ADD COMMANDS TO RUN AFTER COMPOSER UPDATE COMMAND
        $file = file_get_contents(base_path('composer.json'));
        $data = json_decode($file, true);
        if (!Str::contains($file, "php artisan cms:start --update")) {
            $data['scripts']["post-update-cmd"][] = "php artisan cms:start --update";
            $data['scripts']["post-update-cmd"][] = "php artisan config:clear";
            $data['scripts']["post-update-cmd"][] = "php artisan view:clear";
            $data['scripts']["post-update-cmd"][] = "php artisan cms:version --update";
            file_put_contents(base_path('composer.json'), json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
            echo 'Composer.json updated' . PHP_EOL;
        }

        $this->installArticle();
        Artisan::queue('migrate:refresh', ['-q' => '']);
        echo 'Migrations refreshed' . PHP_EOL;
        $this->languagePick();
        $this->composer->dumpAutoloads();
        $this->seed();
    }

    protected function update()
    {
        $this->publish();
        $this->adminPanelPublish();
        Artisan::queue('cms:prepareUrls', ['-q' => '']);
        Artisan::queue('cms:pageLanguages:fix', ['-q' => '']);
        WidgetService::createFrontServices();

        Artisan::queue('migrate');
        $this->composer->dumpAutoloads();
        $this->seed();
    }

    private function seed()
    {
        Artisan::queue('db:seed');
        echo 'Database seeded!' . PHP_EOL;
    }

    private function adminPanelPublish($install = false)
    {
        echo 'Publishing AdminPanel. Please wait...' . PHP_EOL;
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelPublic", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelSass", '--force' => true]);
        if ($install) {
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelSassVariables", '--force' => true]);
        }
        if (File::exists(public_path() . '/admin/vendor/admin-panel/css')) {
            File::moveDirectory(public_path() . '/admin/vendor/admin-panel/css', public_path() . '/vendor/admin-panel/css', true);
        }
        File::moveDirectory(public_path() . '/vendor/DataTables', public_path() . '/admin/vendor/DataTables', true);
        File::moveDirectory(public_path() . '/vendor/fontawesome', public_path() . '/admin/vendor/fontawesome', true);
        File::moveDirectory(public_path() . '/vendor/jquery-ui', public_path() . '/admin/vendor/jquery-ui', true);
        File::moveDirectory(public_path() . '/vendor/admin-panel', public_path() . '/admin/vendor/admin-panel', true);
        File::deleteDirectory(public_path() . '/vendor');
        File::deleteDirectory(public_path() . '/images');
        echo 'AdminPanel published' . PHP_EOL;
    }

    private function pickTemplate()
    {
        $template = $this->choice('Pick cms template', ['Default', 'AdminPro'], 'Default');
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cms" . $template . "Layout", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cms" . $template . "LayoutVariables", '--force' => true]);
    }

    private function publish($install = false)
    {
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsSass", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsPublic", '--force' => true]);
        if ($install) {
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsConfig", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsViews", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsLang", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsCustom", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsMenuHelper", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsUser", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsMiddleware", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsSeeders", '--force' => true]);
        } else {
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsCustom"]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cmsViews"]);
        }

        echo 'Files published' . PHP_EOL;
    }

    private function delete()
    {
        File::delete(resource_path() . '/views/welcome.blade.php');
        File::delete(resource_path() . '/views/home.blade.php');
        File::delete(app_path() . '/Http/Controllers/HomeController.php');
    }

    private function versionUpdate()
    {
        CmsVersion::updateVersionInConfig();
    }

    private function languagePick()
    {
        $choiceLanguages = [];
        $installedLanguages = LanguageHelper::getAll();
        foreach (CmsServiceProvider::LANGUAGES as $code => $lang) {
            if (!in_array($code, $installedLanguages)) {
                $choiceLanguages[$code] = $lang;
            }
        }
        if (count($choiceLanguages)) {
            $languageCode = $this->choice(
                'Pick language you want to install',
                $choiceLanguages
            );
            LanguageHelper::languageCreate($languageCode, $choiceLanguages[$languageCode]);
            unset($choiceLanguages[$languageCode]);
            if (count($choiceLanguages)) {
                $anotherLang = $this->confirm('Install another language?', true);
                if ($anotherLang) {
                    return $this->languagePick();
                }
            }
        }
        $installedLanguages = LanguageHelper::getAll();
        $main = $this->choice(
            'Which language should be main language?',
            $installedLanguages
        );
        $file = File::get(database_path('/seeds/LanguageSeeder.php'));
        $data = "//SEED_HERE
        
                Config::where('name', 'default_lang')->update(['value' => '$main']);
                Config::where('name', 'default_front_language')->update(['value' => '$main']);";
        $file = Str::replaceFirst('//SEED_HERE', $data, $file);
        File::put(database_path('/seeds/LanguageSeeder.php'), $file);
        return true;
    }

    private function installArticle()
    {
        $installArticle = $this->confirm('Would you like to install Article Source Model?', true);
        if (!$installArticle) {
            echo PHP_EOL;
            return;
        }
        $sourceModel = new SourceModel('Article', true, false, true);
        $sourceModel->addField(new Field('string', 'name'));
        $sourceModel->addField(new Field('boolean', 'active', ['default' => '0']));
        $sourceModel->addField(new Field('date', 'start_date', ['nullable' => 1]));
        $sourceModel->addField(new Field('date', 'end_date', ['nullable' => 1]));
        $sourceModel->addField(new Field('text', 'main_image'));
        $sourceModel->addField(new Field('integer', 'gallery_id', ['nullable' => 1]));
        $sourceModel->addContentField(new Field('string', 'title'));
        $sourceModel->addContentField(new Field('string', 'slug'));
        $sourceModel->addContentField(new Field('text', 'abstract', ['nullable' => 1]));
        $sourceModel->addContentField(new Field('text', 'content'));
        $sourceModel->addContentField(new Field('string', 'language'));
        if (!$sourceModel->handle()) {
            $sourceModel->handleSourceModel();
            return;
        }
        File::copy(__DIR__ . '\\..\\..\\sourceModels\\Article\\lang\\article.php', resource_path('\\lang\\default\\models\\article.php'));
        File::copy(__DIR__ . '\\..\\..\\sourceModels\\Article\\views\\admin\\article\\create.blade.php', resource_path('\\views\\admin\\article\\create.blade.php'));
        File::copy(__DIR__ . '\\..\\..\\sourceModels\\Article\\views\\admin\\article\\edit.blade.php', resource_path('\\views\\admin\\article\\edit.blade.php'));
        echo 'Article created possibly successfully' . PHP_EOL;
    }
}
