# CMS

Cms Package

`composer require xsoft/cms 2.*`

## Installation

###Add:
1. In your _Providers/AppServiceProvider::register()_ : `Schema::defaultStringLength(191);` 
2. To your _.env_ file:

```
ADMIN_DOMAIN=admin.YOUR_DOMAIN
FRONT_DOMAIN=YOUR_DOMAIN
```

###Use command

`php artisan cms:start --install`

### **Warning!**

Using this command can break application, so use it only on project Init!

In order for Hermit/Logs to log errors use command `php artisan logs:install` and follow instructions given in console.

#### Initialize styles

Add 
```
mix.sass('resources/sass/AdminPanel/main.scss', 'public/admin/vendor/admin-panel/css/adminPanel.css')
.sass('resources/sass/admin/skin.scss', 'public/admin/css')
.sass('resources/sass/admin/custom.scss', 'public/admin/css');
``` 
In your _webpack.mix.js_

Then run `npm install` and `npm run dev`

## Commands

* `cms:start {--install} {--update}`
    * creates storage:link and storage structure
    * `--install` - first command that should be run after downloading the package
    * `--update` - run this when it is specified in update notes
* `cms:version {--update}` 
    * displays currently installed version of cms
    * `--update` - updates version in configs table
* `make:cms:directories` - checks and creates necessary directories
* `make:cms:globalWidget` - create global widget
* `make:cms:section` - create custom section
* `make:cms:widgetTemplate {name?}` - create custom widget
* `make:cms:model {model_name}` - create source model

## Updates

```
php artisan cms:start --update
```

### * -> 2.1
Reworked front views rendering and added customizable url for pages.

**Information about how to update version 1.\* to 2.1 is specified in `V2UPDATE.md` file.**

    
# Variables with data accessible in front views

`resources\views\front\templates\sections\*`<br>
`resources\views\front\templates\section.blade.php`
#### `Section $section`

---
##
`resources\views\front\templates\widgets\*`
#### `FrontWidget $data`

Data for widgets is build based on specified inputs on widget modal. Inputs which value should be passed to front widget must have 'data' class. That data is then accesible by `get($element)` method where `$element` is the created input name.

If you want to manage your data before passing it to widgets blade file add to that widgets modal in `resources\views\admin\templates\widgets\modals\YOUR_WIDGET`:
```
    <input id="source_model" name="custom_front_model" type="text" value="NAME" class="data static no-clear" hidden>
    <input id="source_content_type" name="custom_content_type" type="text" value="CONTENT_TYPE" class="data static no-clear" hidden>
```
* `NAME` - partial name of the front service where you can manage your data (`app\Htpp\Services\Front\'NAME'Service.php`)
* `CONTENT_TYPE` - name of method inside `app\Htpp\Services\Front\'NAME'Service.php` that will be called to get data

Method must have the following format
*IMPORTANT:* DO NOT OVERRIDE `$data` VARIABLE. It is an array that have important information that is used on widget rendering process. However you can add whatever you need to it and then access on blade by `$data->get('your_index')`.
```
    static public function CONTENT_TYPE($widget, $data, $pageLanguage)
    {
            ***YOUR CODE HERE***
        return [
            'data' => $data,
            'blade' => $widget->template->blade_name,
        ];
    }
```

---
##
`resources\views\front\templates\widget.blade.php`
#### `FrontWidget $frontWidget`

---
##
`resources\views\front\layouts\*`<br>
`resources\views\front\index.blade.php`
#### `DataProcessor $dataProcessor`

# Front objects Classes
##Column
`Xsoft\Cms\Helpers\FrontProcessors\Column`

Constructor accepts PageLanguage object, Section object, column order, column size.
###Available methods:
* `getSize()` - returns column size (for bootstraps css 'col' class)
* `getContainers()` - returns Collection of Container objects 
* `getWidgets()` - returns Collection of FrontWidget objects based on widgets added to this column in administration panel
* `getView()` - returns View object (contains widgets ready to render)

##DataProcessor
`Xsoft\Cms\Helpers\FrontProcessors\DataProcessor`

Constructor requires PageLanguage object.
###Available methods:
* `getPageLanguage()` - returns PageLanguage object
* `getLanguage()` - returns language code fetched from given PageLanguage
* `getSections()` - returns Collection of Section objects
* `getHeaderMenu()` - returns View object for header menu
* `getFooterMenu()` - returns View object for footer menu
* `getSourceObject()` - returns Model object when page is a single view of specified source model otherwise returns null
* `getMetaData()` - returns View object of metadata specified in `resources\views\front\layouts\partials\metaData.blade.php`
* `getPageOptions()` - returns an array of options specified in administration panel for given in constructor PageLanguage object
* `getMainPageUrl()` - returns string with url to current language main page
* `getFrontLanguageChangeData()` - returns array of urls for `resources\views\front\layouts\partials\languageChange.blade.php`

`Each menu is defined in resources\views\front\layouts\partials\menu.blade.php`

##FrontWidget
`Xsoft\Cms\Helpers\FrontProcessors\FrontWidget`

Constructor requires array of data processed in `Xsoft\Cms\Services\Front\WidgetService` (if widget displays data from source model data is processed in: `App\Http\Services\Front\'MODEL_NAME'Service.php`).
### Available methods:
* `get($element)` - `$element` - index of data to return it's value or null if given element doesn’t exist
* `all()` - returns array with data
* `getTag()` - returns PageSection objects template tag field value
* `getClasses()` - returns string of classes defined in administration panel for this widget or null
* `getStyles()` - returns string of styles defined in administration panel for this widget or null
* `getBladeName()` - returns Widget objects template blade_name field value

##Section
`Xsoft\Cms\Helpers\FrontProcessors\Section`

Constructor requires PageLanguage object nad PageSection object.
###Available methods:
* `getPageSection()` - returns PageSection object
* `getClasses()` - returns string of classes defined in administration panel for this section or null
* `getStyles()` - returns string of styles defined in administration panel for this section or null
* `getSlug()` - returns PageSection objects slug field value
* `getBladeName()` - returns PageSection objects template blade_name field value
* `getTag()` - returns PageSection objects template tag field value
* `getContainers()` - returns Collection of Container objects (only those that are attached directly to section)
* `getColumns()` - returns Collection of Column objects
* `getColumn($order)` - returns Column object specified by `$order` variable
* `getColumnView($order)` - returns View object of Column object specified by `$order` variable
* `getView()` - returns View object for section
* `getSectionContent()` - returns View object of all content wrapped by specified containers

#Partials

###FileManager
```
@include('cms::admin.filemanager.manager',[
    'type' => 'single|multi', (REQUIRED)
    'fileTypes' => ['image','icon',...], (REQUIRED)
    'name' => INPUT_NAME, (REQUIRED)
    'data' => EXISTING_VALUE,
    'label' => INPUT_LABEL,
    'sublabel' => INPUT_SUBLABEL,
    'required' => true|false,
    'deletable' => true|false
])
```

###RouteSelect
```
{!! RouteSelector::build(
    INPUT_LABEL,
    INPUT_NAME,
    EXISTING_VALUE
) !!}
```

###URL
```
@include('cms::admin.partials.url',[
    'label' => INPUT_LABEL, (REQUIRED)
    'item' => OBJECT_WITH_URL, (REQUIRED)
    'name' => INPUT_NAME (REQUIRED)
])
```

###GallerySelect
```
@include('cms::admin.partials.gallerySelect',[
    'label' => INPUT_LABEL,
    'name' => INPUT_NAME,
    'value' => GALLERY_ID
])
```
