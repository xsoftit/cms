@section('js')
    @parent
    <script>
        body = $('body');
        sectionModals = {
            'add': $('#add-section-modal'),
            'edit': $('#edit-section-modal'),
        };
        let pageSectionTemplateSelect = sectionModals.add.find('#page_section_template_select');

        $(document).ready(function () {

            pageSectionTemplateSelect.on('change', function () {
                pageSectionTemplateChange();
            });

            body.on('click', '#add-section-button', function () {
                let data = {};
                let type = sectionModals.add.find("input[name='action_type']").val();
                let content = null;
                data.page_language_id = sectionModals.add.find("input[name='page_language_id']").val();
                if(type === 'new'){
                    content = $('#section-new');
                    data.page_section_template_id = sectionModals.add.find("select[name='page_section_template_id']").val();
                    data.name = sectionModals.add.find("input[name='name']").val();
                }else{
                    content = $('#section-copy');
                    data.page_section_id = sectionModals.add.find("select[name='page_section_id']").val();
                    data.with_content = sectionModals.add.find("input[name='with_content']").is(':checked') ? 1 : 0;
                }
                if (validateForm($(this).parents('form'),content)) {
                    addSectionToPage(data);
                }
            });

            body.on('click','#section-new-link',function(){
                sectionModals.add.find("input[name='action_type']").val('new');
            });

            body.on('click','#section-copy-link',function(){
                sectionModals.add.find("input[name='action_type']").val('copy');
            });

            body.on('change','#page-select',function(){
                let page_select = $(this);
                let section_select = $('#section-select');
                let box = $('#section-select-box');
               if(page_select.val()){
                   section_select.find('option').each(function(){
                      if(!$(this).hasClass('.no-delete')){
                          $(this).remove();
                      }
                   });
                   $.ajax({
                       headers: {
                           'X-CSRF-TOKEN': '{{csrf_token()}}'
                       },
                       method: "POST",
                       data: {
                           page_id: page_select.val(),
                       },
                       url: '{{route('admin.pages.getSections')}}',
                       success: function (data) {
                           console.log(data);
                           $.each(data, function(i,v){
                               section_select.append('<option value="'+v.id+'">'+v.name+'</option>')
                           });
                           section_select.trigger('change');
                       },
                       error: function (data) {
                           alert('"{{__('app.alerts.ajaxError')}}"');
                       }
                   });
                   box.show();
               } else{
                   box.hide();
               }
            });

            body.on('click', '.edit-section', function () {
                currentSection = $(this).closest('li').attr('data-id');
                let loader = $(this).closest('.preview-box').find('.loader');
                prepareEditSectionModal(currentSection);
            });

            body.on('click', '.collapse-trigger', function () {
                let header = $(this).parents('.section-preview-header');
                let content = header.siblings('.section-preview');
                let id = header.parent().attr('data-id');
                let lang = header.parents('.tab-pane').attr('id');
                if (content.height() == 0) {
                    $(this).css('transform', 'rotate(0deg)');
                    content.animate({
                        height: content.get(0).scrollHeight,
                        padding: 10
                    }, 350);
                    content.removeClass('overflow-hidden');
                    localStorage.removeItem('section-' + id);
                } else {
                    content.animate({
                        height: 0,
                        padding: 0
                    }, 350);
                    content.addClass('overflow-hidden');
                    $(this).css('transform', 'rotate(180deg)');
                    localStorage.setItem('section-' + id, 'true');
                }
            });

            body.on('click', '#update-section-button', function () {
                let data = {};
                data.section_id = sectionModals.edit.find("input[name='section_id']").val();
                data.page_id = sectionModals.edit.find("input[name='page_id']").val();
                data.name = sectionModals.edit.find("input[name='name']").val();
                data.active = sectionModals.edit.find("input[name='active']").is(':checked') ? 1 : 0;
                data.classes = sectionModals.edit.find("input[name='classes']").val();
                data.styles = sectionModals.edit.find("textarea[name='styles']").val();
                updateSection(data);
            });

            body.on('click', '#edit-section-modal #deleteSection', function () {
                // let modal = $(this).parents('#edit-section-modal');
                let deleteContent = sectionModals.edit.find('.delete-content');
                deleteContent.css('display', 'flex');
            });

            body.on('click', '#edit-section-modal #deleteSectionCancel', function () {
                // let modal = $(this).parents('#edit-section-modal');
                let deleteContent = sectionModals.edit.find('.delete-content');
                deleteContent.css('display', 'none');
            });

            body.on('click', '#edit-section-modal #deleteSectionConfirm', function () {
                // let modal = $(this).parents('#edit-section-modal');
                let section_id = sectionModals.edit.find('#section-id').val();
                let page_id = sectionModals.edit.find('#page-id').val();
                let deleteContent = sectionModals.edit.find('.delete-content');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    data: {
                        section_id: section_id,
                        page_id: page_id,
                    },
                    url: '{{route('admin.sections.delete')}}',
                    success: function (data) {
                        sectionModals.edit.modal('hide');
                        body.find('.sections-sortable li[data-id="' + section_id + '"]').remove();
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    },
                    complete: function () {
                        deleteContent.css('display', 'none');
                    }
                });
            });

        });

        //----------------------FUNCTIONS----------------------

        function pageSectionTemplateChange() {
            if(pageSectionTemplateSelect.val()){
                let url = '{{route('admin.pageSectionTemplates.preview',['pageTemplate'=>'PAGE_TEMPLATE_ID_PLACEHOLDER'])}}';
                url = url.replace('PAGE_TEMPLATE_ID_PLACEHOLDER', pageSectionTemplateSelect.val());

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    url: url,
                    success: function (data) {
                        sectionModals.add.find('#page-section-preview').html(data);
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    }
                });
            }
        }

        function updateSection(data) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                data: data,
                url: '{{route('admin.sections.update')}}',
                success: function (response) {
                    let element = body.find('.sections-sortable li[data-id="' + data.section_id + '"]');
                    element.find('h3 span').text(data.name);
                    if (data.active) {
                        element.removeClass('element-no-active');
                    } else {
                        element.addClass('element-no-active');
                    }
                    sectionModals.edit.modal('hide');
                    showAlert("{{__('app.sections.alerts.updated')}}", 'success');
                },
                error: function (data) {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            });
        }

        function addSectionToPage(data) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                data: data,
                url: '{{route('admin.pages.addSection')}}',
                success: function (data) {
                    body.find('.sections-sortable').append('<li data-id="' + data.section_id + '">' + data.preview + '</li>');
                    sectionModals.add.modal('hide');
                    sectionModals.add.find('input').val('');
                    sectionModals.add.find('select').val('').trigger('change');
                    sectionModals.add.find('#section-new-link').click();
                    showAlert("{{__('app.sections.alerts.added')}}", 'success');
                },
                error: function (data) {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            });
        }

        function prepareEditSectionModal(section_id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    section_id: section_id,
                    page_language_id: currentPageLangId
                },
                method: "POST",
                url: '{{route('admin.sections.edit')}}',
                success: function (data) {
                    fillEditSectionModal(sectionModals.edit, data.section);
                },
                error: function (data) {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            });
        }

        function fillEditSectionModal(modal, section) {
            modal.find("input[name='section_id']").val(section.id);
            modal.find("input[name='name']").val(section.name);
            modal.find("input[name='page_id']").val(section.pivot.page_id);
            modal.find("input[name='classes']").val(section.classes);
            modal.find("textarea[name='styles']").val(section.styles);
            modal.find('.nav-item:first-child .nav-link').click();
            let checkbox = modal.find("input[name='active']");
            if (section.pivot.active) {
                checkbox.attr('checked', true);
            } else {
                checkbox.attr('checked', false);
            }
            modal.modal('show');
        }


    </script>
@endsection
