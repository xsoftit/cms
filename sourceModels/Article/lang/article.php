<?php

return [

    'menu' => 'Articles',
    'articleList' => 'List of articles',
    'articleContent' => 'Content will be based on Article',
    'title' => [
        'index' => 'Articles',
        'create' => 'Create Article',
        'edit' => 'Edit Article'
    ],
    'subtitle' => [
        'index' => 'List of Articles',
        'create' => 'Create form for Article',
        'edit' => 'Edit form for Article'
    ],
    'breadcrumbs' => [
        'index' => 'Articles',
        'create' => 'Create',
        'edit' => 'Edit'
    ],
    'fields' => [
        'name' => 'Name',
        'url' => 'Url',
        'active' => 'Active',
        'inactive' => 'Inactive',
        'title' => 'Title',
        'content' => 'Content',
        'abstract' => 'Abstract',
        'start_date' => 'Publish start',
        'end_date' => 'Publish end',
        'main_image' => 'Main image',
        'gallery' => 'Gallery'
    ],
    'alerts' => [
        'added' => 'Article added',
        'updated' => 'Article updated',
        'titleEmpty' => 'Title is empty',
        'contentEmpty' => 'Content is empty'
    ]

];
