<?php

namespace Xsoft\Cms\Helpers\FrontProcessors;

use Illuminate\Support\Collection;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Services\Front\WidgetService;

class Column
{
    protected $size;
    protected $containers;
    protected $widgets;
    protected $view;

    public function __construct(PageLanguage $pageLanguage, Section $section, $order, $size)
    {
        $this->size = $size;
        $widgets = new Collection();
        $this->containers = $section->getPageSection()->containers->where('placement_column', $order)->sortByDesc('placement_order');
        foreach ($section->getPageSection()->getColumnWidgets($order) as $widget) {
            $widgets->add(WidgetService::getFrontWidget($pageLanguage, $widget, $section->getPageSection()->id));
        }
        $this->widgets = $widgets;
        $this->view = $this->prepareView();
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getContainers()
    {
        return $this->containers;
    }

    public function getWidgets()
    {
        return $this->widgets;
    }

    public function getView()
    {
        return $this->view;
    }

    private function prepareView()
    {
        $containers = $this->containers;
        $view = $this->handleViewWithContainers($containers);
        return $view;
    }

    private function handleViewWithContainers(Collection $containers)
    {
        $view = "";
        if (count($containers) > 0) {
            $container = $containers->pop();
            $container->view = $this->handleViewWithContainers($containers);
            $view = view('front.templates.container', ['container' => $container]);
        } else if (!empty($this->widgets)) {
            $view = view('front.templates.column', ['frontWidgets' => $this->widgets]);
        }
        return $view;
    }

}
