<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'options',
        'name'
    ];

    // RELATIONS

    public function sections()
    {
        return $this->belongsToMany('Xsoft\Cms\Models\PageSection')->withPivot('order', 'active')->orderBy('order')->with('template', 'widgets');
    }

    public function activeSections()
    {
        return $this->belongsToMany('Xsoft\Cms\Models\PageSection')->withPivot('order', 'active')->orderBy('order')->where('active', 1)->with('template', 'widgets');
    }

    public function pageLanguages()
    {
        return $this->hasMany('Xsoft\Cms\Models\PageLanguage');
    }

    // ATTRIBUTES

    public function getSourceModelIdAttribute()
    {
        $output = null;
        $pageLanguage = $this->pageLanguages()->first();
        if ($pageLanguage) {
            $output = $pageLanguage->source_model_id;
        }
        return $output;
    }

    public function getSourceModelAttribute()
    {
        $output = new SourceModel();
        $id = $this->source_model_id;
        if ($id) {
            $output = SourceModel::find($id);
        }
        return $output;
    }

    // METHODS

    public function getWidgets()
    {
        $sections = $this->sections;
        $all_widgets_names = [];
        $all_widgets = [];
        foreach ($sections as $section) {
            dd($this, $section->getColumnWidgets(0), $section->widgets);
            foreach ($section->widgets as $widget) {
                if (!in_array($widget->blade_name, $all_widgets_names)) {
                    $all_widgets_names[] = $widget->blade_name;
                    $all_widgets[] = $widget;
                }

            }
        }
        return $all_widgets;
    }

}
