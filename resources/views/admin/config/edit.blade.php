@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.config.title')}}
@endsection

@section('title')
    {{__('app.config.title')}}
@endsection

@section('subtitle')
    {{__('app.config.subtitle')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.config.title')]) !!}
@endsection

@section('page-content')
    @include('cms::admin.config.partials.pills',['active' => 'edit'])
    <form action="{{route('admin.config.update')}}" method="POST">
        @csrf
        <div class="row">
            <div class="accordion accordion-bordered order-2" id="accordion" role="tablist">
                @foreach(CmsConfig::categories() as $key => $category)
                    @if($category == 'version')
                        @continue
                    @endif
                    <div class="card {{$category}}"
                         @if($category == 'domains' && !CmsConfig::get('multi_domains')->value) style="display: none" @endif>
                        <div class="card-header" role="tab" id="heading-1">
                            <a data-toggle="collapse" href="#accordion-collapse-{{$key}}" class="collapsed">
                                {{__('app.config.categories.'.$category)}}
                            </a>
                        </div>
                        <div id="accordion-collapse-{{$key}}" class="collapse" role="tabpanel"
                             data-parent="#iaccordion">
                            <div class="card-body">
                                <div class="row">
                                    @foreach(CmsConfig::getByCategory($category) as $field)
                                        <div class="col-12 @if($category !== 'domains') col-md-6 @endif">
                                            <div class="form-group">
                                                <label>{{__('app.config.fields.'.$field->label)}}</label>
                                                @if($field->description)
                                                    <small
                                                        class="mr10">{{ __('app.config.descriptions.'.$field->description)!=''? "(".__('app.config.descriptions.'.$field->description).")":"" }}
                                                    </small>
                                                @endif
                                                @if($field->type == 'lang')
                                                    <select class="select2" name="{{$field->name}}">
                                                        @foreach(LanguageHelper::getAll() as $lang)
                                                            <option value="{{$lang}}"
                                                                    @if($lang == $field->value) selected @endif>{{$lang}}</option>
                                                        @endforeach
                                                    </select>
                                                @elseif($field->type == 'front_lang')
                                                    <select class="select2" name="{{$field->name}}">
                                                        @foreach(LanguageHelper::getAllFront() as $lang)
                                                            <option value="{{$lang}}"
                                                                    @if($lang == $field->value) selected @endif>{{$lang}}</option>
                                                        @endforeach
                                                    </select>
                                                @elseif($field->type == 'checkbox')
                                                    <div class="animated-checkbox">
                                                        <label>
                                                            <input id="{{$field->name}}" type="checkbox"
                                                                   name="{{$field->name}}"
                                                                   @if($field->value == 1) checked @endif>
                                                            <span class="label-text text-capitalize"></span>
                                                        </label>
                                                    </div>
                                                @elseif($field->type == 'domains')
                                                    @php($data = json_decode($field->value,true))
                                                    @php($i = 0)
                                                    @foreach($data as $name => $value)
                                                        <div class="row domains-row mb10" data-number="{{$i}}">
                                                            <div class="col-5">
                                                                <input value="{{$name}}" class="form-control"
                                                                       name="domains[{{$i}}][name]">
                                                            </div>
                                                            <div class="col-5">
                                                                <select class="select2"
                                                                        name="domains[{{$i}}][language]">
                                                                    @foreach(LanguageHelper::getAllFront() as $lang)
                                                                        <option value="{{$lang}}"
                                                                                @if($lang == $value) selected @endif>{{$lang}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-2">
                                                                <span
                                                                    class="btn btn-outline-danger remove-domain">{{__('app.delete')}}</span>
                                                            </div>
                                                            @php($i++)
                                                        </div>
                                                    @endforeach
                                                    <span id="add-domain"
                                                          class="btn btn-secondary float-right">{{__('app.add')}}</span>
                                                @elseif($field->type == 'image')
                                                    <br>
                                                    @include('cms::admin.filemanager.manager',[
                                                        'type' => 'single',
                                                        'fileTypes' => ['image'],
                                                        'name' => $field->name,
                                                        'data' => $field->value,
                                                        'deletable' => true
                                                    ])
                                                @else
                                                    <input type="text" class="form-control" name="{{$field->name}}"
                                                           value="{{$field->value}}">
                                                @endif
                                            </div>
                                            <br>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @if(auth()->user()->hasPermission('urls.show'))
            <div class="row">
                <h3 class="mt30 mb10">{{__('app.config.urls')}}</h3>
                <div class="accordion accordion-bordered order-2" id="url-accordion" role="tablist">
                    @foreach($urls as $key => $model)
                        <div class="card">
                            <div class="card-header" role="tab" id="heading-1">
                                <a data-toggle="collapse" href="#accordion-collapse-{{Str::slug($key)}}"
                                   class="collapsed">
                                    {{$key}}
                                </a>
                            </div>
                            <div id="accordion-collapse-{{Str::slug($key)}}" class="collapse" role="tabpanel"
                                 data-parent="#iaccordion">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($model as $url)
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>[{{$url['language']}}] {{$url['name']}}
                                                        <small>(Id: {{$url['id']}})</small>
                                                    </label>
                                                    <div class="row">
                                                        <div class="text-right">
                                                            <span class="url-span">
                                                                {{$url['hostWithPrefix']}}
                                                            </span>
                                                        </div>
                                                        <div class="col" style="padding-left: 0">
                                                            <input type="text" name="url[{{$url['url_id']}}]"
                                                                   class="form-control"
                                                                   value="{{$url['url']}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <h3 class="mt30 mb10">{{__('app.config.redirects')}}</h3>
                <div class="accordion accordion-bordered order-2" id="redirects-accordion" role="tablist">
                    <div class="card">
                        <div class="card-header" role="tab" id="heading-1">
                            <a data-toggle="collapse" href="#accordion-collapse-redirects"
                               class="collapsed">
                                {{__('app.config.redirects')}}
                            </a>
                        </div>
                        <div id="accordion-collapse-redirects" class="collapse" role="tabpanel"
                             data-parent="#iaccordion">
                            <div class="card-body">
                                <div class="form-group">
                                    @foreach($redirects as $redirect)
                                        <div class="row redirect-item" data-number="{{$redirect->id}}">
                                            <div class="col-4">
                                                <input type="text" name="redirect[{{$redirect->id}}][from]"
                                                       class="form-control"
                                                       value="{{$redirect->from}}">
                                            </div>
                                            <div class="col-1 text-center" style="font-size: 28px">
                                                <i class="af af-arrow-right"></i>
                                            </div>
                                            <div class="col-4">
                                                <input type="text" name="redirect[{{$redirect->id}}][to]"
                                                       class="form-control"
                                                       value="{{$redirect->to}}">
                                            </div>
                                            <div class="col-2">
                                                    <span
                                                        class="btn btn-outline-danger remove-redirect">{{__('app.delete')}}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                    <span id="add-redirect"
                                          class="btn btn-secondary float-right">{{__('app.add')}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row mt30">
            <div class="col-12">
                <button type="submit" class="btn btn-outline-success float-right">{{__('app.save')}}</button>
            </div>
        </div>
    </form>
    <template id="domains-item">
        <div class="row domains-row mb10" data-number="NUMBER_PLACEHOLDER">
            <div class="col-5">
                <input value="" class="form-control"
                       name="domains[NUMBER_PLACEHOLDER][name]">
            </div>
            <div class="col-5">
                <select class="select2" name="domains[NUMBER_PLACEHOLDER][language]">
                    @foreach(LanguageHelper::getAllFront() as $lang)
                        <option>{{$lang}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2">
                <span class="btn btn-outline-danger remove-domain">{{__('app.delete')}}</span>
            </div>
        </div>
    </template>
    <template id="redirects-item">
        <div class="row redirect-item" data-number="NUMBER_PLACEHOLDER">
            <div class="col-4">
                <input type="text" name="redirect[NUMBER_PLACEHOLDER][from]"
                       class="form-control"
                       value="">
            </div>
            <div class="col-1 text-center" style="font-size: 28px">
                <i class="af af-arrow-right"></i>
            </div>
            <div class="col-4">
                <input type="text" name="redirect[NUMBER_PLACEHOLDER][to]"
                       class="form-control"
                       value="">
            </div>
            <div class="col-2">
                <span class="btn btn-outline-danger remove-redirect">{{__('app.delete')}}</span>
            </div>
        </div>
    </template>
@endsection

@section('js')
    @parent
    <script>
        checkDomains();
        $('a[href="#accordion-collapse-2"]').click(function () {
            removeOptions();
        });
        $('#add-domain').on('click', function () {
            let html = $('#domains-item').html();
            let number = 0;
            $('.domains-row').each(function () {
                let oldNumber = parseInt($(this).attr('data-number'));
                if (oldNumber >= number) {
                    number = oldNumber + 1;
                    console.log(number);
                }
            });
            html = $(html.replace(new RegExp('NUMBER_PLACEHOLDER', 'g'), number));
            html.find('select').selectInit();
            html.find('select').val('');
            $(this).before(html);
            removeOptions();
        });
        $('body').on('click', '.remove-redirect', function () {
            $(this).parents('.redirect-item').remove();
            removeOptions();
        });

        $('#add-redirect').on('click', function () {
            let html = $('#redirects-item').html();
            let number = 0;
            $('.redirect-item').each(function () {
                let oldNumber = parseInt($(this).attr('data-number'));
                if (oldNumber >= number) {
                    number = oldNumber + 1;
                    console.log(number);
                }
            });
            html = $(html.replace(new RegExp('NUMBER_PLACEHOLDER', 'g'), number));
            $(this).before(html);
        });
        $('body').on('click', '.remove-domain', function () {
            $(this).parents('.domains-row').remove();
            removeOptions();
        });
        $('body').on('change', '.domains-row select', function () {
            removeOptions();
        });

        function removeOptions() {
            let vals = [];
            $('body').find('.domains-row').each(function () {
                vals.push($(this).find('select').val());
            });
            $('body').find('.domains-row select option').each(function () {
                let select = $(this).parents('select');
                if (vals.includes($(this).val())) {
                    if (select.val() !== $(this).val()) {
                        $(this).attr('disabled', 'disabled');
                    }
                } else {
                    $(this).removeAttr('disabled');
                }
                select.select2('destroy').selectInit();
            });
        }

        $('#multi_domains').on('change', function () {
            checkDomains();
        });

        function checkDomains() {
            if ($('#multi_domains').is(':checked')) {
                $('.domains').show();
            } else {
                $('.domains').hide();
            }
        }
    </script>
@endsection
