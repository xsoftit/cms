<?php

return [
    'name' => 'User',
    'menu' => 'Users',
    'actions' => 'Actions',
    'changePassword' => 'Change password',
    'changeRole' => 'Change role',
    'activate' => 'Activate',
    'deactivate' => 'Deactivate',
    'confirmationQuestion' => 'Are you sure that you want to <b>:action</b> user <b>:user?</b>',
    'title' => [
        'index' => 'Users',
        'edit' => 'Edit :name user',
        'create' => 'Create new user',
        'confirmation' => 'Confirmation'
    ],
    'subtitle' => [
        'index' => 'System users management',
        'edit' => ':name user configuration',
    ],
    'fields' => [
        'name' => 'Name',
        'email' => 'Email',
        'active' => 'Active',
        'inactive' => 'Inactive',
        'password' => 'Password',
        'role' => 'Role'
    ],
    'alerts' => [
        'saved' => 'User :name saved successfully!',
        'activated' => 'User :name activated!',
        'deactivated' => 'User :name deactivated!',
        'passwordChanged' => 'Password changed successfully!',
        'roleChanged' => 'Role changed successfully!',
    ]
];
