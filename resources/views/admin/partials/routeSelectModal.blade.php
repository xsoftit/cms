<div class="modal fade upper-modal" id="route-selector-modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('app.routeSelect.choose')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>{{__('app.routeSelect.chooseType')}}</label>
                    <div class="form-control" style="padding: 0;">
                        <select class="form-control select-data-ajax data type-select"
                                data-url="{{route('admin.sourceModels.getRouteTypes')}}">
                            <option selected></option>
                        </select>
                    </div>
                </div>
                <div class="form-group url-select-box" style="display: none;">
                    <label>{{__('app.routeSelect.choose')}}</label>
                    <div class="form-control" style="padding: 0;">
                        <select class="form-control select-data-ajax url-select"
                                data-url="{{route('admin.sourceModels.getRouteValues')}}">
                            <option selected></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="add-route-button" class="btn btn-outline-success"
                        style="margin: 0">{{__('app.add')}}</button>
                <button type="reset" class="btn btn-outline-light"
                        data-dismiss="modal">{{__('app.close')}}</button>
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>
        let routeSelectModal = $('#route-selector-modal');
        body.on('click', '.route-select-button', function () {
            routeSelectModal.attr('data-name', $(this).attr('data-name'));
            routeSelectModal.modal('show');
        });
        routeSelectModal.on('change', '.type-select', function () {
            if ($(this).val()) {
                routeSelectModal.find('.url-select-box').show();
                routeSelectModal.find('.url-select').selectAjaxInit({class: $(this).val()});
            } else {
                routeSelectModal.find('.url-select-box').hide();
            }
        });
        routeSelectModal.on('hide.bs.modal', function () {
            routeSelectModal.find('.type-select').val('').trigger('change');
            routeSelectModal.find('.url-select-box').hide();
        });
        routeSelectModal.on('click', '#add-route-button', function () {
            let urlSelect = routeSelectModal.find('.url-select');
            let name = routeSelectModal.attr('data-name');
            if (urlSelect.val()) {
                $('.route-select-link[name="' + name + '"]').val(urlSelect.val());
                routeSelectModal.modal('hide');
            }
        });
    </script>
@endsection
