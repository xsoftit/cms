<li><b>{{__('app.widgets.labels.buttonText')}}:</b> <span id="text">{{$widget->getContent('text', $pageLanguage)}}</span></li>
<li><b>{{__('app.widgets.labels.buttonLink')}}:</b> <span id="link">{{$widget->getContent('link', $pageLanguage)}}</span></li>
<li><b>{{__('app.widgets.labels.buttonTitle')}}:</b> <span id="title">{{$widget->getContent('title', $pageLanguage)}}</span></li>
<li><b>{{__('app.widgets.labels.class')}}:</b> <span id="class">{{$widget->getContent('class', $pageLanguage)}}</span></li>
