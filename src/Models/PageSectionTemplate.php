<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class PageSectionTemplate extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'blade_name',
        'columns'
    ];

    //METHODS

    public function preview($editable = null)
    {
        return view('cms::admin.templates.section', ['section' => $this, 'editable' => $editable])->render();
    }

    public function getColumns()
    {
        return json_decode($this->columns, true);
    }
}
