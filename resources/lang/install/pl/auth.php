<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "name" => "Nazwa",
    "email" => "Email",
    "login" => "Login",
    "failed" => "Niepoprawne dane",
    "inactive" => "Konto jest nieaktynwe",
    "password" => "Hasło",
    "register" => "Zarejestruj",
    "throttle" => "Zbyt wiele prób logowania. Spróbuj poownie za :seconds sekund",
    "reset_send" => "Wyślij link resetowania hasła",
    "remember_me" => "Zapamiętaj mnie",
    "confirm_login" => "Zaloguj",
    "confirm_reset" => "Resetuj",
    "reset_password" => "Zresetuj hasło",
    "forgot_password" => "Zapomniałeś hasła?",
    "confirm_register" => "Zarejestruj",
];
