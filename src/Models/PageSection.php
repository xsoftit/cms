<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PageSection extends Model
{
    protected $fillable = [
        'page_id',
        'section_template_id',
        'active',
        'name',
        'slug',
        'order',
        'classes',
        'styles'
    ];

    // RELATIONS

    public function template()
    {
        return $this->belongsTo('Xsoft\Cms\Models\PageSectionTemplate', 'section_template_id');
    }

    public function widgets()
    {
        return $this->belongsToMany('Xsoft\Cms\Models\Widget')->withPivot('placement_column', 'placement_order')->with('contents', 'template');
    }

    public function containers()
    {
        return $this->hasMany('Xsoft\Cms\Models\Container');
    }

    public function page()
    {
        return $this->belongsToMany('Xsoft\Cms\Models\Page');
    }

    // ATTRIBUTES

    public function getBladeNameAttribute()
    {
        return $this->template->blade_name;
    }

    //METHODS

    public function preview($editable = null, $pageLanguage = null)
    {
        return view('cms::admin.templates.section', ['section' => $this, 'editable' => $editable, 'pageLanguage' => $pageLanguage])->render();
    }

    public function getColumns()
    {
        return $this->template->getColumns();
    }

    public function getColumnWidgets($column)
    {
        return $this->widgets->where('pivot.placement_column', $column)->sortBy('pivot.placement_order');
    }


    public function getFront($pageLanguage = null)
    {
        $view = view('front.templates.section', ['section' => $this, 'pageLanguage' => $pageLanguage]);
        return $view;
    }

    public function getFrontColumn($column, $pageLanguage)
    {
        $html = '';
        foreach ($this->getContainers($column) as $container) {
            $html .= '<' . $container->tag . ' class="' . $container->classes . '" style="' . $container->style . '">';
        }
        foreach ($this->getColumnWidgets($column) as $widget) {
            $html .= $widget->getFrontContent($pageLanguage, $this);
        }
        foreach ($this->getContainers($column) as $container) {
            $html .= '</' . $container->tag . '>';
        }
        return $html;
    }

    public function getContainers($column)
    {
        return $this->containers->where('placement_column', $column)->sortBy('placement_order');
    }

    public function generatePreview($pageLanguage)
    {
        $preview = '{*REPLACE*}';
        $columns = '';
        $sectionContainers = $this->getContainers(-1);
        if ($sectionContainers->count()) {
            $this->generateSectionContainers($sectionContainers, $preview);
        }
        if (auth()->user()->hasPermission('sections.edit')) {
            $preview = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.frame', ['id' => $this->id]), $preview);
        }
        foreach ($this->getColumns() as $column_order => $column) {
            $columns .= $this->generateColumn($column, $column_order, $pageLanguage);
        }
        $preview = Str::replaceFirst('{*REPLACE*}', $columns, $preview);
        return $preview;
    }

    public function generateSectionContainers($containers, &$preview)
    {
        $amount = $containers->count();
        foreach ($containers as $key => $container) {
            if ($container == $containers->first()) {
                $preview = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.sectionContainer', ['container' => $container,'amount' => $amount, 'first' => true])->render(), $preview);
            } elseif ($container == $containers->last()) {
                $preview = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.sectionContainer', ['container' => $container,'amount' => $amount, 'last' => true])->render(), $preview);
            } else {
                $preview = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.sectionContainer', ['container' => $container,'amount' => $amount])->render(), $preview);
            }
        }
    }

    public function generateColumn($column, $column_order, $pageLanguage)
    {

        $view = view('cms::admin.templates.partials.column', ['column' => $column])->render();
        $containers = $this->getContainers($column_order);
        $amount = $containers->count();
        foreach ($containers as $container) {
            if ($container == $containers->first()) {
                $view = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.widgetContainer', ['container' => $container,'amount' => $amount, 'first' => true])->render(), $view);
            } elseif ($container == $containers->last()) {
                $view = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.widgetContainer', ['container' => $container,'amount' => $amount, 'last' => true])->render(), $view);
            } else {
                $view = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.widgetContainer', ['container' => $container,'amount' => $amount])->render(), $view);
            }
        }
        if (auth()->user()->hasPermission('sections.edit')) {
            $view = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.frame', ['id' => $this->id, 'column_order' => $column_order]), $view);
        }
        $view = Str::replaceFirst('{*REPLACE*}', view('cms::admin.templates.partials.widgets', ['section' => $this, 'column_order' => $column_order, 'pageLanguage' => $pageLanguage])->render(), $view);
        return $view;
    }

}
