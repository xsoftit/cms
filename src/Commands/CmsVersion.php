<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Xsoft\Cms\Models\Config;

class CmsVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:version {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $version = self::getVersionFromComposer();
        if ($version) {
            echo PHP_EOL . 'xsoft/cms v.' . $version . PHP_EOL;
            if ($this->option('update')) {
                self::versionUpdate($version);
            }
        } else {
            echo 'Could not get version for xsoft/cms package' . PHP_EOL;
        }
    }


    static private function versionUpdate($version)
    {
        $config_version = Config::where('name', 'cms_version')->first();
        if (!$config_version) {
            Config::create([
                'name' => 'cms_version',
                'label' => 'cmsVersion',
                'value' => $version,
                'category' => 'version',
                'type' => 'version'
            ]);
            echo 'Config version entry created' . PHP_EOL;
        } else {
            $config_version->update([
                'value' => $version
            ]);
            echo 'Config version entry updated' . PHP_EOL;
        }
    }

    static private function getVersionFromComposer()
    {
        $file = file_get_contents(base_path('vendor/composer/installed.json'));
        $array = json_decode($file, true);
        $version = null;
        foreach ($array as $package) {
            if ($package['name'] == 'xsoft/cms') {
                $version = $package['version'];
                break;
            }
        }
        return $version;
    }

    static public function updateVersionInConfig()
    {
        $version = self::getVersionFromComposer();
        if ($version) {
            self::versionUpdate($version);
        } else {
            echo 'Could not get version for xsoft/cms package' . PHP_EOL;
        }
    }
}
