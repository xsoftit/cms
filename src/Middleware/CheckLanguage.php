<?php

namespace Xsoft\Cms\Middleware;

use Closure;
use Xsoft\Cms\Helpers\CmsConfig;
use Xsoft\Cms\Helpers\LanguageHelper;
use Xsoft\Cms\Models\Redirect;

class CheckLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = 404;
        if (function_exists('debugbar')) {
            debugbar()->disable();
        }
        $redirect = Redirect::where('from',url()->full())->first();
        if($redirect){
            return redirect($redirect->to,301);
        }
        if (CmsConfig::get('multi_domains')->value) {
            $domains = json_decode(CmsConfig::get('domains')->value, true);
            $currentDomain = key_exists('HTTPS_HOST', $_SERVER) ? $_SERVER['HTTPS_HOST'] : $_SERVER['HTTP_HOST'];
            if (key_exists($currentDomain, $domains)) {
                $language = $domains[$currentDomain];
            }
        } else {
            $params = explode('/', $request->route('url'), 2);
            if ($params[0]) {
                $language = $params[0];
            } else {
                if (key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER)) {
                    $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
                } else {
                    $language = CmsConfig::get('default_front_language')->value;
                }
            }
        }
        $request->merge([
            'language' => $language
        ]);
        return $next($request);
    }
}
