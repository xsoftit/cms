<?php

namespace Xsoft\Cms\Helpers\FrontProcessors;

use Xsoft\Cms\Helpers\FrontMenuHelper;
use Xsoft\Cms\Helpers\LanguageHelper;
use Xsoft\Cms\Models\Language;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Services\Front\SectionService;

class DataProcessor
{
    protected $pageLanguage;
    protected $sourceObject;
    protected $sections;
    protected $headerMenu;
    protected $footerMenu;
    protected $pageOptions;

    public function __construct(PageLanguage $pageLanguage)
    {
        $this->pageLanguage = $pageLanguage;
        $this->sourceObject = $pageLanguage->object;
        $this->headerMenu = FrontMenuHelper::build($pageLanguage, 'main');
        $this->footerMenu = FrontMenuHelper::build($pageLanguage, 'footer');
        if ($pageLanguage->page->options) {
            $this->pageOptions = json_decode($pageLanguage->page->options, true);
        }
        $this->sections = SectionService::makeSections($pageLanguage);
    }

    public function getPageLanguage()
    {
        return $this->pageLanguage;
    }

    public function getLanguage()
    {
        return $this->pageLanguage->language;
    }

    public function getSections()
    {
        return $this->sections;
    }

    public function getHeaderMenu()
    {
        return $this->headerMenu;
    }

    public function getFooterMenu()
    {
        return $this->footerMenu;
    }

    public function getSourceObject()
    {
        return $this->sourceObject;
    }

    public function getMetaData()
    {
        $metaData = view('front.layouts.partials.metaData', ['data' => $this]);
        return $metaData;
    }

    public function getPageOptions()
    {
        $output = [];
        if ($this->pageOptions) {
            $output = $this->pageOptions;
        }
        return $output;
    }

    public function getMainPageUrl()
    {
        $language = $this->getLanguage();
        return LanguageHelper::getMainPageUrl($language);
    }


    public function getFrontLanguageChangeData()
    {
        $pageLanguage = $this->getPageLanguage();
        $current_language = $pageLanguage->language;
        $pageLanguages = $pageLanguage->page->pageLanguages;
        $languages = Language::where('front', 1)->where('hasMain', 1)->pluck('code')->toArray();
        $output = [];
        foreach ($languages as $language) {
            $pLang = $pageLanguages->where('language', $language)->where('active',1)->first();
            if ($language == $current_language) {
                $url = 'current';
            } else {
                $url = LanguageHelper::getMainPageUrl($language);
                if ($pLang) {
                    if (!$pLang->source_model) {
                        $url = $pLang->url(true);
                    } else {
                        $objectUrl = $pageLanguage->object->getContent($language)->url(true);
                        if($objectUrl) {
                            $url = $pageLanguage->object->getContent($language)->url(true);
                        }
                    }
                }
            }
            $output[$language] = $url;
        }
        return $output;
    }
}
