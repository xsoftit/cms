<header>
    @include('front.layouts.partials.header.languageChange',['languages'  => $dataProcessor->getFrontLanguageChangeData()])
    @include('front.layouts.partials.header.logo')
    {!! $dataProcessor->getHeaderMenu() !!}
</header>
