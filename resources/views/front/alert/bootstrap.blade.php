@if (session()->has('alert.messages'))
    @foreach(session()->get('alert.messages') as $message)
    <div class="alert alert-{{ $message['style'] }} alert-dismissible fade in show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button><!-- /close -->
        {!! $message['message'] !!}
    </div><!-- /alert -->
    @endforeach
@endif
