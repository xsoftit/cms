<?php

return [
    'name' => 'Language',
    'title' => [
        'index' => 'Languages',
        'edit' => 'Edit :name admin language',
        'create' => 'Create new admin language'
    ],
    'subtitle' => [
        'index' => 'Admin languages list',
        'edit' => 'Translations for admin panel',
    ],
    'fields' => [
        'name' => 'Name',
        'code' => 'Code',
        'fields' => 'Fields',
        'active' => 'Active',
        'inactive' => 'Inactive'
    ],
    'showEmpty' => 'Show only empty fields',
    'data' => 'Data',
    'alerts' => [
        'saved' => 'Language :name saved successfully!',
        'activated' => 'Language :name activated!',
        'deactivated' => 'Language :name deactivated!',
        'isMainLanguage' => "You can't deactivate main admin Language!"
    ]
];
