<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Xsoft\Cms\Traits\UrlTrait;

class ExampleModelContent extends Model
{
    use SoftDeletes, UrlTrait;

    protected $fillable = [
//FILLABLE
        'FLOOR_NAME_id',
    ];

    const PARENT_MODEL = 'ExampleModel';

    //RELATIONS
    public function parent()
    {
        return $this->belongsTo('App\ExampleModel', 'FLOOR_NAME_id');
    }

    //METHODS
    static public function fields($string = false)
    {
        $fields = [
//FILLABLE
            'FLOOR_NAME_id',
        ];
        if ($string) {
            $output = '';
            foreach ($fields as $key => $field) {
                $output .= $field;
                if ($key + 1 != count($field)) {
                    $output .= '; ';
                }
            }
        } else {
            $output = $fields;
        }
        return $output;
    }

}
