<?php

return [
    "data" => "Dane",
    "menu" => "Języki",
    "name" => "Język",
    "activate" => "Aktywuj",
    "showEmpty" => "Pokazuj tylko puste pola",
    "deactivate" => "Dezaktywuj",
    "confirmationQuestion" => "Czy na pewno chcesz <b>:action</b> język<b>:user?</b>",
    'title' => [
        "edit" => "Edycja języka strony (:name)",
        "index" => "Język",
        "create" => "Nowy język strony",
        "confirmation" => "Potwierdzenie",
    ],
    'subtitle' => [
        "edit" => "Tłumaczenia dla statycznych treści strony",
        "index" => "Lista języków strony"
    ],
    'fields' => [
        "code" => "Kod",
        "name" => "Nazwa",
        "active" => "Aktywny",
        "fields" => "Pola",
        "inactive" => "Nieaktywny"
    ],
    'alerts' => [
        "saved" => "Język :name zapisany pomyślnie!",
        "deleted" => "Język :name usunięty pomyślnie!",
        "activated" => "Język :name aktywowany!",
        "deactivated" => "Język :name dezaktywowany!",
        "isMainLanguage" => "Nie można dezaktywować głównego języka strony!",
        'cantDelete' => [
            "pages" => "Język :name nie może być usunięty. Istnieją strony w tym języku!",
            "config" => "Język :name używany jest w konfiguracji i nie może być usunięty!",
        ]
    ],
    'extraInfo' => [
        "noMain" => "Strona głowna nie zdefiniowana",
        "noPages" => "Brak stron",
        "noActive" => "Jezyk jest nieaktywny",
    ],
    'questions' => [
        "delete" => "Czy na pewno chcesz usunąc ten język?"
    ]
];
