@php($fullClassName = 'App\\'.$className)
<div class="modal fade" id="set-prefix" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('app.setPrefix')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="set-prefix-form" action="{{route('admin.config.setPrefix')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>{!!  __('app.setPrefixDescription') !!}</label>
                        <div class="row">
                            <div class="col-12">
                                @foreach($fullClassName::prefixes() as $key => $val)
                                    <span class="btn btn-sm btn-outline-info prefix-button"
                                          data-value="[{{$key}}]">{{__($val)}}</span>
                                @endforeach
                            </div>
                        </div>
                        <input type="hidden" name="class_name" value="{{$className}}">
                        <input name="prefix" id="prefix" class="form-control" value="{{$fullClassName::getPrefix()}}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="save-prefix-button" class="btn btn-outline-success"
                            style="margin: 0">{{__('app.save')}}</button>
                    <button type="reset" class="btn btn-outline-light"
                            data-dismiss="modal">{{__('app.close')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>
        $('.prefix-button').on('click', function () {
            let value = $(this).attr('data-value');
            let inputValue = $('#prefix').val();
            value = inputValue + '/' + value;
            $('#prefix').val(value);
        });
    </script>
@endsection

