@component('cms::admin.templates.widgetModal',get_defined_vars())
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.type')}}</label>
            <select id="type" name="type" class="form-control data">
                <option value="headerIcon" selected>{{__('app.widgets.texticonTypes.headerIcon')}}</option>
                <option value="freeIcon">{{__('app.widgets.texticonTypes.freeIcon')}}</option>
                <option value="number">{{__('app.widgets.texticonTypes.number')}}</option>
            </select>
    </div>
    <div class="texticon-image" style="display: none">
        @include('cms::admin.filemanager.manager',[
        'name' => 'icon',
        'label' => __('app.widgets.labels.icon'),
        'type' => 'single',
        'fileTypes' => ['icon']
        ])
    </div>
    <div class="texticon-number" style="display: none">
        <div class="form-group">
            <label class="label-text">{{__('app.widgets.labels.number')}}</label>
            <input type="number" id="number" name="number" class="form-control data">
        </div>
    </div>
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.header')}}</label>
        <input id="header" name="header" class="form-control data">
    </div>
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.text')}}</label>
        <textarea id="text" name="text" class="form-control data"></textarea>
    </div>
@endcomponent

@section('js')
    @parent
    <script>
        let texticonModal = $('#texticon-modal');
        let typeSelect = texticonModal.find('#type');

        texticonModal.on('show.bs.modal', function () {
            checkType();
        });
        typeSelect.on('change', function () {
            checkType();
        });

        function checkType() {
            if (typeSelect.val() === 'number') {
                texticonModal.find('.texticon-number').show();
                texticonModal.find('.texticon-image').hide();
            } else {
                texticonModal.find('.texticon-number').hide();
                texticonModal.find('.texticon-image').show();
            }
        }
    </script>
@endsection
