<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'name' => 'Page',
    'menu' => 'Pages',
    'widget' => 'Widget',
    'section' => 'Section',
    'column' => 'Column',
    'confirmDeactivate' => 'Confirm Page Deactivation',
    'confirmActivate' => 'Confirm Page Activation',
    'publishConfirm' => 'Confirm Page Publishing',
    'header' => 'Header',
    'footer' => 'Footer',
    'addWidget' => 'Add widget',
    'addNewSection' => 'Add section',
    'deleteWarning' => 'You will loose all contents included on this page',
    'options' => 'Options',
    'import' => 'Import',
    'export' => 'Export',
    'singleView' => 'Single View',
    'title' => [
        'index' => 'Pages List',
        'create' => 'Create New Page',
        'editDetails' => 'Edit Page Details',
        'addLanguageVersion' => 'Add Language Version',
        'createWidget' => 'Create new widget',
        'moveWidget' => 'Move widget',
        'createSection' => 'Create new section',
        'addContainer' => 'Add container',
        'editContainer' => 'Edit container',
        'export' => 'Export Page',
        'import' => 'Import Page'
    ],
    'subtitle' => [
        'index' => 'List of pages on website',
    ],
    'breadcrumbs' => [
        'index' => 'Pages',
        'create' => 'Creating Page'
    ],
    'buttons' => [
        'new' => 'New Page',
        'editDetails' => 'Edit page details',
        'active1' => 'Deactivate',
        'active0' => 'Activate',
        'publish' => 'Publish',
        'addLanguageVersion' => 'Add Language Version',
        'importPage' => 'Import Page',
        'exportPage' => 'Export Page',
        'exportPages' => 'Export Pages',
        'previewPage' => 'Preview Page'
    ],
    'form' => [
        'name' => 'Page name',
        'metaTitle' => 'Page title',
        'description' => 'Description',
        'active' => 'Is active',
        'isMain' => 'Is Main Page',
        'chooseLanguageForPage' => 'Select Language',
        'pageWidgetTemplateChoice' => 'Choose widget template',
        'pageSectionTemplateChoice' => 'Choose section template',
        'sectionName' => 'Section name',
        'widgetName' => 'Widget name',
        'contentFromModel' => 'Get page content from model',
        'modelSelect' => 'Select model to be associated with this page',
        'modelSelectPlaceholder' => 'No model selected',
        'noModelsFound' => 'No available models were found',
        'moveWidgetQuestion' => 'Do You want move or copy widget?',
        'url' => 'Url',
        'urlInfo' => "You can change this page URL's in related model"
    ],
    'alerts' => [
        'updated' => 'Successfully updated',
        'added' => 'Successfully added',
        'activated' => 'Successfully activated',
        'deactivated' => 'Successfully deactivated',
        'published' => 'Successfully published'
    ],
    'questions' => [
        'delete' => 'Are you sure you want delete this page?',
        'publish' => 'Do you want to publish this page?',
        'confirmActivate' => 'Do you want to activate this page?',
        'confirmDeactivate' => 'Do you want to deactivate this page?',
        'export' => 'Do you want to export page structure to file?',
    ],
];
