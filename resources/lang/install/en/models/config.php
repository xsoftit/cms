<?php

return [
    'menu' => 'Config',
    'title' => 'Config',
    'subtitle' => 'Edit system preferences',
    'urls' => "URL's",
    'categories' => [
        'general' => 'General',
        'page' => 'Page data'
    ],
    'fields' => [
        'defaultLang' => 'Default admin language',
        'defaultFrontLanguage' => 'Default front language',
        'logo1' => 'Main logo',
        'logo2' => 'Secondary logo',
        'companyName' => 'Company name',
        'companyAddress1' => 'Company address 1',
        'companyAddress2' => 'Company address 2',
        'companyPhone' => 'Company phone number',
        'companyEmail' => 'Company email address',
        'facebookLink' => 'Facebook link',
        'twitterLink' => 'Twitter link',
        'youtubeLink' => 'YouTube link',
        'googleApiKey' => 'Google Api Key',
        'titlePrefix' => 'Title prefix',
        'adminLogo' => 'Admin logo',
        'urlWithLanguage' => "Should URL's generate with language prefix"
    ],
    'descriptions' => [
        'mainLogo' => '',
        'secondaryLogo' => '',
        'adminLogo' => ''
    ],
    'alert' => [
        'success' => 'Config saved successful'
    ]
];
