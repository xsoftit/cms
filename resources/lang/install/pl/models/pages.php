<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    "menu" => "Strony",
    "name" => "Strona",
    "column" => "Kolumna",
    "export" => "Export",
    "footer" => "Stopka",
    "header" => "Nagłówek",
    "import" => "Import",
    "widget" => "Widżet",
    "options" => "Opcje",
    "section" => "Sekcja",
    "addWidget" => "Dodaj widżet",
    "singleView" => "- Pojedyczny widok",
    "addNewSection" => "Dodaj sekcję",
    "deleteWarning" => "Stracisz całą zawartość strony",
    "publishConfirm" => "Potwierdź opublikowanie strony",
    "confirmActivate" => "Potwierdź aktywację strony",
    "confirmDeactivate" => "Potwierdź dezaktywację strony",
    'title' => [
        "index" => "Lista stron",
        "create" => "Nowa strona",
        "export" => "Export Strony",
        "import" => "Import Strony",
        "moveWidget" => "Przenoszenie widżetu",
        "editDetails" => "Edcyaj szczegółów strony",
        "addContainer" => "Nowy kontener",
        "createWidget" => "Nowy widżet",
        "createSection" => "Nowa sekcja",
        "editContainer" => "Edycja kontenera",
        "addLanguageVersion" => "Nowa wersja językowa",
    ],
    'subtitle' => [
        "index" => "List wszystkich stron Twojego serwisu",
    ],
    'breadcrumbs' => [
        "index" => "Strony",
        "create" => "Tworzenie strony",
    ],
    'buttons' => [
        "new" => "Nowa strona",
        "active0" => "Aktywuj",
        "active1" => "Dezaktywuj",
        "publish" => "Opublikuj",
        "exportPage" => "Export stron",
        "importPage" => "Import strony",
        "editDetails" => "Edytuj stronę",
        "exportPages" => "Export wszystkich stron",
        "previewPage" => "Podgląd strony",
        "addLanguageVersion" => "Nowa wersja językowa",
    ],
    'form' => [
        "url" => "Url",
        "name" => "Nazwa strony",
        "active" => "Aktywna",
        "isMain" => "Strona główna",
        "urlInfo" => "Możesz zmienić URL w powiązanym ze stroną modelu",
        "metaTitle" => "Meta tytuł strony",
        "widgetName" => "Nazwa widżetu",
        "description" => "Meta opis strony",
        "modelSelect" => "Wybierz powiązany model",
        "sectionName" => "Nazwa sekcji",
        "noModelsFound" => "Brak dostępnych modeli",
        "contentFromModel" => "Pobierz zawartość strony z modelu",
        "moveWidgetQuestion" => "Chcesz przenieść czy skopiować widżet?",
        "chooseLanguageForPage" => "Wybierz język",
        "modelSelectPlaceholder" => "Model nie wybrany",
        "pageWidgetTemplateChoice" => "Wybierz szablon widżetu",
        "pageSectionTemplateChoice" => "Wybierz szablon sekcji",
    ],
    'alerts' => [
        "added" => "Strona dodana pomyślnie",
      "updated" => "Strona zaktualizowana pomyślnie",
      "activated" => "Strona aktywowana pomyślnie",
      "published" => "Strona opublikowana pomyślnie",
      "deactivated" => "Strona dezaktywowana pomyślnie",
    ],
    'questions' => [
        "delete" => "Na pewno chcesz usunąc tą stronę?",
      "export" => "Na pewno chcesz exportować tą stronę do pliku?",
      "publish" => "Na pewno chcesz opublikować tą stronę?",
      "confirmActivate" => "Na pewno chcesz aktywować tą stronę?",
      "confirmDeactivate" => "Na pewno chcesz dezaktywować tą stronę?",
    ],
];
