<li class="{{$class}} list-group-item" data-id="NUMBER_PLACEHOLDER">
    <div class="row">
        <div class="col-md-3 col-sm-12">
            @include('cms::admin.sourceModels.partials.columnTypeSelect',['name'=>$name])
        </div>
        <div class="col-md-3 col-sm-12">
            <input class="form-control" type="text" name="{{$name}}[NUMBER_PLACEHOLDER][name]">
        </div>
        <div class="col-md-3 col-sm-12">
            <input class="form-control" type="text" name="{{$name}}[NUMBER_PLACEHOLDER][modifiers][default]">
        </div>
        <div class="col-md-2 col-sm-12">
            <div class="animated-checkbox">
                <label>
                    <input type="checkbox" name="{{$name}}[NUMBER_PLACEHOLDER][modifiers][nullable]">
                    <span
                        class="label-text text-capitalize">{{__('app.sourceModels.fields.column.nullable')}}</span>
                </label>
            </div>
            <div class="animated-checkbox">
                <label>
                    <input type="checkbox" name="{{$name}}[NUMBER_PLACEHOLDER][modifiers][unique]">
                    <span
                        class="label-text text-capitalize">{{__('app.sourceModels.fields.column.unique')}}</span>
                </label>
            </div>
        </div>
        <div class="col-md-1 col-sm-12">
            <i class="af af-minus-circle color-danger remove-column"></i>
        </div>
    </div>
</li>
