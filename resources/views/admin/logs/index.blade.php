@extends('cms::admin.layouts.app')

@section('page-title')
    Logs
@endsection

@section('title')
    Logs
@endsection
@section('subtitle')
    This is your place
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],['Logs'],['Log Archives',route('logs.archives.index')]) !!}
@endsection

@section('css')
    @parent
    <style>
        table {
            word-break: break-word;
        }
    </style>
@endsection

@section('page-content')
    @include('cms::admin.logs.prioritiesForm')
    <hr>
    <ul class="nav nav-tabs">
        @foreach($types as $type)
            <li class="nav-item"><a class="nav-link @if($type['name']==="ERROR") active show @endif "
                                    data-toggle="tab"
                                    href="#table-{{$type['name']}}">{{$type['name']}}</a></li>
        @endforeach
    </ul>
    <div class="tab-content" id="myTabContent">
        @foreach($tables as $key => $logsTable)
            <div class="tab-pane fade @if($key==="ERROR") active show @endif " id='table-{{$key}}'>
                @dataTable($logsTable)
            </div>
        @endforeach
    </div>
@endsection
