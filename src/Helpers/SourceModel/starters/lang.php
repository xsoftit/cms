<?php

return [

    'name' => 'ExampleModels',
    'menu' => 'ExampleModels',
    'title' => [
        'index' => 'ExampleModels',
        'create' => 'Create ExampleModel',
        'edit' => 'Edit ExampleModel'
    ],
    'subtitle' => [
        'index' => 'List of ExampleModels',
        'create' => 'Create form for ExampleModel',
        'edit' => 'Edit form for ExampleModel'
    ],
    'breadcrumbs' => [
        'index' => 'ExampleModels',
        'create' => 'Create',
        'edit' => 'Edit'
    ],
    'fields' => [
        'name' => 'Name',
        'url' => 'Url',
        'active' => 'Active',
        'inactive' => 'Inactive',
        'title' => 'Title',
        'content' => 'Content',
    ],
    'alerts' => [
        'added' => 'ExampleModel added',
        'updated' => 'ExampleModel updated'
    ]

];
