<?php

namespace Xsoft\Cms\Observers;

use Xsoft\Cms\Models\PageSection;
use Illuminate\Support\Facades\DB;

class PageSectionObserver
{
    /**
     * Handle the page section "created" event.
     *
     * @param  \Xsoft\Cms\Models\PageSection $pageSection
     * @return void
     */
    public function created(PageSection $pageSection)
    {
        //
    }

    /**
     * Handle the page section "updated" event.
     *
     * @param  \Xsoft\Cms\Models\PageSection $pageSection
     * @return void
     */
    public function updated(PageSection $pageSection)
    {
        //
    }

    /**
     * Handle the page section "deleted" event.
     *
     * @param  \Xsoft\Cms\Models\PageSection $pageSection
     * @return void
     */
    public function deleted(PageSection $pageSection)
    {
        //
    }

    public function deleting(PageSection $pageSection)
    {
        $widgets = $pageSection->widgets;
        foreach ($widgets as $widget) {
            DB::table('page_section_widget')
                ->where('page_section_id', $pageSection->id)
                ->where('widget_id', $widget->id)
                ->delete();
            $connections = DB::table('page_section_widget')->where('widget_id', $widget->id)->count();
            if (!$connections) {
                $widget->delete();
            }
        }
    }


    /**
     * Handle the page section "restored" event.
     *
     * @param  \Xsoft\Cms\Models\PageSection $pageSection
     * @return void
     */
    public function restored(PageSection $pageSection)
    {
        //
    }

    /**
     * Handle the page section "force deleted" event.
     *
     * @param  \Xsoft\Cms\Models\PageSection $pageSection
     * @return void
     */
    public function forceDeleted(PageSection $pageSection)
    {
        //
    }
}
