<?php

namespace Xsoft\Cms\Services\Admin;

use Xsoft\Cms\Helpers\FrontMenuHelper;
use Xsoft\Cms\Models\Language;
use Xsoft\DataTables\DataTable;
use Illuminate\Support\Facades\Cache;

class FrontLanguageService
{
    public function getTable($request = null)
    {
        $table = new DataTable('languages', 'admin.frontLanguages.indexAjax');
        $table->setOptions(['searching' => false, 'lengthChange' => false, 'info' => false]);
        $table->setColumns([
            [__('app.frontLanguages.fields.name'), 'name'],
            [__('app.frontLanguages.fields.code'), 'code'],
            [__('app.frontLanguages.fields.active'), 'active'],
        ]);
        if (auth()->user()->hasPermission('front_languages.edit') || auth()->user()->hasPermission('front_languages.exportImport')) {
            $table->addColumn(["", 'id', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width1']]);
        }
        $table->addToQuery(
            ['where', 'front', 1]
        );
        return $table;
    }

    public function prepareOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            $buttons = '';
            if (auth()->user()->hasPermission('front_languages.edit') || auth()->user()->hasPermission('front_languages.exportImport')) {
                $buttons .= '<div class="d-flex actions">';
                if (auth()->user()->hasPermission('front_languages.exportImport')) {
                    $buttons .= '<a class="btn btn-outline-info btn-outline-rounded btn-outline-icon " href="' . route("admin.frontLanguages.export", [$row->id]) . '">' . __('app.export') . '</a>';
                }
                if (auth()->user()->hasPermission('front_languages.edit')) {
                    $buttons .= '<a class="btn btn-outline-light btn-outline-rounded btn-outline-icon " href="' . route("admin.frontLanguages.edit", [$row->id]) . '">' . __('app.edit') . '</a>';
                }
                $buttons .= '</div>';
            }
            if ($buttons) {
                $output['aaData'][] = [
                    $row->name,
                    $row->code,
                    $row->active ? __('app.frontLanguages.fields.active') : '<span class="text-danger">' . __('app.frontLanguages.fields.inactive') . '</span>',
                    $buttons,
                ];
            } else {
                $output['aaData'][] = [
                    $row->name,
                    $row->code,
                    $row->active ? __('app.frontLanguages.fields.active') : '<span class="text-danger">' . __('app.frontLanguages.fields.inactive') . '</span>'
                ];
            }
        }
    }

    public function create($request)
    {
        $language = Language::create([
            'name' => $request->get('name'),
            'code' => strtolower($request->get('code')),
            'front' => 1,
            'fields' => json_encode($request->get('fields'))
        ]);
        FrontMenuHelper::createMenu($language->code,'main');
        FrontMenuHelper::createMenu($language->code,'footer');

        //$this->cacheSave($language);

        return $language;
    }

    public function update($request, Language $language)
    {
        $language->update([
            'name' => $request->get('name'),
            'code' => strtolower($request->get('code')),
            'fields' => json_encode($request->get('fields'))
        ]);

        $this->cacheSave($language);

        return $language;
    }

    public function prepareArrayForExport(Language $language){
        $data = [];
        $data['code'] = $language->code;
        $data['name'] = $language->name;
        $data['fields'] = json_decode($language->fields,true);
        return $data;
    }

    private function cacheSave($language)
    {
        $key = 'front-language-fields-' . $language->code;
        $fields = json_decode($language->fields, true);
        Cache::forever($key, $fields);
    }
}
