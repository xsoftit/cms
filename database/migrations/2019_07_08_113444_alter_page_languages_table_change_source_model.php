<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterPageLanguagesTableChangeSourceModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pageLanguages = \Xsoft\Cms\Models\PageLanguage::where('source_model_id', '!=', null)->get();
//        if (Schema::hasColumn('page_languages', 'source_model_id')) {
//            Schema::table('page_languages', function (Blueprint $table) {
//                $table->dropColumn('source_model_id');
//            });
//        }
        Schema::table('page_languages', function (Blueprint $table) {
            $table->string('source_model')->nullable();
        });
        foreach ($pageLanguages as $pageLanguage) {
            $model_name = \Xsoft\Cms\Models\SourceModel::find($pageLanguage->source_model_id)->first();
            if ($model_name) {
                $model_name = $model_name->model_name;
            }
            $pageLanguage->update([
                'source_model' => $model_name,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('page_languages', 'source_model')) {
            Schema::table('page_languages', function (Blueprint $table) {
                $table->dropColumn('source_model');
            });
        }
    }
}
