body = $('body');

Jodit.defaultOptions.controls.paragraph.list = {
    p: 'Normal',
    h1: 'Heading 1',
    h2: 'Heading 2',
    h3: 'Heading 3',
    h4: 'Heading 4',
    h5: 'Heading 5',
    blockquote: 'Quote',
    label: 'Label'
};

Jodit.modules.FileManager = function (editor) {
    this.insertFiles = function (files) {
        $.each(files, function () {
            let file = null;
            let newFile = null;
            if ($(this).find('img').length) {
                file = $(this).find('img');
                newFile = document.createElement("img");
                newFile.setAttribute('src', file.attr('src'));
            }
            if ($(this).find('video').length) {
                file = $(this).find('source');
                newFile = document.createElement("video");
                let source = document.createElement('source');
                source.setAttribute('src', file.attr('src'));
                newFile.appendChild(source);
            }
            editor.selection.insertNode(newFile);
            editor.setEditorValue();
        });
    };
};
var joditEditor, joditEditorShort = null;

(function ($) {
    $.fn.selectInit = function () {
        this.select2({
            language: userLanguage,
            minimumResultsForSearch: 8,
            dropdownPosition: 'below',
            placeholder: function placeholder() {
                if ($(this).data('placeholder')) {
                    return $(this).data('placeholder');
                } else {
                    return '...';
                }
            }
        });
    };

    $.fn.selectAjaxInit = function (additionalParams) {
        $.each(this, function () {
            $(this).select2({
                language: userLanguage,
                allowClear: $(this).attr('data-clear'),
                placeholder: $(this).attr('data-placeholder'),
                ajax: {
                    url: '' + $(this).attr('data-url'),
                    delay: 500,
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            additionalParams: additionalParams,
                            // type: 'public',
                        };
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
            });
        })
    };

})(jQuery);


window.onload = function () {
    $(document).ready(function () {
        initClock();
        $("select:not(.select-data-ajax):not(.phpdebugbar-datasets-switcher):not(.regular-select)").selectInit();
        $('.select-data-ajax').selectAjaxInit();
        let url = document.location.toString();
        let tabLink = null;
        if (url.match('#')) {
            tabLink = $('.nav-tabs a[href="#' + url.split('#')[1] + '"]');
        } else {
            let hash = localStorage.getItem('hash');
            if (hash) {
                if ($('.nav-tabs a[href="' + hash + '"]').length > 0) {
                    tabLink = $('.nav-tabs a[href="' + hash + '"]')
                    window.location.hash = hash;
                } else {
                    localStorage.removeItem('hash');
                    tabLink = $('.nav-tabs li:first-child a')
                }
            } else {
                localStorage.removeItem('hash');
                tabLink = $('.nav-tabs li:first-child a')
            }
        }
        tabLink.click();
        $('.nav-tabs a').on('click', function (e) {
            if (!$(this).attr('href').includes('http') && !$(this).hasClass('no-save')) {
                localStorage.setItem('hash', $(this).attr('href'));
                window.location.hash = $(this).attr('href');
            }
        });
    });
};

$('body').on('shown.bs.modal', function (e) {
    reinitJodit($(e.target));
});
$('body').on('shown.bs.tab', function (e) {
    reinitJodit($($(e.target).attr("href")));
});

$('.extra-info').tooltip({
    html: true,
    template: '<div class="tooltip tooltip-info" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
});
$('.extra-info-success').tooltip({
    html: true,
    template: '<div class="tooltip tooltip-success" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
});
$('.extra-info-warning').tooltip({
    html: true,
    template: '<div class="tooltip tooltip-warning" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
});
$('.extra-info-danger').tooltip({
    html: true,
    template: '<div class="tooltip tooltip-danger" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
});

initRouteSelector();

function initRouteSelector(){
    let routeSelector = body.find('.route-selector');
    $.each(routeSelector, function () {
        if (!($(this).parents('.modal').length > 0)) {
            $(this).find('.outside-route').click();
        }
    });
    let routeRadio = {
        'inner': routeSelector.find('.inner-route'),
        'outside': routeSelector.find('.outside-route')
    };
    routeRadio.inner.on('click', function () {
        let rS = $(this).parents('.route-selector');
        let data = {
            'select': rS.find('.inner-route-select'),
            'input': rS.find('#link')
        };
        $(this).addClass('data');
        data.select.attr('disabled', false);
        rS.find('.outside-route').removeClass('data');
        data.input.attr('disabled', true);
    });
    routeRadio.outside.on('click', function () {
        let rS = $(this).parents('.route-selector');
        let data = {
            'select': rS.find('.inner-route-select'),
            'input': rS.find('#link')
        };
        $(this).addClass('data');
        data.select.attr('disabled', true);
        rS.find('.inner-route').removeClass('data');
        data.input.attr('disabled', false);
    });
}

function validateForm(form, content) {
    let result = true;
    if (content) {
        $.each(content.find('input,select,textarea'), function (i, v) {
            if ($(this).attr('required') && !$(this).val()) {
                result = false;
            }
        });
    } else {
        $.each(form.find('input,select,textarea'), function (i, v) {
            if ($(this).attr('required') && !$(this).val()) {
                result = false;
            }
        });
    }
    if (!result) {
        form.find('input[type="submit"]').click();
    }
    return result;
}

function initClock() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $('#clock').html(h + ":" + m + ":" + s);
    setTimeout(initClock, 500);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function loadLastPage(tabLink) {
    let tab = $(tabLink.attr('href'));
    let name = 'lastPage' + tabLink.attr('data-code');
    let lastPage = localStorage.getItem(name);
    let element = tab.find('.show-page-edit[data-id="' + lastPage + '"]');
    if (element.length > 0) {
        element.click();
    } else {
        element = tab.find('.show-page-edit:first-child');
        if (element.length > 0) {
            element.click();
            localStorage.removeItem(name);
        }
    }
}

function reinitJodit(parent) {
    if (!parent.find('textarea').length) {
        return false;
    }
    parent.find('.jodit_container').remove();
    let jodit = parent.find('textarea');
    $.each(jodit, function () {
        initJodit($(this)[0]);
    });
}

currentEditor = null;

function initJodit(element) {
    if (!$(element).hasClass('no-jodit')) {
        if ($(element).hasClass('short')) {
            joditEditorShort = new Jodit(element, {
                minHeight: 50,
                buttons: ['code', '|', 'bold', 'italic', 'underline'],
                buttonsMD: ['code', '|', 'bold', 'italic', 'underline'],
                buttonsXS: ['code', '|', 'bold', 'italic', 'underline'],
                events: {
                    getIcon: function (name, control, clearName) {
                        var code = clearName;

                        switch (clearName) {
                            case 'source':
                                code = 'code';
                                break;
                        }

                        return '<i style="font-size:14px" class="af af-' + code + ' af-xs"></i>';
                    }
                }
            });
        } else {
            joditEditor = new Jodit(element, {
                minHeight: 400,
                removeButtons: ['strikethrough', 'eraser', 'file', 'cut', 'copyformat', 'fullsize', 'selectall', 'print', 'about', '\n'],
                extraButtons: [{
                    title: 'File manager',
                    name: 'fileManager',
                    exec: function (editor) {
                        dataType = 'jodit';
                        fileTypes = '["image","video"]';
                        name = $(element).attr('name');
                        currentEditor = editor;
                        getFiles();
                    }
                }],
                events: {
                    getIcon: function (name, control, clearName) {
                        var code = clearName;

                        switch (clearName) {
                            case 'symbol':
                                code = 'exclamation';
                                break;
                            case 'hr':
                                code = 'minus';
                                break;
                            case 'left':
                            case 'right':
                            case 'justify':
                            case 'center':
                                code = 'align-' + name;
                                break;
                            case 'brush':
                                code = 'tint';
                                break;
                            case 'fontsize':
                                code = 'text-height';
                                break;
                            case 'ul':
                            case 'ol':
                                code = 'list-' + name;
                                break;
                            case 'source':
                                code = 'code';
                                break;
                            case 'fileManager':
                                code = 'images';
                                break;
                        }

                        return '<i style="font-size:14px" class="af af-' + code + ' af-xs"></i>';
                    },
                    afterInit: function (editor) {
                        editor.fileManager = new Jodit.modules.FileManager(editor);
                    }
                },
            });
        }
    }
}

customMenuParent = null;

// Custom MENU
body.on('click', function () {
    $(".custom-menu").hide(100);
});

body.on("contextmenu", '[data-toggle="custom-menu"]', function (event) {
    event.preventDefault();
    customMenuParent = $(this);
    let element = $(this).attr('data-target');
    $(".custom-menu").hide(100);
    $(element).finish().toggle(100).css({
        top: event.pageY + "px",
        left: event.pageX + "px"
    });
});


$(".custom-menu li").click(function () {
    let action = $(this).attr("data-action");
    console.log(action);
    window[action]();
    $(".custom-menu").hide(100);
});
