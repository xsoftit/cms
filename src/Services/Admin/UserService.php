<?php

namespace Xsoft\Cms\Services\Admin;

use Xsoft\DataTables\DataTable;
use Illuminate\Support\Facades\Hash;
use Xsoft\Cms\Helpers\CmsConfig;
use App\User;

class UserService
{
    public function getTable()
    {
        $table = new DataTable('users', 'admin.users.indexAjax');
        $table->setOptions(['searching' => false, 'lengthChange' => false, 'info' => false, 'order' => '0,asc']);
        $table->setColumns([
            ['ID', 'users.id', ['class' => 'width5']],
            [__('app.users.fields.name'), 'users.name'],
            [__('app.users.fields.email'), 'email'],
            [__('app.users.fields.role'), 'roles.name'],
            [__('app.users.fields.active'), 'active'],
        ]);
        if (auth()->user()->hasPermission('users.edit')) {
            $table->addColumn(["", 'id', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width1']]);
        }
        $table->addToQuery(
            ['select', 'users.*'],
            ['leftJoin', 'model_has_roles as mhr', 'model_id', '=', 'users.id'],
            ['leftJoin', 'roles', 'roles.id', '=', 'mhr.role_id'],
            ['where', 'roles.name', '!=', 'superadmin']
        );
        return $table;
    }

    public function prepareOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            if (auth()->user()->hasPermission('users.edit')) {
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                    $row->email,
                    $row->role()->name,
                    $row->active ? __('app.users.fields.active') : '<span class="text-danger">' . __('app.users.fields.inactive') . '</span>',
                    "<a href='" . route('admin.users.edit', [$row->id]) . "' class='btn btn-outline-light'>" . __('app.edit') . "</a>"
                ];
            }else{
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                    $row->email,
                    $row->role()->name,
                    $row->active ? __('app.users.fields.active') : '<span class="text-danger">' . __('app.users.fields.inactive') . '</span>',
                ];
            }
        }
    }

    public function create($request)
    {
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'language' => CmsConfig::get('default_lang')
        ]);
        $user->assignRole($request->get('role'));

        return $user;
    }

    public function update($request, $user)
    {
        $user->update([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
        ]);

        return $user;
    }

}
