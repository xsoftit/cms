@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.sourceModels.title.summary')}}
@endsection

@section('title')
    {{__('app.sourceModels.title.summary')}}
@endsection
@section('subtitle')
    {{__('app.sourceModels.subtitle.summary')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.sourceModels.breadcrumbs.index'),route('admin.sourceModels.index')],[__('app.sourceModels.summary')]) !!}
@endsection

@section('page-content')
    <h1 style="color:red">Run COMPOSER DUMPAUTOLOAD</h1>
    {!! $summary !!}
@endsection

