<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagePageSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_page_section', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('page_section_id');
            $table->bigInteger('page_id');
            $table->integer('order')->default(0);
            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->index('page_section_id');
            $table->index('page_id');
            $table->index('active');
            $table->index('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_page_section');
    }
}
