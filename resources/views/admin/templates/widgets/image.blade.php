<li><b>{{__('app.widgets.labels.image')}}:</b>
    <div id="image">
        @include('cms::admin.templates.widgets.partials.files',['files' => (array)$widget->getContent('image',$pageLanguage)])
    </div>
</li>
