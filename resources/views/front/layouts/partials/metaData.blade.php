<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
@if($data->getSourceObject())
    @php($object = $data->getSourceObject())
@else
    @php($object = $data->getPageLanguage())
@endif
<title>{{page_data('title_prefix')}} {{$object->meta_title}}</title>
<meta name="description" content="{{$object->meta_description}}">
<meta property="og:title" content="{{$object->meta_title}}">
<meta property="og:type" content="article">
<meta property="og:url" content="{{Request::url()}}">
<meta property="og:description" content="{{$object->meta_description}}">
<meta property="og:image" content="{{$object->meta_image}}">
