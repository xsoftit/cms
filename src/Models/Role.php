<?php

namespace Xsoft\Cms\Models;

use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    public static function all($columns = []){
        return SpatieRole::where('name','!=','superadmin')->get();
    }
}
