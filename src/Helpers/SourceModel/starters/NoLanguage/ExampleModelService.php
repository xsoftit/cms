<?php

namespace App\Services;

use App\ExampleModel;
use Illuminate\Support\Str;
use Xsoft\DataTables\DataTable;

class ExampleModelService
{
    public function getTable()
    {
        $table = new DataTable('example_models', 'admin.exampleModels.indexAjax');
        $table->setOptions(['searching' => false, 'lengthChange' => false, 'info' => false, 'order' => '0,asc']);
        $table->setColumns([
            ['ID', 'id', ['class' => 'width5']],
            [__('app.exampleModels.fields.name'), 'name'],
            [__('app.exampleModels.fields.slug'), 'slug'],
            [__('app.exampleModels.fields.active'), 'active'],
        ]);
        if (auth()->user()->hasPermission('example_models.edit')) {
            $table->addColumn(["", 'id', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width1']]);
        }
        return $table;
    }

    public function prepareOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            if (auth()->user()->hasPermission('example_models.edit')) {
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                    $row->slug,
                    $row->active ? __('app.exampleModels.fields.active') : '<span class="text-danger">' . __('app.exampleModels.fields.inactive') . '</span>',
                    "<a href='" . route('admin.exampleModels.edit', [$row->id]) . "' class='btn btn-outline-light'>" . __('app.edit') . "</a>"
                ];
            } else {
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                    $row->slug,
                    $row->active ? __('app.exampleModels.fields.active') : '<span class="text-danger">' . __('app.exampleModels.fields.inactive') . '</span>',
                ];
            }
        }
    }

    public function create($request)
    {
        $data = $request->all();
        if (!$request->get('slug')) {
            $data['slug'] = Str::slug($request->get('name'));
        }
        $data['active'] = $request->get('active') ? 1 : 0;
        return ExampleModel::create($data);
    }

    public function update($request, $exampleModel)
    {
        $data = $request->all();
        $data['active'] = $request->get('active') ? 1 : 0;
        $exampleModel->update($data);
        return $exampleModel;
    }

}
