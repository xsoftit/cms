<?php

namespace Xsoft\Cms\Helpers\SourceModel;

class Field
{

    const FIELD_TYPES = [
        'string',
        'text',
        'longText',
        'integer',
        'double',
        'float',
        'boolean',
        'timestamp',
        'date',
        'time',
        'year',
        'binary',
    ];

    protected $type;
    protected $name;
    protected $modifiers;

    public function __construct($type, $name, array $modifiers = [])
    {
        $this->type = $type;
        $this->name = $name;
        foreach ($modifiers as $key => &$modifier) {
            if ($key != 'default') {
                $modifier = null;
            }
        }
        $this->modifiers = $modifiers;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getName()
    {
        return $this->name;
    }

//    static public function consoleDisplayAvailableFields()
//    {
//        foreach (Field::FIELD_TYPES as $key => $type) {
//            echo '[' . $key . '] ' . $type . PHP_EOL;
//        }
//    }
//
//    static public function consoleDisplayAvailableModifiers()
//    {
//        foreach (Field::FIELD_MODIFIERS as $key => $modifier) {
//            echo '[' . $key . '] ' . $modifier . PHP_EOL;
//        }
//    }

    public function addModifier($modifier)
    {

    }

    public function getModifiers()
    {
        return $this->modifiers;
    }
}
