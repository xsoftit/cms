<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "add" => "Dodaj",
    "copy" => "Kopiuj",
    "edit" => "Edytuj",
    "move" => "Przenieś",
    "save" => "Zapisz",
    "close" => "Zamknij",
    "cancel" => "Anuluj",
    "create" => "Stwórz",
    "delete" => "Usuń",
    "logout" => "Wyloguj",
    "search" => "Szukaj",
    "update" => "Aktualizuj",
    "confirm" => "Tak",
    "discard" => "Nie",
    "profile" => "Profil",
    "instruction" => "Instrukcja",
    "saveAndBack" => "Zapisz i wróć",
    "routeSelect" => [
        'choose' => 'Wybierz URL',
        'chooseType' => "Wybierz typ URL'a",
    ],
    'datatables' => [
        "loading" => "Ładowanie...",
        "emptyTable" => "Brak rekordów",
        "searchPlaceholder" => "Szukaj",
    ],
    'deleteButton' => [
        "title" => "Usuń",
        "cancel" => "Nie",
        "confirm" => "Tak",
        "question" => "Czy na pewno chcesz usunąć ten wpis?",
    ],
    'dashboard' => [
        "menu" => "Pulpit",
        "title" => "Pulpit",
        "subtitle" => "Panel administracyjny",
    ],
    'alerts' => [
        "urlExist" => "Url ':url' istnieje",
        "ajaxError" => "Coś poszło nie tak",
        "urlExistWithLanguage" => "Url ':url' z językiem ':language' istnieje"
    ],
    'galleries' => include('models/galleries.php'),
    'config' => include('models/config.php'),
    'roles' => include('models/roles.php'),
    'pages' => include('models/pages.php'),
    'languages' => include('models/languages.php'),
    'frontLanguages' => include('models/frontLanguages.php'),
    'users' => include('models/users.php'),
    'pageConfig' => include('models/pageConfig.php'),
    'widgets' => include('models/widgets.php'),
    'sections' => include('models/sections.php'),
    'containers' => include('models/containers.php'),
    'sourceModels' => include('models/sourceModels.php'),
];

