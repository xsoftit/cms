<div class="modal fade widget-modal" id="{{$widget->blade_name.'-modal'}}" role="dialog"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link text-uppercase no-save" data-toggle="tab"
                           href="#{{$widget->blade_name.'-content'}}">{{__('app.widgets.content')}}</a>
                    </li>
                    @if(auth()->user()->hasPermission('widgets.create'))
                        <li>
                            <a class="nav-link text-uppercase no-save" data-toggle="tab"
                               href="#{{$widget->blade_name.'-layout'}}">{{__('app.widgets.layout')}}</a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div id="{{$widget->blade_name.'-content'}}" class="tab-pane fade">
                        <input id="widget-id" name="widget-id" hidden>
                        <input id="content-data" name="content-data" hidden>
                        {{ $slot }}
                    </div>
                    @if(auth()->user()->hasPermission('widgets.create'))
                        <div id="{{$widget->blade_name.'-layout'}}" class="tab-pane fade">
                            <hr>
                            <div class="form-group">
                                <label class="label-text">{{__('app.widgets.labels.tag')}}</label>
                                    <select name="tag" class="form-control data">
                                        <option value="">{{__('app.widgets.tags.none')}}</option>
                                        <option value="div">{{__('app.widgets.tags.div')}}</option>
                                        <option value="h1">{{__('app.widgets.tags.header1')}}</option>
                                        <option value="h2">{{__('app.widgets.tags.header2')}}</option>
                                        <option value="h3">{{__('app.widgets.tags.header3')}}</option>
                                        <option value="h4">{{__('app.widgets.tags.header4')}}</option>
                                        <option value="h5">{{__('app.widgets.tags.header5')}}</option>
                                        <option value="span">{{__('app.widgets.tags.span')}}</option>
                                        <option value="label">{{__('app.widgets.tags.label')}}</option>
                                        <option value="p">{{__('app.widgets.tags.paragraph')}}</option>
                                        <option value="li">{{__('app.widgets.tags.listItem')}}</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label class="label-text">{{__('app.widgets.labels.classes')}}</label>
                                <input id="classes" name="classes" class="form-control data" disabled>
                            </div>
                            <div class="form-group">
                                <label class="label-text">{{__('app.widgets.labels.styles')}}</label>
                                <textarea id="styles" name="styles" class="form-control data no-jodit"
                                          disabled></textarea>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
            <div class="modal-footer">
                @if(auth()->user()->hasPermission('widgets.delete'))
                    <button type="button" id="deleteWidget"
                            class="btn btn-outline-danger margin-right-auto">{{__('app.deleteButton.title')}}</button>
                @endif
                <button id="submit-widget" type="submit" class="btn btn-outline-success">{{__('app.save')}}</button>
                <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                    {{__('app.close')}}
                </button>
            </div>
            @if(auth()->user()->hasPermission('widgets.delete'))
                <div class="delete-content">
                    <h3>{{__('app.widgets.deleteQuestion')}}<br>
                        <small>({{__('app.widgets.deleteWarning')}})</small>
                    </h3>
                    <div class="row">
                        <div class="col-12">
                            <button type="button" id="deleteWidgetConfirm"
                                    class="btn btn-outline-danger">{{__('app.deleteButton.confirm')}}</button>
                            <button type="button" id="deleteWidgetCancel"
                                    class="btn btn-outline-light">{{__('app.deleteButton.cancel')}}</button>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
