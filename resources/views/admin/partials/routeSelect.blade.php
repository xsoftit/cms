<div class="form-group">
    <label>{{$label}}</label>
    <div class="input-group mb-3">
        <input type="text" class="form-control data route-select-link" name="{{$name}}"
               placeholder="{{__('app.widgets.url.enterUrl')}}" @if($value) value="{{$value}}" @endif>
        <div class="input-group-append">
            <button class="btn btn-secondary route-select-button" data-name="{{$name}}"
                    type="button">{{__('app.routeSelect.choose')}}</button>
        </div>
    </div>
</div>
