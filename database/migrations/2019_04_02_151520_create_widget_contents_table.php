<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('widget_id');
            $table->string('language');
            $table->longText('content');
            $table->timestamps();
            $table->index('widget_id');
            $table->index('language');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget_contents');
    }
}
