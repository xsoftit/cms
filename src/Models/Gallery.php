<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = [
        'name',
        'options'
    ];

    public function items()
    {
        return $this->hasMany(GalleryItem::class);
    }
}
