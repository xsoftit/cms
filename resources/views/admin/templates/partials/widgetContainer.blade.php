<div class="section-column-frame">
    <h3 class="container-header">
                    <span data-container="{{$container->id}}">{{$container->tag}}
                        <small>({{$container->classes}})</small></span>
        @if(auth()->user()->hasPermission('sections.edit'))
            <div class="float-right">
                <i class="af af-arrow-down move-down-container" @if(isset($last) || $amount < 2) style="display: none" @endif
                data-container="{{$container->id}}" data-type="widget"></i>
                <i class="af af-arrow-up move-up-container" @if(isset($first) || $amount < 2) style="display: none" @endif
                data-container="{{$container->id}}" data-type="widget"></i>
                <i class="af af-edit edit-container" data-container="{{$container->id}}"
                   data-type="widget"></i>
            </div>
        @endif
    </h3>
    {*REPLACE*}
</div>
