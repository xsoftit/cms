<div class="form-group">
    <label>{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</label>
    <input type="number" name="{{$field->getName()}}" value="{\{old('{{$field->getName()}}')}}" class="form-control">
</div>
