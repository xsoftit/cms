<div class="row">
    @foreach($section->getColumns() as $column)
        <div class="col-{{$column->getSize()}}">
            {!! $column->getView() !!}
        </div>
    @endforeach
</div>

