@section('modals')
    @parent
    <div class="modal fade" id="edit-container-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.editContainer')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="edit-container-form">
                    <div class="modal-body">
                        <input hidden name="container_id" id="container_id">
                        <div class="form-group">
                            <label class="label-text">{{__('app.containers.labels.tag')}}</label>
                            <input id="container_tag" name="tag" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="label-text">{{__('app.containers.labels.classes')}}</label>
                            <input id="container_classes" name="classes" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="label-text">{{__('app.containers.labels.styles')}}</label>
                            <textarea id="container_styles" name="styles" class="form-control no-jodit"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="deleteContainer"
                                class="btn btn-outline-danger margin-right-auto">{{__('app.deleteButton.title')}}</button>
                        <button type="button" id="edit-container-button" class="btn btn-outline-success"
                                style="margin: 0">{{__('app.update')}}</button>
                        <button type="reset" class="btn btn-outline-light"
                                data-dismiss="modal">{{__('app.close')}}</button>
                    </div>
                </form>
                <div class="delete-content">
                    <h3>{{__('app.widgets.deleteQuestion')}}<br>
                        <small>({{__('app.widgets.deleteWarning')}})</small>
                    </h3>
                    <div class="row">
                        <div class="col-12">
                            <button type="button" id="deleteContainerConfirm"
                                    class="btn btn-outline-danger">{{__('app.deleteButton.confirm')}}</button>
                            <button type="button" id="deleteContainerCancel"
                                    class="btn btn-outline-light">{{__('app.deleteButton.cancel')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
