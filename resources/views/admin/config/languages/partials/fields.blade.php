@foreach($template as $key => $temp)
    @php($currentKey = $parentKey.'-'.$key)
    @if(is_array($temp))
        <div class="accordion accordion-bordered order-2" id="accordion{{$currentKey}}" role="tablist">
            <div class="card">
                <div class="card-header" role="tab" id="heading-1">
                    <h6 class="mb-0">
                        <a data-toggle="collapse" href="#collapse{{$currentKey}}" class="collapsed">
                            {{$key}}
                        </a>
                    </h6>
                </div>
                <div id="collapse{{$currentKey}}" class="collapse" role="tabpanel" data-parent="#accordion{{$currentKey}}">
                    <div class="card-body">
                        <div class="row">
                            @include('cms::admin.config.languages.partials.fields', ['template' => $temp, 'parentKey' => $currentKey])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="col-12 order-1">
            <div class="form-group row">
                <label for="type" class="col-sm-2 col-form-label">{{$key}}</label>
                <div class="col-sm-10">
                    @if(!isset($language))
                        <input type="text" class="form-control" placeholder="{{$temp}}" name="fields{{LanguageHelper::parseKey($currentKey)}}" value="">
                    @else
                        <input type="text" class="form-control" placeholder="{{$temp}}" name="fields{{LanguageHelper::parseKey($currentKey)}}" value="{{$language->checkKey($currentKey)}}">
                    @endif
                </div>
            </div>
        </div>
    @endif
@endforeach

