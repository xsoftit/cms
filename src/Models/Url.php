<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Xsoft\Cms\Helpers\CmsConfig;

class Url extends Model
{
    protected $fillable = [
        'url',
        'model',
        'language',
        'model_id',
        'prefix',
        'fullUrl'
    ];

    public static function get($model, $id)
    {
        $object = Url::where('model', $model)->where('model_id', $id)->first();
        if ($object) {
            return $object->url;
        }
        return '';
    }

    public function getPageLanguage()
    {
        if ($this->model == 'PageLanguage') {
            $pageLanguage = PageLanguage::where('id', $this->model_id)->where('language', $this->language)->where('active',1)->first();
        } else {
            $pageLanguage = PageLanguage::where('source_model', $this->model)->where('language', $this->language)->where('active',1)->first();
            if ($pageLanguage) {
                $model = 'App\\' . $this->model;
                $object = $model::find($this->model_id);
                if (defined($object . '::CONTENT_MODEL')) {
                    $object->content = $object->getContent($pageLanguage->language, true);
                }
                $pageLanguage->object = $object;
            }
        }
        return $pageLanguage;
    }

    public static function findObject($url, $language)
    {

        $urlObject = Url::where('fullUrl', $url)->where('language', $language)->first();
        return $urlObject;
    }

    public static function handlePageLanguage($model, $className, $object = null)
    {
        if (!$model->source_model) {
            $pageUrl = $model->slug;
            self::urlCreate($pageUrl, $model->language, $className, $model->id, $object);
        }
    }

    public static function handleSourceModel($model, $className, $object = null)
    {
        $source_model = SourceModel::where('model_name', $className)->first();
        $language = $model->language ? $model->language : CmsConfig::get('default_front_language')->value;
        if ($source_model) {
            if ($model->parent) {
                self::urlCreate($model->slug, $language, $className, $model->parent->id, $object);
            } else {
                self::urlCreate($model->slug, $language, $className, $model->id, $object);
            }
        }

    }

    public static function handleUpdate($urlString, $url, $object = null)
    {
        $fullUrl = self::toUrl($url->displayPrefix($object) . '/' . $urlString);
        if (self::checkUrlUpdate($fullUrl, $url)) {
            $urlString = self::toUrl($urlString);
            $url->url = $urlString ? $urlString : '';
            $url->fullUrl = $fullUrl ? $fullUrl : '';
            $url->save();
            return true;
        } else {
            return self::handleUrlError(self::toUrl($fullUrl), $url->language);
        }
    }

    public static function updatePrefix($url, $prefix)
    {
        $url->prefix = str_replace('//', '/', $prefix);
        $fullUrl = self::toUrl($url->displayPrefix() . '/' . $url->url);
        $url->fullUrl = $fullUrl ? $fullUrl : '';
        $url->save();
    }

    public static function getUrl($model, $className)
    {
        $query = Url::where('model', $className);
        if ($model->parent) {
            $query = $query->where('model_id', $model->parent->id);
        } else {
            $query = $query->where('model_id', $model->getAttribute('id'));
        }
        if ($model->language) {
            $query = $query->where('language', $model->language);
        }
        return $query->first();

    }

    public static function checkAll()
    {
        foreach (Url::all() as $url) {
            if (!self::checkUrlUpdate($url->url, $url)) {
                self::handleUrlError($url->fullUrl, $url->language);
                return false;
            }
        }
        return true;
    }

    private static function urlCreate($url, $language, $className, $id, $object = null)
    {
        $prefix = null;
        $displayPrefix = '';
        $urlObject = Url::where('model', $className)->first();
        if ($urlObject) {
            $prefix = $urlObject->prefix;
            $displayPrefix = $urlObject->displayPrefix($object);
        }
        $url = self::checkUrlCreate(self::toUrl($displayPrefix . '/' . $url), $url, $language);
        $fullUrl = self::toUrl($displayPrefix . '/' . $url);
        Url::create([
            'url' => $url ? self::toUrl($url) : '',
            'language' => $language,
            'model' => $className,
            'model_id' => $id,
            'prefix' => $prefix,
            'fullUrl' => $fullUrl ? $fullUrl : ''
        ]);
    }

    private static function checkUrlCreate($fullUrl, $url, $language)
    {
        $urlsCount = 0;
        if ($url) {
            $urlsCount = Url::where('fullUrl', $fullUrl)->where('language', $language)->count();
        }
        if ($urlsCount) {
            return $url . '-' . date('dmyhis');
        }
        return $url;
    }

    private static function checkUrlUpdate($url, $urlObject)
    {
        $urlsCount = 0;
        if ($url) {
            $urlsCount = Url::where('fullUrl', $url)->where('language', $urlObject->language)->where('id', '!=', $urlObject->id)->count();
        }
        if ($urlsCount) {
            return false;
        }
        return true;
    }

    public static function toUrl($str)
    {
        $newStr = '';
        $explodedStr = explode('/', $str);
        foreach ($explodedStr as $item) {
            $slug = Str::slug($item);
            if ($slug) {
                $newStr .= $slug . '/';
            }
        }
        return substr($newStr, 0, -1);

    }

    public function getHostAttribute()
    {
        $multiDomains = CmsConfig::get('multi_domains')->value;
        if (!$multiDomains) {
            return config('cms.FRONT_DOMAIN') . '/' . $this->language . '/';
        } else {
            $domains = json_decode(CmsConfig::get('domains')->value, true);
            $domainName = array_search($this->language, $domains);
            return  $domainName. '/';
        }
    }

    public function displayPrefix($object = null)
    {
        if (!$object) {
            if ($this->model == 'PageLanguage') {
                $object = PageLanguage::find($this->model_id);
            } else {
                $class = 'App\\' . $this->model;
                $object = $class::find($this->model_id);
                if (defined($class . '::CONTENT_MODEL')) {
                    $object = $object->getContent($this->language);
                }
            }
        }
        $values = explode('/', $this->prefix);
        foreach ($values as &$v) {
            if (Str::startsWith($v, '[') && Str::endsWith($v, ']')) {
                $v = Str::replaceFirst('[', '', $v);
                $v = Str::replaceFirst(']', '', $v);
                $v = $object->$v;
            }
        }
        return self::toUrl(implode('/', $values));
    }

    public function getHostWithPrefixAttribute()
    {
        $return = $this->host;
        if ($this->prefix) {
            $return .= $this->displayPrefix() . '/';
        }
        return $return;
    }

    private static function handleUrlError($url, $language)
    {
        alert()->warning(__('app.alerts.urlExistWithLanguage', ['url' => $url, 'language' => $language]));
        return false;
    }
}
