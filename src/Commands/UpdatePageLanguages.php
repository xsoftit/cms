<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\SourceModel;

class UpdatePageLanguages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:pageLanguages:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $pageLanguages = PageLanguage::where('source_model_id', '!=', null)->get();

        foreach ($pageLanguages as $pageLanguage) {
            $source_model_id = $pageLanguage->source_model_id;
            $sourceModel = SourceModel::find($source_model_id);
            if ($sourceModel) {
                $pageLanguage->update([
                    'source_model' => $sourceModel->model_name
                ]);
            }
        }

        echo 'Page Languages updated' . PHP_EOL;
    }
}
