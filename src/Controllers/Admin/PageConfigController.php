<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Hermit\Logs\Log;
use Illuminate\Http\Request;
use Xsoft\Cms\Helpers\CmsConfig;
use Xsoft\Cms\Helpers\RouteSelectHelper;
use Xsoft\Cms\Models\Menu;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\Widget;
use Xsoft\Cms\Models\WidgetTemplate;

class PageConfigController extends Controller
{
    public function index()
    {
        $menuLangs = [];
        $firstMenu = Menu::where('lang',CmsConfig::get('default_front_language')->value)->get()->groupBy('lang');
        $otherMenus = Menu::where('lang','!=',CmsConfig::get('default_front_language'))->orderBy('lang')->get()->groupBy('lang');
        foreach($firstMenu as $key => $menu){
            $menuLangs[$key] = $menu;
        }
        foreach($otherMenus as $key => $menu){
            $menuLangs[$key] = $menu;
        }
        $globalWidgets = Widget::where('is_global', 1)->get();
        $widget_templates = WidgetTemplate::whereIn('id', $globalWidgets->pluck('widget_template_id'))->get();
        return view('cms::admin.pages.config', compact('menuLangs', 'globalWidgets', 'widget_templates'));
    }

    public function update(Request $request)
    {
        $language = $request->get('lang');
        $menus = Menu::where('lang', $language)->get();
        foreach ($menus as $menu) {
            if ($request->has($menu->type)) {
                $newItems = [];
                $items = array_values($request->get($menu->type)['items']);
                foreach ($items as $key => $item) {
                    if ($item['parent'] === null) {
                        $newItems[$item['key']] = $item;
                    } else {
                        $newItems[$item['parent']]['children'][$item['key']] = $item;
                    }
                }
                $menu->update([
                    'items' => json_encode($newItems),
                ]);
            }
        }
        alert(__('app.pageConfig.alerts.updated'), 'success');
        Log::write('Page', 'Updated page config', 1, $menu->id . ': ' . $menu->lang);
        return redirect()->route('admin.pageConfig.index');
    }

    public function getLink(Request $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        return response()->json(['link' => RouteSelectHelper::pageToLink($pageLanguage)]);
    }

}
