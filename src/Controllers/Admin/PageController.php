<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Hermit\Logs\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Xsoft\Cms\Helpers\FrontMenuHelper;
use Xsoft\Cms\Helpers\FrontProcessors\DataProcessor;
use Xsoft\Cms\Helpers\LanguageHelper;
use Xsoft\Cms\Models\Config;
use Xsoft\Cms\Models\Language;
use Xsoft\Cms\Models\Page;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\PageLanguageVersion;
use Xsoft\Cms\Models\PageSectionTemplate;
use Xsoft\Cms\Models\SourceModel;
use Xsoft\Cms\Models\WidgetTemplate;
use Xsoft\Cms\Requests\Pages\AddLanguageVersionRequest;
use Xsoft\Cms\Requests\Pages\StoreRequest;
use Xsoft\Cms\Requests\Pages\UpdateRequest;
use Xsoft\Cms\Services\Admin\PageService;

class PageController extends Controller
{
    protected $pageService;

    public function __construct()
    {
        $this->pageService = new PageService();
    }

    public function index()
    {
        $languages = Language::frontAll();
        $section_templates = PageSectionTemplate::all();
        $widget_templates = WidgetTemplate::all();
        $source_models = SourceModel::where('in_use', 0)->get();
        return view('cms::admin.pages.index', compact('languages', 'widget_templates', 'section_templates', 'source_models'));
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $page = Page::create([
                'name' => $request->get('name')
            ]);
            $pageLanguage = $this->pageService->pageLanguageCreate($request, $page->id, Config::where('name', 'default_front_language')->first()->value);
            Log::write('Page', 'Added new Page', 1, $page->id . ': ' . $pageLanguage->name . ' [' . $pageLanguage->language . '], Page_id: ' . $page->id);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            Log::write('ERROR', 'Adding new Page', 1, $exception->getMessage());
        }

        return redirect()->route('admin.pages.index');
    }

    public function preview(Request $request)
    {
        $pageLanguage = PageLanguage::where('id', $request->get('page_language_id'))->with('page')->first();
        $preview = $pageLanguage->preview(true);

        return response()->json(['preview' => $preview]);
    }

    public function frontPreview(PageLanguage $pageLanguage)
    {
        $pageLanguage->local = true;
        if ($pageLanguage->source_model_id && !$pageLanguage->sourceModel->singleObject) {
            $pageLanguage->object = $pageLanguage->getObject('', true);
        }
        $dataProcessor = new DataProcessor($pageLanguage);
        $view = view('front.index', compact('dataProcessor'));
        try {
            $html = $view->render();
        } catch (\Exception $exception) {
            $message = strip_tags($exception->getMessage());
            Log::write('ERROR', '(FRONT PAGE PREVIEW) PageLanguage id: ' . $pageLanguage->id . ' name: ' . $pageLanguage->name . ' | ' . $message, 1, ' File: ' . $exception->getFile() . ' : ' . $exception->getLine());
            abort(500);
        }
        return $html;
    }

    public function getPageDetails(Request $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        $options = json_decode($pageLanguage->page->options, true);
        $pageLanguage->url = $pageLanguage->getAttribute('url');
        $pageLanguage->host = $pageLanguage->getAttribute('host');
        return response()->json(['pageLanguage' => $pageLanguage, 'options' => $options]);
    }

    public function updateDetails(UpdateRequest $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        $alert = __('app.pages.alerts.updated');
        $class = 'success';
        $urlAlert = $this->pageService->pageLanguageUpdate($pageLanguage, $request);
        if($urlAlert){
            $alert .= ' ('.$urlAlert.')';
            $class = 'warning';
        }
        $changes = $pageLanguage->getChanges();
        $reload = false;
        if (count($changes) > 1) {
            if (key_exists('name', $changes)) {
                $reload = true;
            }
            if (key_exists('is_main', $changes)) {
                LanguageHelper::handleMainPage($pageLanguage);
                $reload = true;
            }
        }
        Log::write('Page', 'Updated PageLang details', 1, $pageLanguage->id . ': ' . $pageLanguage->name . ' [' . $pageLanguage->language . ']');
        return response()->json(['alert' => $alert, 'reload' => $reload,'class' => $class]);
    }

    public function activatePage(Request $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        $pageLanguage->update(['active' => 1, 'is_main' => 0]);
        FrontMenuHelper::addToMenu($pageLanguage, 'main');
        Log::write('Page', 'Activated PageLanguage', 1, $pageLanguage->id . ': ' . $pageLanguage->page_name . ' [' . $pageLanguage->language . ']');
        return response()->json(['status' => 'success']);
    }

    public function deactivatePage(Request $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        $is_main = $pageLanguage->is_main;
        $pageLanguage->update(['active' => 0, 'is_main' => 0]);
        FrontMenuHelper::removeFromMenu($pageLanguage, 'main');
        if ($is_main) {
            LanguageHelper::handleMainPage($pageLanguage);
        }
        alert(__('app.pages.alerts.deactivated'), 'success');
        Log::write('Page', 'Deactivated PageLanguage details', 1, $pageLanguage->id . ': ' . $pageLanguage->page_name . ' [' . $pageLanguage->language . ']');
        return response()->json(['status' => 'success']);
    }

    public function getLanguagesSelect(Request $request)
    {
        $page = Page::find($request->get('page_language_id'));
        $languages = Language::frontAll()->where('active', 1);
        $page_languages = $page->pageLanguages->pluck('language')->toArray();
        $possible_languages = [];
        foreach ($languages as $language) {
            if (!in_array($language->code, $page_languages)) {
                $possible_languages[] = $language;
            }
        }
        $select = '<div class="form-control"><select name="lang" class="form-control" required>';
        foreach ($possible_languages as $possible_language) {
            $select .= '<option value="' . $possible_language->code . '">' . $possible_language->name . '</option>';
        }
        $select .= '</select></div>';

        return response()->json(['select' => $select]);
    }

    public function addLanguageVersion(AddLanguageVersionRequest $request)
    {
        $page = Page::find($request->get('page_id'));
        $targetLanguage = $request->get('lang');
        $this->pageService->pageLanguageCreate($request,
            $page->id,
            $targetLanguage,
            $page->pageLanguages->first()->source_model_id,
            true
        );

        alert(__('app.pages.alerts.added'), 'success');
        Log::write('Page', 'Added Page language version', 1, $page->id . ': ' . $request->get('page_name') . ' [' . $request->get('lang') . ']');
        return redirect()->route('admin.pages.index');
    }

    public function publish(Request $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        $data = [];
        foreach ($pageLanguage->activeSections as $pageSection) {
            foreach ($pageSection->widgets as $widget) {
                $data[$pageLanguage->id][$pageSection->id][$widget->id] = $widget->getContents($pageLanguage->language);
                if ($widget->template->generate) {
                    $html = $widget->getFrontContent($pageLanguage);
                    $path = resource_path('/views/front/rendered/' . $pageLanguage->id);
                    if (!File::exists($path . '/' . $pageSection->id . '/')) {
                        if (!File::exists($path)) {
                            File::makeDirectory($path);
                        }
                        File::makeDirectory($path . '/' . $pageSection->id . '/');
                    }
                    File::put($path . '/' . $pageSection->id . '/' . $widget->id . '.blade.php', $html);
                }
            }
        }
        $pageLanguage->versions()->where('active', 1)->update(['active' => 0]);
        PageLanguageVersion::create([
            'active' => 1,
            'data' => json_encode($data),
            'page_language_id' => $pageLanguage->id
        ]);
        return response()->json(['status' => 'success'], 200);
    }

    public function delete(Request $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        if (!$pageLanguage->active) {
            $pageLanguage->delete();
        }

        return redirect()->back();
    }

    public function export(Page $page)
    {
        if (!$page) {
            return response()->json('Page not found', 500);
        }
        $output = PageService::pageArrayForExport($page);
        $output = json_encode($output);
        $file_name = now()->format('Y_m_d') . '_'. Str::slug(config('app.name')) .'_' . $page->id . '_' . Str::slug($page->name) . '.txt';
        if (!Storage::exists('public/exportFiles')) {
            mkdir(Storage::path('public/exportFiles'), 0775, true);
            chmod(Storage::path('public/exportFiles'), 0775);
        }
        file_put_contents(storage_path('app/public/exportFiles/' . $file_name), $output);
        sleep(1);
        return Storage::download('public/exportFiles/' . $file_name);
    }

    public function exportAllPages()
    {
        $output = ['multiple_pages' => true];
        foreach (Page::all() as $page) {
            $output[] = PageService::pageArrayForExport($page);
        }
        $output = json_encode($output);
        $file_name = now()->format('Y_m_d') . '_all_pages.txt';
        if (!Storage::exists('public/exportFiles')) {
            mkdir(Storage::path('public/exportFiles'), 0775, true);
            chmod(Storage::path('public/exportFiles'), 0775);
        }
        file_put_contents(storage_path('app/public/exportFiles/' . $file_name), $output);
        sleep(1);
        return Storage::download('public/exportFiles/' . $file_name);
    }

    public function import(Request $request)
    {
        $file = $request->file('import_file');
        $data = file_get_contents($file->getRealPath());
        $data = json_decode($data, true);
        $errors = [];
        if (key_exists('multiple_pages', $data)) {
            unset($data['multiple_pages']);
            foreach ($data as $item) {
                PageService::createPageFromData($item, $errors);
            }
        } else {
            PageService::createPageFromData($data, $errors);
        }
        if (count($errors) > 0) {
            Log::write('ERROR', json_encode($errors), 1, 'Pages Import');
        }

        return redirect()->route('admin.pages.index')->withErrors($errors);
    }

    public function getSections(Request $request)
    {
        $page = Page::find($request->get('page_id'));
        return response()->json($page->sections, 200);
    }

}
