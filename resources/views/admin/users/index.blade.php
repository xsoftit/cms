@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.users.title.index')}}
@endsection

@section('title')
    {{__('app.users.title.index')}}
@endsection

@section('subtitle')
    {{__('app.users.subtitle.index')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make(
        [__('app.dashboard.title'),route('admin.dashboard')],
        [__('app.users.title.index')])
    !!}
@endsection

@section('page-content')
    @if(auth()->user()->hasPermission('users.create'))
        <div class="row">
            <div class="col-12">
            <span data-toggle="modal" data-target="#user-create"
                  class="btn btn-outline-success float-right">{{__('app.add')}}</span>
            </div>
        </div>
    @endif
    @dataTable($table)
@endsection

@section('modals')
    @if(auth()->user()->hasPermission('users.create'))
        @include('cms::admin.users.modals.create')
    @endif
@endsection
