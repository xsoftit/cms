@section('modals')
    @parent
    <div class="modal fade" id="add-new-page" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.create')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.pages.store')}}" method="POST">
                    <div class="modal-body">
                        @csrf
                        {{--<input hidden name="language" id="new_language_code">--}}
                        <div class="form-group">
                            <label for="name" class="label-text">{{__('app.pages.form.name')}}</label>
                            <input class="form-control" name="name" id="new_page_name" value="{{old('name')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="meta_title" class="label-text">{{__('app.pages.form.metaTitle')}}</label>
                            <input class="form-control" name="meta_title" id="new_page_meta_title"
                                   value="{{old('meta_title')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="meta_description"
                                   class="label-text">{{__('app.pages.form.description')}}</label>
                            <textarea class="form-control no-jodit" name="meta_description"
                                      id="new_page_meta_description">{!! old('meta_description') !!}</textarea>
                        </div>
                        <div class="animated-checkbox">
                            <label>
                                <input type="checkbox" name="source_from_model" id="source-from-modal-checkbox">
                                <span
                                    class="label-text text-capitalize">{{__('app.pages.form.contentFromModel')}}</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <div id="model-select" style="display: none;">
                                <label>{{__('app.pages.form.modelSelect')}}</label>
                                <select name="source_model_id">
                                    <option disabled
                                            selected>{{__('app.pages.form.modelSelectPlaceholder')}}</option>
                                    @forelse($source_models as $source_model)
                                        <option
                                            value="{{$source_model->id}}">{{__('app.'.\Illuminate\Support\Str::plural($source_model->display_name).'.name')}}</option>
                                    @empty
                                        <option disabled>{{__('app.pages.form.noModelsFound')}}</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-success"
                                style="margin: 0">{{__('app.create')}}</button>
                        <button type="reset" class="btn btn-outline-light"
                                data-dismiss="modal">{{__('app.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script>
        $(document).ready(function () {
            $('body').find('#add-new-page').find('#source-from-modal-checkbox').on('change', function () {
                if ($(this).is(':checked')) {
                    $('body').find('#add-new-page').find('#model-select').attr('style', 'display:block');
                } else {
                    $('body').find('#add-new-page').find('#model-select').attr('style', 'display:none');
                }
            });
        });
    </script>
@endsection
