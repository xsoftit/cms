@extends('front.layouts.app')

@section('css')
    @parent
@endsection

@section('meta-tags')
    @parent
    {!! $dataProcessor->getMetaData() !!}
@endsection

@section('page-header')
    @include('front.layouts.partials.header')
@endsection

@section('page-content')
    @foreach($dataProcessor->getSections() as $section)
        {!! $section->getView() !!}
    @endforeach
@endsection

@section('page-footer')
    @include('front.layouts.partials.footer')
@endsection
