<?php

namespace Xsoft\Cms;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Xsoft\Cms\Commands\ChangeCmsTemplate;
use Xsoft\Cms\Commands\CmsLanguageCreate;
use Xsoft\Cms\Commands\CmsLanguageUpdate;
use Xsoft\Cms\Commands\CmsStartCommand;
use Xsoft\Cms\Commands\CmsVersion;
use Xsoft\Cms\Commands\CreateGlobalWidget;
use Xsoft\Cms\Commands\CreateSectionTemplate;
use Xsoft\Cms\Commands\CreateWidgetTemplate;
use Xsoft\Cms\Commands\MakeModel;
use Xsoft\Cms\Commands\PrepareResourcesDirectories;
use Xsoft\Cms\Commands\PrepareUrls;
use Xsoft\Cms\Commands\UpdatePageLanguages;
use Xsoft\Cms\Helpers\CmsConfig;
use Xsoft\Cms\Models\File;
use Xsoft\Cms\Models\Gallery;
use Xsoft\Cms\Models\GalleryItem;
use Xsoft\Cms\Models\Page;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\PageSection;
use Xsoft\Cms\Models\Widget;
use Xsoft\Cms\Models\WidgetContent;
use Xsoft\Cms\Observers\FileObserver;
use Xsoft\Cms\Observers\GalleryItemObserver;
use Xsoft\Cms\Observers\GalleryObserver;
use Xsoft\Cms\Observers\PageLanguageObserver;
use Xsoft\Cms\Observers\PageObserver;
use Xsoft\Cms\Observers\PageSectionObserver;
use Xsoft\Cms\Observers\WidgetContentObserver;
use Xsoft\Cms\Observers\WidgetObserver;

class CmsServiceProvider extends ServiceProvider
{
    const LANGUAGES = [
        'en' => 'English',
        'pl' => 'Polski'
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if (!defined('CMS_VER')) {
            define('CMS_VER', '2.2.1');
        }

        $this->publishes([
            __DIR__ . '/../config/' => config_path(),
        ], 'cmsConfig');
        $this->publishes([
            __DIR__ . '/../resources/views/front/' => resource_path() . '/views/front',
        ], 'cmsViews');
        $this->publishes([
            __DIR__ . '/../resources/publishLang/' => resource_path() . '/lang',
        ], 'cmsLang');
        $this->publishes([
            __DIR__ . '/../public/' => public_path(),
        ], 'cmsPublic');
        $this->publishes([
            __DIR__ . '/../resources/sass/admin/' => resource_path() . '/sass/admin/',
        ], 'cmsSass');
        $this->publishes([
            __DIR__ . '/../custom/' => base_path(),
        ], 'cmsCustom');
        $this->publishes([
            __DIR__ . '/../storage/' => storage_path() . '/app/public',
        ], 'cmsStorage');
        $this->publishes([
            __DIR__ . '/../src/Helpers/Publish/MenuHelper.php' => app_path() . '/Helpers/MenuHelper.php',
        ], 'cmsMenuHelper');
        $this->publishes([
            __DIR__ . '/../src/Models/publish/' => app_path(),
        ], 'cmsUser');
        $this->publishes([
            __DIR__ . '/../src/Middleware/Authenticate.php' => app_path() . '/Http/Middleware/Authenticate.php',
            __DIR__ . '/../src/Middleware/RedirectIfAuthenticated.php' => app_path() . '/Http/Middleware/RedirectIfAuthenticated.php',
        ], 'cmsMiddleware');
        $this->publishes([
            __DIR__ . '/../database/seedStarters/' => base_path() . '/database/seeds/',
        ], 'cmsSeeders');

        $this->publishes([
            __DIR__ . '/../resources/sass/layouts/default/_layout.scss' => resource_path() . '/sass/admin/vendor/_layout.scss',
        ], 'cmsDefaultLayout');
        $this->publishes([
            __DIR__ . '/../resources/sass/layouts/default/_variables.scss' => resource_path() . '/sass/AdminPanel/_variables.scss',
        ], 'cmsDefaultLayoutVariables');
        $this->publishes([
            __DIR__ . '/../resources/sass/layouts/AdminPro/_layout.scss' => resource_path() . '/sass/admin/vendor/_layout.scss',
        ], 'cmsAdminProLayout');
        $this->publishes([
            __DIR__ . '/../resources/sass/layouts/AdminPro/_variables.scss' => resource_path() . '/sass/AdminPanel/_variables.scss',
        ], 'cmsAdminProLayoutVariables');

        $this->commands([
            CmsStartCommand::class,
            CreateGlobalWidget::class,
            CreateSectionTemplate::class,
            CreateWidgetTemplate::class,
            MakeModel::class,
            PrepareResourcesDirectories::class,
            ChangeCmsTemplate::class,
            PrepareUrls::class,
            CmsVersion::class,
            CmsLanguageCreate::class,
            CmsLanguageUpdate::class,
            UpdatePageLanguages::class
        ]);
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'cms');
        $this->loadViewsFrom(__DIR__ . '/../resources/datatables', 'datatables');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');


        // Middlewares
        $this->app['router']->aliasMiddleware('role', \Xsoft\AdminPanel\Middlewares\RoleMiddleware::class);
        $this->app['router']->aliasMiddleware('permission', \Xsoft\AdminPanel\Middlewares\PermissionMiddleware::class);
        $this->app['router']->aliasMiddleware('role_or_permission', \Xsoft\AdminPanel\Middlewares\RoleOrPermissionMiddleware::class);
        $this->app['router']->aliasMiddleware('lang_check', \Xsoft\Cms\Middleware\CheckLanguage::class);


        // Observers
        Page::observe(PageObserver::class);
        Widget::observe(WidgetObserver::class);
        PageSection::observe(PageSectionObserver::class);
        PageLanguage::observe(PageLanguageObserver::class);
        WidgetContent::observe(WidgetContentObserver::class);
        File::observe(FileObserver::class);
        Gallery::observe(GalleryObserver::class);
        GalleryItem::observe(GalleryItemObserver::class);

        //Globals
        if (Schema::hasTable('configs')) {
            CmsConfig::setGlobals();
        }

    }


    public function register()
    {
        $this->handleDomain();
        include __DIR__ . '/../routes/web.php';
        //Admin
        $this->app->make('Xsoft\Cms\Controllers\Admin\ConfigController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\DashboardController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\FileController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\FrontLanguageController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\LanguageController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\PageConfigController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\PageController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\PageSectionController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\PageSectionTemplateController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\RoleController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\SourceModelController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\UserController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\WidgetController');
        //Auth
        $this->app->make('Xsoft\Cms\Controllers\Admin\Auth\ForgotPasswordController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\Auth\LoginController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\Auth\ResetPasswordController');
        $this->app->make('Xsoft\Cms\Controllers\Admin\Auth\VerificationController');
        //Hermit
        $this->app->make('Xsoft\Cms\Controllers\Admin\Hermit\LogController');
        //Front
        $this->app->make('Xsoft\Cms\Controllers\Front\PageController');
    }

    private function handleDomain()
    {
        if (key_exists('HTTPS_HOST', $_SERVER) || key_exists('HTTP_HOST', $_SERVER)) {
            $currentDomain = key_exists('HTTPS_HOST', $_SERVER) ? $_SERVER['HTTPS_HOST'] : $_SERVER['HTTP_HOST'];
            if (Str::contains($currentDomain, config('cms.ADMIN_PREFIX'))) {
                config(['cms.ADMIN_DOMAIN' => $currentDomain]);
                return true;
            }
            config(['cms.FRONT_DOMAIN' => $currentDomain]);
        }
    }
}
