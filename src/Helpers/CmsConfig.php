<?php

namespace Xsoft\Cms\Helpers;

use Xsoft\Cms\Models\Config;
use Xsoft\Cms\Models\SourceModel;
use Illuminate\Support\Facades\View;

class CmsConfig
{
    public static function get($name)
    {
        $config = Config::where('name', $name)->first();
        if(!$config){
            $config = new Config();
        }
        return $config;
    }

    public static function getAll(){
        $configs = Config::all();
        return $configs;
    }

    public static function getByCategory($category){
        $configs = Config::where('category', $category)->get();
        return $configs;
    }

    public static function getByType($type){
        $configs = Config::where('type', $type)->get();
        return $configs;
    }

    public static function categories(){
        $categories = Config::distinct()->pluck('category')->toArray();
        return $categories;
    }

    public static function setGlobals(){
        $configs = CmsConfig::getAll();
        $models = SourceModel::all();
        View::share('configs', $configs);
        View::share('models', $models);
    }
}
