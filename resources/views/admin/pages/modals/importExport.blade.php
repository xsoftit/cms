@section('modals')
    @parent
    <div class="modal fade" id="export-page" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.export')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{__('app.pages.questions.export')}}
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirm-export-page"
                            class="btn btn-outline-success">{{__('app.confirm')}}</button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">{{__('app.close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="export-all-pages" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.export')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{__('app.pages.questions.export')}}
                </div>
                <div class="modal-footer">
                    <a href="{{route('admin.pages.exportAll')}}"
                       class="btn btn-success">{{__('app.confirm')}}</a>
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">{{__('app.close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="import-new-page" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{route('admin.pages.import')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('app.pages.title.import')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="file" name="import_file">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-success">{{__('app.pages.import')}}</button>
                        <button type="reset" class="btn btn-outline-light" data-dismiss="modal">{{__('app.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
