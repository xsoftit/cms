<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class PageLanguageVersion extends Model
{
    protected $fillable = [
        'active',
        'data',
        'page_language_id',
    ];
}
