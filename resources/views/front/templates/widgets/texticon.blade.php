@php($type = $data->get('type'))
@if($type == 'headerIcon')
    <img class="icon-badge-2" src="{!!$data->get('path')!!}" alt="{!!$data->get('alt')!!}">
    <h3 class="mt15">{!!$data->get('header')!!}</h3>
    <p>{!!$data->get('text')!!}</p>
@elseif($type == 'number')
    <div class="icon-badge number">{!!$data->get('number')!!}</div>
    <h3 class="mt15">{!!$data->get('header')!!}</h3>
    <p>{!!$data->get('text')!!}</p>
@else
    <div class="icon mb30">
        <img src="{!!$data->get('path')!!}" alt="{!!$data->get('alt')!!}">
    </div>
    <h4>{!!$data->get('header')!!}</h4>
    <p>{!!$data->get('text')!!}</p>
@endif
