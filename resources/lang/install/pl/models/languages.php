<?php

return [
    "data" => "Dane",
    "name" => "Język",
    "showEmpty" => "Pokazuj tylko puste pola",
    'title' => [
        "edit" => "Edycja języka panelu administracyjnego (:name)",
        "index" => "Języki",
        "create" => "Nowy język panelu administracyjnego"
    ],
    'subtitle' => [
        "edit" => "Tłumaczenia dla panelu administracyjnego",
        "index" => "Lista języków panelu administracyjnego",
    ],
    'fields' => [
        "code" => "Kod",
        "name" => "Nazwa",
        "active" => "Aktywny",
        "fields" => "Pola",
        "inactive" => "Nieaktywny"
    ],
    'alerts' => [
        "saved" => "Język :name zapisany pomyślnie!",
        "activated" => "Język :name aktywowany!",
        "deactivated" => "Język :name dezaktywowany!",
        "isMainLanguage" => "Nie można dezaktywować języka głównego!",
    ]
];
