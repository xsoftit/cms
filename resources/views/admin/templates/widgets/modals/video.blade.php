@component('cms::admin.templates.widgetModal',get_defined_vars())
    @include('cms::admin.filemanager.manager',[
        'name' => 'video',
        'label' => __('app.widgets.labels.video'),
        'type' => 'single',
        'fileTypes' => ['video']
        ])
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.width')}}</label>
        <input id="width" name="width" class="form-control data">
    </div>
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.height')}}</label>
        <input id="height" name="height" class="form-control data">
    </div>
    <div class="form-group">
        <div class="animated-checkbox">
            <label>
                <input type="checkbox" name="autoplay" id="autoplay" class="data">
                <span
                    class="label-text text-capitalize">{{__('app.widgets.labels.autoplay')}}</span>
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="animated-checkbox">
            <label>
                <input type="checkbox" name="loop" id="loop" class="data">
                <span
                    class="label-text text-capitalize">{{__('app.widgets.labels.loop')}}</span>
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="animated-checkbox">
            <label>
                <input type="checkbox" name="muted" id="muted" class="data">
                <span
                    class="label-text text-capitalize">{{__('app.widgets.labels.muted')}}</span>
            </label>
        </div>
    </div>
@endcomponent
