<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class CmsPermissionsSeeder extends Seeder
{
    protected $permissions = [
        //General
        'cms.login' => [],
        //Pages
        'pages.show' => ['cms.login'],
        'pages.publish' => ['widgets.edit'],
        'pages.edit' => ['pages.show'],
        'pages.create' => ['pages.edit'],
        'pages.delete' => ['pages.create'],
        'pages.export' => ['pages.show'],
        'pages.import' => ['pages.show'],
        //Sections
        'sections.edit' => ['pages.show'],
        'sections.create' => ['sections.edit'],
        'sections.delete' => ['sections.create'],
        //Widgets
        'widgets.edit' => ['pages.show', 'files.usage'],
        'widgets.create' => ['widgets.edit'],
        'widgets.delete' => ['widgets.create'],
        //Front Languages
        'front_languages.show' => ['cms.login'],
        'front_languages.edit' => ['front_languages.show'],
        'front_languages.create' => ['front_languages.edit'],
        'front_languages.delete' => ['front_languages.create'],
        'front_languages.exportImport' => ['front_languages.show'],
        //Page Config
        'page_config.show' => ['cms.login'],
        'page_config.edit' => ['page_config.show', 'files.usage'],
        //Users
        'users.show' => ['cms.login'],
        'users.edit' => ['users.show'],
        'users.create' => ['users.edit'],
        'users.delete' => ['users.create'],
        //Roles
        'roles.show' => ['cms.login'],
        'roles.edit' => ['roles.show'],
        'roles.create' => ['roles.edit'],
        'roles.delete' => ['roles.create'],
        //Config
        'config.show' => ['cms.login'],
        'config.edit' => ['config.show', 'files.usage'],
        //Admin Languages
        'admin_languages.show' => ['config.show'],
        'admin_languages.edit' => ['admin_languages.show'],
        'admin_languages.create' => ['admin_languages.edit'],
        'admin_languages.delete' => ['admin_languages.create'],
        'admin_languages.exportImport' => ['admin_languages.show'],
        //Logs
        'logs.show' => ['cms.login'],
        //Urls
        'urls.show' => ['cms.login'],
        //Source Models
        'source_models.show' => ['cms.login'],
        //Files
        'files.usage' => ['cms.login'],
        'files.foldersManage' => ['files.usage'],
        'files.manage' => ['files.usage'],
        'files.delete' => ['files.manage'],
        'files.exportImport' => ['files.usage'],
        //Galeries
        'galleries.show' => ['cms.login'],
        'galleries.edit' => ['galleries.show'],
        'galleries.create' => ['galleries.edit'],
        'galleries.delete' => ['galleries.create'],
    ];

    public function run()
    {
        foreach ($this->permissions as $name => $dependencies) {
            if(!Permission::where('name',$name)->first()){
                $newPermission = new Permission([
                    'name' => $name
                ]);
                $newPermission->dependencies = json_encode($dependencies);
                $newPermission->save();
            }
        }
        foreach (Permission::all() as $permission){
            if(!key_exists($permission->name,$this->permissions)){
                $permission->delete();
            }
        }
    }
}
