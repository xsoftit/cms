<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'labels' => [
        "tag" => "Tag",
        "styles" => "Style",
        "classes" => "Klasy",
    ],
    'alerts' => [
        "added" => "Kontener zaktualizowany pomyślnie",
        "updated" => "Kontener dodany pomyślnie"
    ]
];
