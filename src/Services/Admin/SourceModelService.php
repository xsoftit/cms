<?php

namespace Xsoft\Cms\Services\Admin;

use Xsoft\Cms\Models\SourceModel;
use Xsoft\DataTables\DataTable;

class SourceModelService
{
    public function getTable()
    {
        $table = new DataTable('source_models', 'admin.sourceModels.indexAjax');
        $table->setOptions(['searching' => false, 'lengthChange' => false, 'info' => false, 'order' => '0,asc']);
        $table->setColumns([
            ['ID', 'id', ['class' => 'width5']],
            [__('app.sourceModels.fields.model_name'), 'model_name'],
            [__('app.sourceModels.fields.in_use'), 'in_use'],
//            [__('app.sourceModels.fields.fields'), 'model_name'],
            [__('app.sourceModels.fields.singleObject'), 'id', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width10']],
            ['', 'model_name', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width10']]
        ]);
        return $table;
    }

    public function prepareOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            if ($row->in_use) {
                $sourceModel = SourceModel::find($row->id);
                $page_languages = $sourceModel->page_languages;
                $used = '';
                foreach ($page_languages as $key => $page_language) {
                    $used .= $page_language->id . ' - ' . $page_language->name . ' [' . $page_language->language . ']';
                    if ($key + 1 != $page_languages->count()) {
                        $used .= ';<br> ';
                    }
                }
            } else {
                $used = '<span class="text-danger">' . __('app.sourceModels.fields.in_use0') . '</span>';
            }
            $output['aaData'][] = [
                $row->id,
                $row->model_name,
                $used,
                $row->singleObject ? "<a href='" . route('admin.sourceModels.change', [$row->id]) . "' class='btn btn-outline-success'>" . __('app.sourceModels.yes') . "</a>" : "<a href='" . route('admin.sourceModels.change', [$row->id]) . "' class='btn btn-outline-light'>" . __('app.sourceModels.no') . "</a>",
                "<a href='" . route('admin.sourceModels.summary', ['className' => $row->model_name]) . "' class='btn btn-outline-secondary'>" . __('app.sourceModels.summary') . "</a>"
            ];
        }
    }

}
