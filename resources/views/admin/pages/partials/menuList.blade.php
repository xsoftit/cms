@foreach($items as $key => $page)
    <li class="ui-state-default list-group-item" data-key="{{$key}}">
        <div class="animated-checkbox">
            <label>
                <input type="checkbox"
                       name="{{$menu->type}}[items][{{$key}}][show]"
                       {{key_exists('show',$page)?"checked":""}} @if(!auth()->user()->hasPermission('page_config.edit')) disabled @endif>
                <span class="label-text"></span>
            </label>
            <span class="menu-text">{{$page['name']}}</span>
            <small class="menu-text">({{$page['link']}})</small>
            <input class="form-control menu-name" hidden name="{{$menu->type}}[items][{{$key}}][name]"
                   value="{{$page['name']}}">
            <input class="form-control menu-link" hidden name="{{$menu->type}}[items][{{$key}}][link]"
                   value="{{$page['link']}}">
            <input class="form-control menu-parent" hidden name="{{$menu->type}}[items][{{$key}}][parent]"
                   value="{{isset($parent) ? $parent : ''}}">
            <input class="form-control menu-key" hidden name="{{$menu->type}}[items][{{$key}}][key]"
                   value="{{$key}}">
            <i class="af af-edit menu-edit"></i>
            <i class="af af-times menu-delete"></i>
        </div>
        <ul class="front-menu-sortable list-group">
            @if(key_exists('children',$page))
                @if($page['children'])
                    @include('cms::admin.pages.partials.menuList',['items' => $page['children'],'parent' => $key])
                @endif
            @endif
        </ul>
    </li>
@endforeach
