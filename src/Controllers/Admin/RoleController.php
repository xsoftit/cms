<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Xsoft\Cms\Requests\Role\StoreRequest;
use Xsoft\Cms\Services\Admin\RoleService;
use App\User;
use Hermit\Logs\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    protected $roleService;

    public function __construct()
    {
        $this->roleService = new RoleService();
    }

    public function index(Request $request)
    {
        $table = $this->roleService->getTable($request);

        return view('cms::admin.roles.index', compact('table'));
    }

    public function indexAjax(Request $request)
    {
        $table = $this->roleService->getTable();
        $output = $table->output($request);
        $this->roleService->prepareOutput($output);

        return $output;
    }

    public function store(StoreRequest $request)
    {
        $role = Role::create(['name' => $request->get('name')]);

        alert()->success(__('app.roles.alerts.saved', ['name' => $role->name]));
        Log::write('Role', 'Added new role', 1, $role->id . ': ' . $role->name);
        return redirect()->route('admin.roles.edit', compact('role'));
    }

    public function edit(Role $role)
    {
        $user = auth()->user();
        if($user->hasRole($role->name)){
            return redirect()->route('admin.roles.index');
        }
        $allPermissions = Permission::all()->sortBy('id');
        $permissions = [];
        foreach ($allPermissions as $permission) {
            if($user->hasPermission($permission->name)){
                $arr = explode('.', $permission->name);
                $permissions[$arr[0]][] = ['name' => $arr[1], 'dependencies' => $permission->dependencies];
            }
        }

        return view('cms::admin.roles.edit', compact('role', 'permissions'));
    }

    public function update(Request $request, Role $role)
    {
        DB::table('role_has_permissions')->where('role_id',$role->id)->delete();
        if ($request->get('permissions')) {
            foreach ($request->get('permissions') as $permission => $check) {
                $role->givePermissionTo($permission);
            }
        }

        alert()->success(__('app.roles.alerts.saved', ['name' => $role->name]));
        Log::write('Role', 'Updated role', 1, $role->id . ': ' . $role->name);
        if ($request->has('back')) {
            return redirect()->route('admin.roles.index');
        }
        return redirect()->route('admin.roles.edit', compact('role'));
    }

    public function destroy(Role $role)
    {
        foreach (User::all() as $user) {
            if ($user->hasAnyRole([$role->name])) {
                alert()->danger(__('app.roles.alerts.roleHasUsers'));
                return redirect()->back();
            }
        }
        Log::write('Role', 'Deleted role', 1, $role->id . ': ' . $role->name);
        $role->delete();
        return redirect()->route('admin.roles.index');
    }
}
