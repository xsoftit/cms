@section('header')
    <header class="app-header">
        <a class="app-header__logo" href="/">
            <span class="logo-big">
                <img class="img-fluid"
                     src="{{ CmsConfig::get('admin_logo')->image ? CmsConfig::get('admin_logo')->image->path : asset('admin/images/logo-black-gradient.svg')}}"/>
            </span>
            <span class="logo-mini">
                <img class="img-fluid"
                     src="{{ CmsConfig::get('admin_logo')->image ? CmsConfig::get('admin_logo')->image->path : asset('admin/images/logo-mini-black-gradient.svg')}}"/>
            </span>
        </a>
        <span class="app-sidebar__toggle af af-bars" data-toggle="sidebar" aria-label="Hide Sidebar"></span>
        {!! (auth()->user()->role()->name == 'superadmin')?('<h1>'.config('app.name').' <small style="font-size:40%;">Version: COMPOSER('.(CmsConfig::get('cms_version') ? CmsConfig::get('cms_version')->value:'').') - PROVIDER('.CMS_VER.')</small></h1>'):''!!}
        <ul class="app-nav">
            <li class="dropdown">
                <a class="app-nav__item text-uppercase" href="#" data-toggle="dropdown"
                   aria-expanded="false">{{auth()->user()->language}}</a>
                <ul class="language-menu dropdown-menu dropdown-menu-right" x-placement="bottom-end">
                    @foreach(LanguageHelper::getAll() as $lang)
                        <li><a class="dropdown-item text-uppercase"
                               href="{{route('admin.users.languageChange',[$lang])}}">{{$lang}}</a></li>
                    @endforeach
                </ul>
            </li>
            <li class="dropdown">
                <a class="app-nav__item" href="#" data-toggle="dropdown" aria-expanded="false"><i
                        class="af af-user af-lg"></i></a>
                <ul class="dropdown-menu settings-menu dropdown-menu-right" x-placement="bottom-end">
                    <li><a class="dropdown-item" href="{{route('admin.users.profile')}}"><i
                                class="af af-user af-lg"></i>{{__('app.profile')}}</a></li>
                    <li>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST">
                            @csrf()
                            <button type="submit" class="dropdown-item"><i
                                    class="af af-sign-out-alt af-lg"></i>{{__('app.logout')}}</button>
                        </form>
                    </li>
                </ul>
            </li>
            <li>
                <div id="clock" class="app-nav__item">{{date('h:i:s')}}</div>
            </li>
        </ul>
    </header>
@show

@section('js')
    @parent
    <script>
        $('.app-sidebar__toggle').on('click', function () {
            let sidebar_toggle = 0;
            if ($('body').hasClass('sidenav-toggled')) {
                sidebar_toggle = 1;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                data: {
                    sidebar_toggle: sidebar_toggle
                },
                url: '{{route('admin.users.sidebarToggle')}}'
            });
        });
    </script>
@endsection
