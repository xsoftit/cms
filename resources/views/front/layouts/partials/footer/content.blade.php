<div class="row">
    <div class="col-12">
        <span>{{page_data('company_name',$configs)}}</span>
        <span>{{page_data('company_address_1',$configs)}}</span>
        <span>{{page_data('company_address_2',$configs)}}</span>
        <span>{{page_data('company_email',$configs)}}</span>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        {!! $dataProcessor->getFooterMenu() !!}
    </div>
</div>
