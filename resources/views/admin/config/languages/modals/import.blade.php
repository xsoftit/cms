<div class="modal fade" id="language-import" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('app.import')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.config.languages.import')}}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="modal-body">
                        <input type="file" name="import_file">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success">
                        {{__('app.confirm')}}
                    </button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                        {{__('app.close')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
