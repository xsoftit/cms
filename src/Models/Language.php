<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;
use Xsoft\Cms\Helpers\CmsConfig;

class Language extends Model
{
    protected $fillable = [
        'name',
        'code',
        'fields',
        'front',
        'active',
        'hasMain'
    ];

    //Relations

    public function users()
    {
        return $this->hasMany('App\User', 'language', 'code');
    }

    //Methods
    public function getPages()
    {
        return PageLanguage::where('language', $this->code)->get();
    }

    public function countPages()
    {
        return PageLanguage::where('language', $this->code)->count();
    }

    public function extraInfo()
    {
        $info = "";
        if (!$this->countPages()) {
            $info .= __('app.frontLanguages.extraInfo.noPages') . "<br>";
        }
        if (!$this->hasMain) {
            $info .= __('app.frontLanguages.extraInfo.noMain') . "<br>";
        }
        if (!$this->active) {
            $info .= __('app.frontLanguages.extraInfo.noActive') . "<br>";
        }

        return $info;
    }

    public function checkKey($key)
    {
        $fields = json_decode($this->fields, true);
        $arrayKey = explode('-', $key);
        if(is_array($arrayKey)){
            foreach ($arrayKey as $key => $aKey) {
                if ($aKey) {
                    if (!key_exists($aKey, $fields)) {
                        return false;
                    }
                    $fields = $fields[$aKey];
                }
            }
        }
        return $fields;
    }

    public static function frontAll()
    {
        $config = CmsConfig::get('default_lang')->value;
        $default = Language::where('front', 1)->where('code',$config)->get();
        return $default->merge(Language::where('front', 1)->where('code','!=',$config)->orderBy('code','asc')->get());
    }

    public static function activeFrontAll()
    {
        $config = CmsConfig::get('default_lang')->value;
        $default = Language::where('front', 1)->where('code',$config)->get();
        return $default->merge(Language::where('front', 1)->where('code','!=',$config)->where('active',1)->orderBy('code','asc')->get());
    }

    public static function adminAll()
    {
        $config = CmsConfig::get('default_lang')->value;
        $default = Language::where('front', 0)->where('code',$config)->get();
        return $default->merge(Language::where('front', 0)->where('code','!=',$config)->orderBy('code','asc')->get());
    }
}
