<div class="form-group">
    <label>{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</label>
    <input type="number" name="contents[{\{$language}}][{{$field->getName()}}]" value="{\{old('contents[{\{$language}}][{{$field->getName()}}]',$content->{{$field->getName()}})}}" class="form-control">
</div>
