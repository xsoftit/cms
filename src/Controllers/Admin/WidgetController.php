<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Hermit\Logs\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Xsoft\Cms\Helpers\RouteSelectHelper;
use Xsoft\Cms\Models\Gallery;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\PageSection;
use Xsoft\Cms\Models\Widget;
use Xsoft\Cms\Models\WidgetTemplate;
use Xsoft\Cms\Services\Admin\WidgetService;

class WidgetController extends Controller
{
    protected $widgetService;

    public function __construct()
    {
        $this->widgetService = new WidgetService();
    }

    public function addWidget(Request $request)
    {
        $widgetTemplate = WidgetTemplate::find($request->get('widget_template_id'));
        $pageSection = PageSection::find($request->get('section_id'));
        $sectionWidgets = $pageSection->widgets;
        $slug = Str::slug($request->get('name'));
        $duplicate = $sectionWidgets->where('slug', $slug);
        if ($duplicate->count()) {
            $slug .= "-" . $duplicate->count();
        }
        $content = [];
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        if ($widgetTemplate->slug == 'menu') {
            $content = ['lang' => $pageLanguage->language];
        };
        $widget = $this->widgetService->create($widgetTemplate, $request, $slug);
        $this->widgetService->createContent($widget, $pageLanguage->language, $content);
        $this->widgetService->addToSection($pageSection, $widget, $request);

        $widgetPreview = $widget->preview($request->get('lang'));
        return response()->json(['preview' => $widgetPreview]);
    }


    public function getContent(Request $request)
    {
        $widget = Widget::find($request->get('widget_id'));
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        if ($pageLanguage) {
            $contents = json_decode($widget->getContents($pageLanguage->language), true);
        } else {
            $contents = json_decode($widget->getContents($request->get('page_language_id')), true);
        }
        $data = [];
        $this->widgetService->prepareContent($contents, $data);
        $contents = $data;
        if (key_exists('is_gallery', $contents)) {
            $gallery = Gallery::find($contents[$contents['is_gallery']]);
            if ($gallery) {
                $contents[$contents['is_gallery']] = [$gallery->id,$gallery->name];

            }
        }
        return response()->json([
            'content' => $contents,
        ], 200);
    }

    public function saveContent(Request $request)
    {
        $widget = Widget::with('template', 'contents')->find($request->get('widget_id'));
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        if ($pageLanguage) {
            $widgetContent = $widget->contents->where('language', $pageLanguage->language)->first();
        } else {
            $widgetContent = $widget->contents->where('language', $request->get('page_language_id'))->first();
        }
        $contents = $request->get('data');
        if (!key_exists('tag', $contents)) {
            $contents['tag'] = $widgetContent['tag'];
            $contents['classes'] = $widgetContent['classes'];
            $contents['styles'] = $widgetContent['styles'];
        }
        $output_contents = $contents;
        if (key_exists('is_gallery', $contents)) {
            $gallery = Gallery::find($contents[$contents['is_gallery']]);
            if ($gallery) {
                $output_contents[$contents['is_gallery']] = $gallery->name;

            }
        }
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        if (key_exists('files', $contents)) {
            $output_contents = $this->widgetService->storeFiles($widget, $output_contents, $contents);
        }
        if (!$widgetContent) {
            if ($pageLanguage) {
                $this->widgetService->createContent($widget, $pageLanguage->language, $contents);
            }else{
                $this->widgetService->createContent($widget, $request->get('page_language_id'), $contents);
            }
        } else {
            $widgetContent->update([
                'content' => json_encode($contents)
            ]);
        }
        Log::write('WidgetContent', 'Updated Content', 1, $widget->id . ': ' . $widget->name . ' - ' . $widget->language);
        return response()->json([
            'status' => 'updated',
            'contents' => $output_contents,
        ], 200);
    }

    public function delete(Request $request)
    {
        $widget = Widget::find($request->get('widget_id'));
        foreach ($widget->contents as $content) {
            $content->delete();
        }
        DB::table('page_section_widget')->where('widget_id', $widget->id)->delete();
        $widget->delete();
        return response()->json(['ok' => 'ok']);
    }

    public function move(Request $request)
    {
        $max = DB::table('page_section_widget')
            ->where('page_section_id', $request->get('section_id'))
            ->where('placement_column', $request->get('column'))
            ->max('placement_order');
        DB::table('page_section_widget')
            ->where('widget_id', $request->get('widget_id'))
            ->update([
                'page_section_id' => $request->get('section_id'),
                'placement_column' => $request->get('column'),
                'placement_order' => $max + 1
            ]);
        return response()->json(['widget_id' => $request->get('widget_id')]);
    }

    public function copy(Request $request)
    {
        $widget = Widget::find($request->get('widget_id'));
        $contents = $widget->contents;
        $newWidget = $widget->replicate();
        $newWidget->save();
        foreach ($contents as $content) {
            $newContent = $content->replicate();
            $newContent->widget_id = $newWidget->id;
            $newContent->save();
        }
        $max = DB::table('page_section_widget')
            ->where('page_section_id', $request->get('section_id'))
            ->where('placement_column', $request->get('column'))
            ->max('placement_order');
        DB::table('page_section_widget')->insert([
            'widget_id' => $newWidget->id,
            'page_section_id' => $request->get('section_id'),
            'placement_column' => $request->get('column'),
            'placement_order' => $max + 1
        ]);
        return response()->json(['widget_id' => $newWidget->id]);
    }
}
