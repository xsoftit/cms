#UPDATING XSOFT/CMS PACKAGE TO V2
###**IF YOU UPDATE PACKAGE FROM EARLIER VERSION THERE IS A LOT TO CHANGE.**

**The update steps are described below.**

Start with:
```
php artisan cms:start --update
```
After update there should be new services for each source model in `app\Http\Services\Front`.
Those services have methods necessary for front source model widgets to work.

You can change methods inside your `'ModelName'Service.php class`, however do not change return format or remove/override any existing elements of `array $data` as they are necessary to properly render widget view.
Any element that you add to `$data` will be accessible inside widget blade in object of new class FrontWidget `$data`.


Every widget that uses source model data must be updated. Go to every source model widget modal (`resources\views\admin\templates\widgets\modals`) and add inputs:
```
<input id="source_model" name="source_model" type="text" value="MODEL_NAME" class="data static no-clear" hidden>
<input id="source_content_type" name="source_content_type" type="text" value="CONTENT_TYPE" class="data static no-clear" hidden>
```    
* `MODEL_NAME` - name of the model from where data will be fetched (data can be handled inside `app\Htpp\Services\Front\'MODEL_NAME'Service.php`)
* `CONTENT_TYPE` - name of method inside `app\Htpp\Services\Front\'MODEL_NAME'Service.php` that will be called to get data

After you add inputs to your source model widget modal remember to save it on admin panel so that those information could be saved into database.

### Further necessary manual changes:

####replace each occurrence of `$menu` with appropriate DataProcessor method
* getHeaderMenu() - for header menu
* getFooterMenu() - for footer menu

Each menu view is defined in `resources\views\front\layouts\partials\menu.blade.php`

#### front\\index.blade.php
```
@foreach($pageLanguage->activeSections as $pageSection)
    {!! $pageSection->getFront($pageLanguage) !!}
@endforeach
```
#####To:
```
@foreach($dataProcessor->getSections() as $section)
    {!! $section->getView() !!}
@endforeach
```
---
#### front\\layouts\\partials\\footer\\content.blade.php
```
{!! FrontMenuHelper::build($pageLanguage,'footer') !!}
```
#####To:
```
{!! $dataProcessor->getFooterMenu() !!}
```
  
---
#### front\\templates\\section.blade.php
```
@if(!$section->getTag())
    @include('front.templates.sections.'.$section->getBladeName(),['section'=>$section])
@else
    <section
        id="{{$section->getSlug()}}"{!! ($section->getClasses()) ? ' class="'.$section->getClasses().'"' :''!!}{!! ($section->getStyles() ? ' style="'.$section->getStyles().'"' :'') !!}>
            {!! $section->getSectionContent() !!}
    </section>
@endif
```
#### front\\templates\\widget.blade.php
```
@if(!empty($frontWidget->get('all')))
    @if($frontWidget->getTag())
        <{{$frontWidget->getTag()}}{!! ($frontWidget->getClasses()) ? ' class="'.$frontWidget->getClasses().'"' :''!!}{!! ($frontWidget->getStyles()) ? ' style="'.$frontWidget->getStyles().'"' :''!!}>
        @include('front.templates.widgets.'.$frontWidget->getBladeName(),['data'=>$frontWidget])
        </{{$frontWidget->getTag()}}>
    @else
        @include('front.templates.widgets.'.$frontWidget->getBladeName(),['data'=>$frontWidget])
    @endif
@else
    <div>Save widget in Administration Panel in order to initiate data.</div>
@endif
```
     
---
#### Every occurrence
* `{!! $section->getFrontColumn(0, $pageLanguage) !!}` to `{!! $section->getColumnView(0) !!}`
* `{{$section->classes}}` to `{{$section->getClasses()}}`
*
```
@foreach($section->getColumns() as $column_order => $column)
    <div class="col-{{$column}}">
        @foreach($section->getColumnWidgets($column_order) as $widget )
            {!! $widget->getFrontContent($pageLanguage, $section) !!}
        @endforeach
    </div>
@endforeach
```
#####To:
```
@foreach($section->getColumns() as $column)
    <div class="col-{{$column->getSize()}}">
        {!! $column->getView() !!}
    </div>
@endforeach
```
* 
```
@foreach($section->getColumns() as $column_order => $column)
    @foreach($section->getColumnWidgets($column_order) as $widget )
        {!! $widget->getFrontContent($pageLanguage, $section) !!}
    @endforeach
@endforeach
```
#####To:
```
@foreach($section->getColumns() as $column)
    {!! $column->getView() !!}
@endforeach
```

## URL's

### Edit files:

####Multi language SourceModels:
* Add `CONTENT_MODEL = 'SOURCE_MODEL_CONTENT_CLASS_NAME'` constant in main model class
* Add `PARENT_MODEL = 'SOURCE_MODEL_CLASS_NAME''` constant in model content class
* Use `Xsoft\Cms\Traits\UrTrait` in model content class
* Rename content class relation to main model class to: `parent()`
* In model edit view contents fields add: 
```               
@if($content->slug)     
      @include('cms::admin.partials.url',[
            'label' => __('app.MODEL_NAME.fields.url'),
            'item' => $content,
            'name' => 'contents['.$language.'][url]'
        ])
 @endif
 ```

####SourceModels without language versions:
* Remove `CONTENT_MODEL` constant from model class
* Use `Xsoft\Cms\Traits\UrTrait` in model class
* In model edit view add: 
 ```                    
@include('cms::admin.partials.url',[
    'label' => __('app.MODEL_NAME.fields.url'),
    'item' => MODEL_OBJECT,
    'name' => 'url'
])
```
### Run command

`php artisan cms:prepareUrls`

## Menu Helper

Now in your application *MenuHelper.php* you only pass your SourceModel Menu Elements

```
    public static function build()
    {
        return Base::prepare([
            SOURCE_MODELS_MENU_ITEMS
            //NewModelInsertionPlace
        ]);
    }
```

## AdminFonts

To make AdminFonts work properly, change all your usafe of fa* class to af*, for example:

```
.fa => .af

<div class="fa fa-edit"> => <div class="af af-edit">

```

## Languages

Add

```
@foreach($dataProcessor->getPageLanguage()->siblings as $sibling)
    <link rel="alternate" hreflang="{{$sibling->language}}" href="{{$sibling->url}}" />
@endforeach
```
To your */resources/views/front/layouts/app.blade.php* file in top of `<head>` section


#2.1.2 => ?

## MultiDomains

Add
In your *config/cms.php* file
```
    'ADMIN_PREFIX' => env('ADMIN_PREFIX', 'admin.'),
```

