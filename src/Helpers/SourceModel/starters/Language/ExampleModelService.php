<?php

namespace App\Services;

use App\ExampleModel;
use App\ExampleModelContent;
use Illuminate\Support\Str;
use Xsoft\DataTables\DataTable;

class ExampleModelService
{
    public function getTable()
    {
        $table = new DataTable('example_models', 'admin.exampleModels.indexAjax');
        $table->setOptions(['searching' => false, 'lengthChange' => false, 'info' => false, 'order' => '0,asc']);
        $table->setColumns([
            ['ID', 'id', ['class' => 'width5']],
            [__('app.exampleModels.fields.name'), 'name'],
            [__('app.exampleModels.fields.active'), 'active'],
        ]);
        if (auth()->user()->hasPermission('exampleModels.edit')) {
            $table->addColumn(["", 'id', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width1']]);
        }
        return $table;
    }

    public function prepareOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            if (auth()->user()->hasPermission('exampleModels.edit')) {
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                    $row->active ? __('app.exampleModels.fields.active') : '<span class="text-danger">' . __('app.exampleModels.fields.inactive') . '</span>',
                    "<a href='" . route('admin.exampleModels.edit', [$row->id]) . "' class='btn btn-outline-light'>" . __('app.edit') . "</a>"
                ];
            } else {
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                    $row->active ? __('app.exampleModels.fields.active') : '<span class="text-danger">' . __('app.exampleModels.fields.inactive') . '</span>',
                ];
            }
        }
    }

    public function create($request)
    {
        $data = $request->all();
        $data['active'] = $request->get('active') ? 1 : 0;
        $camelName = ExampleModel::create($data);
        foreach ($request->get('contents') as $language => $contents) {
            if ($contents['title'] == null) {
                alert(__('app.exampleModels.alerts.titleEmpty') . ' [' . $language . ']', 'warning');
                continue;
            }
            if ($contents['content'] == null) {
                alert(__('app.exampleModels.alerts.contentEmpty') . ' [' . $language . ']', 'warning');
                continue;
            }
            $slug = $this->getSlug($contents['title'], $language);

            $data = [
                'language' => $language,
                'title' => $contents['title'],
                'slug' => $slug,
                'content' => $contents['content'],
                'FLOOR_NAME_id' => $camelName->id,
                //CONTENT_FIELDS
            ];

            ExampleModelContent::create($data);
        }
        return $camelName;
    }

    public function update($request, $camelName)
    {
        $update_data = $request->all();
        $update_data['active'] = $request->get('active') ? 1 : 0;
        $camelName->update($update_data);

        foreach ($request->get('contents') as $language => $content) {
            $contentCamelName = $camelName->getContent($language);
            if ($content['title'] == null) {
                alert(__('app.exampleModels.alerts.titleEmpty') . ' [' . $language . ']', 'warning');
                continue;
            }
            if ($content['content'] == null) {
                alert(__('app.exampleModels.alerts.contentEmpty') . ' [' . $language . ']', 'warning');
                continue;
            }
            $content['slug'] = $this->getSlug($content['title'], $language, $contentCamelName);

            if (!$contentCamelName->slug) {
                $content['FLOOR_NAME_id'] = $camelName->id;
                $content['language'] = $language;
                $contentCamelName->create($content);
            } else {
                $contentCamelName->update($content);
            }
        }
        return $camelName;
    }

    private function getSlug($text, $language, $contentCamelName = null)
    {
        if (!$contentCamelName) {
            $contentCamelName = new ExampleModelContent();
        }
        $slug = Str::slug(Str::limit($text, 191));
        $duplicated_slug = ExampleModelContent::where('language', $language)
            ->where('slug', $slug)
            ->where('id', '!=', $contentCamelName->id)
            ->first();
        if ($duplicated_slug) {
            alert(__('app.exampleModels.alerts.slugDuplicate') . ' [' . $language . ']', 'warning');
            if (!$contentCamelName->slug) {
                $slug = Str::slug(now());
            } else {
                $slug = $contentCamelName->slug;
            }
        }
        return $slug;
    }

}
