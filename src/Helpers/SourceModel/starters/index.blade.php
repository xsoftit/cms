@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.exampleModels.title.index')}}
@endsection

@section('title')
    {{__('app.exampleModels.title.index')}}
@endsection
@section('subtitle')
    {{__('app.exampleModels.subtitle.index')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.exampleModels.breadcrumbs.index')]) !!}
@endsection

@section('page-content')
    <div class="row">
        <div class="col-12">
            <span class="btn btn-outline-info" data-toggle="modal"
                  data-target="#set-prefix">{{__('app.setPrefix')}}</span>
            <a href="{{route('admin.exampleModels.create')}}"
               class="btn btn-outline-success float-right">{{__('app.add')}}</a>
        </div>
    </div>
    @dataTable($table)
@endsection

@section('modals')
    @include('cms::admin.partials.modals.setPrefix',['className' => 'exampleModelClassNameContent'])
@endsection
