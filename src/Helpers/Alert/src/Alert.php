<?php

/*
 * This file is part of Laravel Alert.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Xsoft\Cms\Helpers\Alert;

use Illuminate\Session\Store;

/**
 * This is the alert class.
 *
 * @author Vincent Klaiber <hello@vinkla.com>
 */
class Alert extends \Vinkla\Alert\Alert
{
    /**
     * The session storage instance.
     *
     * @var \Illuminate\Session\Store
     */
    protected $session;

    /**
     * Create a new alert instance.
     *
     * @param \Illuminate\Session\Store $session
     *
     * @return void
     */
    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Flash an alert.
     *
     * @param string $message
     * @param string $style
     *
     * @return \Vinkla\Alert\Alert
     */
    public function flash(string $message, string $style = 'info'): \Vinkla\Alert\Alert
    {
        $messages = [];
        if(session()->has('alert.messages')){
            $messages = session()->get('alert.messages');
        }
        $messages[] = ['message' => $message, 'style' => $style];
        $this->session->flash('alert.messages', $messages);

        return $this;
    }

}
