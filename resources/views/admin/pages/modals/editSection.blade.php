@section('modals')
    @parent
    <div class="modal fade" id="edit-section-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.sections.title.edit')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link text-uppercase no-save" data-toggle="tab"
                               href="#section-content">{{__('app.sections.content')}}</a>
                        </li>
                        @if(auth()->user()->hasPermission('sections.create'))
                            <li>
                                <a class="nav-link text-uppercase no-save" data-toggle="tab"
                                   href="#section-layout">{{__('app.sections.layout')}}</a>
                            </li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div id="section-content" class="tab-pane fade">
                            <input hidden name="section_id" id="section-id">
                            <input hidden name="page_id" id="page-id">
                            <div class="form-group">
                                <label>{{__('app.sections.form.name')}}</label>
                                <input name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <div class="animated-checkbox">
                                    <label>
                                        <input type="checkbox" name="active" id="active">
                                        <span
                                            class="label-text text-capitalize">{{__('app.sections.form.active')}}</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @if(auth()->user()->hasPermission('sections.create'))
                            <div id="section-layout" class="tab-pane fade">
                                <div class="form-group">
                                    <label class="label-text">{{__('app.sections.form.classes')}}</label>
                                    <input id="classes" name="classes" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="label-text">{{__('app.sections.form.styles')}}</label>
                                    <textarea id="styles" name="styles" class="form-control"></textarea>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    @if(auth()->user()->hasPermission('sections.delete'))
                        <button type="button" id="deleteSection"
                                class="btn btn-outline-danger margin-right-auto">{{__('app.deleteButton.title')}}</button>
                    @endif
                    <button type="submit" id="update-section-button" class="btn btn-outline-success"
                            style="margin: 0">{{__('app.update')}}</button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">{{__('app.close')}}</button>
                </div>
                @if(auth()->user()->hasPermission('sections.delete'))
                    <div class="delete-content">
                        <h3>{{__('app.sections.deleteQuestion')}}<br>
                            <small>({{__('app.sections.deleteWarning')}})</small>
                        </h3>
                        <div class="row">
                            <div class="col-12">
                                <button type="button" id="deleteSectionConfirm"
                                        class="btn btn-outline-danger">{{__('app.deleteButton.confirm')}}</button>
                                <button type="button" id="deleteSectionCancel"
                                        class="btn btn-outline-light">{{__('app.deleteButton.cancel')}}</button>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
