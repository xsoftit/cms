<div class="validation-errors">
    @foreach($errors as $error)
        <p>{{ $error }}</p>
    @endforeach
</div>
