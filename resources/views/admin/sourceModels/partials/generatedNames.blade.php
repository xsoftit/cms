<div class="row">
    @foreach($names as $type => $value)
        <div class="col-lg-4 col-sm-12">
            <div class="form-group">
                <label>{{__('app.sourceModels.names.'.$type)}}</label>
                <input class="form-control"type="text" value="{{$value}}" disabled>
            </div>
        </div>
    @endforeach
</div>
