<div class="modal fade" id="user-create" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('app.users.title.create')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.users.store')}}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="name">{{__('app.users.fields.name')}}*</label>
                                <input class="form-control" name="name" required value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label for="role">{{__('app.users.fields.role')}}*</label>
                                <select name="role" required>
                                    @foreach($roles as $role)
                                        <option value="{{$role->name}}"
                                                @if(old('role') == $role->name) selected @endif>{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">{{__('app.users.fields.email')}}*</label>
                                <input class="form-control" name="email" required value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <label for="password">{{__('app.users.fields.password')}}*</label>
                                <input class="form-control" name="password" type="password" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success">
                        {{__('app.save')}}
                    </button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                        {{__('app.close')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
