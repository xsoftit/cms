<?php

namespace Xsoft\Cms\Observers;

use Xsoft\Cms\Models\GalleryItem;

class GalleryItemObserver
{

    public function deleting(GalleryItem $galleryItem)
    {
        foreach ($galleryItem->contents as $content){
            $content->delete();
        }
    }
}
