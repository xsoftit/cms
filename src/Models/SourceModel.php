<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class SourceModel extends Model
{
    protected $fillable = [
        'model_name',
        'display_name',
        'in_use',
        'fields',
        'singleObject'
    ];

    public function getPageLanguagesAttribute()
    {
        return PageLanguage::where('source_model_id', $this->id)->get();
    }
}
