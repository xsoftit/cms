<?php

return [
    'menu' => 'Roles',
    'title' => [
        'index' => 'Roles',
        'edit' => 'Edit :name role',
        'create' => 'Create new role'
    ],
    'subtitle' => [
        'index' => 'System roles configuration',
        'edit' => ':name role configuration',
    ],
    'fields' => [
        'name' => 'Name'
    ],
    'alerts' => [
        'saved' => 'Role :name saved successfully!',
        'roleHasUsers' => 'You cannot delete role assigned to users!'
    ]
];
