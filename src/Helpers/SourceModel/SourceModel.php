<?php

namespace Xsoft\Cms\Helpers\SourceModel;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class SourceModel
{
    protected $class_name;
    protected $class_plural_name;
    protected $floor_name;
    protected $nice_name;
    protected $table_name;
    protected $camel_name;
    protected $camel_plural_name;
    protected $content_class_name;
    protected $content_class_plural_name;
    protected $content_floor_name;
    protected $content_nice_name;
    protected $content_table_name;
    protected $content_camel_name;
    protected $content_camel_plural_name;
    protected $fields;
    protected $contentFields;
    protected $isInSourceTable;
    protected $isSingleObject;
    protected $app_path;
    protected $resource_path;
    protected $migrations_path;
    protected $model_starters_path;
    protected $hasMultipleLanguageContents;
    protected $migration_name;
    protected $content_migration_name;

    public function __construct($class_name, $isInSourceTable, $isSingleObject, $hasMultipleLanguageContents)
    {
        $this->class_name = $class_name;
        $this->class_plural_name = Str::plural($class_name);
        $this->floor_name = strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/', "_$1", $this->class_name));
        $this->nice_name = preg_replace('/(?<=\\w)(?=[A-Z])/', " ", $this->class_name);
        $this->table_name = Str::plural($this->floor_name);
        $this->camel_name = Str::camel($this->class_name);
        $this->camel_plural_name = Str::plural($this->camel_name);
        $this->content_class_name = $class_name . 'Content';
        $this->content_class_plural_name = Str::plural($this->content_class_name);
        $this->content_floor_name = $this->floor_name . '_content';
        $this->content_nice_name = $this->nice_name . ' Content';
        $this->content_table_name = Str::plural($this->content_floor_name);
        $this->content_camel_name = Str::camel($this->content_class_name);
        $this->content_camel_plural_name = Str::plural($this->content_camel_name);
        $this->isInSourceTable = $isInSourceTable;
        $this->isSingleObject = $isSingleObject;
        $this->hasMultipleLanguageContents = $hasMultipleLanguageContents;
        $this->fields = new Collection();
        $this->contentFields = new Collection();
        $this->model_starters_path = __DIR__ . '\\starters\\';
        $this->app_path = app_path() . '\\';
        $this->resource_path = resource_path() . '\\';
        $this->migrations_path = database_path() . '\\migrations\\';
    }

    // GETTERS
    public function getClassName()
    {
        return $this->class_name;
    }

    public function getClassPluralName()
    {
        return $this->class_plural_name;
    }

    public function getFloorName()
    {
        return $this->floor_name;
    }

    public function getNiceName()
    {
        return $this->nice_name;
    }

    public function getTableName()
    {
        return $this->table_name;
    }

    public function getCamelName()
    {
        return $this->camel_name;
    }

    public function getCamelPluralName()
    {
        return $this->camel_plural_name;
    }

    public function getContentClassName()
    {
        return $this->content_class_name;
    }

    public function getContentClassPluralName()
    {
        return $this->content_class_plural_name;
    }

    public function getContentFloorName()
    {
        return $this->content_floor_name;
    }

    public function getContentTableName()
    {
        return $this->content_table_name;
    }

    public function getContentCamelName()
    {
        return $this->content_camel_name;
    }

    public function getContentCamelPluralName()
    {
        return $this->content_camel_plural_name;
    }

    public function getContentNiceName()
    {
        return $this->content_nice_name;
    }

    public function getAllNames($content = false)
    {
        $output = [
            'class' => $this->getClassName(),
            'class_plural' => $this->getClassPluralName(),
            'nice' => $this->getNiceName(),
            'table' => $this->getTableName(),
            'floor' => $this->getFloorName(),
            'camel' => $this->getCamelName(),
            'camel_plural' => $this->getCamelPluralName()
        ];
        if ($content) {
            $output['contentNames'] = [
                'class' => $this->getContentClassName(),
                'class_plural' => $this->getContentClassPluralName(),
                'nice' => $this->getContentNiceName(),
                'table' => $this->getContentTableName(),
                'floor' => $this->getContentFloorName(),
                'camel' => $this->getContentCamelName(),
                'camel_plural' => $this->getContentCamelPluralName()

            ];
        }
        return $output;
    }

    public function getModelStartersPath()
    {
        return $this->model_starters_path;
    }

    public function getMigrationsPath()
    {
        return $this->migrations_path;
    }

    public function getAppPath()
    {
        return $this->app_path;
    }

    public function getResourcePath()
    {
        return $this->resource_path;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getContentFields()
    {
        return $this->contentFields;
    }

    public function getMigrationName()
    {
        return $this->migration_name;
    }

    public function getContentMigrationName()
    {
        return $this->content_migration_name;
    }

    //PUBLIC FUNCTIONS
    public function hasLanguageContent()
    {
        return $this->hasMultipleLanguageContents;
    }

    public function addField(Field $field)
    {
        $this->fields->push($field);
    }

    public function addContentField(Field $field)
    {
        $this->contentFields->push($field);
    }

    public function handle()
    {
        if (!$this->checkClassName()) {
            echo 'Source model with given class name already exists. Aborting.' . PHP_EOL;
            return false;
        }

        if ($this->hasMultipleLanguageContents) {
            $model_starters = $this->getModelStartersPath() . 'Language\\';
        } else {
            $model_starters = $this->getModelStartersPath() . 'NoLanguage\\';
        }
        //MIGRATION
        $timestamp = now()->format('Y_m_d_His');
        $migration_name = $timestamp . '_create_' . $this->getTableName() . '_table.php';
        $this->migration_name = $migration_name;
        if (File::exists($this->getModelStartersPath() . 'create_example_model_table.php')) {
            $file = File::get($this->getModelStartersPath() . 'create_example_model_table.php');
            $file = str_replace('ExampleModels', $this->getClassPluralName(), $file);
            $file = str_replace('example_models', $this->getTableName(), $file);
            $fields = $this->generateFieldsForMigration();
            $file = str_replace('//FIELDS', $fields, $file);
            File::put($this->getMigrationsPath() . $migration_name, $file);
            echo 'Created Migration: ' . $migration_name . PHP_EOL;
        }
        if ($this->isInSourceTable) {
            self::handleSourceModel();
        }
        //MODEL
        if (File::exists($model_starters . 'ExampleModel.php')) {
            $file = File::get($model_starters . 'ExampleModel.php');
            $file = str_replace('ExampleModel', $this->getClassName(), $file);
            $file = str_replace('slugModel', $this->getFloorName(), $file);
            $fillable = $this->generateFillableFields();
            $file = str_replace('//FILLABLE', $fillable, $file);
            File::put($this->getAppPath() . $this->getClassName() . '.php', $file);
            echo 'Created Model: ' . $this->getClassName() . PHP_EOL;
        }
        //SERVICE
        $services_path = $this->getAppPath() . '\\Http\\Services\\';
        if (File::exists($model_starters . 'ExampleModelService.php')) {
            $file = File::get($model_starters . 'ExampleModelService.php');
            $file = str_replace('ExampleModel', $this->getClassName(), $file);
            $file = str_replace('exampleModels', $this->getCamelPluralName(), $file);
            $file = str_replace('exampleModel', $this->getCamelName(), $file);
            $file = str_replace('example_models', $this->getTableName(), $file);
            $file = str_replace('FLOOR_NAME', $this->getFloorName(), $file);
            $file = str_replace('contentCamelName', $this->getContentCamelName(), $file);
            $file = str_replace('camelName', $this->getCamelName(), $file);
            $fields = $this->generateContentFieldsForCRUD();
            $file = str_replace('//CONTENT_FIELDS', $fields, $file);
            $service_name = $this->getClassName() . 'Service';
            File::put($services_path . $service_name . '.php', $file);
            echo 'Created Service: ' . $service_name . PHP_EOL;
        }
        //SERVICE-FRONT
        $services_path = $this->getAppPath() . '\\Http\\Services\\Front\\';
        if (File::exists($model_starters . 'ExampleModelFrontService.php')) {
            $file = File::get($model_starters . 'ExampleModelFrontService.php');
            $file = str_replace('ExampleModel', $this->getClassName(), $file);
            $file = str_replace('exampleModel', $this->getCamelName(), $file);
            $file = str_replace('example_models', $this->getTableName(), $file);
            $service_name = $this->getClassName() . 'Service';
            File::put($services_path . $service_name . '.php', $file);
            echo 'Created Service: ' . $service_name . PHP_EOL;
        }
        //VIEWS
        $views_path = $this->getResourcePath() . 'views\\admin\\' . $this->getCamelName();
        if (!File::exists($views_path)) {
            File::makeDirectory($views_path);
        }
        if (File::exists($this->getModelStartersPath() . 'index.blade.php')) {
            $file = File::get($this->getModelStartersPath() . 'index.blade.php');
            $file = str_replace('exampleModels', $this->getCamelPluralName(), $file);
            $file = str_replace('exampleModelClassName', $this->getClassName(), $file);
            File::put($views_path . '\\index.blade.php', $file);
            echo 'Created view: index' . PHP_EOL;
        }

        if ($this->hasMultipleLanguageContents) {
            $this->handleWithLanguage($model_starters, $views_path, $timestamp);
        } else {
            $this->handleWithoutLanguage($model_starters, $views_path);
        }

        echo PHP_EOL;
        self::finish();
        return true;
    }

    //PRIVATE FUNCTIONS

    private function checkClassName()
    {
        $correct = true;
        if (Schema::hasTable('source_models')) {
            $source_models = \Xsoft\Cms\Models\SourceModel::where('model_name', $this->getClassName())->first();
            if ($source_models) {
                $correct = false;
            }
        }
        return $correct;
    }

    private function generateFieldsForMigration()
    {
        $output = "";
        foreach ($this->fields as $field) {
            $output .= view('cms::admin.sourceModels.createPartials.migrationField', ['field' => $field])->render();
        }
        return $output;
    }

    private function generateContentFieldsForMigration()
    {
        $output = "";
        foreach ($this->contentFields as $field) {
            $output .= view('cms::admin.sourceModels.createPartials.migrationField', ['field' => $field])->render();
        }
        return $output;
    }

    private function generateFillableFields()
    {
        $output = "";
        foreach ($this->fields as $field) {
            $output .= view('cms::admin.sourceModels.createPartials.fillableFields', ['field' => $field])->render();
        }
        return $output;
    }

    private function generateFillableContentFields()
    {
        $output = "";
        foreach ($this->contentFields as $field) {
            $output .= view('cms::admin.sourceModels.createPartials.fillableFields', ['field' => $field])->render();
        }
        return $output;
    }

    private function generateContentFieldsForCRUD()
    {
        $output = "";
        $static_fields = [
            'title',
            'content',
            'language',
            'slug'
        ];
        foreach ($this->contentFields as $field) {
            if (!in_array($field->getName(), $static_fields)) {
                $output .= view('cms::admin.sourceModels.createPartials.crudFields', ['field' => $field])->render();
            }
        }
        return $output;
    }

    private function generateInputs($field_type, $input_type)
    {
        $inputs = [
            'string' => 'string',
            'text' => 'textarea',
            'longText' => 'textarea',
            'integer' => 'number',
            'double' => 'number',
            'float' => 'number',
            'boolean' => 'checkbox',
            'timestamp' => 'date',
            'date' => 'date',
            'time' => 'date',
            'year' => 'date',
            'binary' => 'textarea',
        ];
        $output = '';
        foreach ($this->$field_type as $field) {
            if ($field->getName() != 'language' && $field->getName() != 'slug') {
                $output .= view('cms::admin.sourceModels.createPartials.inputs.' . $input_type . '.' . $field_type . '.' . $inputs[$field->getType()], ['field' => $field]);
            }
        }
        $output = str_replace('{\\{', '{{', $output);
        return $output;
    }

    private function handleWithLanguage($model_starters, $views_path, $timestamp)
    {
        //MIGRATION
        $migration_name = $timestamp . '_create_' . $this->getContentTableName() . '_table.php';
        $this->content_migration_name = $migration_name;
        if (File::exists($model_starters . 'create_example_model_contents_table.php')) {
            $file = File::get($model_starters . 'create_example_model_contents_table.php');
            $file = str_replace('ExampleModelContents', $this->getContentClassPluralName(), $file);
            $file = str_replace('example_model_contents', $this->getContentTableName(), $file);
            $file = str_replace('example_model_id', $this->getFloorName() . '_id', $file);
            $fields = $this->generateContentFieldsForMigration();
            $file = str_replace('//FIELDS', $fields, $file);
            File::put($this->getMigrationsPath() . $migration_name, $file);
            echo 'Created Migration: ' . $migration_name . PHP_EOL;
        }
        //MODEL
        if (File::exists($model_starters . 'ExampleModelContent.php')) {
            $file = File::get($model_starters . 'ExampleModelContent.php');
            $file = str_replace('ExampleModel', $this->getClassName(), $file);
            $file = str_replace('camelName', $this->getCamelName(), $file);
            $file = str_replace('FLOOR_NAME', $this->getFloorName(), $file);
            $fillable = $this->generateFillableContentFields();
            $file = str_replace('//FILLABLE', $fillable, $file);
            File::put($this->getAppPath() . $this->getContentClassName() . '.php', $file);
            echo 'Created Model: ' . $this->getContentClassName() . PHP_EOL;
        }
        //VIEWS
        if (File::exists($model_starters . 'views\\create.blade.php')) {
            $file = File::get($model_starters . 'views\\create.blade.php');
            $contentInputs = $this->generateInputs('contentFields', 'create');
            $file = str_replace('CONTENT_FIELD_INPUTS', $contentInputs, $file);
            $inputs = $this->generateInputs('fields', 'create');
            $file = str_replace('FIELD_INPUTS', $inputs, $file);
            $file = str_replace('exampleModels', $this->getCamelPluralName(), $file);
            $file = str_replace('exampleModel', $this->getCamelName(), $file);
            File::put($views_path . '\\create.blade.php', $file);
            echo 'Created view: create' . PHP_EOL;
        }
        if (File::exists($model_starters . 'views\\edit.blade.php')) {
            $file = File::get($model_starters . 'views\\edit.blade.php');
            $inputs = $this->generateInputs('contentFields', 'edit');
            $file = str_replace('CONTENT_FIELD_INPUTS', $inputs, $file);
            $inputs = $this->generateInputs('fields', 'edit');
            $file = str_replace('FIELD_INPUTS', $inputs, $file);
            $file = str_replace('exampleModels', $this->getCamelPluralName(), $file);
            $file = str_replace('exampleModel', $this->getCamelName(), $file);
            File::put($views_path . '\\edit.blade.php', $file);
            echo 'Created view: edit' . PHP_EOL;
        }
    }

    private function handleWithoutLanguage($model_starters, $views_path)
    {
        //VIEWS
        if (File::exists($model_starters . 'views\\create.blade.php')) {
            $file = File::get($model_starters . 'views\\create.blade.php');
            $inputs = $this->generateInputs('fields', 'create');
            $file = str_replace('FIELD_INPUTS', $inputs, $file);
            $file = str_replace('exampleModels', $this->getCamelPluralName(), $file);
            $file = str_replace('exampleModel', $this->getCamelName(), $file);
            File::put($views_path . '\\create.blade.php', $file);
            echo 'Created view: create' . PHP_EOL;
        }
        if (File::exists($model_starters . 'views\\edit.blade.php')) {
            $file = File::get($model_starters . 'views\\edit.blade.php');
            $inputs = $this->generateInputs('fields', 'edit');
            $file = str_replace('FIELD_INPUTS', $inputs, $file);
            $file = str_replace('exampleModels', $this->getCamelPluralName(), $file);
            $file = str_replace('exampleModel', $this->getCamelName(), $file);
            File::put($views_path . '\\edit.blade.php', $file);
            echo 'Created view: edit' . PHP_EOL;
        }
    }

    public function handleSourceModel()
    {
        $widgets = [
            'List',
            'Content',
        ];
        //ADD_MODEL_CONTENT_WIDGET_TEMPLATE
        foreach ($widgets as $widget) {

            $name = $this->getNiceName() . ' ' . $widget;
            $widget_template_slug = Str::slug($name);
            $blade_name = Str::camel($widget_template_slug);
//            SeederHelper::createWidgetTemplate($name, $widget_template_slug, $blade_name);

            $seeder = file_get_contents(database_path('seeds/') . 'ElementsSeeder.php');
            $seeder = str_replace('//SEED_HERE', 'if (!WidgetTemplate::where(\'slug\', \'' . $widget_template_slug . '\')->first()) {
            $widget = WidgetTemplate::create([
                \'name\' => \'' . $name . '\',
                \'slug\' => \'' . $widget_template_slug . '\',
                \'blade_name\' => \'' . $blade_name . '\',
                \'generate\' => 0,
            ]);
            echo \'Seeded WidgetTemplate \' . $widget->name . PHP_EOL;
        }
        //SEED_HERE', $seeder);
            file_put_contents(database_path('seeds/') . 'ElementsSeeder.php', $seeder);

            if (!File::exists(resource_path('views/admin/templates/widgets/' . $blade_name . '.blade.php'))) {
                copy(__DIR__ . '/../../../resources/views/admin/starters/widget.blade.php', resource_path('views/admin/templates/widgets/' . $blade_name . '.blade.php'));
            }
            if (!File::exists(resource_path('views/admin/templates/widgets/modals' . $blade_name . '.blade.php'))) {
                $widgetModal = file_get_contents(__DIR__ . '/../../../resources/views/admin/starters/widgetModal.blade.php');
                $input = "";
                if ($widget == 'Content') {
                    $input = "<input id=\"source_model\" name=\"source_model\" type=\"text\" value=\"" . $this->getClassName() . "\" class=\"data static no-clear\" hidden>
    <input id=\"source_content_type\" name=\"source_content_type\" type=\"text\" value=\"content\" class=\"data static no-clear\" hidden>";
                }
                if ($widget == 'List') {
                    $input = "<input id=\"source_model\" name=\"source_model\" type=\"text\" value=\"" . $this->getClassName() . "\" class=\"data static no-clear\" hidden>
    <input id=\"source_content_type\" name=\"source_content_type\" type=\"text\" value=\"list\" class=\"data static no-clear\" hidden>
    <div class=\"form-group\">
        <label class=\"label-text\">{{__('app.widgets.labels.articles.amount')}}</label>
        <input id=\"amount\" name=\"amount\" type=\"number\" min=\"0\" class=\"form-control data\">
    </div>";
                }
                $widgetModal = str_replace('{*{SOURCE-MODEL}*}', $input, $widgetModal);
                file_put_contents(resource_path('views/admin/templates/widgets/modals/' . $blade_name . '.blade.php'), $widgetModal);
            }
            if (!File::exists(resource_path('views/front/templates/widgets/' . $blade_name . '.blade.php'))) {
                copy(__DIR__ . '/../../../resources/views/front/starters/widget.blade.php', resource_path('views/front/templates/widgets/' . $blade_name . '.blade.php'));
            }
            echo 'Created WidgetTemplate :' . PHP_EOL . "\tname: " . $name . PHP_EOL . "\tslug: " . $widget_template_slug . PHP_EOL . "\tblade_name: " . $blade_name;

            echo $widget_template_slug . PHP_EOL;
        }

        //SOURCE_MODEL
        if (File::exists(database_path('seeds/') . 'ElementsSeeder.php')) {
            $seeder = File::get(database_path('seeds/') . 'ElementsSeeder.php');
            $seeder = str_replace('//SEED_HERE', 'if (!SourceModel::where(\'model_name\', \'' . $this->getClassName() . '\')->first()) {
                $widget = SourceModel::create([
                    \'model_name\' => \'' . $this->getClassName() . '\',
                    \'display_name\' => \'' . $this->getCamelName() . '\',
                    \'section_template_slug\' => "",
                    \'widget_template_slug\' => "' . (isset($widget_template_slug) ? $widget_template_slug : '') . '",
                    \'singleObject\' => "' . ($this->isSingleObject ? 1 : 0) . '",
                ]);
                echo \'Seeded SourceModel \' . $widget->name . PHP_EOL;
            }
            //SEED_HERE', $seeder);
            File::put(database_path('seeds/') . 'ElementsSeeder.php', $seeder);
            echo 'Added to ElementsSeeder' . PHP_EOL;
        }
        echo 'Added model to source_models table' . PHP_EOL;
    }

    private function finish()
    {
        //PERMISSIONS
        $permissions_seeder_path = database_path() . '\\seeds\\PermissionsSeeder.php';
        $permissions = "
        //" . $this->getClassName() . "
        '" . $this->getCamelPluralName() . ".show' => ['cms.login'],
        '" . $this->getCamelPluralName() . ".edit' => ['" . $this->getCamelPluralName() . ".show'],
        '" . $this->getCamelPluralName() . ".create' => ['" . $this->getCamelPluralName() . ".edit'],
        '" . $this->getCamelPluralName() . ".delete' => ['" . $this->getCamelPluralName() . ".create'],
        //SEED_HERE";
        if (File::exists($permissions_seeder_path)) {
            $seeder = File::get($permissions_seeder_path);
            $seeder = str_replace('//SEED_HERE', $permissions, $seeder);
            File::put($permissions_seeder_path, $seeder);
            echo 'Seeded permissions' . PHP_EOL;
        }

        //CONTROLLER
        $controllers_path = $this->getAppPath() . 'Http\\Controllers\\';
        if (File::exists($this->getModelStartersPath() . 'ExampleModelController.php')) {
            $file = File::get($this->getModelStartersPath() . 'ExampleModelController.php');
            $file = str_replace('ExampleModels', $this->getClassPluralName(), $file);
            $file = str_replace('ExampleModel', $this->getClassName(), $file);
            $file = str_replace('exampleModels', $this->getCamelPluralName(), $file);
            $file = str_replace('exampleModel', $this->getCamelName(), $file);
            $controller_name = $this->getClassName() . 'Controller';
            File::put($controllers_path . $controller_name . '.php', $file);
            echo 'Created Controller: ' . $controller_name . PHP_EOL;
        }

        //ROUTES
        if (File::exists($this->getModelStartersPath() . 'routes')) {
            $routes = File::get(app_path() . '\\..\\routes\\web.php');
            $file = File::get($this->getModelStartersPath() . 'routes');
            $file = str_replace('ExampleModels', $this->getClassPluralName(), $file);
            $file = str_replace('ExampleModel', $this->getClassName(), $file);
            $file = str_replace('exampleModels', $this->getCamelPluralName(), $file);
            $file = str_replace('exampleModel', $this->getCamelName(), $file);
            $routes .= $file;
            File::put(app_path() . '\\..\\routes\\web.php', $routes);
            echo 'Added routes' . PHP_EOL;
        }

        echo 'Added permissions to PermissionsSeeder' . PHP_EOL;

        //LOCALISATION
        $lang_path = $this->getResourcePath() . 'lang\\default\\';
        if (File::exists($this->getModelStartersPath() . 'lang.php')) {
            $file = File::get($this->getModelStartersPath() . 'lang.php');
            $file = str_replace('ExampleModels', Str::plural($this->getNiceName()), $file);
            $file = str_replace('ExampleModel', $this->getNiceName(), $file);
            File::put($lang_path . 'models\\' . $this->getCamelName() . '.php', $file);
            echo 'Created lang file: ' . $this->getCamelName() . PHP_EOL;
            $main_lang_file = File::get($lang_path . 'app.php');
            $main_lang_file = str_replace('];', "    '" . Str::plural($this->getCamelName()) . "' => include('models/" . $this->getCamelName() . ".php'),
];", $main_lang_file);
//    '" . $this->getCamelName() . "' => '" . $this->getNiceName() . "',
            File::put($lang_path . 'app.php', $main_lang_file);
        }

        //MENU
        $menu_helper_route = $this->getAppPath() . 'Helpers\\MenuHelper.php';
        if (File::exists($menu_helper_route)) {
            $file = File::get($menu_helper_route);
            $file = str_replace('//NewModelInsertionPlace', 'MenuItem::create(__("app.' . $this->getCamelPluralName() . '.menu"), route("admin.' . $this->getCamelPluralName() . '.index"), \'af-question\')->activeOn([
                "' . $this->getCamelName() . '/*",
            ]),
            //NewModelInsertionPlace', $file);
            File::put($menu_helper_route, $file);
            echo 'Added to Menu' . PHP_EOL;
        }

//        Artisan::call('db:seed --class=ElementsSeeder');
        echo PHP_EOL . 'Update files with necessary database fields and run commands:' . PHP_EOL . 'php artisan migrate --seed' . PHP_EOL;
    }
}
