<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Hermit\Logs\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Xsoft\Cms\Models\File;
use Xsoft\Cms\Services\Admin\FileService;

class FileController extends Controller
{
    protected $fileService;

    public function __construct()
    {
        $this->fileService = new FileService();
    }

    public function get(Request $request)
    {
        $backTrace = [];
        $filesArray = $this->fileService->getFiles($request, $backTrace);
        return response()->json(['data' => $filesArray, 'backTrace' => $backTrace]);
    }

    public function preview(Request $request)
    {
        $files = File::whereIn('id', json_decode($request->get('ids'), true))->get();
        $filesArray = $this->fileService->previewFiles($files);
        return response()->json($filesArray);
    }

    public function move(Request $request)
    {
        $file = $this->fileService->move($request);
        return response()->json($file);
    }

    public function createFolder(Request $request)
    {
        $file = $this->fileService->createDirectory($request);
        return response()->json($file);
    }

    public function createFiles(Request $request)
    {
        $files = $request->File('files');
        if (!$files) {
            return response()->json([]);
        }
        foreach ($files as $file) {
            $this->fileService->createFile($request, $file);
        }
        return response()->json($files);
    }

    public function uploadThumb(Request $request)
    {
        $thumb = $request->File('thumb');
        if (!$thumb) {
            return response()->json([]);
        }
        $extension = Str::lower($thumb->getClientOriginalExtension());
        $filename = uniqid() . '.' . $extension;
        $storage_path = Storage::disk(config('cms.FILES_DISK'))->putFileAs('files', $thumb, $filename);
        $parentFile = File::find($request->get('id'));
        $parentFile->thumb_path = $storage_path;
        $parentFile->save();

        return response()->json(['thumb' => $parentFile->thumb(true)]);
    }

    public function delete(Request $request)
    {
        $file = File::find($request->get('file_id'));
        $permission = $this->getPermission($file);
        if(auth()->user()->hasPermission($permission)){
            $file->delete();
        }
        return response()->json([]);
    }

    public function checkIfEmpty(Request $request)
    {
        $files = File::where('parent_id', $request->get('file_id'))->get();
        if ($files->count() > 0) {
            return response()->json(['empty' => '0']);
        } else {
            return response()->json(['empty' => '1']);
        }
    }

    public function rename(Request $request)
    {
        $file = File::find($request->get('file_id'));
        $permission = $this->getPermission($file);
        if(auth()->user()->hasPermission($permission)) {
            $file->name = $request->get('name');
            if ($request->get('alt')) {
                $file->alt = $request->get('alt');
            }
            $file->save();
        }
        return response()->json(['name' => $file->name, 'alt' => $file->alt]);
    }

    public function export()
    {
        $output = json_encode(File::all()->toArray());
        $file_name = now()->format('Y_m_d') . '_' . config('app.name') . '_files';
        if (!Storage::exists('public/exportFiles')) {
            mkdir(Storage::path('public/exportFiles'), 0775, true);
            chmod(Storage::path('public/exportFiles'), 0775);
        }
        file_put_contents(storage_path('app/public/exportFiles/' . $file_name . '.txt'), $output);
        sleep(2);
        return response()->json(['filename' => $file_name, 'success' => true]);
    }

    public function downloadExportFile($filename)
    {
        if (Storage::exists('public/exportFiles/' . $filename . '.txt')) {
            return Storage::download('public/exportFiles/' . $filename . '.txt');
        } else {
            return 'error';
        }
    }

    public function import(Request $request)
    {
        $file = $request->file('import_file');
        $data = file_get_contents($file->getRealPath());
        $data = json_decode($data, true);
        $errors = [];
        File::truncate();
        foreach ($data as $file) {
            try {
                File::create([
                    'id' => $file['id'],
                    'name' => $file['name'],
                    'storage_path' => $file['storage_path'],
                    'type' => $file['type'],
                    'parent_id' => $file['parent_id'],
                    'alt' => $file['alt'],
                ]);
            } catch (\Exception $exception) {
                $data[] = $exception->getMessage();
            }
        }
        if (count($errors) > 0) {
            Log::write('ERROR', json_encode($errors), 1, 'Files Import');
        }
        return redirect()->route('admin.pages.index')->withErrors($errors);
    }

    private function getPermission($file){
        if($file->type == 'directory'){
            return 'files.foldersManage';
        }else{
            return 'files.manage';
        }
    }
}
