@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.galleries.title.index')}}
@endsection

@section('title')
    {{__('app.galleries.title.index')}}
@endsection

@section('subtitle')
    {{__('app.galleries.subtitle.index')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make(
        [__('app.dashboard.title'),route('admin.dashboard')],
        [__('app.galleries.title.index')])
    !!}
@endsection

@section('page-content')
    <div class="row">
        <div class="col-12">
            <a href="{{route('admin.galleries.create')}}"
               class="btn btn-outline-success float-right">{{__('app.add')}}</a>
        </div>
    </div>
    @dataTable($table)
@endsection
