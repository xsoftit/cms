@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.frontLanguages.title.index')}}
@endsection

@section('title')
    {{__('app.frontLanguages.title.index')}}
@endsection

@section('subtitle')
    {{__('app.frontLanguages.subtitle.index')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.frontLanguages.title.index')]) !!}
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-12">
            @if(auth()->user()->hasPermission('front_languages.create'))
                <span class="btn btn-outline-success float-right" data-toggle="modal"
                      data-target="#language-create">{{__('app.add')}}</span>
            @endif
            @if(auth()->user()->hasPermission('front_languages.exportImport'))
                <span class="btn btn-outline-secondary float-right" data-toggle="modal"
                      data-target="#language-import">{{__('app.import')}}</span>
            @endif
        </div>
    </div>
    @dataTable($table)
@endsection

@section('modals')
    @if(auth()->user()->hasPermission('front_languages.create'))
        @include('cms::admin.frontLanguages.modals.create')
    @endif
    @if(auth()->user()->hasPermission('front_languages.exportImport'))
        @include('cms::admin.frontLanguages.modals.import')
    @endif
@endsection
