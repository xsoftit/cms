@if(auth()->user()->hasPermission('files.usage'))
    @include('cms::admin.filemanager.modal')
@endif
@include('cms::admin.partials.routeSelectModal')
@section('modals')
@show
