<?php

return [
    'app' => include('app.php'),
    'auth' => include('auth.php'),
    'passwords' => include('passwords.php'),
    'validation' => include('validation.php'),
    'filemanager' => include('filemanager.php')
];
