@section('modals')
    @parent
    <div class="modal fade" id="add-widget-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.createWidget')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add-section-form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>{{__('app.pages.form.widgetName')}}</label>
                            <input name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="page_template_id"
                                   class="label-text">{{__('app.pages.form.pageWidgetTemplateChoice')}}</label>
                                <select class="form-control data" name="widget_template_id"
                                        id="widget_template_select"
                                        required>
                                    <option value="" selected
                                            disabled>{{__('app.pages.form.pageWidgetTemplateChoice')}}</option>
                                    @foreach(\Xsoft\Cms\Models\WidgetTemplate::orderBy('name','asc')->get() as $widget_template)
                                        <option value="{{$widget_template->id}}"
                                                @if(old('widget_template_id') == $widget_template->id) selected @endif>{{$widget_template->name}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="add-widget-button" class="btn btn-outline-success"
                                style="margin: 0">{{__('app.create')}}</button>
                        <button type="reset" class="btn btn-outline-light"
                                data-dismiss="modal">{{__('app.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
