<?php

namespace Xsoft\Cms\Observers;

use Xsoft\Cms\Models\Gallery;

class GalleryObserver
{

    public function deleting(Gallery $gallery)
    {
        foreach ($gallery->items as $item){
            $item->delete();
        }
    }
}
