<?php

namespace Xsoft\Cms\Services\Admin;


use Hermit\Logs\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Xsoft\Cms\Models\Page;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\PageSection;
use Xsoft\Cms\Models\PageSectionTemplate;
use Xsoft\Cms\Models\SourceModel;
use Xsoft\Cms\Models\Url;
use Xsoft\Cms\Models\Widget;
use Xsoft\Cms\Models\WidgetContent;
use Xsoft\Cms\Models\WidgetTemplate;

class PageService
{
    public function pageLanguageCreate($request, $page_id, $language, $source_model_id = null, $copy_widgets = null)
    {
        $source_model = null;
        if (!$source_model_id) {
            $source_model_id = $request->get('source_model_id');
        }
        $source_model = SourceModel::find($request->get('source_model_id'));
        $model = null;
        if ($source_model) {
            $model = $source_model->model_name;
        }
        $pageLanguage = PageLanguage::create([
            'page_id' => $page_id,
            'language' => $language,
            'name' => $request->get('name'),
            'slug' => Str::slug($request->get('name')),
            'meta_title' => $request->get('meta_title'),
            'meta_description' => $request->get('meta_description'),
            'is_main' => 0,
            'active' => 0,
            'source_model_id' => $source_model_id,
            'source_model' => $model,
        ]);
        if ($source_model) {
            $source_model->update([
                'in_use' => 1
            ]);
        }
        if ($copy_widgets) {
            $this->copyWidgetsForPageLanguage($pageLanguage);
        }
        return $pageLanguage;
    }

    private function copyWidgetsForPageLanguage($pageLanguage)
    {
        foreach ($pageLanguage->sections as $section) {
            foreach ($section->widgets as $widget) {
                $content = $widget->contents->first();
                if ($content) {
                    $array = json_decode($content->content, true);
                    if (key_exists('tag', $array) && key_exists('classes', $array) && key_exists('styles', $array)) {
                        $new_array = [
                            'tag' => $array['tag'],
                            'classes' => $array['classes'],
                            'styles' => $array['styles']
                        ];
                        WidgetContent::create([
                            'widget_id' => $widget->id,
                            'language' => $pageLanguage->language,
                            'content' => json_encode($new_array)
                        ]);
                    }
                }
            }
        }
    }

    public function pageLanguageUpdate($pageLanguage, $request)
    {
        $pageLanguage->update([
            'name' => $request->get('name'),
            'slug' => Str::slug($request->get('name')),
            'meta_title' => $request->get('meta_title'),
            'meta_description' => $request->get('meta_description'),
            'is_main' => ($request->get('is_main') == 'true' && !$pageLanguage->source_model_id) ? 1 : 0
        ]);
        if ($request->get('options')) {
            $page = $pageLanguage->page;
            $page->update([
                'options' => json_encode($request->get('options'))
            ]);
        }
        $alert = $request->session()->get('alert');
        if($alert){
            if(key_exists('messages',$alert)){
                $request->session()->forget('alert');
                return $alert['messages'][0]['message'];
            }
        }
        return '';
    }

    public static function pageArrayForExport($page)
    {
        $output = [
            'name' => $page->name,
            'options' => $page->options,
        ];
        foreach ($page->pageLanguages as $pageLanguage) {
            $pageLanguageArray = $pageLanguage->toArray();
            unset($pageLanguageArray['id']);
            unset($pageLanguageArray['page_id']);
            unset($pageLanguageArray['active']);
            unset($pageLanguageArray['is_main']);
            unset($pageLanguageArray['created_at']);
            unset($pageLanguageArray['updated_at']);
            $pageLanguageArray['url'] = $pageLanguage->url;
            $output['pageLanguages'][] = $pageLanguageArray;
        }
        foreach ($page->sections as $section) {
            $sectionArray = $section->toArray();
            $sectionArray['template'] = $sectionArray['template']['slug'];
            unset($sectionArray['id']);
            unset($sectionArray['created_at']);
            unset($sectionArray['updated_at']);
            unset($sectionArray['pivot']['page_id']);
            unset($sectionArray['pivot']['page_section_id']);
            unset($sectionArray['section_template_id']);
            foreach ($sectionArray['widgets'] as $key => $widget) {
                $sectionArray['widgets'][$key]['template'] = $sectionArray['widgets'][$key]['template']['slug'];
                unset($sectionArray['widgets'][$key]['id']);
                unset($sectionArray['widgets'][$key]['widget_template_id']);
                unset($sectionArray['widgets'][$key]['created_at']);
                unset($sectionArray['widgets'][$key]['updated_at']);
                unset($sectionArray['widgets'][$key]['pivot']['page_section_id']);
                unset($sectionArray['widgets'][$key]['pivot']['widget_id']);
                foreach ($widget['contents'] as $k => $content) {
                    unset($sectionArray['widgets'][$key]['contents'][$k]['id']);
                    unset($sectionArray['widgets'][$key]['contents'][$k]['widget_id']);
                    unset($sectionArray['widgets'][$key]['contents'][$k]['created_at']);
                    unset($sectionArray['widgets'][$key]['contents'][$k]['updated_at']);
                }
            }
            $output['sections'][] = $sectionArray;
        }
        return $output;
    }

    public static function createPageFromData($data, &$errors)
    {
        try {
            DB::beginTransaction();
            if (!key_exists('pageLanguages', $data)) {
                return 0;
            }
            $page = Page::create([
                'name' => $data['name'],
                'options' => $data['options']
            ]);
            foreach ($data['pageLanguages'] as $pageLanguage) {
                $pageLanguage['page_id'] = $page->id;
                $duplicate_slug = PageLanguage::where('slug', $pageLanguage['slug'])->where('language', $pageLanguage['language'])->where('source_model_id', $pageLanguage['source_model_id'])->first();
                if ($duplicate_slug) {
                    $pageLanguage['name'] = $pageLanguage['name'] . ' ' . now()->format('H:i:s');
                    $pageLanguage['slug'] = Str::slug($pageLanguage['name']);
                }
                $newPageLanguage = PageLanguage::create($pageLanguage);
                if(key_exists('url',$pageLanguage)){
                    if($pageLanguage['url']){
                        $url = Url::where('model', 'PageLanguage')->where('model_id', $newPageLanguage->id)->where('language',$newPageLanguage->language)->first();
                        if($url){
                            $url->update([
                                'url' => $pageLanguage['url'],
                            ]);
                        }
                    }
                }
                $sourceModel = SourceModel::where('model_name',$pageLanguage['source_model'])->first();
                if($sourceModel){
                    $sourceModel->update([
                        'in_use' => 1
                    ]);
                }
            }
            if (!key_exists('sections', $data)) {
                return 0;
            }
            foreach ($data['sections'] as $dataSection) {
                $pageSectionTemplate = PageSectionTemplate::where('slug', $dataSection['template'])->first();
                if (!$pageSectionTemplate) {
                    throw new \Exception('Page Section Template not found');
                }
                unset($dataSection['template']);
                $dataSection['section_template_id'] = $pageSectionTemplate->id;
                $pageSection = PageSection::create($dataSection);
                DB::table('page_page_section')->insert([
                    'page_section_id' => $pageSection->id,
                    'page_id' => $page->id,
                    'order' => $dataSection['pivot']['order'],
                    'active' => $dataSection['pivot']['active']
                ]);
                if (!key_exists('widgets', $dataSection)) {
                    continue;
                }
                foreach ($dataSection['widgets'] as $dataWidget) {
                    $widgetTemplate = WidgetTemplate::where('slug', $dataWidget['template'])->first();
                    if (!$widgetTemplate) {
                        throw new \Exception('Page Section Template not found');
                    }
                    unset($dataWidget['template']);
                    $dataWidget['widget_template_id'] = $widgetTemplate->id;
                    $widget = Widget::create($dataWidget);
                    DB::table('page_section_widget')->insert([
                        'page_section_id' => $pageSection->id,
                        'widget_id' => $widget->id,
                        'placement_column' => $dataWidget['pivot']['placement_column'],
                        'placement_order' => $dataWidget['pivot']['placement_order'],
                    ]);
                    if (!key_exists('contents', $dataWidget)) {
                        continue;
                    }
                    foreach ($dataWidget['contents'] as $dataContents) {
                        $dataContents['widget_id'] = $widget->id;
                        WidgetContent::create($dataContents);
                    }
                }
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            $errors[] = $exception->getMessage();
        }
    }
}
