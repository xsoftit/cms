<?php

namespace Xsoft\Cms\Controllers\Admin\Hermit;

use Hermit\Logs\LogArchive;
use Hermit\Logs\MainLogArchiveController;
use Illuminate\Http\Request;
use Xsoft\Cms\Services\Hermit\LogService;

class LogArchiveController extends MainLogArchiveController
{
    protected $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function index()
    {
        $dataTable = $this->logService->getLogArchivesTable();
        return view('cms::admin.logs.archives.index', compact('dataTable'));
    }


    public function indexAjax(Request $request)
    {
        $table = $this->logService->getLogArchivesTable();
        $output = $table->output($request);
        $this->logService->prepareLogArchivesOutput($output);
        unset($output['collection']);
        return $output;
    }

    public function details(LogArchive $logArchive)
    {
        $data = json_decode(gzuncompress($logArchive->content));
        $data = collect($data);
        $data->sortByDesc('id');
        return view('cms::admin.logs.archives.details',compact('logArchive','data'));
    }
}
