@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.exampleModels.title.create')}}
@endsection

@section('title')
    {{__('app.exampleModels.title.create')}}
@endsection
@section('subtitle')
    {{__('app.exampleModels.subtitle.create')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.exampleModels.breadcrumbs.index'),route('admin.exampleModels.index')],[__('app.exampleModels.breadcrumbs.create')]) !!}
@endsection

@section('page-content')
    <form action="{{route('admin.exampleModels.store')}}" method="post">
        @csrf
        @method('POST')
        FIELD_INPUTS
        <div class="form-group">
            <button type="submit" class="btn btn-success form float-right">{{__('app.save')}}</button>
        </div>
    </form>
@endsection
