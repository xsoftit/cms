<?php

namespace Xsoft\Cms\Services\Admin;

use Xsoft\DataTables\DataTable;

class RoleService
{
    public function getTable()
    {
        $table = new DataTable('roles', 'admin.roles.indexAjax');
        $table->setOptions(['searching' => false, 'lengthChange' => false, 'info' => false, 'order' => '0,asc']);
        $table->setColumns([
            ['ID', 'id',['class' => 'width5']],
            [__('app.roles.fields.name'), 'name'],
        ]);
        if (auth()->user()->hasPermission('roles.edit')) {
            $table->addColumn(["", 'id', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width1']]);
        }
        $table->addToQuery(
            ['where', 'name','!=', 'superadmin']
        );
        return $table;
    }

    public function prepareOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            if (auth()->user()->hasPermission('roles.edit')) {
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                    auth()->user()->hasRole($row->name) ? '' : "<a href='" . route('admin.roles.edit', [$row->id]) . "' class='btn btn-outline-light'>" . __('app.edit') . "</a>"
                ];
            }else{
                $output['aaData'][] = [
                    $row->id,
                    $row->name
                ];
            }
        }
    }

}
