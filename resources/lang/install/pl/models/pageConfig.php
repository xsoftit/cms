<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    "add" => "Dodaj element menu",
    "link" => "Link",
    "menu" => "Konfiguracja stron",
    "name" => "Nazwa",
    "title" => "Konfiguracja stron",
    "subtitle" => "Zarządzanie menu i globalnymi widżetami stron",
    "globalWidgets" => "Widżety globalne",
    "menuNotDefined" => "Menu niezdefiniowana",
    'alerts' => [
        "updated" => "Menu zaktualizowane pomyślnie!",
    ],
    'manage' => [
        "main" => "Menu głowne",
        "menu" => "Zarządzaj menu",
        "footer" => "Menu stopki",
    ]

];
