<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Xsoft\Cms\Models\Container;

class ContainerController extends Controller
{

    public function add(Request $request)
    {
        $order = Container::where('placement_column', $request->get('placement_column'))
            ->where('page_section_id', $request->get('page_section_id'))
            ->max('placement_order');
        $data = $request->all();
        $data['placement_order'] = $order + 1;
        $container = Container::create($data);

        return response()->json(['data' => $container->toArray()]);
    }


    public function get(Request $request)
    {
        $container = Container::find($request->get('container_id'));
        return response()->json(['data' => $container->toArray()]);
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $container = Container::find($request->get('container_id'));
        $container->update($data);

        return response()->json(['data' => $data]);
    }

    public function delete(Request $request)
    {
        $container = Container::find($request->get('container_id'));
        $containers = Container::where('placement_order', '>', $container->placement_order)
            ->where('page_section_id', $container->page_section_id)
            ->where('placement_column', $container->placement_column)
            ->get();
        foreach ($containers as $cont) {
            $cont->placement_order = $cont->placement_order - 1;
            $cont->update();
        }
        $container->delete();

        return response()->json(['success' => true]);
    }

    public function move(Request $request)
    {
        $dir = $request->get('dir');
        $container = Container::find($request->get('container_id'));
        $max = Container::where('placement_column', $container->placement_column)
            ->where('page_section_id', $container->page_section_id)
            ->max('placement_order');
        if ($dir == 1 && $container->placement_order > 0) {
            $nextContainer = Container::where('placement_order', $container->placement_order - 1)
                ->where('page_section_id', $container->page_section_id)
                ->where('placement_column', $container->placement_column)
                ->first();
            $container->placement_order = $container->placement_order - 1;
            $nextContainer->placement_order = $nextContainer->placement_order + 1;
        }
        if ($dir == 0 && $container->placement_order < $max) {
            $nextContainer = Container::where('placement_order', $container->placement_order + 1)
                ->where('page_section_id', $container->page_section_id)
                ->where('placement_column', $container->placement_column)
                ->first();
            $container->placement_order = $container->placement_order + 1;
            $nextContainer->placement_order = $nextContainer->placement_order - 1;
        }
        $container->update();
        $nextContainer->update();
        return response()->json(['success' => true]);
    }
}
