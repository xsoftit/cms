<?php

namespace Xsoft\Cms\Helpers\FrontProcessors;

class FrontWidget
{
    protected $data;
    protected $bladeName;
    protected $renderedView;

    public function __construct(array $data)
    {
        $this->data = $data['data'];
        if (key_exists('blade', $data)) {
            $this->bladeName = $data['blade'];
        }
        if (key_exists('view', $data)) {
            $this->renderedView = $data['view'];
        }
    }

    public function get(string $element, $default_return = null)
    {
        $output = null;
        if ($element == 'all') {
            $output = $this->data;
        } else if (key_exists($element, $this->data)) {
            $output = $this->data[$element];
        }
        if (!$output) {
            $output = $default_return;
        }
        return $output;
    }

    public function all()
    {
        return $this->data;
    }

    public function getTag()
    {
        return $this->get('tag');
    }

    public function getClasses()
    {
        return $this->get('classes');
    }

    public function getStyles()
    {
        return $this->get('styles');
    }

    public function getBladeName()
    {
        return $this->bladeName;
    }

    public function getRenderedView()
    {
        return $this->renderedView;
    }
}
