@section('modals')
    @parent
    <div class="modal fade" id="move-widget-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.moveWidget')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <h3>{{__('app.pages.form.moveWidgetQuestion')}}</h3>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" id="move-widget-button" class="btn btn-outline-success">{{__('app.move')}}</button>
                    <button type="button" id="copy-widget-button" class="btn btn-outline-info">{{__('app.copy')}}</button>
                    <button type="reset" class="btn btn-outline-light"
                            data-dismiss="modal">{{__('app.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
