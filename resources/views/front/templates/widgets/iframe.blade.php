<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.6.4/iframeResizer.min.js"></script>
<iframe id="oc-iframe" src="{{$data->get('source')}}" width="100%" height="100%"
        scrolling="no" style="border: none; height: 100%" allow="fullscreen"></iframe>
<script type="text/javascript">
    iFrameResize({
        checkOrigin: false,
        messageCallback: function (messageData) {
            ocIframe = document.getElementById("oc-iframe");
            ocStyle = window.getComputedStyle(ocIframe)
        }
    }, '#oc-iframe');
</script>

