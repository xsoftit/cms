<div class="widget-preview @if(auth()->user()->hasPermission('widgets.edit')) widget-preview-edit @endif"
     data-target="#{{$widget->blade_name.'-modal'}}"
     data-name="{{$widget->name}}" data-id="{{$widget->id}}">
    <div class="widget-header"> {{$widget->name}}
        <small>({{$widget->template->name}})</small>
    </div>
    <ul class="widget-list">
        @if(view()->exists('admin.templates.widgets.'.$widget->blade_name))
            @include('admin.templates.widgets.'.$widget->blade_name,['widget'=>$widget,'pageLanguage'=>$pageLanguage])
        @else
            @include('cms::admin.templates.widgets.'.$widget->blade_name,['widget'=>$widget,'pageLanguage'=>$pageLanguage])
        @endif
    </ul>
    <input type="hidden" name="widget_id" value="{{$widget->id}}">
</div>
