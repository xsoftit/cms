<?php

namespace Xsoft\Cms\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'role' => 'required|exists:roles,name',
            'email' => 'required|unique:users',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('validation.custom.name.required'),
            'role.required' => __('validation.custom.role.required'),
            'role.exists' => __('validation.custom.role.exists'),
            'email.required' => __('validation.custom.email.required'),
            'email.unique' => __('validation.custom.email.unique',['object' => __('app.users.name')]),
            'password.required' => __('validation.custom.password.required'),
        ];
    }
}
