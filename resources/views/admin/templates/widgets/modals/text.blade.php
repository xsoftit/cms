@component('cms::admin.templates.widgetModal',get_defined_vars())
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.textarea')}}</label>
        <textarea id="text" name="text" class="form-control data jodit"></textarea>
    </div>
@endcomponent
