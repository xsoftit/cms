@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.exampleModels.title.create')}}
@endsection

@section('title')
    {{__('app.exampleModels.title.create')}}
@endsection
@section('subtitle')
    {{__('app.exampleModels.subtitle.create')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.exampleModels.breadcrumbs.index'),route('admin.exampleModels.index')],[__('app.exampleModels.breadcrumbs.create')]) !!}
@endsection

@section('page-content')
    <form action="{{route('admin.exampleModels.store')}}" method="post">
        @csrf
        @method('POST')
        FIELD_INPUTS
        @php($languages = LanguageHelper::getAllFront())
        <ul class="nav nav-tabs mt20">
            @foreach($languages as $language)
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-{{$language}}">{{$language}}</a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content" id="myTabContent">
            @foreach($languages as $language)
                <div class="tab-pane fade" id='tab-{{$language}}'>
                    CONTENT_FIELD_INPUTS
                </div>
            @endforeach
        </div>
        <div class="form-group h-20">
            <button type="submit" class="btn btn-success form float-right">{{__('app.save')}}</button>
        </div>
    </form>
@endsection
