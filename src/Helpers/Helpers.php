<?php

/**
 * Returns value + $additional form $array when $key exists and is not null or $return when not
 *
 * @param array $array
 * @param string $key
 * @param string $return
 * @param string $additional
 * @return bool/int
 */
function kv($array, $key, $return = '', $additional = '')
{
    if (key_exists($key, $array)) {
        if ($array[$key]) {
            if ($additional) {
                return $array[$key] . $additional;
            }
            return $array[$key];
        }
    }
    return $return;

}

function page_data($value, $configs = null)
{
    if ($configs) {
        $config = $configs->where('name', $value)->first();
    } else {
        $config = \Xsoft\Cms\Helpers\CmsConfig::get($value);
    }
    if ($config) {
        if ($config->type == 'image') {
            if ($config->image) {
                $path = $config->image->storage_path;
            } else {
                $path = \Xsoft\Cms\Models\File::where('name', 'default.jpg')->first();
                if (!$path) {
                    return null;
                }
                $path = $path->storage_path;
            }
            return \Illuminate\Support\Facades\Storage::url($path);
        }
        return $config->value;
    }
    return null;
}

function global_widget($value, $pageLanguage)
{
    return \Xsoft\Cms\Models\Widget::getFrontGlobal($value, $pageLanguage);
}

function makeTranslation($vendorFilePath, $array)
{
    $vendorFile = include(base_path() . '/vendor/xsoft/cms/resources/lang/default/' . $vendorFilePath);
    return array_merge_recursive($vendorFile, $array);
}
