<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain(config('cms.FRONT_DOMAIN'))->middleware('lang_check', 'web')->name('front.')->group(function () {
    Route::get('/{url?}', 'Xsoft\Cms\Controllers\Front\PageController@index')->where('url', '[A-Za-z0-9_/-]+')->name('index');
});

Route::domain(config('cms.ADMIN_DOMAIN'))->middleware('web')->name('admin.')->group(function () {
    Route::get('login', 'Xsoft\Cms\Controllers\Admin\Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Xsoft\Cms\Controllers\Admin\Auth\LoginController@login');
    Route::post('logout', 'Xsoft\Cms\Controllers\Admin\Auth\LoginController@logout')->name('logout');
    Route::middleware(['auth'])->group(function () {
        Route::get('/', 'Xsoft\Cms\Controllers\Admin\DashboardController@index')->name('dashboard')->middleware('permission:cms.login');
        Route::get('/instruction', 'Xsoft\Cms\Controllers\Admin\DashboardController@instruction')->name('instruction')->middleware('permission:cms.login');
        Route::prefix('pages')->name('pages.')->group(function () {
            Route::get('/', 'Xsoft\Cms\Controllers\Admin\PageController@index')->name('index')->middleware('permission:pages.show');
            Route::post('create', 'Xsoft\Cms\Controllers\Admin\PageController@store')->name('store')->middleware('permission:pages.create');
            Route::post('preview', 'Xsoft\Cms\Controllers\Admin\PageController@preview')->name('preview')->middleware('permission:pages.show');
            Route::get('frontPreview/{pageLanguage}', 'Xsoft\Cms\Controllers\Admin\PageController@frontPreview')->name('frontPreview')->middleware('permission:pages.show');
            Route::post('getPageDetails', 'Xsoft\Cms\Controllers\Admin\PageController@getPageDetails')->name('getPageDetails')->middleware('permission:pages.show');
            Route::post('updateDetails', 'Xsoft\Cms\Controllers\Admin\PageController@updateDetails')->name('updateDetails')->middleware('permission:pages.edit');
            Route::post('activatePage', 'Xsoft\Cms\Controllers\Admin\PageController@activatePage')->name('activatePage')->middleware('permission:pages.edit');
            Route::post('deactivatePage', 'Xsoft\Cms\Controllers\Admin\PageController@deactivatePage')->name('deactivatePage')->middleware('permission:pages.edit');
            Route::post('addLanguageVersion', 'Xsoft\Cms\Controllers\Admin\PageController@addLanguageVersion')->name('addLanguageVersion')->middleware('permission:pages.create');
            Route::post('getLanguagesSelect', 'Xsoft\Cms\Controllers\Admin\PageController@getLanguagesSelect')->name('getLanguagesSelect')->middleware('permission:pages.create');
            Route::post('addSection', 'Xsoft\Cms\Controllers\Admin\PageSectionController@addSection')->name('addSection')->middleware('permission:sections.create');
            Route::post('publish', 'Xsoft\Cms\Controllers\Admin\PageController@publish')->name('publish');
            Route::post('delete', 'Xsoft\Cms\Controllers\Admin\PageController@delete')->name('delete')->middleware('permission:pages.delete');
            Route::get('export/{page}', 'Xsoft\Cms\Controllers\Admin\PageController@export')->name('export')->middleware('permission:pages.export');
            Route::get('exportAll', 'Xsoft\Cms\Controllers\Admin\PageController@exportAllPages')->name('exportAll')->middleware('permission:pages.export');
            Route::post('import', 'Xsoft\Cms\Controllers\Admin\PageController@import')->name('import')->middleware('permission:pages.import');
            Route::post('getSections', 'Xsoft\Cms\Controllers\Admin\PageController@getSections')->name('getSections')->middleware('permission:sections.create');
        });
        Route::prefix('pageSectionTemplates')->name('pageSectionTemplates.')->group(function () {
            Route::post('preview/{pageSectionTemplate}', 'Xsoft\Cms\Controllers\Admin\PageSectionTemplateController@preview')->name('preview')->middleware('permission:pages.show');
        });
        Route::prefix('sections')->name('sections.')->group(function () {
            Route::post('edit', 'Xsoft\Cms\Controllers\Admin\PageSectionController@edit')->name('edit')->middleware('permission:sections.edit');
            Route::post('update', 'Xsoft\Cms\Controllers\Admin\PageSectionController@update')->name('update')->middleware('permission:sections.edit');
            Route::post('delete', 'Xsoft\Cms\Controllers\Admin\PageSectionController@delete')->name('delete')->middleware('permission:sections.delete');
            Route::post('addWidget', 'Xsoft\Cms\Controllers\Admin\WidgetController@addWidget')->name('addWidget')->middleware('permission:widgets.create');
            Route::post('deleteWidget', 'Xsoft\Cms\Controllers\Admin\WidgetController@delete')->name('deleteWidget')->middleware('permission:widgets.delete');
            Route::post('sort', 'Xsoft\Cms\Controllers\Admin\PageSectionController@sort')->name('sort')->middleware('permission:sections.edit');
        });
        Route::prefix('containers')->name('containers.')->group(function () {
            Route::post('/add', 'Xsoft\Cms\Controllers\Admin\ContainerController@add')->name('add')->middleware('permission:sections.edit');
            Route::post('/edit', 'Xsoft\Cms\Controllers\Admin\ContainerController@edit')->name('edit')->middleware('permission:sections.edit');
            Route::post('/get', 'Xsoft\Cms\Controllers\Admin\ContainerController@get')->name('get')->middleware('permission:sections.edit');
            Route::post('/delete', 'Xsoft\Cms\Controllers\Admin\ContainerController@delete')->name('delete')->middleware('permission:sections.edit');
            Route::post('/move', 'Xsoft\Cms\Controllers\Admin\ContainerController@move')->name('move')->middleware('permission:sections.edit');
        });
        Route::prefix('widgetContents')->name('widgetContents.')->group(function () {
            Route::post('/getContent', 'Xsoft\Cms\Controllers\Admin\WidgetController@getContent')->name('getContent')->middleware('permission:widgets.edit');
            Route::post('/saveContent', 'Xsoft\Cms\Controllers\Admin\WidgetController@saveContent')->name('saveContent')->middleware('permission:widgets.edit');
            Route::post('/move', 'Xsoft\Cms\Controllers\Admin\WidgetController@move')->name('move')->middleware('permission:widgets.edit');
            Route::post('/copy', 'Xsoft\Cms\Controllers\Admin\WidgetController@copy')->name('copy')->middleware('permission:widgets.edit');
        });
        Route::prefix('pageConfig')->name('pageConfig.')->group(function () {
            Route::get('/', 'Xsoft\Cms\Controllers\Admin\PageConfigController@index')->name('index')->middleware('permission:page_config.show');
            Route::post('update', 'Xsoft\Cms\Controllers\Admin\PageConfigController@update')->name('update')->middleware('permission:page_config.edit');
            Route::post('getLink', 'Xsoft\Cms\Controllers\Admin\PageConfigController@getLink')->name('getLink')->middleware('permission:page_config.edit');
        });
        Route::prefix('site/languages')->name('frontLanguages.')->group(function () {
            Route::get('/', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@index')->name('index')->middleware('permission:front_languages.show');
            Route::get('ajax', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@indexAjax')->name('indexAjax')->middleware('permission:front_languages.show');
            Route::post('create', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@store')->name('store')->middleware('permission:front_languages.create');
            Route::get('{language}/edit', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@edit')->name('edit')->middleware('permission:front_languages.edit');
            Route::post('{language}/edit', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@update')->name('update')->middleware('permission:front_languages.edit');
            Route::post('{language}/change/state', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@stateChange')->name('change.state')->middleware('permission:front_languages.delete');
            Route::post('{language}/delete', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@delete')->name('delete')->middleware('permission:front_languages.delete');
            Route::get('{language}/export', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@export')->name('export')->middleware('permission:front_languages.exportImport');
            Route::post('import', 'Xsoft\Cms\Controllers\Admin\FrontLanguageController@import')->name('import')->middleware('permission:front_languages.exportImport');
        });
        Route::prefix('users')->name('users.')->group(function () {
            Route::get('/', 'Xsoft\Cms\Controllers\Admin\UserController@index')->name('index')->middleware('permission:users.show');
            Route::get('ajax', 'Xsoft\Cms\Controllers\Admin\UserController@indexAjax')->name('indexAjax')->middleware('permission:users.show');
            Route::post('create', 'Xsoft\Cms\Controllers\Admin\UserController@store')->name('store')->middleware('permission:users.create');
            Route::get('{user}/edit', 'Xsoft\Cms\Controllers\Admin\UserController@edit')->name('edit')->middleware('permission:users.edit');
            Route::post('{user}/edit', 'Xsoft\Cms\Controllers\Admin\UserController@update')->name('update')->middleware('permission:users.edit');
            Route::delete('{user}', 'Xsoft\Cms\Controllers\Admin\UserController@destroy')->name('destroy')->middleware('permission:users.delete');
            Route::get('change/language/{lang}', 'Xsoft\Cms\Controllers\Admin\UserController@languageChange')->name('languageChange')->middleware('permission:cms.login');
            Route::post('sidebarToggle', 'Xsoft\Cms\Controllers\Admin\UserController@sidebarToggle')->name('sidebarToggle')->middleware('permission:cms.login');
            Route::get('profile', 'Xsoft\Cms\Controllers\Admin\UserController@profile')->name('profile')->middleware('permission:cms.login');
            Route::prefix('{user}/change')->name('change.')->group(function () {
                Route::post('state', 'Xsoft\Cms\Controllers\Admin\UserController@stateChange')->name('state')->middleware('permission:users.edit');
                Route::post('password', 'Xsoft\Cms\Controllers\Admin\UserController@passwordChange')->name('password')->middleware('permission:users.edit');
                Route::post('role', 'Xsoft\Cms\Controllers\Admin\UserController@roleChange')->name('role')->middleware('permission:users.edit');
            });
        });
        Route::prefix('config')->name('config.')->group(function () {
            Route::get('/', 'Xsoft\Cms\Controllers\Admin\ConfigController@edit')->name('edit')->middleware('permission:config.show');
            Route::post('/', 'Xsoft\Cms\Controllers\Admin\ConfigController@update')->name('update')->middleware('permission:config.show');
            Route::post('/setPrefix', 'Xsoft\Cms\Controllers\Admin\ConfigController@setPrefix')->name('setPrefix')->middleware('permission:cms.login');
            Route::prefix('languages')->name('languages.')->group(function () {
                Route::get('/', 'Xsoft\Cms\Controllers\Admin\LanguageController@index')->name('index')->middleware('permission:admin_languages.show');
                Route::get('ajax', 'Xsoft\Cms\Controllers\Admin\LanguageController@indexAjax')->name('indexAjax')->middleware('permission:admin_languages.show');
                Route::post('create', 'Xsoft\Cms\Controllers\Admin\LanguageController@store')->name('store')->middleware('permission:admin_languages.create');
                Route::get('{language}/edit', 'Xsoft\Cms\Controllers\Admin\LanguageController@edit')->name('edit')->middleware('permission:admin_languages.edit');
                Route::post('{language}/edit', 'Xsoft\Cms\Controllers\Admin\LanguageController@update')->name('update')->middleware('permission:admin_languages.edit');
                Route::post('{language}/change/state', 'Xsoft\Cms\Controllers\Admin\LanguageController@stateChange')->name('change.state')->middleware('permission:admin_languages.delete');
                Route::get('{language}/export', 'Xsoft\Cms\Controllers\Admin\LanguageController@export')->name('export')->middleware('permission:admin_languages.exportImport');
                Route::post('import', 'Xsoft\Cms\Controllers\Admin\LanguageController@import')->name('import')->middleware('permission:admin_languages.exportImport');
            });
        });
        Route::name('roles.')->prefix('roles')->group(function () {
            Route::get('/', 'Xsoft\Cms\Controllers\Admin\RoleController@index')->name('index')->middleware('permission:roles.show');
            Route::get('ajax', 'Xsoft\Cms\Controllers\Admin\RoleController@indexAjax')->name('indexAjax')->middleware('permission:roles.show');
            Route::post('create', 'Xsoft\Cms\Controllers\Admin\RoleController@store')->name('store')->middleware('permission:roles.create');
            Route::get('{role}/edit', 'Xsoft\Cms\Controllers\Admin\RoleController@edit')->name('edit')->middleware('permission:roles.edit');
            Route::post('{role}/edit', 'Xsoft\Cms\Controllers\Admin\RoleController@update')->name('update')->middleware('permission:roles.edit');
            Route::delete('{role}', 'Xsoft\Cms\Controllers\Admin\RoleController@destroy')->name('destroy')->middleware('permission:roles.destroy');
        });
        Route::name('galleries.')->prefix('galleries')->group(function () {
            Route::get('/', 'Xsoft\Cms\Controllers\Admin\GalleryController@index')->name('index')->middleware('permission:galleries.show');
            Route::get('ajax', 'Xsoft\Cms\Controllers\Admin\GalleryController@indexAjax')->name('indexAjax')->middleware('permission:galleries.show');
            Route::get('create', 'Xsoft\Cms\Controllers\Admin\GalleryController@create')->name('create')->middleware('permission:galleries.create');
            Route::post('create', 'Xsoft\Cms\Controllers\Admin\GalleryController@store')->name('store')->middleware('permission:galleries.create');
            Route::get('{gallery}/edit', 'Xsoft\Cms\Controllers\Admin\GalleryController@edit')->name('edit')->middleware('permission:galleries.edit');
            Route::post('{gallery}/edit', 'Xsoft\Cms\Controllers\Admin\GalleryController@update')->name('update')->middleware('permission:galleries.edit');
            Route::delete('{gallery}', 'Xsoft\Cms\Controllers\Admin\GalleryController@destroy')->name('destroy')->middleware('permission:galleries.destroy');
            Route::get('getForSelect', 'Xsoft\Cms\Controllers\Admin\GalleryController@getForSelect')->name('getForSelect')->middleware('permission:cms.login');
        });
        Route::name('files.')->prefix('files')->group(function () {
            Route::post('/', 'Xsoft\Cms\Controllers\Admin\FileController@get')->name('get')->middleware('permission:files.usage');
            Route::post('/preview', 'Xsoft\Cms\Controllers\Admin\FileController@preview')->name('preview')->middleware('permission:files.usage');
            Route::post('/move', 'Xsoft\Cms\Controllers\Admin\FileController@move')->name('move')->middleware('permission:files.manage');
            Route::post('/delete', 'Xsoft\Cms\Controllers\Admin\FileController@delete')->name('delete')->middleware('permission:files.delete|files.foldersManage');
            Route::post('/checkIfEmpty', 'Xsoft\Cms\Controllers\Admin\FileController@checkIfEmpty')->name('checkIfEmpty')->middleware('permission:files.foldersManage');
            Route::post('/rename', 'Xsoft\Cms\Controllers\Admin\FileController@rename')->name('rename')->middleware('permission:files.manage|files.foldersManage');
            Route::post('/uploadThumb', 'Xsoft\Cms\Controllers\Admin\FileController@uploadThumb')->name('uploadThumb')->middleware('permission:files.usage');
            Route::name('create.')->prefix('create')->group(function () {
                Route::post('directory', 'Xsoft\Cms\Controllers\Admin\FileController@createFolder')->name('folder')->middleware('permission:files.foldersManage');
                Route::post('files', 'Xsoft\Cms\Controllers\Admin\FileController@createFiles')->name('files')->middleware('permission:files.manage');
            });
            Route::post('export', 'Xsoft\Cms\Controllers\Admin\FileController@export')->name('export')->middleware('permission:files.exportImport');
            Route::get('downloadExportFile/{filename}', 'Xsoft\Cms\Controllers\Admin\FileController@downloadExportFile')->name('downloadExportFile')->middleware('permission:files.exportImport');
            Route::post('import', 'Xsoft\Cms\Controllers\Admin\FileController@import')->name('import')->middleware('permission:files.exportImport');
        });
        Route::prefix('sourceModels')->name('sourceModels.')->group(function () {
            Route::get('/', 'Xsoft\Cms\Controllers\Admin\SourceModelController@index')->name('index')->middleware('permission:source_models.show');
            Route::get('ajax', 'Xsoft\Cms\Controllers\Admin\SourceModelController@indexAjax')->name('indexAjax')->middleware('permission:source_models.show');
            Route::get('change/{sourceModel}', 'Xsoft\Cms\Controllers\Admin\SourceModelController@change')->name('change')->middleware('permission:source_models.show');
            Route::get('getRouteTypes', 'Xsoft\Cms\Controllers\Admin\SourceModelController@getRouteTypes')->name('getRouteTypes')->middleware('permission:cms.login');
            Route::get('getRouteValues', 'Xsoft\Cms\Controllers\Admin\SourceModelController@getRouteValues')->name('getRouteValues')->middleware('permission:cms.login');
            Route::get('create', 'Xsoft\Cms\Controllers\Admin\SourceModelController@create')->name('create')->middleware('permission:source_models.show');
            Route::post('create', 'Xsoft\Cms\Controllers\Admin\SourceModelController@store')->name('store')->middleware('permission:source_models.show');
            Route::post('generateNamesInputs/{className}', 'Xsoft\Cms\Controllers\Admin\SourceModelController@generateNamesInputs')->name('generateNamesInputs')->middleware('permission:source_models.show');
            Route::get('summary/{className}', 'Xsoft\Cms\Controllers\Admin\SourceModelController@summary')->name('summary')->middleware('permission:source_models.show');
        });
    });
});

Route::prefix('logs')->middleware('web')->name('logs.')->group(function () {
    Route::get('/', 'Xsoft\Cms\Controllers\Admin\Hermit\LogController@index')->name('index')->middleware('permission:logs.show');
    Route::get('indexAjax', 'Xsoft\Cms\Controllers\Admin\Hermit\LogController@indexAjax')->name('indexAjax')->middleware('permission:logs.show');
    Route::post('priorities', 'Xsoft\Cms\Controllers\Admin\Hermit\LogController@savePriorities')->name('savePriorities')->middleware('role:superadmin');
    Route::prefix('archives')->name('archives.')->group(function () {
        Route::get('/', 'Xsoft\Cms\Controllers\Admin\Hermit\LogArchiveController@index')->name('index')->middleware('permission:logs.show');
        Route::get('/ajax', 'Xsoft\Cms\Controllers\Admin\Hermit\LogArchiveController@indexAjax')->name('indexAjax')->middleware('permission:logs.show');
        Route::get('/details/{logArchive}', 'Xsoft\Cms\Controllers\Admin\Hermit\LogArchiveController@details')->name('details')->middleware('permission:logs.show');
    });
});
