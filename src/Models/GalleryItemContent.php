<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryItemContent extends Model
{
    protected $fillable = [
        'title',
        'gallery_item_id',
        'language',
        'url',
        'description',
        'options'
    ];

    public function item()
    {
        return $this->belongsTo(GalleryItem::class);
    }
}
