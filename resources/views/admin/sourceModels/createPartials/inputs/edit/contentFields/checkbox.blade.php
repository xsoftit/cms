<div class="animated-checkbox">
    <label>
        <input type="checkbox" name="contents[{\{$language}}][{{$field->getName()}}]"{\{$content->{{$field->getName()}}?' checked="true"':''}}>
        <span
            class="label-text text-capitalize">{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</span>
    </label>
</div>
