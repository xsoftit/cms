@section('js')
    <script>
        @if(auth()->user())
            userLanguage = '{{ auth()->user()->language }}';
        @else
            userLanguage = '{{\Xsoft\Cms\Helpers\CmsConfig::get('default_lang')->value}}';
        @endif
    </script>
    <script src="/admin/vendor/admin-panel/js/jquery-3.2.1.min.js"></script>
    <script src="/admin/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/admin/vendor/admin-panel/js/popper.min.js"></script>
    <script src="/admin/vendor/admin-panel/js/bootstrap.min.js"></script>
    <script src="/admin/vendor/admin-panel/js/plugins/jquery.dataTables.min.js"></script>
    <script src="/admin/vendor/admin-panel/js/plugins/dataTables.bootstrap.min.js"></script>
    <script src="/admin/vendor/admin-panel/js/plugins/select2.min.js"></script>
    <script src="/admin/vendor/admin-panel/js/plugins/select2Languages/pl.js"></script>
    <script src="/admin/vendor/jodit/jodit.min.js"></script>
    <script src="/admin/vendor/admin-panel/js/main.js"></script>
    <script src="/admin/vendor/main.js"></script>
    <script src="/admin/js/custom.js"></script>
    @stack('datatables-js')
    <script>
        @if(count($errors->all()))
        @foreach($errors->all() as $error)
        showAlert('{{$error}}', 'danger');

        @endforeach
        @endif
    </script>


@show
