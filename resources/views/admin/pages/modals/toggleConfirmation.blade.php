@section('modals')
    @parent
    <div class="modal fade" id="activate-confirm" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.confirmActivate')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="page_language_id">
                    <div class="text-center">
                        {{__('app.pages.questions.confirmActivate')}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="activate-confirm-button" data-id="" data-lang=""
                            class="btn btn-outline-success">{{__('app.confirm')}}</button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">{{__('app.close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deactivate-confirm" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.confirmDeactivate')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="page_language_id">
                    <div class="text-center">
                        {{__('app.pages.questions.confirmDeactivate')}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="deactivate-confirm-button" data-id="" data-lang=""
                            class="btn btn-outline-danger">{{__('app.confirm')}}</button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">{{__('app.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
