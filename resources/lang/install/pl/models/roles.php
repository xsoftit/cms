<?php

return [
    'menu' => 'Role',
    'title' => [
        "edit" => "Edycja roli :name",
        "index" => "Role",
        "create" => "Nowa rola",
    ],
    'subtitle' => [
        "edit" => "Konfiguracja roli :name",
        "index" => "Konfiguracja roli systemowych",
    ],
    'fields' => [
        'name' => 'Nazwa'
    ],
    'alerts' => [
        "saved" => "Rola :name zapisana pomyślnie!",
        "roleHasUsers" => "Nie możesz usunąć roli przypisanej do użytkowników!"
    ]
];
