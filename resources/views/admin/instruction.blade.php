@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.instruction')}}
@endsection

@section('title')
    {{__('app.instruction')}}
@endsection

@section('subtitle')
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#english">English</a></li>
        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#polski">Polski</a></li>
    </ul>
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.instruction')]) !!}
@endsection

@section('page-content')
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade" id='english'>
            <p>Hello World!<br>This instruction will be dull, full of misspells and inappropriate for some people.</p>
            <p></p>
        </div>
        <div class="tab-pane fade active show" id='polski'>
            <a href="https://docs.google.com/document/d/1np8krj0WEYvItCt6widBkLOd30HprKb2ifwyLoutgt8/edit?usp=sharing"
               target="_blank">Troszkę inna wersja na dysku google</a>
        </div>
        <div class="accordion accordion-bordered order-2 instruction" id="instruction-accordion" role="tablist">
            <div class="card">
                <div class="card-header" role="tab" id="heading-1">
                    <a data-toggle="collapse" href="#instruction-collapse-1" class="collapsed">
                        Dodawanie Strony
                    </a>
                </div>
                <div id="instruction-collapse-1" class="collapse" role="tabpanel" data-parent="#instruction-accordion">
                    <div class="card-body">
                        Aby dodać nową stronę przejdź <a target="_blank" href="{{route('admin.pages.index')}}">tutaj</a>,
                        wybierz język a następnie "{{__('app.pages.buttons.new')}}".
                        <br>
                        Po uzupełnieniu pól w podglądzie danej strony możesz dodać sekcje a następnie widżety do sekcji.
                        <br>
                        Aby strona była dostępna dla użytkowników należy opublikować wprowadzone zmiany
                        przyciskiem "{{__('app.pages.buttons.publish')}}" oraz aktywować stronę przyciskiem
                        "{{__('app.pages.buttons.active0')}}".
                        <br>
                        Jeżeli dana wersja językowa nie posiada zdefiniowanej strony głównej to nie będzie możliwości
                        wejścia na stronę w danym języku. Wybierz stronę, która ma być stroną główną a następnie
                        kliknij "{{__('app.pages.buttons.editDetails')}}". Zaznacz "{{__('app.pages.form.isMain')}}" i
                        zapisz zmiany.
                        <br>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="heading-1">
                    <h6 class="mb-0">
                        <a data-toggle="collapse" href="#instruction-collapse-2" class="collapsed">
                            Edycja Strony
                        </a>
                    </h6>
                </div>
                <div id="instruction-collapse-2" class="collapse" role="tabpanel" data-parent="#instruction-accordion">
                    <div class="card-body">
                        Jeżeli zostały wprowadzone jakieś zmiany w strukturze strony lub zmieniona została zawartość
                        widżetów należy wcisnąć przycisk "{{__('app.pages.buttons.publish')}}" aby zmiany były widoczne
                        dla użytkowników.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="heading-1">
                    <h6 class="mb-0">
                        <a data-toggle="collapse" href="#instruction-collapse-3" class="collapsed">
                            Dodawanie wersji językowej istniejącej strony
                        </a>
                    </h6>
                </div>
                <div id="instruction-collapse-3" class="collapse" role="tabpanel" data-parent="#instruction-accordion">
                    <div class="card-body">
                        Aby dodać nową wersję językową strony przejdź
                        <a target="_blank" target="_blank" href="{{route('admin.pages.index')}}">tutaj</a>,
                        wybierz stronę a następnie "{{__('app.pages.buttons.addLanguageVersion')}}".
                        <br>
                        Pole "{{__('app.pages.form.shareImages')}}" oznacza, że wersje językowe będą dzieliły ze sobą
                        wszystkie widżety związane z obrazkami. Jeżeli zmiany zostana wprowadzone na jednej wersji
                        językowej to na drugiej także zostaną wprowadzone.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="heading-1">
                    <h6 class="mb-0">
                        <a data-toggle="collapse" href="#instruction-collapse-4" class="collapsed">
                            Ogólne dane strony
                        </a>
                    </h6>
                </div>
                <div id="instruction-collapse-4" class="collapse" role="tabpanel" data-parent="#instruction-accordion">
                    <div class="card-body">
                        Ogólne dane strony można ustawić<a target="_blank" href="{{route('admin.config.edit')}}">tutaj</a>.
                        <br>
                        Jeżeli dane mają możliwość tłumaczenia na inne języki to są to widżety, które można
                        edytować <a target="_blank" href="{{route('admin.pageConfig.index')}}">tutaj</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="heading-1">
                    <h6 class="mb-0">
                        <a data-toggle="collapse" href="#instruction-collapse-5" class="collapsed">
                            Języki strony
                        </a>
                    </h6>
                </div>
                <div id="instruction-collapse-5" class="collapse" role="tabpanel" data-parent="#instruction-accordion">
                    <div class="card-body">
                        Jeżeli chcesz dodać nowy język strony przejdź
                        <a target="_blank" href="{{route('admin.frontLanguages.index')}}">tutaj</a>.
                        <br>
                        Nie będzie możliwe dodawanie stron w języku, który nie został aktywowany.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="heading-1">
                    <h6 class="mb-0">
                        <a data-toggle="collapse" href="#instruction-collapse-6" class="collapsed">
                            Menu
                        </a>
                    </h6>
                </div>
                <div id="instruction-collapse-6" class="collapse" role="tabpanel" data-parent="#instruction-accordion">
                    <div class="card-body">
                        Menu można dostosować <a target="_blank" href="{{route('admin.pageConfig.index')}}">tutaj</a>.
                        <br>
                        Dla każdego języka menu definiowane jest osobno. Strony możesz zamieniać miejscami oraz oznaczać
                        czy mają być widoczne czy nie. Aby zachować zmiany należy wcisnąć przycisk "{{__('app.save')}}"
                        dla każdej wersji językowej osobno.
                        <br>
                        <b>UWAGA!</b>
                        <br>
                        Zapisanie zmian spowoduje przeładowanie strony więc edytuj menu każdego języka osobno.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="heading-1">
                    <h6 class="mb-0">
                        <a data-toggle="collapse" href="#instruction-collapse-7" class="collapsed">
                            Wyłaczanie strony
                        </a>
                    </h6>
                </div>
                <div id="instruction-collapse-7" class="collapse" role="tabpanel" data-parent="#instruction-accordion">
                    <div class="card-body">
                        Jeżeli nie chcesz, aby jakaś strona nie była widoczna dla użytkownika możesz ją wyłączyć
                        przyciskiem "{{__('app.pages.buttons.active1')}}". Zostanie ona automatycznie usunięta z menu
                        jeżeli się w nim znajdowała.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
