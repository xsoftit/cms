<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    "new" => "Nowa sekcja",
    "copy" => "Kopiuj istniejącą sekcję",
    "layout" => "Szablon",
    "content" => "Zawartość",
    "deleteWarning" => "Zostanie ona usunięty ze wszystkich powiązanych stron",
    "deleteQuestion" => "Czy na pewno chcesz usunąć tą sekcję?",
    'title' => [
        "edit" => "Edycja sekcji",
        "create" => "Nowa sekcja"
    ],
    'form' => [
        "name" => "Nazwa sekcji",
        "active" => "Akywna",
        "styles" => "Style tagu sekcji",
        "classes" => "Klasy tagu sekcji",
        "pageChoice" => "Wybierz stronę",
        "withContent" => "Z zawartością",
        'withContentDescription' => 'Zawartość będzie brana z tej samej wersji językowej',
        "sectionChoice" => "Wybierz sekcję",
        "pageSectionTemplateChoice" => "Wybierz szablon sekcji",
    ],
    'alerts' => [
        "added" => "Sekcja dodana pomyślnie",
        "updated" => "Sekcja zaktualizowana pomyślnie"
    ],
];
