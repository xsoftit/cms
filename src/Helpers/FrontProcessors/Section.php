<?php

namespace Xsoft\Cms\Helpers\FrontProcessors;

use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\PageSection;
use Xsoft\Cms\Services\Front\SectionService;

class Section
{
    protected $pageSection;
    protected $containers;
    protected $columns;
    protected $sectionContent;
    protected $view;

    public function __construct(PageLanguage $pageLanguage, PageSection $pageSection)
    {
        $this->containers = $pageSection->containers->where('placement_column', "-1")->sortByDesc('placement_order');
        $this->pageSection = $pageSection;
        $this->columns = SectionService::getSectionColumns($pageLanguage, $this);
        $this->view = $this->prepareView();
    }

    public function getPageSection()
    {
        return $this->pageSection;
    }

    public function getClasses()
    {
        return $this->pageSection->classes;
    }

    public function getSlug()
    {
        return $this->pageSection->slug;
    }

    public function getStyles()
    {
        return $this->pageSection->styles;
    }

    public function getBladeName()
    {
        $bladeName = "pageSectionTemplateNotFound";
        if ($this->pageSection->template) {
            $bladeName = $this->pageSection->template->blade_name;
        }
        return $bladeName;
    }

    public function getTag()
    {
        $tag = null;
        if ($this->pageSection->template) {
            $tag = $this->pageSection->template->tag;
        }
        return $tag;
    }

    public function getContainers()
    {
        return $this->containers;
    }

    public function getColumns()
    {
        return $this->columns;
    }


    public function getColumn($order)
    {
        $column = $this->columns->filter(function ($value, $key) use ($order) {
            if ($key == $order) {
                return true;
            }
        })->first();
        return $column;
    }

    public function getColumnView($order)
    {
        return $this->getColumn($order)->getView();
    }

    public function getView()
    {
        return $this->view;
    }

    public function getSectionContent()
    {
        return $this->sectionContent;
    }

    private function prepareView()
    {
        $containers = $this->containers;
        $this->sectionContent = $this->handleViewWithContainers($containers);
        $view = view('front.templates.section', ['section' => $this]);
        return $view;
    }

    private function handleViewWithContainers($containers)
    {
        $view = null;
        if (count($containers) > 0) {
            $container = $containers->pop();
            $container->view = self::handleViewWithContainers($containers);
            $view = view('front.templates.container', ['container' => $container]);
        } else if (!empty($this->columns)) {
            $view = view('front.templates.sections.' . $this->getBladeName(), ['section' => $this]);
        }
        return $view;
    }
}
