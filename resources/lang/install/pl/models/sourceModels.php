<?php

return [

    "no" => "Nie",
    "yes" => "Tak",
    "menu" => "Modele",
    'title' => [
        "edit" => "Edycja modelu",
        "index" => "Modele",
        'create' => 'Tworzenie nowego modelu',
        'summary' => 'Podsumowanie'
    ],
    'subtitle' => [
        "index" => "Lista modeli",
        "edit" => "Formularz edycji modelu",
        'create' => 'Formularz tworzenia nowego modelu',
        'summary' => 'Podsumowanie utworzonego modelu'
    ],
    'breadcrumbs' => [
        "index" => "Modele",
        "edit" => "Edycja modelu",
        'create' => 'Tworzenie modelu',
        'summary' => 'Podsumowanie'
    ],
    'fields' => [
        "model_name" => "Nazwa modelu",
        "in_use" => "W użyciu",
        "in_use0" => "Nie używany",
        "fields" => "Dostępne pola",
        "singleObject" => "Pojedynczy obiekt",
        'className' => 'Class Name',
        'sourceTable' => 'Wpis do tabeli source_models - umożliwia tworzenie stron powiązanych z tym modelem',
        'languageContent' => 'Model z treścią w różnych językach',
        'columns' => 'Kolumny',
        'contentColumns' => 'Kolumny dla tabeli treści językowych (kolumna z id głównego modelu zostanie wygenerowana automatycznie)',
        'column' => [
            'type' => 'Typ',
            'name' => 'Nazwa',
            'default' => 'Domyślna wartość',
            'nullable' => 'Nullable',
            'unique' => 'Unikalne',
        ],
        'addColumn' => 'Dodaj kolumnę'
    ],
    'summary' => 'Podsumowanie',
    'generateNames' => 'Podejrzyj nazwy',
    'names' => [
        'class_plural' => 'Liczba mnoga nazwy klasy',
        'nice' => 'Ładna nazwa - przykład: menu',
        'table' => 'Nazwa tabeli w bazie danych',
        'floor' => 'Floor name - przykład: nazwa migracji',
        'camel' => 'Camel name',
        'camel_plural' => 'Liczba mnoga dla camel name - przykład: routes/web.php'
    ]

];
