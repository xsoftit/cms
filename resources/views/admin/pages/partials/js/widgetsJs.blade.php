@section('js')
    @parent
    <script>
        body = $('body');
        let widgetModals = {
            'add': $('#add-widget-modal'),
            'move': $('#move-widget-modal'),
        };
        let placementTarget;
        let currentSection;
        let currentColumn;
        let draggable,droppable;
        let formData = new FormData();

        $(document).ready(function () {
            // body.on('click', '.widget-preview', function () {
            //     currentTab = $(this).closest('.tab-pane');
            // });
            body.on('click', '.add-widget', function () {
                currentSection = $(this).attr('data-section');
                currentColumn = $(this).attr('data-column');
                placementTarget = $(this);
                widgetModals.add.modal('show');
            });

            widgetModals.add.on('click', '#add-widget-button', function () {
                let data = [];
                data['name'] = widgetModals.add.find("input[name='name']").val();
                data['widgetTemplateId'] = widgetModals.add.find('#widget_template_select').val();
                if (validateForm($(this).parents('form'))) {
                    addWidgetToSection(data);
                }
            });


            body.on('click', '.widget-preview.widget-preview-edit ', function (e) {
                let modal = $($(this).attr('data-target'));
                let widget_id = $(this).attr('data-id');
                modal.find('#widget-id').val(widget_id);
                modal.find('.nav-item:first-child .nav-link').click();
                modal.find('.modal-title').html($(this).attr('data-name'));
                getWidgetContent(widget_id, modal);
            });

            body.on('click', '.widget-modal #deleteWidget', function () {
                let modal = $(this).parents('.widget-modal');
                let deleteContent = modal.find('.delete-content');
                deleteContent.css('display', 'flex');
            });

            body.on('click', '.widget-modal #deleteWidgetCancel', function () {
                let modal = $(this).parents('.widget-modal');
                let deleteContent = modal.find('.delete-content');
                deleteContent.css('display', 'none');
            });

            body.on('click', '.widget-modal #deleteWidgetConfirm', function () {
                let modal = $(this).parents('.widget-modal');
                let widget_id = modal.find('#widget-id').val();
                let deleteContent = modal.find('.delete-content');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    data: {
                        widget_id: widget_id
                    },
                    url: '{{route('admin.sections.deleteWidget')}}',
                    success: function (data) {
                        modal.modal('hide');
                        body.find('.widget-preview[data-id="' + widget_id + '"]').remove();
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    },
                    complete: function () {
                        deleteContent.css('display', 'none');
                    }
                });
            });


            body.on('click', '.widget-modal #submit-widget', function () {
                let modal = $(this).parents('.widget-modal');
                let widget_id = modal.find('#widget-id').val();
                let widget_preview = $('.widget-preview[data-id="' + widget_id + '"]');
                let data = modal.find('.data');
                $.each(data, function () {
                    let attrName = $(this).attr('name');
                    let dataValue = $(this).val();
                    let el = $(this);
                    if (Array.isArray(dataValue)) {
                        let newDataValue = '';
                        $.each(dataValue, function (i, v) {
                            newDataValue += v + ' ';
                        })
                        dataValue = newDataValue;
                    }
                    fillFormData(el, dataValue, attrName);
                });
                setWidgetContent(widget_id, formData, widget_preview, modal);
            });

            body.on('show.bs.modal', '.widget-modal', function () {
                let modal = $(this);
                let tag = modal.find('select[name="tag"]');
                checkTag(modal, tag);
                if (modal.find('.route-selector').length > 0) {
                    modal.find('.route-selector').find('.outside-route').click();
                }
            });

            body.on('hide.bs.modal', '.widget-modal', function () {
                $.each($(this).find('.data'), function () {
                        if (!$(this).hasClass('no-clear')) {
                            if ($(this).is('select')) {
                                if ($(this).attr('multiple') || $(this).hasClass('select-data-ajax')) {
                                    $(this).val('');
                                } else {
                                    $(this).find('option:first-child').prop('selected', 'selected');
                                }
                                $(this).trigger('change');
                            }
                            else if ($(this).attr('type') === 'checkbox') {
                                $(this).removeAttr('checked');
                            } else {
                                $(this).val('');
                            }
                        }
                    }
                );
                $(this).find('.manager-preview').html('');
                let radio = $(this).find('#outside-route');
                if (radio) {
                    radio.click();
                }
            });

            body.on('change', '.widget-modal select[name="tag"]', function () {
                let tag = $(this);
                let modal = tag.parents('.widget-modal');
                checkTag(modal, tag);
            });

        });

        //----------------------FUNCTIONS----------------------

        function resetFormData() {
            formData = new FormData();
        }

        function fillFormData(el, dataValue, attrName) {
            let formDataName = 'data';
            if (dataValue) {
                dataValue = dataValue.replace(/(?:\r\n|\r|\n)/g, "<br>");
            } else {
                dataValue = '';
            }
            if (el.attr('name') === 'existing_items') {
                formData.append('data[existing_items][]', dataValue);
            } else if (el.attr('type') === 'checkbox') {
                formData.append(formDataName + '[' + attrName + ']', el.is(':checked'));
            } else {
                if (attrName.includes('.')) {
                    let splitName = attrName.split('.');
                    $.each(splitName, function (index, value) {
                        formDataName += '[' + value + ']';
                    });
                    formData.append(formDataName, el.val());
                } else {
                    formDataName += '[' + attrName + ']';
                    formData.append(formDataName, dataValue);
                }
            }
        }

        function checkTag(modal, tag) {
            if (tag.val()) {
                modal.find('#classes,#styles').removeAttr('disabled');
            } else {
                modal.find('#classes,#styles').attr('disabled', 'disabled');
                modal.find('#classes,#styles').val('');
            }
        }

        function addWidgetToSection(data) {
            let url = '{{route('admin.sections.addWidget')}}';
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                data: {
                    widget_template_id: data['widgetTemplateId'],
                    name: data['name'],
                    section_id: currentSection,
                    column: currentColumn,
                    page_language_id: currentPageLangId
                },
                url: url,
                success: function (data) {
                    placementTarget.before(data.preview);
                    widgetModals.add.modal('hide');
                    widgetModals.add.find('input').val('');
                    showAlert("{{__('app.widgets.alerts.added')}}", 'success');
                    initWidgetsDropable();
                },
                error: function (data) {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            });
        }


        function getWidgetContent(widget_id, modal) {
            resetFormData();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    widget_id: widget_id,
                    page_language_id: currentPageLangId
                },
                method: "POST",
                url: '{{route('admin.widgetContents.getContent')}}',
                success: function (data) {
                    modal.find('.preview-group').hide();
                    modal.find('input[name="content-data"]').val(JSON.stringify(data.content));
                    $.each(data.content, function (index, value) {
                        if (value) {
                            if (value.files) {
                                modal.find('.preview-group').show();
                                insertFile(index, value, modal);
                            } else {
                                let element = modal.find('#' + index);
                                if (element.length <= 0) {
                                    element = modal.find('*[name="' + index + '"]');
                                }
                                if(!element.hasClass('static')){
                                    if(index === 'is_gallery'){
                                        if(data.content[value]){
                                            let option = $('<option selected></option>').val(data.content[value][0]).text(data.content[value][1]);
                                            let gallerySelect = modal.find('*[name="' + value + '"]');
                                            gallerySelect.append(option).trigger('change');
                                        }
                                    }
                                    if (element.is('select')) {
                                        if (element.attr('multiple')) {
                                            element.find('option').each(function () {
                                                if (value.includes($(this).val())) {
                                                    $(this).prop('selected', 'selected')
                                                }
                                            });
                                        } else {
                                            element.val(value);
                                        }
                                        element.trigger('change');
                                    }
                                    else if (element.attr('type') === 'checkbox') {
                                        if (value === 'true') {
                                            element.attr('checked', 'checked');
                                        }
                                    } else {
                                        element.val(value);
                                    }
                                }
                            }
                        }
                    });
                    modal.modal('show');
                },
                error: function (data) {
                    alert("{{__('app.alerts.ajaxError')}}");
                }
            });
        }

        function setWidgetContent(widget_id, contents, widget_preview, modal) {
            contents.set('widget_id', widget_id);
            contents.set('page_language_id', currentPageLangId);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: contents,
                enctype: 'multipart/form-data',
                cache: false,
                contentType: false,
                processData: false,
                type: "POST",
                dataType: 'json',
                url: '{{route('admin.widgetContents.saveContent')}}',
                success: function (data) {
                    if (data['status'] === 'updated') {
                        dataInsert(data['contents'], widget_preview, '');
                        modal.modal('hide');
                        showAlert("{{__('app.widgets.alerts.updated')}}", 'success');
                    }
                },
                error: function (data) {
                    alert("{{__('app.alerts.ajaxError')}}");
                }
            }).done(function () {
                resetFormData();
            });
        }

        function dataInsert(data, widget_preview, key) {
            $.each(data, function (index, value) {
                if (typeof value == 'object') {
                    dataInsert(value, widget_preview, setKey(key, index));
                } else {
                    widget_preview.find('#' + setKey(key, index).replace('.', '-')).html(value);
                }
            });
            key = '';
        }

        function setKey(key, index) {
            if (key) {
                return key + '.' + index;
            }
            return index;
        }

        function insertFile(index, data, modal) {
            let fileBox = modal.find('#' + index.replace('.', '-') + '-preview-modal');
            let deletable = fileBox.attr('data-deletable');
            fileBox.html("");
            let files = [];
            $.each(data.files, function (index, value) {
                files.push(value['id']);
                let block = '';
                if (value.type === 'image' || value.type === 'icon') {
                    block = '<img src="' + value['path'] + '">';
                }
                else if (value.type === 'video') {
                    block = '<video width="100%" height="100%"><source src="' + value['path'] + '" type="video/mp4"/></video>'
                }
                else {
                    block = '<img src="/admin/images/file.png"/>';
                }
                let toAppend = '<li class="ui-state-default list-group-item" data-id="' + value['id'] + '">' +
                    '<div class="gallery-image-preview-modal">' +
                    block +
                    '</div>' +
                    '<span>' + value['name'] + '</span>' +
                    (deletable ? '<div class="remove_item float-right"><i class="afs af-times"></i></div>' : '') +
                    '</li>'
                fileBox.append(toAppend);
            });
            if (data.type === 'multi') {
                $('#multiInput[name="' + index + '"]').val(JSON.stringify(files));
                fileBox.sortable({
                    axis: "y",
                    placeholder: "list-group-item",
                    forcePlaceholderSize: true,
                });
            } else {
                $('#singleInput[name="' + index + '"]').val(JSON.stringify(files));
            }
            fileBox.disableSelection();
        }

        function initWidgetsDropable() {
            @if(auth()->user()->hasPermission('widgets.edit'))
            $('.widget-preview-edit').draggable({
                delay: 200,
                appendTo: "body",
                snapTolerance: 300,
                revert: "invalid",
            });
            $('.add-widget').droppable({
                classes: {
                    "ui-droppable-hover": "active"
                },
                tolerance: "pointer",
                drop: function (event, ui) {
                    draggable = ui.draggable;
                    droppable = $(this);
                    widgetModals.move.modal('show');
                }
            });
            @endif
        }

        widgetModals.move.on('click','#move-widget-button',function(){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                data: {
                    widget_id: draggable.attr('data-id'),
                    section_id: droppable.attr('data-section'),
                    column: droppable.attr('data-column')
                },
                url: '{{route('admin.widgetContents.move')}}',
                success: function (data) {
                    let newDraggable = draggable.clone();
                    draggable.remove();
                    droppable.before(newDraggable);
                    widgetModals.move.modal('hide');
                    newDraggable.css('top',0);
                    newDraggable.css('left',0);
                    initWidgetsDropable();
                },
                error: function (data) {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            });
        });

        widgetModals.move.on('click','#copy-widget-button',function(){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                data: {
                    widget_id: draggable.attr('data-id'),
                    section_id: droppable.attr('data-section'),
                    column: droppable.attr('data-column')
                },
                url: '{{route('admin.widgetContents.copy')}}',
                success: function (data) {
                    let newDraggable = draggable.clone();
                    newDraggable.attr('data-id',data.widget_id);
                    droppable.before(newDraggable);
                    widgetModals.move.modal('hide');
                    newDraggable.css('top',0);
                    newDraggable.css('left',0);
                    initWidgetsDropable();
                },
                error: function (data) {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            });
        });
        widgetModals.move.on('hide.bs.modal',function(){
            draggable.css('top',0);
            draggable.css('left',0);
        });

    </script>
@endsection
