<?php

namespace Xsoft\Cms\Observers;

use Illuminate\Support\Facades\Storage;
use Xsoft\Cms\Models\File;

class FileObserver
{

    public function deleting(File $file)
    {
        Storage::disk(config('cms.FILES_DISK'))->delete($file->storage_path);
    }

}
