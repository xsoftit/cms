@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.articles.title.create')}}
@endsection

@section('title')
    {{__('app.articles.title.create')}}
@endsection
@section('subtitle')
    {{__('app.articles.subtitle.create')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.articles.breadcrumbs.index'),route('admin.articles.index')],[__('app.articles.breadcrumbs.create')]) !!}
@endsection

@section('page-content')
    <form action="{{route('admin.articles.store')}}" method="post">
        @csrf
        @method('POST')
        <div class="form-group">
            <label>{{__('app.articles.fields.name')}}</label>
            <input type="text" name="name" value="{{old('name')}}" class="form-control">
        </div>
        <div class="form-group mt20">
            <div class="form-control">
                @include('cms::admin.filemanager.manager',[
                    'name' => 'main_image',
                    'label' => __('app.articles.fields.main_image').'*',
                    'type' => 'single',
                    'fileTypes' => ['image'],
                    'data' => old('main_image'),
                    'required' => 'true'
                ])
            </div>
        </div>
        @include('cms::admin.partials.gallerySelect')
        <div class="form-group">
            <label>{{__('app.articles.fields.start_date')}}</label>
            <input id="start_date" type="text" class="form-control" name="start_date" value="{{old('start_date')}}"
                   autocomplete="off">
        </div>
        <div class="form-group">
            <label>{{__('app.articles.fields.end_date')}}</label>
            <input id="end_date" type="text" class="form-control" name="end_date" value="{{old('end_date')}}"
                   autocomplete="off">
        </div>
        @php($languages = LanguageHelper::getAllFront())
        <ul class="nav nav-tabs mt20">
            @foreach($languages as $language)
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-{{$language}}">{{$language}}</a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content" id="myTabContent">
            @foreach($languages as $language)
                <div class="tab-pane fade" id='tab-{{$language}}'>
                    <div class="form-group">
                        <label>{{__('app.articles.fields.title')}}</label>
                        <input type="text" name="contents[{{$language}}][title]" class="form-control"
                               value="{{old('contents['.$language.'][title]')}}">
                    </div>
                    <div class="form-group">
                        <label>{{__('app.articles.fields.abstract')}}</label>
                        <textarea type="text" name="contents[{{$language}}][abstract]"
                                  class="form-control no-jodit">{{old('contents['.$language.'][abstract]')}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>{{__('app.articles.fields.content')}}</label>
                        <textarea type="text" name="contents[{{$language}}][content]"
                                  class="form-control jodit">{{old('contents['.$language.'][content]')}}</textarea>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="form-group h-20">
            <button type="submit" class="btn btn-success form float-right">{{__('app.save')}}</button>
        </div>
    </form>
@endsection

@section('js')
    @parent
    <script src="/admin/vendor/admin-panel/js/plugins/bootstrap-datepicker.min.js"></script>
    <script src="/admin/js/bootstrap-datepicker-locales.js"></script>
    <script>
        $('#start_date,#end_date').datepicker({
            format: "yyyy-mm-dd",
            weekStart: 1,
            language: userLanguage,
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true
        });
    </script>
@endsection
