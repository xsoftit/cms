<select name="{{$name}}[NUMBER_PLACEHOLDER][type]">
    <option value="string" selected>string</option>
    <option value="text">text</option>
    <option value="longText">longText</option>
    <option value="integer">integer</option>
    <option value="double">double</option>
    <option value="float">float</option>
    <option value="boolean">boolean</option>
    <option value="timestamp">timestamp</option>
    <option value="date">date</option>
    <option value="time">time</option>
    <option value="year">year</option>
    <option value="binary">binary</option>
</select>
