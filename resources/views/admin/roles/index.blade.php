@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.roles.title.index')}}
@endsection

@section('title')
    {{__('app.roles.title.index')}}
@endsection

@section('subtitle')
    {{__('app.roles.subtitle.index')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make(
        [__('app.dashboard.title'),route('admin.dashboard')],
        [__('app.roles.title.index')])
    !!}
@endsection

@section('page-content')
    @if(auth()->user()->hasPermission('roles.create'))
        <div class="row">
            <div class="col-12">
            <span data-toggle="modal" data-target="#role-create"
                  class="btn btn-outline-success float-right">{{__('app.add')}}</span>
            </div>
        </div>
    @endif
    @dataTable($table)
@endsection

@section('modals')
    @if(auth()->user()->hasPermission('roles.create'))
        @include('cms::admin.roles.modals.create')
    @endif
@endsection
