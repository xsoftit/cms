<div class="modal fade" id="user-role" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('app.users.changeRole')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.users.change.role',['user' => $user])}}" method="POST"
                  enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="role">{{__('app.users.fields.role')}}*</label>
                                <select class="form-control" name="role" required>
                                    @foreach($roles as $role)
                                        <option value="{{$role->name}}"
                                                @if(old('role',$user->role()->name) == $role->name) selected @endif>{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success">
                        {{__('app.save')}}
                    </button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                        {{__('app.close')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
