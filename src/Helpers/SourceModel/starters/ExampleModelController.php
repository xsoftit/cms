<?php

namespace App\Http\Controllers;

use App\ExampleModel;
use App\Services\ExampleModelService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ExampleModelController extends Controller
{
    protected $exampleModelService;

    public function __construct()
    {
        $this->exampleModelService = new ExampleModelService();
    }

    public function index(Request $request)
    {
        $table = $this->exampleModelService->getTable($request);

        return view('admin.exampleModel.index', compact('table'));
    }

    public function indexAjax(Request $request)
    {
        $table = $this->exampleModelService->getTable();
        $output = $table->output($request);
        $this->exampleModelService->prepareOutput($output);

        return $output;
    }

    public function create()
    {
        return view('admin.exampleModel.create');
    }

    public function store(Request $request)
    {
        $exampleModel = $this->exampleModelService->create($request);
        alert(__('app.exampleModels.alerts.added'), 'success');
        return redirect()->route('admin.exampleModels.edit', ['exampleModel' => $exampleModel]);
    }

    public function edit(ExampleModel $exampleModel)
    {
        return view('admin.exampleModel.edit', compact('exampleModel'));
    }

    public function update(Request $request, ExampleModel $exampleModel)
    {
        $exampleModel = $this->exampleModelService->update($request, $exampleModel);
        alert(__('app.exampleModels.alerts.updated'), 'success');
        if ($request->has('back')) {
            return redirect()->route('admin.exampleModels.index');
        }
        return redirect()->route('admin.exampleModels.edit', ['exampleModel' => $exampleModel]);
    }

    public function destroy(ExampleModel $exampleModel)
    {
        $exampleModel->delete();
        return redirect()->route('admin.exampleModels.index');
    }
}
