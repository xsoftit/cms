<div class="form-group">
    <label>{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</label>
    <input type="text" name="contents[{\{$language}}][{{$field->getName()}}]" class="form-control"
           value="{\{old('contents['.$language.'][{{$field->getName()}}]')}}">
</div>
