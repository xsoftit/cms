<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    "in" => ":attribute jest niewłaściwy",
    "ip" => ":attribute musi być poprawnym adresem IP",
    "url" => ":attribute ma zły format",
    "date" => ":attribute nie jest poprawną datą",
    "file" => ":attribute musi być plikiem",
    "ipv4" => ":attribute musi być poprawnym adresem IPv4",
    "ipv6" => ":attribute musi być poprawnym adresem IPv6",
    "json" => ":attribute musi być poprawnym JSON'em",
    "same" => ":attribute i :other muszą być takie same",
    "after" => ":attribute musi być datą większą niż :date",
    "alpha" => ":attribute może zawierać tylko litery",
    "array" => ":attribute musi być tablicą",
    "email" => ":attribute musi być poprawnym adresem email",
    "image" => ":attribute musi być obrazem",
    "mimes" => ":attribute musi być plikiem z rozszerzenim :values",
    "regex" => ":attribute ma zły format",
    "before" => ":attribute musi być datą mniejszą niż :date",
    "digits" => ":attribute mus mieć :digits cyfr",
    "exists" => ":attribute jest niewłaściwy",
    "filled" => ":attribute nie może być pusty",
    "not_in" => ":attribute jest niewłaściwy",
    "string" => ":attribute musi być tekstem",
    "unique" => ":attribute jest zajęty",
    "boolean" => ":attribute musi być prawdą lub fałszem",
    "integer" => ":attribute musi być liczbą całkowitą",
    "numeric" => ":attribute musi być cyfrą",
    "present" => ":attribute musi być obecny",
    "accepted" => ":attribute musi być zaakceptowany",
    "distinct" => ":attribute ma duplikat",
    "in_array" => ":attribute nie istnieje w :other",
    "required" => ":attribute jest wymagany",
    "timezone" => ":attribute musi być strefą czasową",
    "uploaded" => "Błąd wysyłki :attribute",
    "alpha_num" => ":attribute może zawierać tylko litery i cyfry",
    "confirmed" => "Potwierdzenie pola  :attribute jest nieprawidłowe",
    "different" => ":attribute i :other muszą być inne",
    "mimetypes" => ":attribute musi być plikiem z rozszerzenim :values",
    "not_regex" => ":attribute ma zły format",
    "active_url" => ":attribute nie jest poprawnym adresem URL",
    "alpha_dash" => ":attribute może zawierać tylko litery, cyfry, myślniki i podkreślenia",
    "dimensions" => ":attribute ma złe wymiary",
    "date_format" => ":attribute musi być w formacie :format",
    "required_if" => ":attribute jest wymagany gdy :other = :value",
    "required_with" => ":attribute jest wymagany gdy :values jest obecny",
    "after_or_equal" => ":attribute musi być datą większą równą :date",
    "digits_between" => ":attribute musi być między :min a :max",
    "before_or_equal" => ":attribute musi być datą mniejszą równą :date",
    "required_unless" => ":attribute jest wymagany chyba że :other = :value",
    "required_without" => ":attribute jest wymagany gdy :values jest nieobecny",
    "required_with_all" => ":attribute jest wymagany gdy :values są obecne",
    "required_without_all" => ":attribute jest wymagany gdy :values są nieobecne",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'name' => [
            "unique" => ":object z taką nazwą już istnieje",
            "required" => "Pole nazwa jest wymagane"
        ],
        'meta_title' => [
            "required" => "Pole Tytuł strony jest wymagane",
        ],
        'language' => [
            "required" => "Pole Język jest wymagane",
        ],
        'code' => [
            "unique" => ":object z takim kodem już istnieje",
            "required" => "Pole Kod jest wymagane"
        ],
        'email' => [
            "unique" => ":object z takim emailem już istnieje",
            "required" => "Pole Email jest wymagane"
        ],
        'role' => [
            "exists" => "Podana rola nie istnieje",
            "required" => "Pole Rola jest wymagane"
        ],
        'password' => [
            "required" => "Pole Hasło jest wymagane",
        ],
        'image' => [
            "required" => "Pole Obraz jest wymagane",
        ],
        'video' => [
            "required" => "Pole Video jest wymagane",
        ],
        'icon' => [
            "required" => "Pole Ikona jest wymagane",
        ],
    ],

];
