<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('page_id');
            $table->string('language');
            $table->string('name');
            $table->string('slug');
            $table->string('meta_title');
            $table->text('meta_description')->nullable();
            $table->boolean('active')->default(0);
            $table->boolean('is_main')->default(0);
            $table->timestamps();
            $table->index('slug');
            $table->index('active');
            $table->index('is_main');
            $table->index('language');
            $table->bigInteger('source_model_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_languages');
    }
}
