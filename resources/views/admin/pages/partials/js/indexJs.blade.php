@section('js')
    @parent
    <script>
        body = $('body');
        pageListInitTop= null;
        buttonsInitTop = null;
        let pageLanguageModals = {
            'create': $('#add-new-page'),
            'activate': $('#activate-confirm'),
            'deactivate': $('#deactivate-confirm'),
            'editDetails': $('#edit-details'),
            'addLanguageVersion': $('#add-language-version-modal'),
            'publish': $('#publish-confirm-modal'),
        };
        let currentPageLangId;

        $(document).on('scroll', function () {
            checkFixed();
        });

        $(document).ready(function () {
            checkFixed();
            body.on('click', '.show-page-edit', function () {
                let element = $(this);
                if (!$(this).hasClass('active')) {
                    let page_language_id = element.attr('data-id');
                    let lang_code = element.attr('data-lang');
                    let loader = $('#edit-preview-' + lang_code).siblings('.loader');
                    loader.show();
                    localStorage.setItem('lastPage', page_language_id);
                    reloadContent(page_language_id, lang_code, function () {
                        $('.show-page-edit').removeClass('active');
                        element.addClass('active');
                        loader.hide();
                        body.on('click', '#add-new-section', function () {
                            sectionModals.add.find("input[name='page_language_id']").val(page_language_id);
                            sectionModals.add.find('#page-section-preview').html('<div style="width: 100%" class="text-center">\n' + "{{__('app.pages.form.pageSectionTemplateChoice')}}" + '\n</div>');
                            sectionModals.add.modal('show');
                        });
                    });
                }
            });

            loadLastPage();

            body.on('click', '#edit-details-button', function () {
                // let modal = body.find('#edit-details');
                let loader = $(this).closest('.preview-box').find('.loader');
                loader.show();
                preparePageDetailsModal(pageLanguageModals.editDetails, $(this).attr('data-id'));
                loader.hide();
                pageLanguageModals.editDetails.modal('show');
            });

            body.on('click', '#submit-details', function () {
                // let modal = body.find('#edit-details');
                let loader = $(this).closest('.preview-box').find('.loader');
                loader.show();
                updateDetails(pageLanguageModals.editDetails);
                loader.hide();
                pageLanguageModals.editDetails.modal('hide');
            });

            body.on('click', '#activate-button', function () {
                pageLanguageModals.activate.find('#activate-confirm-button').attr('data-id', $(this).attr('data-id'));
                pageLanguageModals.activate.find('#activate-confirm-button').attr('data-lang', $(this).attr('data-lang'));
                pageLanguageModals.activate.modal('show');
            });
            body.on('click', '#deactivate-button', function () {
                pageLanguageModals.deactivate.find('#deactivate-confirm-button').attr('data-id', $(this).attr('data-id'));
                pageLanguageModals.deactivate.find('#deactivate-confirm-button').attr('data-lang', $(this).attr('data-lang'));
                pageLanguageModals.deactivate.modal('show');
            });

            body.on('click', '#deactivate-confirm-button', function () {
                let page_language_id = $(this).attr('data-id');
                let language = $(this).attr('data-lang');
                deactivatePage(page_language_id, language);
            });
            body.on('click', '#activate-confirm-button', function () {
                let page_language_id = $(this).attr('data-id');
                let lang = $(this).attr('data-lang');
                activatePage(page_language_id, lang);
                pageLanguageModals.activate.modal('hide');
            });

            body.on('click', '#edit-details #deletePage', function () {
                let deleteContent = pageLanguageModals.editDetails.find('.delete-content');
                deleteContent.css('display', 'flex');
            });

            body.on('click', '#edit-details #deletePageCancel', function () {
                let deleteContent = pageLanguageModals.editDetails.find('.delete-content');
                deleteContent.css('display', 'none');
            });

            body.on('click', '.add-language-version', function () {
                // let modal = $('#add-language-version-modal');
                pageLanguageModals.addLanguageVersion.find('input[name="page_id"]').val($(this).attr('data-id'));
                pageLanguageModals.addLanguageVersion.find('input[name="origin_lang"]').val($(this).attr('data-lang'));
                getOptionsForLanguagesSelect(pageLanguageModals.addLanguageVersion, $(this).attr('data-id'));
                pageLanguageModals.addLanguageVersion.modal('show');
            });

            body.on('click', '.preview-page', function () {
                currentPageLangId = $(this).attr('data-id');
                previewPage();
            });

            body.on('click', '.publish-version', function () {
                currentPageLangId = $(this).attr('data-id');
                pageLanguageModals.publish.modal('show');
            });

            pageLanguageModals.publish.on('click', '#publish-confirm-button', function () {
                publishPage();
            });

        });

        //----------------------FUNCTIONS----------------------

        function loadLastPage() {
            let lastPage = localStorage.getItem('lastPage');
            let element = body.find('.show-page-edit[data-id="' + lastPage + '"]');
            if (element.length > 0) {
                element.parents('.card').find('.card-header').find('button').click();
                element.click();
            } else {
                element = body.find('.card-header');
                element = $(element[0]).find('button');
                if (element.length > 0) {
                    element.click();
                    localStorage.removeItem('lastPage');
                }
            }
        }

        function reloadContent(page_language_id, lang_code, callback) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    page_language_id: page_language_id,
                    lang_code: lang_code
                },
                method: "POST",
                url: '{{route('admin.pages.preview')}}',
                success: function (data) {
                    let preview = data.preview;
                    currentPageLangId = page_language_id;
                    $('#edit-preview').html(preview);
                    // $('#update_preview').html(data.template);
                    $('#edit-preview').find('.sections-sortable').sortable({
                        axis: 'y',
                        placeholder: "section-preview",
                        forcePlaceholderSize: true,
                        handle: ".af-arrows-alt",
                        stop: function () {
                            let data = $(this).sortable('toArray', {attribute: 'data-id'});
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                                },
                                data: {
                                    sort: data,
                                    page_language_id: page_language_id
                                },
                                method: "POST",
                                url: '{{route('admin.sections.sort')}}',
                            });
                        }
                    });
                    $('#edit-preview').find('.sections-sortable li').each(function () {
                        if (localStorage.getItem('section-' + $(this).attr('data-id'))) {
                            $(this).find('.section-preview').css('height','0');
                            $(this).find('.section-preview').css('padding','0');
                            $(this).find('.section-preview').addClass('overflow-hidden');
                            $(this).find('.collapse-trigger').css('transform', 'rotate(180deg)');
                        }
                    });
                    initWidgetsDropable();
                    callback();
                },
                error: function (data) {
                    alert("{{__('app.alerts.ajaxError')}}");
                }
            });
        }

        function preparePageDetailsModal(modal, page_language_id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    page_language_id: page_language_id,
                },
                method: "POST",
                url: '{{route('admin.pages.getPageDetails')}}',
                success: function (data) {
                    fillPageDetailsModal(modal, data.pageLanguage, data.options);
                },
                error: function (data) {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            });
        }

        function fillPageDetailsModal(modal, pageLanguage, options) {
            modal.find("input[name='page_language_id']").val(pageLanguage.id);
            modal.find("input[name='name']").val(pageLanguage.name);
            modal.find("textarea[name='meta_description']").val(pageLanguage.meta_description);
            modal.find("input[name='meta_title']").val(pageLanguage.meta_title);
            modal.find("input[name='url']").val(pageLanguage.url);
            modal.find(".url-span").html(pageLanguage.host);
            if(pageLanguage.source_model_id){
                modal.find('.url-input-row').hide();
                modal.find('.url-info').show();
                modal.find('.is-main-checkbox').hide();
            }else{
                modal.find('.url-input-row').show();
                modal.find('.url-info').hide();
                modal.find('.is-main-checkbox').show();
            }
            $.each(modal.find('.options input'), function () {
                if ($(this).attr('type') === 'checkbox') {
                    $(this).attr('checked', false);
                } else {
                    $(this).val('');
                }
            });
            $.each(options, function (i, v) {
                let input = modal.find("input[name='options[" + i + "]']");
                if (input.attr('type') === 'checkbox') {
                    if (v === 'true') {
                        input.attr('checked', true);
                    }
                } else {
                    input.val(v);
                }
            });
            if (!pageLanguage.active) {
                modal.find('#deletePage').show();
            } else {
                modal.find('#deletePage').hide();
            }
            let checkbox = modal.find("input[name='is_main']");
            if (pageLanguage.active) {
                if (pageLanguage.is_main) {
                    checkbox.attr('checked', true);
                } else {
                    checkbox.attr('checked', false);
                }
                checkbox.removeAttr('disabled');
            } else {
                checkbox.attr('checked', false);
                checkbox.attr('disabled', 'disabled');
            }
        }

        function updateDetails(modal) {
            let data = new FormData();
            $.each(modal.find('input,textarea'), function () {
                let el = $(this);
                let name = el.attr('name');
                if (el.attr('type') == 'checkbox') {
                    data.append(name, el.is(':checked'));
                } else {
                    data.append(name, el.val());
                }
            });
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: data,
                enctype: 'multipart/form-data',
                cache: false,
                contentType: false,
                processData: false,
                type: "POST",
                dataType: 'json',
                url: '{{route('admin.pages.updateDetails')}}',
                success: function (data) {
                    if (data['reload']) {
                        location.reload();
                    } else {
                        showAlert(data.alert, data.class);
                    }
                },
                error: function (data) {
                    modal.modal('hide');
                    showAlert(data, 'danger');
                }
            });
        }

        function deactivatePage(page_language_id, language) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    page_language_id: page_language_id,
                    language: language,
                },
                method: "POST",
                url: '{{route('admin.pages.deactivatePage')}}',
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                    showAlert(data, 'danger');
                }
            });
        }

        function activatePage(page_language_id, language) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    page_language_id: page_language_id,
                    language: language,
                },
                method: "POST",
                url: '{{route('admin.pages.activatePage')}}',
                success: function (data) {
                    reloadContent(page_language_id, language, function () {
                        showAlert("{{__('app.pages.alerts.activated')}}", 'success');
                    });
                    $('.show-page-edit[data-id="' + page_language_id + '"]').removeClass('element-no-active');
                },
                error: function (data) {
                    showAlert(data, 'danger');
                }
            });
        }

        function getOptionsForLanguagesSelect(modal, page_language_id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    page_language_id: page_language_id,
                },
                method: "POST",
                url: '{{route('admin.pages.getLanguagesSelect')}}',
                success: function (data) {
                    modal.find('#languages-select').html(data.select);
                    modal.find('#languages-select select').selectInit();
                },
                error: function (data) {
                    showAlert(data, 'danger');
                }
            });
        }

        function publishPage() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    page_language_id: currentPageLangId,
                },
                method: "POST",
                url: '{{route('admin.pages.publish')}}',
                success: function (data) {
                    pageLanguageModals.publish.modal('hide');
                    showAlert('{{__('app.pages.alerts.published')}}', 'success');
                },
                error: function (data) {
                    showAlert(data, 'danger');
                }
            });
        }

        $.fn.getOffset = function (elementTop) {
            var viewportTop = $(window).scrollTop() + 56;
            return viewportTop - elementTop;
        };

        function checkFixed() {
            let pageList = $('.pages-list .accordion');
            let buttons = $('.preview-buttons .buttons');
            if (pageList.length) {
                if(!pageListInitTop){
                    pageListInitTop = pageList.offset().top;
                }
                let pageListOffset = pageList.getOffset(pageListInitTop)
                if (pageListOffset > 0) {
                    let width = pageList.width();
                    pageList.addClass('fixed-parent');
                    pageList.css('top', $('header').height() + 10);
                    pageList.css('width', width);
                } else {
                    pageList.removeClass('fixed-parent');
                    pageList.css('top', 0);
                    pageList.css('width', '100%');
                }
            }
            if (buttons.length) {
                if(!buttonsInitTop){
                    buttonsInitTop = buttons.offset().top;
                }
                let buttonsOffset = buttons.getOffset(buttonsInitTop);
                if (buttonsOffset > 0) {
                    let width = buttons.width();
                    buttons.addClass('fixed-parent');
                    buttons.css('top', $('header').height());
                    buttons.css('width', width);
                } else {
                    buttons.removeClass('fixed-parent');
                    buttons.css('top', 0);
                    buttons.css('width', '100%');
                }
            }
        }

        function previewPage() {
            let url = '{{route('admin.pages.frontPreview',['pageLanguage'=>'PAGE_LANGUAGE_ID_PLACEHOLDER'])}}';
            url = url.replace('PAGE_LANGUAGE_ID_PLACEHOLDER', currentPageLangId);
            window.open(url,"","fullscreen=yes");
        }

    </script>
@endsection
