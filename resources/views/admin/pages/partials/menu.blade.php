<div class="form-group">
    <div class="row">
        <div
            class="@if(auth()->user()->hasPermission('page_config.edit')) col-6 @else col-12 @endif">
            <label>{{__('app.pageConfig.manage.'.$menu->type)}}</label>
            <ul class="front-menu-sortable list-group" data-type="{{$menu->type}}">
                @include('cms::admin.pages.partials.menuList',['items' => json_decode($menu->items,true)])
            </ul>
        </div>
        @if(auth()->user()->hasPermission('page_config.edit'))
            <div class="col-2">
                <div class="menu-add">
                    <button data-lang="{{$lang}}" type="button" data-type="{{$menu->type}}"
                            class="btn btn-outline-info add-menu-item"><i
                            class="af af-arrow-left"></i>{{__('app.pageConfig.add')}}</button>
                </div>
            </div>
            <div class="col-4 add-menu-fields" data-lang="{{$lang}}" data-type="{{$menu->type}}">
                <div class="form-group">
                    <label class="label-text">{{__('app.pageConfig.name')}}</label>
                    <input name="name" class="form-control item-name">
                </div>
                {!! RouteSelector::build(__('app.pageConfig.link'),'link.'.$lang.'.'.$menu->type) !!}
            </div>
        @endif
    </div>
</div>
