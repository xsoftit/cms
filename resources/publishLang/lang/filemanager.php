<?php

if(auth()->user()) {
    $langKey = 'language-fields-' . auth()->user()->language;
    if (Illuminate\Support\Facades\Cache::has($langKey)) {
        $fields = Illuminate\Support\Facades\Cache::get($langKey);
        if (key_exists('filemanager', $fields)) {
            return $fields['filemanager'];
        }
    }
}
