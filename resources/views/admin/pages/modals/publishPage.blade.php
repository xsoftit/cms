@section('modals')
    @parent
    <div class="modal fade" id="publish-confirm-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.publishConfirm')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="page_language_id">
                    <div class="text-center">
                        {{__('app.pages.questions.publish')}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="publish-confirm-button" data-id="" data-lang=""
                            class="btn btn-outline-success">{{__('app.confirm')}}</button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">{{__('app.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
