<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Xsoft\Cms\Requests\User\ChangePasswordRequest;
use Xsoft\Cms\Requests\User\ChangeRoleRequest;
use Xsoft\Cms\Requests\User\StoreRequest;
use Xsoft\Cms\Requests\User\UpdateRequest;
use Xsoft\Cms\Models\Role;
use Xsoft\Cms\Services\Admin\UserService;
use App\User;
use Hermit\Logs\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function index(Request $request)
    {
        $table = $this->userService->getTable($request);
        $roles = Role::all();

        return view('cms::admin.users.index', compact('table', 'roles'));
    }

    public function indexAjax(Request $request)
    {
        $table = $this->userService->getTable();
        $output = $table->output($request);
        $this->userService->prepareOutput($output);

        return $output;
    }

    public function store(StoreRequest $request)
    {
        $user = $this->userService->create($request);

        alert()->success(__('app.users.alerts.saved', ['name' => $user->name]));
        Log::write('User', 'Added new user', 1, $user->id . ': ' . $user->name);
        return redirect()->route('admin.users.edit', compact('user'));
    }

    public function edit(User $user)
    {
        $roles = Role::all();
        return view('cms::admin.users.edit', compact('user', 'roles'));
    }

    public function update(UpdateRequest $request, User $user)
    {
        $user = $this->userService->update($request, $user);

        alert()->success(__('app.users.alerts.saved', ['name' => $user->name]));
        Log::write('User', 'Updated user', 1, $user->id . ': ' . $user->name);
        if ($request->has('back')) {
            return redirect()->route('admin.users.index');
        }
        return redirect()->route('admin.users.edit', compact('user'));
    }

    public function destroy(User $user)
    {
        Log::write('User', 'Deleted user', 1, $user->id . ': ' . $user->name);
        $user->delete();
        return redirect()->route('admin.users.index');
    }

    public function stateChange(User $user)
    {
        if ($user->active) {
            $user->active = 0;
            alert()->success(__('app.users.alerts.deactivated', ['name' => $user->name]));
            Log::write('User', 'Deactivated user', 1, $user->id . ': ' . $user->name);
        } else {
            $user->active = 1;
            alert()->success(__('app.users.alerts.activated', ['name' => $user->name]));
            Log::write('User', 'Activated user', 1, $user->id . ': ' . $user->name);
        }
        $user->save();

        return redirect()->route('admin.users.edit', compact('user'));
    }

    public function passwordChange(ChangePasswordRequest $request, User $user)
    {
        $user->password = Hash::make($request->get('password'));
        $user->save();

        alert()->success(__('app.users.alerts.passwordChanged'));
        Log::write('User', 'Changed user password', 1, $user->id . ': ' . $user->name);
        return redirect()->route('admin.users.edit', compact('user'));
    }

    public function roleChange(ChangeRoleRequest $request, User $user)
    {
        $user->syncRoles($request->get('role'));

        alert()->success(__('app.users.alerts.roleChanged'));
        Log::write('User', 'Changed user role', 1, $user->id . ': ' . $user->name . ' - ' . $request->get('role'));
        return redirect()->route('admin.users.edit', compact('user'));
    }

    public function languageChange($lang)
    {
        $user = auth()->user();
        $user->language = $lang;
        $user->save();

        Log::write('User', 'Changed user language', 1, $user->id . ': ' . $user->name . ' - ' . $user->language);
        return redirect()->back();
    }

    public function sidebarToggle(Request $request)
    {
        $user = auth()->user();
        $user->sidebar_toggle = $request->get('sidebar_toggle');
        $user->save();

        return response()->json(['data' => $request->get('sidebar_toggle')]);
    }

    public function profile(){
        $user = auth()->user();
        return view('cms::admin.users.edit', compact('user'));
    }

}
