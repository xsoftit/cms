@component('cms::admin.templates.widgetModal',get_defined_vars())
    <input id="source_model" name="source_model" type="text" value="Article" class="data static" hidden>
    <input id="source_content_type" name="source_content_type" type="text" value="list" class="data static" hidden>
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.elementsAmount')}}</label>
        <input id="amount" name="amount" type="number" min="0" class="form-control data">
    </div>
@endcomponent
