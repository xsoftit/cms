<?php

namespace Xsoft\Cms\Services\Admin;

use Xsoft\Cms\Models\Language;
use Xsoft\DataTables\DataTable;
use Illuminate\Support\Facades\Cache;

class LanguageService
{
    public function getTable($request = null)
    {
        $table = new DataTable('languages', 'admin.config.languages.indexAjax');
        $table->setOptions(['searching' => false, 'lengthChange' => false, 'info' => false]);
        $table->setColumns([
            [__('app.languages.fields.name'), 'name'],
            [__('app.languages.fields.code'), 'code'],
            [__('app.languages.fields.active'), 'active'],
        ]);
        if (auth()->user()->hasPermission('admin_languages.edit') || auth()->user()->hasPermission('admin_languages.exportImport')) {
            $table->addColumn(["", 'id', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width1']]);
        }
        $table->addToQuery(
            ['where', 'front', 0]
        );
        return $table;
    }

    public function prepareOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            $buttons = '';
            if (auth()->user()->hasPermission('admin_languages.edit') || auth()->user()->hasPermission('admin_languages.exportImport')) {
                $buttons .= '<div class="d-flex actions">';
                if (auth()->user()->hasPermission('admin_languages.exportImport')) {
                    $buttons .= '<a class="btn btn-outline-info btn-outline-rounded btn-outline-icon " href="' . route("admin.config.languages.export", [$row->id]) . '">' . __('app.export') . '</a>';
                }
                if (auth()->user()->hasPermission('admin_languages.edit')) {
                    $buttons .= '<a class="btn btn-outline-light btn-outline-rounded btn-outline-icon " href="' . route("admin.config.languages.edit", [$row->id]) . '">' . __('app.edit') . '</a>';
                }
                $buttons .= '</div>';
            }
            if ($buttons) {
                $output['aaData'][] = [
                    $row->name,
                    $row->code,
                    $row->active ? __('app.languages.fields.active') : '<span class="text-danger">' . __('app.languages.fields.inactive') . '</span>',
                    $buttons
                ];
            } else {
                $output['aaData'][] = [
                    $row->name,
                    $row->code,
                    $row->active ? __('app.languages.fields.active') : '<span class="text-danger">' . __('app.languages.fields.inactive') . '</span>',
                ];
            }
        }
    }

    public function create($request)
    {
        $language = Language::create([
            'name' => $request->get('name'),
            'code' => strtolower($request->get('code')),
            'fields' => json_encode($request->get('fields'))
        ]);

        self::cacheSave($language);

        return $language;
    }

    public function update($request, Language $language)
    {
        $language->update([
            'name' => $request->get('name'),
            'code' => strtolower($request->get('code')),
            'fields' => json_encode($request->get('fields'))
        ]);

        self::cacheSave($language);

        return $language;
    }

    public function prepareArrayForExport(Language $language)
    {
        $data = [];
        $data['code'] = $language->code;
        $data['name'] = $language->name;
        $data['fields'] = json_decode($language->fields, true);
        return $data;
    }

    public static function cacheSave($language)
    {
        $key = 'language-fields-' . $language->code;
        $fields = json_decode($language->fields, true);
        Cache::forever($key, $fields);
    }
}
