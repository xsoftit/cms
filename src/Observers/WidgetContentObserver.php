<?php

namespace Xsoft\Cms\Observers;

use Xsoft\Cms\Models\WidgetContent;

class WidgetContentObserver
{
    /**
     * Handle the widget content "created" event.
     *
     * @param  \Xsoft\Cms\Models\WidgetContent $widgetContent
     * @return void
     */
    public function created(WidgetContent $widgetContent)
    {
        //
    }

    /**
     * Handle the widget content "updated" event.
     *
     * @param  \Xsoft\Cms\Models\WidgetContent $widgetContent
     * @return void
     */
    public function updated(WidgetContent $widgetContent)
    {
        //
    }

    /**
     * Handle the widget content "deleted" event.
     *
     * @param  \Xsoft\Cms\Models\WidgetContent $widgetContent
     * @return void
     */
    public function deleted(WidgetContent $widgetContent)
    {
        //
    }

    public function deleting(WidgetContent $widgetContent)
    {
        //
    }

    /**
     * Handle the widget content "restored" event.
     *
     * @param  \Xsoft\Cms\Models\WidgetContent $widgetContent
     * @return void
     */
    public function restored(WidgetContent $widgetContent)
    {
        //
    }

    /**
     * Handle the widget content "force deleted" event.
     *
     * @param  \Xsoft\Cms\Models\WidgetContent $widgetContent
     * @return void
     */
    public function forceDeleted(WidgetContent $widgetContent)
    {
        //
    }
}
