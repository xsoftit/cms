<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Hermit\Logs\Log;
use Illuminate\Http\Request;
use Xsoft\Cms\Models\Gallery;
use Xsoft\Cms\Services\Admin\GalleryService;

class GalleryController extends Controller
{
    protected $galleryService;

    public function __construct()
    {
        $this->galleryService = new GalleryService();
    }

    public function index(Request $request)
    {
        $table = $this->galleryService->getTable($request);

        return view('cms::admin.galleries.index', compact('table'));
    }

    public function indexAjax(Request $request)
    {
        $table = $this->galleryService->getTable();
        $output = $table->output($request);
        $this->galleryService->prepareOutput($output);

        return $output;
    }

    public function create()
    {
        return view('cms::admin.galleries.create');
    }

    public function store(Request $request)
    {
        $gallery = $this->galleryService->create($request);
        alert(__('app.galleries.alerts.created'), 'success');
        return redirect()->route('admin.galleries.edit', ['gallery' => $gallery]);
    }

    public function edit(Gallery $gallery)
    {
        return view('cms::admin.galleries.edit', compact('gallery'));
    }

    public function update(Request $request, Gallery $gallery)
    {
        $gallery = $this->galleryService->update($gallery, $request);
        alert(__('app.galleries.alerts.updated', ['name' => $gallery->name]), 'success');
        if ($request->has('back')) {
            return redirect()->route('admin.galleries.index');
        }
        return redirect()->route('admin.galleries.edit', ['gallery' => $gallery]);
    }

    public function destroy(Gallery $gallery)
    {
        $gallery->delete();
        alert(__('app.galleries.alerts.deleted'), 'success');
        return redirect()->route('admin.galleries.index');
    }

    public function getForSelect(Request $request)
    {
        $data = [];
        $galleries = Gallery::where('name','like','%'.$request->get("search").'%')->get();
        foreach ($galleries as $gallery) {
            $data[] = [
                'id' => $gallery->id,
                'text' => $gallery->name
            ];
        }
        return json_encode($data);
    }
}
