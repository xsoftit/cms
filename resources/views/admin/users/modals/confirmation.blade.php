<div class="modal fade" id="user-confirmation" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('app.users.title.confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.users.change.state',['user' => $user->id])}}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <p class="modal-text">
                                @if($user->active)
                                    {!! __('app.users.confirmationQuestion',['action' => __('app.users.deactivate'),'user' => $user->name]) !!}
                                @else
                                    {!! __('app.users.confirmationQuestion',['action' =>  __('app.users.activate'),'user' => $user->name]) !!}
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success">
                        {{__('app.confirm')}}
                    </button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                        {{__('app.discard')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
