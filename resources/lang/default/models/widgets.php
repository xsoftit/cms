<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content' => 'Content',
    'layout' => 'Layout',
    'deleteQuestion' => 'Are you sure you want delete this widget?',
    'deleteWarning' => 'It will be removed from all related sites',
    'noImages' => 'No images',
    'labels' => [
        'chosen' => 'Chosen',
        'text' => 'Text',
        'textarea' => 'Text',
        'shortText' => 'Text',
        'buttonText' => 'Button Text',
        'backgroundText' => 'Background Text',
        'source' => 'Source',
        'buttonLink' => 'Button Link',
        'header' => 'Header',
        'class' => 'Style',
        'number' => 'Number',
        'type' => 'Type',
        'existingImage' => 'Existing image',
        'replaceImage' => 'Replace existing image with new one',
        'existingImages' => 'Existing images in gallery',
        'addImages' => 'Add images',
        'galleryImages' => 'Images in gallery',
        'image' => 'Image',
        'gallery' => 'Gallery',
        'tag' => 'Surrounding tag',
        'classes' => 'Classes for defined tag',
        'styles' => 'Styles for defined tag',
        'icon' => 'Icon',
        'video' => 'Video',
        'width' => 'Width',
        'height' => 'Height',
        'autoplay' => 'Autoplay',
        'loop' => 'Loop',
        'elementsAmount' => 'Elements amount',
        'muted' => 'Muted',
        'onPage' => 'Elements on page',
        'personName' => 'Person name',
        'personPosition' => 'Person position',
        'latitude' => 'Latitude',
        'longitude' => 'Longitude',
        'namePlaceholder' => 'Name placeholder',
        'emailPlaceholder' => 'Email placeholder',
        'descriptionPlaceholder' => 'Description placeholder',
        'extraInfo' => 'Extra info',
        'projectsLink' => 'Link to projects',
        'buttonTitle' => 'Link title'
    ],
    'tags' => [
        'none' => 'None',
        'div' => 'Div',
        'header1' => 'Header 1',
        'header2' => 'Header 2',
        'header3' => 'Header 3',
        'header4' => 'Header 4',
        'header5' => 'Header 5',
        'label' => 'Label',
        'span' => 'Span',
        'paragraph' => 'Paragraph',
        'listItem' => 'List item'
    ],
    'classes' => [
        'primary' => 'Primary',
        'secondary' => 'Secondary',
        'big' => 'Big',
        'border' => 'Border'
    ],
    'texticonTypes' => [
        'number' => 'Number',
        'freeIcon' => 'Icon',
        'headerIcon' => 'Icon in header',
    ],
    'url' => [
        'selectPage' => 'Select Page to generate URL from',
        'enterUrl' => 'Enter URL'
    ],
    'alerts' => [
        'updated' => 'Successfully updated',
        'added' => 'Successfully added'
    ],
];
