<?php

namespace Xsoft\Cms\Helpers;

use Xsoft\Cms\Models\PageLanguage;

class RouteSelectHelper
{
    public static function build($label = null, $name = 'link', $value = null)
    {
        if (!$label) {
            $label = __('app.widgets.routeSelect');
        }
        $pageLanguages = PageLanguage::where('active', 1)->get();
        return view('cms::admin.partials.routeSelect', ['pageLanguages' => $pageLanguages, 'label' => $label, 'name' => $name,'value' => $value]);
    }

    public static function pageToLink($pageLanguage){
        return $pageLanguage->url;
    }
}
