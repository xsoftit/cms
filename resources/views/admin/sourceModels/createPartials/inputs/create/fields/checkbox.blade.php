<div class="animated-checkbox">
    <label>
        <input type="checkbox" name="{{$field->getName()}}">
        <span
            class="label-text text-capitalize">{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</span>
    </label>
</div>
