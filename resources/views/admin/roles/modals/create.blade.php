<div class="modal fade" id="role-create" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('app.roles.title.create')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.roles.store')}}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <label for="name">{{__('app.roles.fields.name')}}*</label>
                            <input class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success">
                        {{__('app.save')}}
                    </button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                        {{__('app.close')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
