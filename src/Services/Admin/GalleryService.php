<?php

namespace Xsoft\Cms\Services\Admin;

use Xsoft\Cms\Models\Gallery;
use Xsoft\Cms\Models\GalleryItem;
use Xsoft\Cms\Models\GalleryItemContent;
use Xsoft\DataTables\DataTable;

class GalleryService
{
    public function getTable($request = null)
    {
        $table = new DataTable('galleries', 'admin.galleries.indexAjax');
        $table->setOptions(['searching' => false, 'lengthChange' => false, 'info' => false, 'order' => '0,asc']);
        $table->setColumns([
            ['ID', 'galleries.id', ['class' => 'width5']],
            [__('app.galleries.fields.name'), 'galleries.name'],
        ]);
        if (auth()->user()->hasPermission('galleries.edit')) {
            $table->addColumn(["", 'id', ['orderable' => 'false', 'searchable' => 'false', 'class' => 'width1']]);
        }
        return $table;
    }

    public function prepareOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            if (auth()->user()->hasPermission('galleries.edit')) {
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                    "<a href='" . route('admin.galleries.edit', [$row->id]) . "' class='btn btn-outline-light'>" . __('app.edit') . "</a>"
                ];
            } else {
                $output['aaData'][] = [
                    $row->id,
                    $row->name,
                ];
            }
        }
    }

    public function create($request)
    {
        $gallery = Gallery::create([
            'name' => $request->get('name'),
            'options' => $request->get('options') ? json_encode($request->get('options')) : null
        ]);
        $items = $request->get('items');
        if ($items) {
            foreach ($items as $key => $item) {
                if ($item['photo']) {
                    self::createGalleryItem($gallery, $item, $key);
                }
            }
        }
        return $gallery;
    }

    public function update(Gallery $gallery, $request)
    {
        $gallery->update([
            'name' => $request->get('name'),
            'options' => $request->get('options') ? json_encode($request->get('options')) : null
        ]);
        $ids = [];
        $items = $request->get('items');
        if ($items) {
            $order = 0;
            foreach ($items as $key => $item) {
                $order++;
                $galleryItem = $gallery->items()->where('id', $key)->first();
                if ($galleryItem) {
                    self::updateGalleryItem($galleryItem, $item, $order);
                    $ids[] = $galleryItem->id;
                } else {
                    if ($item['photo']) {
                        $newGalleryItem = self::createGalleryItem($gallery, $item, $order);
                        $ids[] = $newGalleryItem->id;
                    }
                }
            }
        }
        foreach ($gallery->items as $item){
            if(!in_array($item->id,$ids)){
                $item->delete();
            }
        }
        return $gallery;
    }

    private static function createGalleryItem($gallery, $item, $order)
    {
        $galleryItem = GalleryItem::create([
            'photo' => $item['photo'],
            'order' => $order,
            'gallery_id' => $gallery->id,
            'options' => key_exists('options', $item) ? json_encode($item['options']) : null
        ]);
        if (key_exists('contents', $item)) {
            foreach ($item['contents'] as $lang => $content) {
                if ($content['title']) {
                    self::createGalleryItemContent($galleryItem, $content, $lang);
                } else {
                    alert($order.'.'.__('app.galleries.alerts.titleEmpty', ['lang' => $lang]), 'warning');
                }
            }
        }
        return $galleryItem;
    }

    private static function updateGalleryItem($galleryItem, $item, $order)
    {
        $galleryItem->update([
            'photo' => $item['photo'],
            'order' => $order,
            'options' => key_exists('options', $item) ? json_encode($item['options']) : null
        ]);
        if (key_exists('contents', $item)) {
            foreach ($item['contents'] as $lang => $content) {
                $galleryContent = $galleryItem->getContent($lang,false);
                if($galleryContent){
                    self::updateGalleryItemContent($galleryContent, $content);
                }else{
                    if ($content['title']) {
                        self::createGalleryItemContent($galleryItem, $content, $lang);
                    } else {
                        alert($order.'.'.__('app.galleries.alerts.titleEmpty', ['lang' => $lang]), 'warning');
                    }
                }
            }
        }
    }

    private static function createGalleryItemContent($galleryItem, $content, $lang)
    {
        GalleryItemContent::create([
            'title' => $content['title'],
            'gallery_item_id' => $galleryItem->id,
            'language' => $lang,
            'url' => key_exists('url', $content) ? $content['url'] : null,
            'description' => key_exists('description', $content) ? $content['description'] : null,
            'options' => key_exists('options', $content) ? json_encode($content['options']) : null
        ]);
    }

    private static function updateGalleryItemContent($galleryItemContent, $content)
    {
        $galleryItemContent->update([
            'title' => $content['title'],
            'url' => key_exists('url', $content) ? $content['url'] : null,
            'description' => key_exists('description', $content) ? $content['description'] : null,
            'options' => key_exists('options', $content) ? json_encode($content['options']) : null
        ]);
    }
}
