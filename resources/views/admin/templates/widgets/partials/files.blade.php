<div class="images-flex">
    @forelse($widgetService->getFiles($files,6) as $file)
        @if($file['type'] == 'image' || $file['type'] == 'icon')
            <div class="image-box">
                <img class="image-preview" src="{{$file['path']}}">
            </div>
        @endif
        @if($file['type'] == 'video')
            <div class="image-box">
                <video height="150" muted preload="auto" controls="true">
                    <source src="{{$file['path']}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
        @endif
    @empty
        {{__('app.widgets.noImages')}}
    @endforelse
</div>
