<?php

return [
    'menu' => 'Galleries',
    'name' => 'Gallery',
    'chooseGallery' => 'Choose gallery...',
    'title' => [
        'index' => 'Galleries',
        'edit' => 'Edit gallery :name',
        'create' => 'Create new gallery'
    ],
    'subtitle' => [
        'index' => 'System galleries list',
        'edit' => ':name gallery edit form',
        'create' => 'Gallery creation form'
    ],
    'fields' => [
        'name' => 'Name',
        'photo' => 'Photo',
        'title' => 'Title',
        'url' => 'Link',
        'description' => 'Description',
        'items' => 'Gallery items'
    ],
    'alerts' => [
        'created' => 'Gallery created successfully!',
        'updated' => 'Gallery :name updated successfully!',
        'deleted' => 'Gallery deleted successfully!',
        'titleEmpty' => 'Title is empty [:lang] '
    ],
    'buttons' => [
        'addItem' => 'Add gallery item'
    ]
];
