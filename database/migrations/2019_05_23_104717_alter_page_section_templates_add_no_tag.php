<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPageSectionTemplatesAddNoTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_section_templates', function (Blueprint $table) {
            $table->boolean('tag')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('page_section_templates', 'tag')) {
            Schema::table('page_section_templates', function (Blueprint $table) {
                $table->dropColumn('tag');
            });
        }
    }
}
