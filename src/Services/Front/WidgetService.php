<?php

namespace Xsoft\Cms\Services\Front;

use Xsoft\Cms\Helpers\FrontProcessors\FrontWidget;
use Xsoft\Cms\Models\File;
use Xsoft\Cms\Models\Gallery;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\SourceModel;
use Xsoft\Cms\Models\Widget;

class WidgetService
{
    static public function regular($widget, $data)
    {
        return [
            'data' => $data,
            'blade' => $widget->template->blade_name,
        ];
    }

    static public function list($widget, $data, $pageLanguage)
    {
        $where = [];
        $where[] = ['active', 1];
        if (!empty($data['source_model'])) {
            $model = 'App\\' . $data['source_model'];
            if ($model::CONTENT_MODEL) {
                $where[] = ['language', $pageLanguage->language];
            }
            if (key_exists('amount', $data)) {
                $data['data'] = $model::where($where)->limit($data['amount'])->get();
            } else {
                $data['data'] = $model::where($where)->get();
            }
        }
        return [
            'data' => $data,
            'blade' => $widget->template->blade_name,
        ];
    }

    static public function content($widget, $data, $pageLanguage)
    {
        if ($data['source_model'] == $pageLanguage->source_model) {
            $data['object'] = $pageLanguage->object;
        }
        return [
            'data' => $data,
            'blade' => $widget->template->blade_name,
        ];
    }

    static public function gallery($widget, $data, $pageLanguage)
    {
        $galleryName = $data['is_gallery'];
        $galleryId = $data[$galleryName];
        $data[$galleryName] = [];
        if ($galleryId) {
            $gallery = Gallery::find($galleryId);
            if ($gallery) {
                foreach ($gallery->items->sortBy('order') as $key => $item) {
                    $photoArray = json_decode($item->photo, true);
                    if (key_exists(0, $photoArray)) {
                        $photo = File::find($photoArray[0]);
                        $content = $item->getContent($pageLanguage->language, false);
                        $item->photo = $photo;
                        $item->content = $content;
                        if ($photo) {
                            $data[$galleryName][$key] = $item;
                        }
                    }
                }
            }
        }
        return [
            'data' => $data,
            'blade' => $widget->template->blade_name,
        ];
    }

    static public function getFrontWidget(PageLanguage $pageLanguage, Widget $widget, $pageSectionId)
    {
        if ($widget->template->generate && !$pageLanguage->local && !$widget->is_global && \Illuminate\Support\Facades\File::exists(resource_path('views/front/rendered/' . $pageLanguage->id . '/' . $pageSectionId . '/' . $widget->id))) {
            $data = [
                'data' => ['PRE-RENDERED'],
                'view' => view('front.rendered.' . $pageLanguage->id . '.' . $pageSectionId . '.' . $widget->id)
            ];
        } else {
            $data = self::handleWidget($pageLanguage, $widget);
        }
        $frontWidget = new FrontWidget($data);
        return $frontWidget;
    }

    static public function handleWidget(PageLanguage $pageLanguage, Widget $widget)
    {
        $data = [];
        $data['page_language'] = $pageLanguage->language;
        $widget->getWidgetService()->prepareContent(json_decode($widget->getContents($pageLanguage->language), true), $data);
        foreach ($data as $dataKey => &$d) {
            if (is_array($d)) {
                if (key_exists('files', $d)) {
                    if ($d['type'] == 'multi') {
                        foreach ($d['files'] as $key => $file) {
                            $d['images'][$key] = $file;
                            $d['images'][$key]['path'] = $file['path'];
                            $d['images'][$key]['alt'] = $file['name'];
                        }
                    } else {
                        if (key_exists(0, $d['files'])) {
                            $data[$dataKey]['path'] = $d['files'][0]['path'];
                            $data[$dataKey]['alt'] = $d['files'][0]['name'];
                        }
                    }
                }
            }
        }
        if ($widget->template->slug == 'menu') {
            $data['active'] = $pageLanguage;
        }
        $service = 'Xsoft\\Cms\\Services\\Front\\WidgetService';
        $method = 'regular';
        if (key_exists('custom_front_model', $data)) {
            if (key_exists('custom_content_type', $data)) {
                $service = 'App\\Services\\Front\\' . $data['custom_front_model'] . 'Service';
                $method = $data['custom_content_type'];
            }
        } elseif (key_exists('source_model', $data)) {
            if (key_exists('source_content_type', $data)) {
                $sourceModel = SourceModel::where('model_name', $data['source_model'])->first();
                if ($sourceModel) {
                    $service = 'App\\Services\\Front\\' . $sourceModel->model_name . 'Service';
                    $method = $data['source_content_type'];
                } else {
                    $data['ERROR'] = 'SOURCE MODEL - ' . $data['source_model'] . ' - NOT FOUND';
                }
            }
        } elseif (key_exists('content_type', $data)) {
            //will soon be deprecated
            $method = $data['content_type'];
        }
        $data = $service::$method($widget, $data, $pageLanguage);
        return $data;
    }
}
