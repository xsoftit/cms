<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{

    protected $fillable = [
        'page_section_id',
        'placement_column',
        'placement_order',
        'tag',
        'classes',
        'style'
    ];

    // RELATIONS

    public function section()
    {
        return $this->belongsTo('Xsoft\Cms\Models\PageSection','page_section_id');
    }
}
