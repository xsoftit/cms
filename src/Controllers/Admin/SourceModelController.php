<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Xsoft\Cms\Helpers\SourceModel\Field;
use Xsoft\Cms\Helpers\SourceModel\SourceModel as SourceModelCreator;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\SourceModel;
use Xsoft\Cms\Services\Admin\SourceModelService;

class SourceModelController extends Controller
{
    protected $sourceService;

    public function __construct()
    {
        $this->sourceService = new SourceModelService();
    }

    public function index(Request $request)
    {
        $table = $this->sourceService->getTable($request);

        return view('cms::admin.sourceModels.index', compact('table'));
    }

    public function indexAjax(Request $request)
    {
        $table = $this->sourceService->getTable();
        $output = $table->output($request);
        $this->sourceService->prepareOutput($output);

        return $output;
    }

    public function change(SourceModel $sourceModel)
    {
        if ($sourceModel->singleObject) {
            $sourceModel->singleObject = 0;
        } else {
            $sourceModel->singleObject = 1;
        }
        $sourceModel->update();

        return redirect()->back();
    }

    public function getRouteTypes(Request $request)
    {
        $data = [];
        if (self::checkData(__('app.pages.name'), $request->get('search'))) {
            $data[] = ['id' => 'PageLanguage', 'text' => __('app.pages.name')];
        }
        $sourceModels = SourceModel::all();
        foreach ($sourceModels as $sourceModel) {
            $text = __('app.' . Str::plural($sourceModel->display_name).'.name');
            if (self::checkData($text, $request->get('search'))) {
                $data[] = [
                    'id' => $sourceModel->model_name,
                    'text' => $text
                ];
            }
        }
        return json_encode($data);
    }

    public function getRouteValues(Request $request)
    {
        $data = [];
        $params = $request->get('additionalParams');
        if (key_exists('class', $params)) {
            if ($params['class'] == 'PageLanguage') {
                $values = PageLanguage::all();
            } else {
                $class = 'App\\' . $params['class'];
                if (defined( $class . '::CONTENT_MODEL')) {
                    $class = "App\\" . $class::CONTENT_MODEL;
                }
                $values = $class::all();
            }
            foreach ($values as $value) {
                if (self::checkData($value->route_select_label, $request->get('search'))) {
                    $data[] = [
                        'id' => '/' . $value->url(),
                        'text' => $value->route_select_label
                    ];
                }
            }
        }
        return json_encode($data);
    }

    private static function checkData($data, $search)
    {
        if ($search) {
            if (strpos(strtolower($data), strtolower($search)) !== false) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    public function create()
    {
        return view('cms::admin.sourceModels.create');
    }

    public function store(Request $request)
    {
        $className = Str::ucfirst($request->get('className'));
        $sourceTable = $request->get('sourceTable') ? true : false;
        $languageContent = $request->get('languageContent') ? true : false;
        $singleObject = $request->get('singleObject') ? true : false;
        $sourceModel = new \Xsoft\Cms\Helpers\SourceModel\SourceModel($className, $sourceTable, $singleObject, $languageContent);
        foreach ($request->get('fields') as $field_data) {
            if ($this->validateFieldData($field_data)) {
                continue;
            }
            if ($languageContent && $field_data['name'] == 'slug') {
                continue;
            }
            $field = new Field($field_data['type'], $field_data['name'], $field_data['modifiers']);
            $sourceModel->addField($field);
        }
        if ($languageContent) {
            foreach ($request->get('contentFields') as $content_field_data) {
                if ($this->validateFieldData($content_field_data)) {
                    continue;
                }
                $field = new Field($content_field_data['type'], $content_field_data['name'], $content_field_data['modifiers']);
                $sourceModel->addContentField($field);
            }
        }
        $sourceModel->handle();
        sleep(2);
        Artisan::call('migrate', ['--seed' => true]);
        Artisan::call('make:cms:directories');
        $view = view('cms::admin.sourceModels.partials.summary', ['sourceModel' => $sourceModel]);
        $html = $view->render();
        File::put(resource_path('createdSourceModels/' . $sourceModel->getClassName() . '.html'), $html);
        return redirect()->route('admin.sourceModels.summary', ['className' => $sourceModel->getClassName()]);
    }

    private function validateFieldData(&$data)
    {
        $correct = true;
        if (!is_array($data)) {
            $correct = false;
        }
        if (!key_exists('type', $data) && !key_exists('name', $data)) {
            $correct = false;
        }
        if ($correct) {
            if (key_exists('default', $data['modifiers'])) {
                if ($data['modifiers']['default'] === null) {
                    unset($data['modifiers']['default']);
                }
            }
        }
        return !$correct;
    }

    public function generateNamesInputs($className)
    {
        $className = Str::ucfirst($className);
        $sourceModel = new SourceModelCreator($className, true, false, true);
        $names = $sourceModel->getAllNames();
        unset($names['class']);
        $view = view('cms::admin.sourceModels.partials.generatedNames', ['names' => $names]);
        return response()->json(['inputs' => $view->render()]);
    }

    public function summary($className)
    {
        $summary = 'Summary not found';
        if (File::exists(resource_path('createdSourceModels/' . $className . '.html'))) {
            $summary = File::get(resource_path('createdSourceModels/' . $className . '.html'));
        }
        return view('cms::admin.sourceModels.summary', ['summary' => $summary]);
    }
}
