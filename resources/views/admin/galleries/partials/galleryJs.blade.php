@section('js')
    @parent
    <script>
        let body = $('body');
        body.on('click', '.remove-galleries-item', function () {
            $(this).parents('.gallery-item').remove();
        });
        $( ".gallery-list" ).sortable({
            items: "> .gallery-item",
            helper: '.af-arrows-alt',
            axis: 'y'
        });

        $('#addItem').on('click', function () {
            let template = $('#galleryItemTemplate').html();
            let row = $('#addItemRow');
            let number = 0;
            $('.gallery-item').each(function(){
                let oldNumber = parseInt($(this).attr('data-id'));
                if(oldNumber >= number){
                    number = oldNumber + 1;
                    console.log(number);
                }
            });
            template = template.replace(new RegExp('NUMBER_PLACEHOLDER', 'g'), number);
            $(template).insertBefore(row);
            $('.gallery-item').find('.select-data-ajax').selectAjaxInit();
        });
    </script>
@endsection
