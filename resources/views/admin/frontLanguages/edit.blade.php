@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.frontLanguages.title.index')}} | {{__('app.frontLanguages.title.edit',['name' => $language->name])}}
@endsection

@section('title')
    {{__('app.frontLanguages.title.edit',['name' => $language->name])}}
@endsection

@section('subtitle')
    {{__('app.frontLanguages.subtitle.edit',['name' => $language->name])}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.frontLanguages.title.index'),route('admin.frontLanguages.index')],[$language->name]) !!}
@endsection

@section('page-content')
    @if(auth()->user()->hasPermission('front_languages.delete'))
        <div class="row">
            <div class="col-12">
                @if($language->active)
                    <span class="btn btn-outline-warning float-right" data-toggle="modal"
                          data-target="#language-confirmation">{{__('app.users.deactivate')}}</span>
                @else
                    <span class="btn btn-outline-info float-right" data-toggle="modal"
                          data-target="#language-confirmation">{{__('app.users.activate')}}</span>
                @endif
            </div>
        </div>
    @endif
    <form method="POST" class="forms-sample" action="{{route('admin.frontLanguages.update',[$language])}}">
        @csrf
        @method('POST')
        <h4>{{__('app.frontLanguages.data')}}</h4>
        <div class="form-group row">
            <div class="col-6">
                <label for="name">{{__('app.frontLanguages.fields.name')}} *</label>
                <input type="text" class="form-control" id="name" name="name" value="{{old('name',$language->name)}}">
            </div>
            <div class="col-6">
                <label for="name">{{__('app.frontLanguages.fields.code')}}</label>
                <input type="text" class="form-control" id="code" name="code"
                       value="{{old('code',$language->code)}}">
            </div>
        </div>
        <h4>{{__('app.frontLanguages.fields.fields')}}</h4>
        <div class="form-group row">
            <div class="col-9">
                <label for="name">{{__('app.search')}} </label>
                <input type="text" class="form-control" id="search">
            </div>
            <div class="col-3 form-check form-check-primary mt30">
                <div class="animated-checkbox">
                    <label>
                        <input type="checkbox" id="showEmpty">
                        <span class="label-text">{{__('app.frontLanguages.showEmpty')}}</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                @include('cms::admin.frontLanguages.partials.fields',['template' => $template, 'parentKey' => ''])
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <button type="button" class="btn btn-outline-danger float-left" data-toggle="modal"
                        data-target="#delete-front-language">{{__('app.delete')}}</button>
                <input type="submit" class="btn btn-outline-success float-right" value="{{__('app.save')}}" name="save">
                <input type="submit" class="btn btn-outline-secondary float-right" value="{{__('app.saveAndBack')}}"
                       name="back">
            </div>
        </div>
    </form>
@endsection

@section('modals')
    @if(auth()->user()->hasPermission('front_languages.delete'))
        @include('cms::admin.frontLanguages.modals.confirmation')
    @endif
@endsection

@section('js')
    @parent
    <script src="/js/hoverable-collapse.js"></script>
    <script>
        var showEmptyCheckbox = $('#showEmpty');
        var search = $('#search');

        showEmptyCheckbox.on('click', function () {
            if (showEmptyCheckbox.is(':checked')) {
                showEmpty();
            } else {
                showAll();
            }
        });

        search.on('keyup', function () {
            setTimeout(function () {
                filter(search.val());
            }, 500);
        });

        function filter(text) {
            if (showEmptyCheckbox.is(':checked')) {
                filterEmpty(text);
            } else {
                filterAll(text);
            }
        }

        function filterEmpty(text) {
            $('.accordion').each(function () {
                let accordion = $(this);
                let accordionHide = true;
                accordion.find('input').each(function () {
                    let input = $(this);
                    if (checkText(input.attr('placeholder'), text) || checkText(input.parent().siblings('label').text(), text)) {
                        accordionHide = false;
                        input.parent().parent().show()
                    } else {
                        input.parent().parent().hide()
                    }
                });
                if (accordionHide) {
                    accordion.hide();
                } else {
                    accordion.show();
                }
            });
        }

        function filterAll(text) {
            $('.accordion').each(function () {
                let accordion = $(this);
                let accordionHide = true;
                accordion.find('input').each(function () {
                    let input = $(this);
                    if (input.val()) {
                        if (checkText(input.val(), text) || checkText(input.parent().siblings('label').text(), text)) {
                            accordionHide = false;
                            input.parent().parent().show()
                        } else {
                            input.parent().parent().hide()
                        }
                    } else {
                        if (checkText(input.attr('placeholder'), text) || checkText(input.parent().siblings('label').text(), text)) {
                            accordionHide = false;
                            input.parent().parent().show()
                        } else {
                            input.parent().parent().hide()
                        }
                    }
                });
                if (accordionHide) {
                    accordion.hide();
                } else {
                    accordion.show();
                }
            });
        }

        function showEmpty() {
            $('.accordion').each(function () {
                let accordion = $(this);
                let accordionHide = true;
                accordion.find('input').each(function () {
                    let input = $(this);
                    if (!input.val()) {
                        accordionHide = false;
                    } else {
                        input.parent().parent().hide()
                    }
                });
                if (accordionHide) {
                    accordion.hide();
                }
            });
        }

        function showAll() {
            $('.accordion').each(function () {
                let accordion = $(this);
                accordion.show();
                accordion.find('input').each(function () {
                    $(this).parent().parent().show()
                });
            });
        }

        function checkText(value, text) {
            value = value.toLowerCase();
            text = text.toLowerCase();
            if (value.indexOf(text) !== -1) {
                return true;
            }
            return false;
        }
    </script>
@endsection
