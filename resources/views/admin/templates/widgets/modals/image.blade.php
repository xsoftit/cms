@component('cms::admin.templates.widgetModal',get_defined_vars())
    @include('cms::admin.filemanager.manager',[
    'name' => 'image',
    'label' => __('app.widgets.labels.image'),
    'type' => 'single',
    'fileTypes' => ['image']
    ])
@endcomponent
