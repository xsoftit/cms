@if(!empty($frontWidget->get('all')))
    @if($frontWidget->getTag())
        <{{$frontWidget->getTag()}}{!! ($frontWidget->getClasses()) ? ' class="'.$frontWidget->getClasses().'"' :''!!}{!! ($frontWidget->getStyles()) ? ' style="'.$frontWidget->getStyles().'"' :''!!}>
        @include('front.templates.widgets.'.$frontWidget->getBladeName(),['data'=>$frontWidget])
        </{{$frontWidget->getTag()}}>
    @else
        @include('front.templates.widgets.'.$frontWidget->getBladeName(),['data'=>$frontWidget])
    @endif
@else
    <div>Save widget in Administration Panel in order to initiate data.</div>
@endif

