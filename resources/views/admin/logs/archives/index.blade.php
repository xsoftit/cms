@extends('cms::admin.layouts.app')

@section('page-title')
    Log Archives
@endsection

@section('title')
    Log Archives
@endsection
@section('subtitle')
    This is your place
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],['Logs',route('logs.index')],['Log Archives']) !!}
@endsection

@section('page-content')
    @dataTable($dataTable)
@endsection
