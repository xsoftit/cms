<?php

return [
    'menu' => 'Galerie',
    'name' => 'galeria',
    'chooseGallery' => 'Wybierz galerię...',
    'title' => [
        'index' => 'Galerie',
        'edit' => 'Edycja galerii :name',
        'create' => 'Nowa galeria'
    ],
    'subtitle' => [
        'index' => 'Lista galerii systemu',
        'edit' => 'Formularz edycji galerii :name',
        'create' => 'Formularz tworzenia galerii'
    ],
    'fields' => [
        'name' => 'Nazwa',
        'photo' => 'Obraz',
        'title' => 'Tytuł',
        'url' => 'Link',
        'description' => 'Opis',
        'items' => 'Elementy galerii'
    ],
    'alerts' => [
        'created' => 'Galeria utworzona pomyślnie!',
        'updated' => 'Galeria :name zaktualizowana pomyślnie!',
        'deleted' => 'Galeria usunięta pomyślnie!',
        'titleEmpty' => 'Tytuł pusty [:lang] '
    ],
    'buttons' => [
        'addItem' => 'Dodaj element galerii'
    ]
];
