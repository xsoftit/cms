@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.articles.title.edit')}}
@endsection

@section('title')
    {{__('app.articles.title.edit')}}
@endsection
@section('subtitle')
    {{__('app.articles.subtitle.edit')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.articles.breadcrumbs.index'),route('admin.articles.index')],[__('app.articles.breadcrumbs.edit')]) !!}
@endsection

@section('page-content')
    <form action="{{route('admin.articles.update',['article'=>$article->id])}}" method="post">
        @csrf
        @method('POST')
        <div class="form-group">
            <label>{{__('app.articles.fields.name')}}</label>
            <input type="text" name="name" value="{{old('name',$article->name)}}" class="form-control">
        </div>
        <div class="form-group mt20">
            <div class="form-control">
                @include('cms::admin.filemanager.manager',[
                    'name' => 'main_image',
                    'label' => __('app.articles.fields.main_image').'*',
                    'type' => 'single',
                    'fileTypes' => ['image'],
                    'data' => old('main_image',$article->main_image),
                    'required' => 'true'
                ])
            </div>
        </div>
        @include('cms::admin.partials.gallerySelect',['value' => $article->gallery])
        <div class="form-group">
            <label>{{__('app.articles.fields.start_date')}}</label>
            <input id="start_date" type="text" class="form-control" name="start_date"
                   value="{{old('start_date',$article->start_date)}}"
                   autocomplete="off">
        </div>
        <div class="form-group">
            <label>{{__('app.articles.fields.end_date')}}</label>
            <input id="end_date" type="text" class="form-control" name="end_date"
                   value="{{old('end_date',$article->end_date)}}"
                   autocomplete="off">
        </div>
        @php($languages = LanguageHelper::getAllFront())
        <ul class="nav nav-tabs mt20">
            @foreach($languages as $language)
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-{{$language}}">{{$language}}</a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content" id="myTabContent">
            @foreach($languages as $language)
                @php($content = $article->getContent($language))
                <div class="tab-pane fade" id='tab-{{$language}}'>
                    <div class="form-group">
                        <label>{{__('app.articles.fields.title')}}</label>
                        <input type="text" name="contents[{{$language}}][title]" class="form-control"
                               value="{{old('contents['.$language.'][title]',$content->title)}}">
                    </div>
                    @if($content->slug)
                        @include('cms::admin.partials.url',[
                            'label' => __('app.articles.fields.url'),
                            'item' => $content,
                            'name' => 'contents['.$language.'][url]'
                        ])
                    @endif
                    <div class="form-group">
                        <label>{{__('app.articles.fields.abstract')}}</label>
                        <textarea type="text" name="contents[{{$language}}][abstract]"
                                  class="form-control no-jodit">{{old('contents['.$language.'][abstract]',$content->abstract)}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>{{__('app.articles.fields.content')}}</label>
                        <textarea type="text" name="contents[{{$language}}][content]" class="form-control jodit"
                        >{{old('contents['.$language.'][content]',$content->content)}}</textarea>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="animated-checkbox">
            <label>
                <input type="checkbox" name="active" {{$article->active?"checked='true'":""}}>
                <span
                    class="label-text text-capitalize">{{__('app.articles.fields.active')}}</span>
            </label>
        </div>
        <div class="form-group h-20">
            @if(auth()->user()->hasPermission('articles.delete'))
                @deleteButton(route('admin.articles.destroy',[$article->id]))
            @endif
            <input type="submit" class="btn btn-success float-right" value="{{__('app.save')}}" name="save">
            <input type="submit" class="btn btn-secondary float-right" value="{{__('app.saveAndBack')}}" name="back">
        </div>
    </form>
@endsection

@section('js')
    @parent
    <script src="/admin/vendor/admin-panel/js/plugins/bootstrap-datepicker.min.js"></script>
    <script src="/admin/js/bootstrap-datepicker-locales.js"></script>
    <script>
        $('#start_date,#end_date').datepicker({
            format: "yyyy-mm-dd",
            weekStart: 1,
            language: userLanguage,
            orientation: "bottom left",
            autoclose: true,
            todayHighlight: true
        });
    </script>
@endsection
