<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content' => 'Content',
    'layout' => 'Layout',
    'new' => 'New section',
    'copy' => 'Copy existing section',
    'deleteQuestion' => 'Are you sure you want delete this section?',
    'deleteWarning' => 'It will be removed from all related sites',
    'title' => [
        'create' => 'Add New Section',
        'edit' => 'Edit Section',
    ],
    'form' => [
        'pageChoice' => 'Choose page',
        'sectionChoice' => 'Choose section',
        'withContent' => 'With content',
        'withContentDescription' => 'Content will be taken from same language version',
        'name' => 'Section name',
        'active' => 'Is active',
        'pageSectionTemplateChoice' => 'Choose section template',
        'classes' => 'Classes for section tag',
        'styles' => 'Styles for section tag'
    ],
    'alerts' => [
        'updated' => 'Successfully updated',
        'added' => 'Successfully added',
    ],
];
