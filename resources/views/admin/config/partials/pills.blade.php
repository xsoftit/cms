<ul class="nav nav-tabs" id="pills-tab">
    <li class="nav-item">
        <a class="nav-link @if($active == 'edit') active @endif"
           href="{{route('admin.config.edit')}}">{{__('app.config.title')}}</a>
    </li>
    @if(auth()->user()->hasPermission('admin_languages.show'))
        <li class="nav-item">
            <a class="nav-link @if($active == 'language') active @endif"
               href="{{route('admin.config.languages.index')}}">{{__('app.languages.title.index')}}</a>
        </li>
    @endif
</ul>
