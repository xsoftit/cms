"responsive": 'true',
"pagingType": "simple_numbers",
"processing": true,
"serverSide": true,
"destroy": true,
"autoWidth": false,
"pageLength": 10,
"order": [[0, 'desc']],
"language": {
    "decimal": "",
    "emptyTable": "{{__('app.datatables.emptyTable')}}",
    "thousands": ",",
    "lengthMenu": "_MENU_",
    "loadingRecords": "{{__('app.datatables.loading')}}",
    "processing": "<img src='/admin/images/ajax.gif'/>",
    "searchPlaceholder": "{{__('app.datatables.searchPlaceholder')}}",
    "paginate": {
        "next": ">",
        "previous": "<"
    }
},

