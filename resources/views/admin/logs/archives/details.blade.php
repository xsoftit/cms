@extends('cms::admin.layouts.app')

@section('page-title')
    Log Archives Details
@endsection
@section('title')
    Log Archives Details
@endsection
@section('subtitle')
    {{$logArchive->comment}} {{$logArchive->from}} - {{$logArchive->to}}
@endsection

@section('css')
    @parent
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],['Logs',route('logs.index')],['Log Archives',route('logs.archives.index')],['Details']) !!}
@endsection

@section('page-content')
    <table border="1px" style="width: 100%;" id="table">
        <thead>
        <tr>
            <th style="width: 3%">ID</th>
            <th style="width: 5%">TYPE</th>
            <th style="width: 50%">DESCRIPTION</th>
            <th style="width: 10%">AUTHOR</th>
            <th style="width: 15%">COMMENT</th>
            <th style="width: 8%">IP</th>
            <th style="width: 8%">DATE</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $log)
            <tr>
                <td>{{$log->id}}</td>
                <td>{{$log->type}}</td>
                <td>{{$log->message}}</td>
                <td>{{$log->author_id}}</td>
                <td>{{$log->comments}}</td>
                <td>{{$log->ip}}</td>
                <td>{{$log->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('js')
    @parent
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#table').DataTable();
        })
    </script>
@endsection
