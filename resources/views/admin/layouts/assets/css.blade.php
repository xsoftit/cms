@section('css')
    <link href="/admin/vendor/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="/admin/vendor/jquery-ui/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/admin/vendor/jquery-ui/jquery-ui.theme.css" rel="stylesheet">
    <link href="/admin/vendor/fontawesome/css/all.min.css" rel="stylesheet">
    <link href="/admin/vendor/jodit/jodit.min.css" rel="stylesheet">
    <link href="/admin/vendor/admin-panel/css/adminPanel.css" rel="stylesheet">
    <link href="/admin/css/skin.css" rel="stylesheet">
    <link href="/admin/css/custom.css" rel="stylesheet">
@show
