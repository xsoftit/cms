<?php

return [
    'name' => 'Language',
    'menu' => 'Languages',
    'activate' => 'Activate',
    'deactivate' => 'Deactivate',
    'confirmationQuestion' => 'Are you sure that you want to <b>:action</b> language <b>:user?</b>',
    'title' => [
        'index' => 'Languages',
        'edit' => 'Edit :name site language',
        'create' => 'Create new site language',
        'confirmation' => 'Confirmation'
    ],
    'subtitle' => [
        'index' => 'Site languages list',
        'edit' => 'Translations for site static texts',
    ],
    'fields' => [
        'name' => 'Name',
        'code' => 'Code',
        'fields' => 'Fields',
        'active' => 'Active',
        'inactive' => 'Inactive'
    ],
    'showEmpty' => 'Show only empty fields',
    'data' => 'Data',
    'alerts' => [
        'saved' => 'Language :name saved successfully!',
        'activated' => 'Language :name activated!',
        'deactivated' => 'Language :name deactivated!',
        'isMainLanguage' => "You can't deactivate main Language!",
        'deleted' => 'Language :name deleted successfully!',
        'cantDelete' => [
            'pages' => 'Language :name cannot be deleted. There are pages in this language!',
            'config' => 'Language :name is used in page configuration and cannot be deleted!',
        ]
    ],
    'extraInfo' => [
        'noPages' => 'No pages',
        'noMain' => 'Main page not defined',
        'noActive' => 'Language is not active'
    ],
    'questions' => [
        'delete' => 'Do you want to delete this language?'
    ]
];
