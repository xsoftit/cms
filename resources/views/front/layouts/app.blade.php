<!DOCTYPE html>
<html lang="{{ $dataProcessor->getLanguage() }}">
@section('head')
    <head>
        @foreach($dataProcessor->getPageLanguage()->siblings as $sibling)
            <link rel="alternate" hreflang="{{$sibling->language}}" href="{{$sibling->url}}"/>
        @endforeach
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('meta-tags')
        @yield('custom-tags')
        @include('front.layouts.assets.css')
    </head>
@show
@section('body')
    <body id="front">
    @include('front.alert.bootstrap')
    <div id="scroll">
        @include('front.layouts.partials.content')
    </div>
    @include('front.layouts.assets.js')
    </body>
@show
</html>
