<?php

return [
    "menu" => "Konfiguracja",
    "title" => "Konfiguracja",
    "subtitle" => "Edycja ustawień systemu",
    'urls' => "URL'e",
    'categories' => [
        "page" => "Dane strony",
        "general" => "Główne"
    ],
    'fields' => [
        "logo1" => "Główne logo strony",
        "logo2" => "Dodatkowe logo strony",
        "adminLogo" => "Logo panelu administracyjnego",
        "companyName" => "Nazwa firmy",
        "defaultLang" => "Domyślny język panelu administracyjnego",
        "titlePrefix" => "Prefix tytułu stron",
        "twitterLink" => "Link do Fwittera",
        "youtubeLink" => "Link do YouTube",
        "companyEmail" => "Adres email firmy",
        "companyPhone" => "Numer telefonu firmy",
        "facebookLink" => "Link do Facebooka",
        "googleApiKey" => "Google Api Key",
        "companyAddress1" => "Adres firmy 1",
        "companyAddress2" => "Adres firmy 2",
        "urlWithLanguage" => "URL'e generowane z prefixem językowym",
        "defaultFrontLanguage" => "Domyślny język strony",
    ],
    'descriptions' => [
        'mainLogo' => '',
        'secondaryLogo' => '',
        'adminLogo' => ''
    ],
    'alert' => [
        "success" => "Konfiguracja zapisana pomyślnie"
    ]
];
