<?php

namespace Xsoft\Cms\Controllers\Admin\Hermit;

use Xsoft\Cms\Services\Hermit\LogService;
use Hermit\Logs\LogType;
use Hermit\Logs\MainLogController;
use Illuminate\Http\Request;

class LogController extends MainLogController
{

    protected $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function index()
    {
        $types = LogType::where('name', '!=', 'MAIN_PRIORITY')->get()->toArray();
        $tables = [];
        foreach ($types as $type) {
            $tables[$type['name']] = $this->logService->getLogsTable($type['name']);
        }
        return view('cms::admin.logs.index', compact('tables', 'types'));
    }

    public function indexAjax(Request $request)
    {
        $type = $request->get('type');
        $table = $this->logService->getLogsTable($type);
        $output = $table->output($request);
        $this->logService->prepareLogsOutput($output);
        return $output;
    }

    public function savePriorities(Request $request)
    {
        $new = $request->all();
        $logTypes = LogType::all();
        foreach ($logTypes as $logType) {
            if (key_exists($logType->name, $new)) {
                $logType->priority = $new[$logType->name];
                $logType->save();
            }
        }
        return redirect()->route('logs.index');
    }
}
