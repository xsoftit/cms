@component('cms::admin.templates.widgetModal',get_defined_vars())
    {*{SOURCE-MODEL}*}
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.WIDGET_LABEL')}}</label>
        <input id="OPTION_ID" name="OPTION_ID" class="form-control data">
    </div>
@endcomponent
