<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourceModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('source_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('model_name');
            $table->string('display_name');
            $table->boolean('in_use')->default(0);
            $table->string('section_template_slug');
            $table->string('widget_template_slug');
            $table->boolean('singleObject')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_models');
    }
}
