<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'inactive' => 'Account is inactive',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'password' => 'Password',
    'login' => 'Login',
    'email' => 'Email',
    'remember_me' => 'Remember me',
    'confirm_login' => 'Login',
    'forgot_password' => 'Forgot Your password?',
    'register' => 'Register',
    'name' => 'Naem',
    'confirm_register' => 'Register',
    'reset_password' => 'Reset Password',
    'reset_send' => 'Send reset link',
    'confirm_reset' => 'Reset'
];
