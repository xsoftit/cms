<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class CmsInitSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!Role::where('name', 'superadmin')->first()) {
            Role::create(['name' => 'superadmin']);
            echo 'Superadmin role created' . PHP_EOL;
        }
        if (!Role::where('name', 'admin')->first()) {
            Role::create(['name' => 'admin']);
            echo 'Admin role created' . PHP_EOL;
        }
    }
}
