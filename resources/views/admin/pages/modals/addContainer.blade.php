@section('modals')
    @parent
    <div class="modal fade" id="add-container-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.addContainer')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add-container-form">
                    <div class="modal-body">
                        <input hidden name="section_id" id="section_id">
                        <input hidden name="column_id" id="column_id">
                        <div class="form-group">
                            <label class="label-text">{{__('app.containers.labels.tag')}}</label>
                            <input id="container_tag" name="tag" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="label-text">{{__('app.containers.labels.classes')}}</label>
                            <input id="container_classes" name="classes" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="label-text">{{__('app.containers.labels.styles')}}</label>
                            <textarea id="container_styles" name="styles" class="form-control no-jodit"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="add-container-button" class="btn btn-outline-success"
                                style="margin: 0">{{__('app.create')}}</button>
                        <button type="reset" class="btn btn-outline-light"
                                data-dismiss="modal">{{__('app.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
