<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class WidgetContent extends Model
{
    protected $fillable = [
        'widget_id',
        'language',
        'content'
    ];

    public function widget()
    {
        return $this->belongsTo('Xsoft\Cms\Models\Widget');
    }

    public function get($key)
    {
        $content = json_decode($this->content, true);
        if (key_exists($key, $content)) {
            return $content[$key];
        }
        return null;
    }

}
