@if($data->get('path'))
    <video preload="auto" {{$data->get('width') ? 'width='.$data->get('width') : 'width=100%'}}
        {{($data->get('height'))? 'height='.$data['height']:''}}
        {{($data->get('autoplay') == 'true')?' autoplay=autoplay':''}}
        {{($data->get('loop') == 'true')? ' loop=loop':''}}
        {{($data->get('muted') == 'true') ? ' muted':''}}>
        <source src="{{$data->get('path')}}" type="video/mp4">
        Your browser does not support the video tag.
    </video>
@endif

