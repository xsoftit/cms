<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;
use Xsoft\Cms\Traits\UrlTrait;
use Illuminate\Support\Facades\URL;

class PageLanguage extends Model
{
    use UrlTrait;

    protected $fillable = [
        'language',
        'name',
        'slug',
        'meta_title',
        'meta_description',
        'active',
        'is_main',
        'versions',
        'page_id',
        'source_model_id',
        'source_model'
    ];

    // REALATIONS

    public function page()
    {
        return $this->belongsTo('Xsoft\Cms\Models\Page')->with('activeSections');
    }

    public function versions()
    {
        return $this->hasMany('Xsoft\Cms\Models\PageLanguageVersion');
    }

    public function sourceModel()
    {
        return $this->belongsTo('Xsoft\Cms\Models\SourceModel');
    }

    // ATTRIBUTES

    public function getSectionsAttribute()
    {
        return $this->page->sections;
    }

    public function getActiveSectionsAttribute()
    {
        return $this->page->activeSections;
    }

    public function getActiveVersionAttribute()
    {
        return $this->versions()->where('active', 1)->first();
    }

    public function getModelObjectsAttribute()
    {
        $model = $this->sourceModel->model_name;
        $objects = null;
        if ($model) {
            $model = 'App\\' . $model;
            $objects = $model::get();
        }
        return $objects;
    }

    public function getMetaImageAttribute()
    {
        return URL::to('/') . page_data('logo_1');
    }

    public function getSiblingsAttribute(){
        return $this->page->pageLanguages;
    }

    // METHODS


    public function preview($editable = null)
    {
        return view('cms::admin.templates.page', ['pageLanguage' => $this, 'editable' => $editable])->render();
    }

    //TO_DELETE
    public function getObject($slug, $first = false)
    {
        if (!$this->source_model_id) {
            return null;
        }
        $objects = $this->model_objects;
        $modelObject = 'App\\' . $this->sourceModel->model_name;
        if ($modelObject::CONTENT_MODEL) {
            $model_id = $this->sourceModel->display_name . '_id';
            $model = 'App\\' .$modelObject::CONTENT_MODEL;
            if ($first) {
                $content = $model::where('language', $this->language)->first();
            } else {
                $content = $model::where('slug', $slug)->where('language', $this->language)->first();
            }
            if (!$content) {
                return null;
            }
            $object = $objects->where('id', $content->$model_id)->first();
            $object->content = $content;
        } else {
            if ($first) {
                if (in_array('language', $modelObject::fields())) {
                    $object = $objects->where('language', $this->language)->first();
                } else {
                    $object = $objects->first();
                }
            } else {
                if (in_array('language', $modelObject::fields())) {
                    $object = $objects->where('slug', $slug)->where('language', $this->language)->first();
                } else {
                    $object = $objects->where('slug', $slug)->first();
                }
            }
        }
        return $object;
    }
}
