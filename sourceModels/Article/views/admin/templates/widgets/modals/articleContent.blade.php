@component('cms::admin.templates.widgetModal',get_defined_vars())
    <input id="source_model" name="source_model" type="text" value="Article" class="data static" hidden>
    <input id="source_content_type" name="source_content_type" type="text" value="content" class="data static" hidden>
    {{__('app.articles.articleContent')}}
@endcomponent
