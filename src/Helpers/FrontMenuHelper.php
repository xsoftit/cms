<?php

namespace Xsoft\Cms\Helpers;

use Xsoft\Cms\Models\Menu;

class FrontMenuHelper
{
    public static function build($page_language, $type)
    {
        $menu = FrontMenuHelper::getMenu($page_language, $type);
        return view('front.layouts.partials.menu', ['menu' => $menu, 'type' => $type]);
    }

    static public function createMenu($language, $type)
    {
        Menu::create([
            'lang' => $language,
            'type' => $type,
            'items' => json_encode([])
        ]);
    }

    static public function addToMenu($page, $type)
    {
        $menu = Menu::where('lang', $page->language)->where('type', $type)->first();
        $items = json_decode($menu->items, true);
        $link = $page->url();
        foreach ($items as &$item) {
            if ($item['link'] == $link) {
                if (key_exists('show', $item)) {
                    return true;
                }
                $item['show'] = 'on';
                return true;
            }
        }
        $items[] = ['name' => $page->name, 'link' => $link];
        $menu->update([
            'items' => json_encode($items)
        ]);
    }

    static public function removeFromMenu($page_language, $type)
    {
        $menu = Menu::where('lang', $page_language->language)->where('type', $type)->first();
        $menu_items = json_decode($menu->items, true);
        $link = $page_language->url();
        foreach ($menu_items as &$item) {
            if ($item['link'] == $link) {
                if (key_exists('show', $item)) {
                    unset($item['show']);
                }
            }
        }
        $menu->update([
            'items' => json_encode($menu_items),
        ]);
    }

    static public function getMenu($page_language, $type)
    {
        $lang = $page_language->language;
        $menu = Menu::where('lang', $lang)->where('type', $type)->first();
        $output = [];
        if ($menu) {
            $link = $page_language->url();
            $menu_items = json_decode($menu->items, true);
            $output = self::getItems($menu_items, $link);
        }
        return $output;
    }

    static private function getItems($menu_items, $link)
    {
        $output = [];
        foreach ($menu_items as $item) {
            if (key_exists('show', $item)) {
                $data = [
                    'name' => $item['name'],
                    'route' => $item['link'],
                    'active' => $item['link'] == $link ? 1 : 0
                ];
                if (key_exists('children', $item)) {
                    $data['children'] = self::getItems($item['children'], $link);
                }
                $output[] = $data;
            }
        }
        return $output;
    }
}
