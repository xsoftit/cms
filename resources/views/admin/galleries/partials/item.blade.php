@php($id = isset($item) ? $item->id : 'NUMBER_PLACEHOLDER')
<div class="gallery-item" data-id="{{$id}}">
    <div class="form-control mt20 mb20">
        <div class="row">
            <div class="col-4">
                @isset($item)
                    @include('cms::admin.filemanager.manager',[
                        'name' => 'items['.$id.'][photo]',
                        'label' => __('app.galleries.fields.photo').'*',
                        'type' => 'single',
                        'fileTypes' => ['image'],
                        'required' => 'true',
                        'thumbs' => true,
                        'data' => $item->photo
                    ])
                @else
                    @include('cms::admin.filemanager.manager',[
                        'name' => 'items['.$id.'][photo]',
                        'label' => __('app.galleries.fields.photo').'*',
                        'type' => 'single',
                        'thumbs' => true,
                        'fileTypes' => ['image'],
                        'required' => 'true'
                    ])
                @endisset
            </div>
            <div class="col-7">
                <ul class="nav nav-tabs">
                    @foreach($languages as $language)
                        <li class="nav-item"><a class="nav-link @if($loop->first) active show @endif no-save"
                                                data-toggle="tab"
                                                href="#galleriesTab-{{$language}}-{{$id}}">{{$language}}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content" id="myTabContent">
                    @foreach($languages as $language)
                        @isset($item)
                            @php($content = $item->getContent($language))
                        @endisset
                        <div class="tab-pane fade @if($loop->first) active show @endif"
                             id='galleriesTab-{{$language}}-{{$id}}'>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>{{__('app.galleries.fields.title')}}</label>
                                        <input class="form-control" name="items[{{$id}}][contents][{{$language}}][title]" @isset($content) value="{{$content->title}}" @endisset>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    {!! RouteSelector::build(__('app.galleries.fields.url'),'items['.$id.'][contents]['.$language.'][url]',(isset($content) ? $content->url : null)) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{__('app.galleries.fields.description')}}</label>
                                <textarea type="text"
                                          name="items[{{$id}}][contents][{{$language}}][description]"
                                          class="form-control no-jodit">@isset($content) {{$content->description}} @endisset</textarea>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-1">
                <span class="remove-galleries-item">
                    <i class="af af-times"></i>
                </span>
                <span class="sort-galleries-item">
                    <i class="af af-arrows-alt"></i>
                </span>
            </div>
        </div>
    </div>
</div>
