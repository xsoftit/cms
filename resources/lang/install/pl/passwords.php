<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "sent" => "Wyślialismy Ci link do resetowania hasła na adres email!",
    "user" => "Nie odnaleziono użytkownika o podanym adresie email",
    "reset" => "Hasło zostało zresetowane!",
    "token" => "Błędny token",
    "password" => "Hasło musi miec conajmniej 6 znaków i być takie samo jak potwierdzenie hasła",

];
