<li><b>{{__('app.widgets.labels.video')}}:</b>
    <div id="video">
        @include('cms::admin.templates.widgets.partials.files',['files' => (array)$widget->getContent('video',$pageLanguage)])
    </div>
</li>
