@component('cms::admin.templates.widgetModal',get_defined_vars())
    <input id="content_type" name="content_type" type="text" value="gallery" class="data" hidden>
    @include('cms::admin.partials.gallerySelect')
@endcomponent
