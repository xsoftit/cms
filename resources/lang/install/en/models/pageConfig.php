<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'menu' => 'Page Config',
    'title' => 'Configure Page',
    'subtitle' => 'Page menu and global widgets configuration',
    'menuNotDefined' => 'Menu not defined',
    'globalWidgets' => 'Global widgets',
    'alerts' => [
        'updated' => 'Menu updated successfully',
    ],
    'add' => 'Add menu item',
    'link' => 'Link',
    'name' => 'Name',
    'manage' => [
        'menu' => 'Manage menus',
        'main' => 'Main menu',
        'footer' => 'Footer menu'
    ]

];
