<?php

return [
    'buttonTextSingle' => 'Choose file',
    'buttonTextMulti' => 'Choose files',
    'title' => 'File manager',
    'files' => 'Files',
    'newFolder' => 'New folder',
    'newFile' => 'Drop files or click here',
    'copyToClipboard' => 'Copy to clipboard',
    'deleteQuestion' => 'Are you sure you want delete',
    'deleteWarning' => 'It will be removed from all widgets',
    'selectAll' => 'Select all',
    'chosenFiles' => 'Chosen files',
    'directoryNotEmpty' => 'Directory not empty',
    'rename' => 'Rename',
    'fileName' => 'File name',
    'directoryName' => 'Directory name',
    'uploadThumb' => 'Upload thumb',
    'alt' => 'Alt',
    'import' => [
        'title' => 'Import files from file',
        'description' => 'This action will truncate files table. Remember that you also have to copy files themselves to public/storage/files.',
        'button' => 'Import'
    ]
];
