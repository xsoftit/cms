<?php

use Xsoft\Cms\Models\PageSectionTemplate;
use Xsoft\Cms\Models\SourceModel;
use Xsoft\Cms\Models\Widget;
use Xsoft\Cms\Models\WidgetTemplate;
use Illuminate\Database\Seeder;

class CmsElementsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!WidgetTemplate::where('slug', 'text')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'Text',
                'slug' => 'text',
                'blade_name' => 'text'
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
        if (!WidgetTemplate::where('slug', 'short-text')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'Short Text',
                'slug' => 'short-text',
                'blade_name' => 'shortText'
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
        if (!WidgetTemplate::where('slug', 'button')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'Button',
                'slug' => 'button',
                'blade_name' => 'button'
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
        if (!WidgetTemplate::where('slug', 'image')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'Image',
                'slug' => 'image',
                'blade_name' => 'image'
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
        if (!WidgetTemplate::where('slug', 'texticon')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'Text&Icon',
                'slug' => 'texticon',
                'blade_name' => 'texticon'
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
        if (!PageSectionTemplate::where('slug', 'one-column')->first()) {
            $sectionTemplate = PageSectionTemplate::create([
                'name' => 'One Column',
                'slug' => 'one-column',
                'blade_name' => 'oneColumn',
                'columns' => json_encode(["12"])

            ]);
            echo 'Seeded Section ' . $sectionTemplate->name . PHP_EOL;
        }
        if (!PageSectionTemplate::where('slug', 'two-columns')->first()) {
            $sectionTemplate = PageSectionTemplate::create([
                'name' => 'Two Columns',
                'slug' => 'two-columns',
                'blade_name' => 'twoColumns',
                'columns' => json_encode(["6", "6"])

            ]);
            echo 'Seeded Section ' . $sectionTemplate->name . PHP_EOL;
        }
        if (!WidgetTemplate::where('slug', 'video')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'Video',
                'slug' => 'video',
                'blade_name' => 'video'
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
        if (!PageSectionTemplate::where('slug', 'three-columns')->first()) {
            $sectionTemplate = PageSectionTemplate::create([
                'name' => 'Three Columns',
                'slug' => 'three-columns',
                'blade_name' => 'threeColumns',
                'columns' => json_encode(["4", "4", "4"])

            ]);
            echo 'Seeded Section ' . $sectionTemplate->name . PHP_EOL;
        }
        if (!WidgetTemplate::where('slug', 'iframe')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'Iframe',
                'slug' => 'iframe',
                'blade_name' => 'iframe'
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
        if (!WidgetTemplate::where('slug', 'youtube-video')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'YouTube video',
                'slug' => 'youtube-video',
                'blade_name' => 'youtubeVideo'
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
        if (!WidgetTemplate::where('slug', 'gallery')->first()) {
            $widget = WidgetTemplate::create([
                'name' => 'Gallery',
                'slug' => 'gallery',
                'blade_name' => 'gallery',
                'generate' => 0,
            ]);
            echo 'Seeded WidgetTemplate ' . $widget->name . PHP_EOL;
        }
    }
}
