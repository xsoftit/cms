<?php

return [
    "menu" => "Użytkownicy",
    "name" => "Użytkownik",
    "actions" => "Akcje",
    "activate" => "Aktywuj",
    "changeRole" => "Zmień role",
    "deactivate" => "Dezaktywuj",
    "changePassword" => "Zmień hasło",
    "confirmationQuestion" => "Czy na pewno chcesz <b>:action</b> użytkownika <b>:user?</b>",
    'title' => [
        "edit" => "Edycja użytkownika :name",
        "index" => "Użytkownicy",
        "create" => "Nowy użytkownik",
        "confirmation" => "Potwierdzenie",
    ],
    'subtitle' => [
        "edit" => "Konfiguracja użytkownika :user",
        "index" => "Zarządzanie użytkownikami systemu",
    ],
    'fields' => [
        "name" => "Nazwa",
        "role" => "Rola",
        "email" => "Email",
        "active" => "Aktywny",
        "inactive" => "Nieaktywny",
        "password" => "Hasło",
    ],
    'alerts' => [
        "saved" => "Użytkownik :name zapisany pomyślnie!",
        "activated" => "Użytkownik :name aktywowany pomyślnie!",
        "deactivated" => "Użytkownik :name dezaktywowany pomyślnie!",
        "roleChanged" => "Rola zmieniona pomyślnie!",
        "passwordChanged" => "Hasło zmienione pomyślnie!",
    ]
];
