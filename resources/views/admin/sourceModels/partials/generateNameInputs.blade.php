@section('js')
    @parent
    <script>
        body = $('body');
        let classNameInput = body.find('#class-name');
        body.on('click', '#generate-names-btn', function (e) {
            e.preventDefault();
            let className = classNameInput.val();
            if (className === '') {
                alert('Enter class name to preview names!');
                return;
            }
            let url = '{{route('admin.sourceModels.generateNamesInputs',['className'=>'CLASS_NAME_PLACEHOLDER'])}}';
            url = url.replace('CLASS_NAME_PLACEHOLDER', className);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                dataType: 'json',
                url: url,
                success: function (response) {
                    body.find('#generated-names').html(response.inputs);
                },
                error: function (response) {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            });
        });
    </script>
@endsection
