@section('js')
    @parent
    <script>
        let body = $('body');
        body.on('click', '.remove-column', function () {
            $(this).parents('.column').remove();
        });
        $('#addColumn').on('click', function () {
            let template = $('#columnTemplate').html();
            let row = $('#addColumn').parent('li');
            let number = 3;
            $('.column').each(function () {
                let oldNumber = parseInt($(this).attr('data-id'));
                if (oldNumber >= number) {
                    number = oldNumber + 1;
                    console.log(number);
                }
            });
            template = template.replace(new RegExp('NUMBER_PLACEHOLDER', 'g'), number);
            $(template).insertBefore(row);
            $('.column').find('select').selectInit();
        });

        body.on('click', '.remove-column', function () {
            $(this).parents('.contentColumn').remove();
        });
        $('#addContentColumn').on('click', function () {
            let template = $('#contentColumnTemplate').html();
            let row = $('#addContentColumn').parent('li');
            let number = 4;
            $('.contentColumn').each(function () {
                let oldNumber = parseInt($(this).attr('data-id'));
                if (oldNumber >= number) {
                    number = oldNumber + 1;
                    console.log(number);
                }
            });
            template = template.replace(new RegExp('NUMBER_PLACEHOLDER', 'g'), number);
            $(template).insertBefore(row);
            $('.contentColumn').find('select').selectInit();
        });

        $('#languageContent').on('click', function () {
            if ($(this).prop('checked')) {
                body.find('#language-content-columns').show();
                body.find('#main-slug').hide();
            } else {
                body.find('#language-content-columns').hide();
                body.find('#main-slug').show();
            }
        });
    </script>
@endsection
