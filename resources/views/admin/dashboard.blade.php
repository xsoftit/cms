@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.dashboard.title')}}
@endsection

@section('title')
    {{__('app.dashboard.title')}}
@endsection

@section('subtitle')
    {{__('app.dashboard.subtitle')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title')]) !!}
@endsection

@section('page-content')
    <iframe width="100%" style="height: 70vh" src="http://{{config('cms.FRONT_DOMAIN')}}">

    </iframe>
@endsection
