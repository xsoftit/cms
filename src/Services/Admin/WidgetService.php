<?php

namespace Xsoft\Cms\Services\Admin;

use Hermit\Logs\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File as FacadeFile;
use Illuminate\Support\Traits\ForwardsCalls;
use Xsoft\Cms\Models\File;
use Xsoft\Cms\Models\SourceModel;
use Xsoft\Cms\Models\Widget;
use Xsoft\Cms\Models\WidgetContent;

class WidgetService
{
    use ForwardsCalls;

    public function storeFiles($widget, &$output_contents, &$contents)
    {
        $keys = [];
        $this->findKeys($contents['files'], $keys);
        foreach ($keys as $key) {
            $keyArray = explode('.', $key);
            $files = $this->setFiles($contents, $keyArray);
            $array = [
                'type' => $this->findType($contents['fileInputType'], $keyArray),
                'files' => $files
            ];
            $contents = $this->setArray($contents, $keyArray, $array, 0);
            $output_contents[$key] = view('cms::admin.templates.widgets.partials.files', ['files' => $array, 'widgetService' => new WidgetService()])->render();
        }

        unset($contents['files']);
        unset($output_contents['files']);
        unset($contents['fileInputType']);
        unset($output_contents['fileInputType']);

        Log::write('Gallery', 'Updated Gallery', 1, $widget->id . ': ' . $widget->name . ' [' . $widget->lang . ']');
        return $output_contents;
    }


    public function getFiles($data, $max = 0)
    {
        if (is_array($data)) {
            $filesIds = $data;
        } else {
            $filesIds = json_decode($data, true);
        }
        $files = [];
        if (key_exists('files', $filesIds)) {
            foreach ($filesIds['files'] as $key => $file) {
                if ($key < $max || $max == 0) {
                    $fileObject = File::find($file);
                    if ($fileObject) {
                        $files[$key] = $fileObject->toArray();
                        $files[$key]['path'] = $fileObject->path;
                    }
                }
            }
        }
        return $files;
    }

    public function create($widgetTemplate, $request, $slug)
    {
        return Widget::create([
            'widget_template_id' => $widgetTemplate->id,
            'name' => $request->get('name'),
            'slug' => $slug,
            'class' => ''
        ]);
    }

    public function createContent($widget, $language, $content)
    {
        return WidgetContent::create([
            'widget_id' => $widget->id,
            'language' => $language,
            'content' => json_encode($content),
        ]);
    }

    public function addToSection($pageSection, $widget, $request)
    {
        $max_widget_order_in_column = DB::table('page_section_widget')
            ->where('page_section_id', $pageSection->id)
            ->where('placement_column', $request->get('column'))
            ->select('placement_order')
            ->orderBy('placement_order', 'desc')
            ->first();
        if ($max_widget_order_in_column) {
            $widgets_count = $max_widget_order_in_column->placement_order + 1;
        } else {
            $widgets_count = 0;
        }
        return DB::table('page_section_widget')->insert([
            'page_section_id' => $pageSection->id,
            'widget_id' => $widget->id,
            'placement_column' => $request->get('column'),
            'placement_order' => $widgets_count
        ]);
    }

    public function prepareContent($contents, &$data, $i = '')
    {
        foreach ($contents as $key => $value) {
            $newKey = $this->prepareKey($i, $key);
            if (is_array($value)) {
                if (key_exists('files', $value)) {
                    $data[$newKey]['files'] = $this->getFiles($value);
                    $data[$newKey]['type'] = $value['type'];
                } else {
                    $this->prepareContent($value, $data, $newKey);
                }
            } else {
                $data[$newKey] = $value;
            }
        }
    }

    private function prepareKey($i, $key)
    {
        if ($i) {
            $newKey = $i . '.' . $key;
        } else {
            $newKey = $key;
        }
        return $newKey;
    }

    private function findKeys($value, &$keys)
    {
        if (!is_array($value)) {
            $keys[] = $value;
        } else {
            foreach ($value as $val) {
                $this->findKeys($val, $keys);
            }
        }
    }

    private function setFiles($contents, $keyArray)
    {
        $files = [];
        foreach ($keyArray as $k) {
            if (!is_array($contents[$k])) {
                if ($contents[$k]) {
                    foreach (json_decode($contents[$k]) as $file) {
                        $files[] = $file;
                    }
                }
            } else {
                $contents = $contents[$k];
            }
        }
        return $files;
    }

    private function setArray($contents, $keyArray, $array, $iterator)
    {
        $key = $keyArray[$iterator];
        if (!is_array($contents[$key])) {
            $contents[$key] = $array;
        } else {
            $contents[$key] = $this->setArray($contents[$key], $keyArray, $array, $iterator + 1);
        }
        return $contents;
    }

    private function findType($types, $keyArray)
    {
        foreach ($keyArray as $k) {
            if (!is_array($types[$k])) {
                return $types[$k];
            } else {
                $types = $types[$k];
            }
        }
    }

    static public function createFrontServices()
    {
        $app_path = app_path() . '\\';
        $model_starters = __DIR__ . '\\..\\..\\Helpers\\SourceModel\\starters\\Language\\';
        $services_path = $app_path . 'Http\\Services\\Front\\';
        $source_models = SourceModel::all();
        foreach ($source_models as $source_model) {
            if ($source_model->model_name) {
                $class_name = $source_model->model_name;
                if (FacadeFile::exists($model_starters . 'ExampleModelFrontService.php')) {
                    $service_name = $class_name . 'Service';
                    if (!FacadeFile::exists($services_path . $service_name . '.php')) {
                        $file = FacadeFile::get($model_starters . 'ExampleModelFrontService.php');
                        $file = str_replace('ExampleModel', $class_name, $file);
                        FacadeFile::put($services_path . $service_name . '.php', $file);
                        echo 'Created Service: ' . $service_name . PHP_EOL;
                    }
                }
            }
        }
    }

}
