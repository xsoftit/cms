<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPagesTableAddName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->string('name')->nullable();
        });
        $pages = \Illuminate\Support\Facades\DB::table('pages')->get();
        $default_front_language = \Xsoft\Cms\Models\Config::where('name', 'default_front_language')->first();
        if ($default_front_language) {
            $default_front_language = $default_front_language->value;
        } else {
            $default_front_language = 'en';
        }
        foreach ($pages as $page) {
            $pageLanguage = \Illuminate\Support\Facades\DB::table('page_languages')->where('page_id', $page->id)->where('language', $default_front_language)->first();
            if ($pageLanguage) {
                $name = $pageLanguage->name;
            } else {
                $name = $page->id;
            }
            \Illuminate\Support\Facades\DB::table('pages')->where('id', $page->id)->update([
                'name' => $name
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('pages', 'name')) {
            Schema::table('pages', function (Blueprint $table) {
                $table->dropColumn('name');
            });
        }
    }
}
