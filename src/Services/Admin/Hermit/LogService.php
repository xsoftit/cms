<?php

namespace Xsoft\Cms\Services\Hermit;

use Carbon\Carbon;
use App\User;
use Xsoft\DataTables\DataTable;

class LogService
{
    public function getLogsTable($type)
    {
        $table = new DataTable('logs', 'logs.indexAjax');
        $table->addParams(['type' => '"' . $type . '"']);
        $table->setOptions(['lengthChange' => false, 'info' => false, 'name' => $type, 'pageLength' => 100]);
        $table->setColumns([
            ['ID', 'id', ['class' => 'width5']],
            ['MESSAGE', 'message', ['class' => 'width32 word-break']],
            ['AUTHOR', 'author_id', ['class' => 'width12']],
            ['COMMENT', 'comments', ['class' => 'width30 word-break']],
            ['IP', 'ip', ['class' => 'width7']],
            ['P', 'priority', ['class' => 'width5']],
            ['DATE', 'created_at', ['class' => 'width8 ']],
        ]);
        $table->addToQuery(
            ['where', 'type', $type],
            ['select', 'logs.*']
        );
        return $table;
    }

    public function prepareLogsOutput(&$output)
    {
        $users = User::where('id', '>', 0)->get();
        foreach ($output['collection'] as $row) {
            $author = $users->find($row->author_id);
            if ($author) {
                $author = $author->id . ' - ' . $author->name;
            } else {
                $author = "SYSTEM";
            }
            $output['aaData'][] = [
                $row->id,
                $row->message,
                $author,
                $row->comments,
                $row->ip,
                $row->priority,
                Carbon::parse($row->created_at)->format('H:i:s d.m.Y')
            ];
        }
    }

    public function getLogArchivesTable()
    {
        $table = new DataTable('log_archives', 'logs.archives.indexAjax');
        $table->setOptions(['lengthChange' => false, 'info' => false, 'pageLength' => 100]);
        $table->setColumns([
            ['ID', 'id', ['class' => 'width5']],
            ['FROM', 'from', ['class' => 'width20']],
            ['TO', 'to', ['class' => 'width20']],
            ['COMMENT', 'comments', ['class' => 'width30 word-break']],
            ['DATE', 'created_at', ['class' => 'width10']],
            ['', 'id', ['class' => 'width15']],
        ]);
        return $table;
    }

    public function prepareLogArchivesOutput(&$output)
    {
        foreach ($output['collection'] as $row) {
            $output['aaData'][] = [
                $row->id,
                Carbon::parse($row->from)->format('d.m.Y H:i:s'),
                Carbon::parse($row->to)->format('d.m.Y H:i:s'),
                $row->comment,
                Carbon::parse($row->created_at)->format('d.m.Y H:i:s'),
                '<a class="btn btn-outline-light" href="' . route('logs.archives.details', ['logArchive' => $row->id]) . '">DETAILS</a>'
            ];
        }
    }

}
