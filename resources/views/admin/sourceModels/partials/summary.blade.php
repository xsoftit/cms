@if($sourceModel->hasLanguageContent())
@php($names = $sourceModel->getAllNames(true))
@else
@php($names = $sourceModel->getAllNames())
@endif
@foreach($names as $type => $value)
@if(is_array($value))
@foreach($value as $key => $item)
content_{{$key}}:{{$item}}<br>
@endforeach
@else
{{$type}}:{{$value}}<br>
@endif
@endforeach
<br>
----------------------------------------------------<br>
Created files:<br>
{{$sourceModel->getMigrationsPath() . '\\' . $sourceModel->getMigrationName()}}<br>
@if($sourceModel->hasLanguageContent()){{$sourceModel->getMigrationsPath() . '\\' . $sourceModel->getContentMigrationName()}}<br>@endif
{{$sourceModel->getAppPath() . '\\'.$sourceModel->getClassName() . '.php'}}<br>
@if($sourceModel->hasLanguageContent()){{$sourceModel->getAppPath() . '\\'.$sourceModel->getContentClassName() . 'php'}}<br>@endif
{{$sourceModel->getAppPath() . '\\Http\\Services\\' . $sourceModel->getClassName() . 'Service'}}<br>
{{$sourceModel->getAppPath() . '\\Http\\Services\\Front\\' . $sourceModel->getClassName() . 'Service.php'}}<br>
{{$sourceModel->getResourcePath() . 'views\\admin\\' . $sourceModel->getCamelName() . '\\index.blade.php'}}<br>
{{$sourceModel->getResourcePath() . 'views\\admin\\' . $sourceModel->getCamelName() . '\\create.blade.php'}}<br>
{{$sourceModel->getResourcePath() . 'views\\admin\\' . $sourceModel->getCamelName() . '\\edit.blade.php'}}<br>
{{$sourceModel->getAppPath() . 'Http\\Controllers\\' . $sourceModel->getClassName() . 'Controller.php'}}<br>
{{resource_path('views\\admin\\templates\\widgets\\') . $sourceModel->getCamelName() . 'List.blade.php'}}<br>
{{resource_path('views\\admin\\templates\\widgets\\modals\\') . $sourceModel->getCamelName() . 'Content.blade.php'}}<br>
{{resource_path('views\\front\\templates\\widgets\\') . $sourceModel->getCamelName() . 'List.blade.php'}}<br>
{{resource_path('views\\front\\templates\\widgets\\') . $sourceModel->getCamelName() . 'Content.blade.php'}}<br>
{{$sourceModel->getResourcePath() . 'lang\\default\\models\\' . $sourceModel->getCamelName() . '.php'}}<br>
----------------------------------------------------<br>
Modified files:<br>
{{database_path('seeds\\').'ElementsSeeder.php'}}<br>
{{database_path('seeds\\').'PermissionsSeeder.php'}}<br>
{{base_path().'\\routes\\web.php'}}<br>
{{$sourceModel->getResourcePath() . 'lang\\default\\app.php'}}<br>
{{$sourceModel->getAppPath() . 'Helpers\\MenuHelper.php'}}<br>
