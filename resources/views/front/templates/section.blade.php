@if(!$section->getTag())
    @include('front.templates.sections.'.$section->getBladeName(),['section'=>$section])
@else
    <section
        id="{{$section->getSlug()}}"{!! ($section->getClasses()) ? ' class="'.$section->getClasses().'"' :''!!}{!! ($section->getStyles() ? ' style="'.$section->getStyles().'"' :'') !!}>
        {!! $section->getSectionContent() !!}
    </section>
@endif
