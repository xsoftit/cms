@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.pages.title.index')}}
@endsection

@section('title')
    {{__('app.pages.title.index')}}
@endsection
@section('subtitle')
    {{__('app.pages.subtitle.index')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.pages.breadcrumbs.index')]) !!}
@endsection

@section('page-content')
    <div class="row">
        <div class="col-2 no-padding">
            <div class="pages-list">
                @if(auth()->user()->hasPermission('pages.create'))
                    <div class="add-new-page btn btn-outline-success extra-info"
                         data-title="{{__('app.pages.buttons.new')}}" data-toggle="modal" data-container=".pages-list"
                         data-target="#add-new-page"><i class="af af-file-medical"></i></div>
                @endif
                @if(auth()->user()->hasPermission('pages.import'))
                    <div class="import-new-page btn btn-outline-info extra-info"
                         data-title="{{__('app.pages.buttons.importPage')}}" data-toggle="modal" data-container=".pages-list"
                         data-target="#import-new-page"><i class="af af-file-import"></i></div>
                @endif
                @if(auth()->user()->hasPermission('pages.export'))
                    <div class="import-new-page btn btn-outline-secondary extra-info"
                         data-title="{{__('app.pages.buttons.exportPages')}}" data-toggle="modal" data-container=".pages-list"
                         data-target="#export-all-pages"><i class="af af-file-export"></i></div>
                @endif
                <div class="accordion" id="accordionExample">
                    @foreach(\Xsoft\Cms\Models\Page::all() as $page)
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link text-left collapsed" type="button"
                                            data-toggle="collapse"
                                            data-target="#collapse-{{$page->id}}" aria-expanded="true"
                                            aria-controls="collapseOne">
                                        <span>{{$page->name}}</span>
                                        @if($page->source_model_id)
                                            &nbsp
                                            <small>({{__('app.'.\Illuminate\Support\Str::plural($page->source_model->display_name).'.name').' '.__('app.pages.singleView')}})</small>
                                        @endif
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-{{$page->id}}" class="collapse" aria-labelledby="headingOne"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    @foreach($page->pageLanguages as $pageLanguage)
                                        <div data-id="{{$pageLanguage->id}}"
                                             class="show-page-edit @if(!$pageLanguage->active) element-no-active @endif"
                                             data-lang="{{$pageLanguage->language}}">
                                            @if($pageLanguage->is_main)
                                                <i class="afs af-home"></i>
                                            @else
                                                <i class="af af-file-alt"></i>
                                            @endif
                                            <span>[{{$pageLanguage->language}}] {{$pageLanguage->name}}</span>
                                            <div class="active-badge"></div>
                                        </div>
                                    @endforeach
                                    @if(auth()->user()->hasPermission('pages.create'))
                                        <div data-id="{{$page->id}}"
                                             class="add-language-version btn btn-outline-success extra-info"
                                             data-title="{{__('app.pages.buttons.addLanguageVersion')}}">
                                            <i class="af af-copy"></i>
                                        </div>
                                    @endif
                                    @if(auth()->user()->hasPermission('pages.export'))
                                        <div data-id="{{$page->id}}" data-toggle="modal" data-target="#export-page"
                                             data-title="{{__('app.pages.buttons.exportPage')}}"
                                             class="export-page btn btn-outline-light extra-info"><i
                                                class="af af-file-export"></i></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-10 preview-box">
            <div class="loader"><img src="/admin/images/ajax.gif"/></div>
            <div id="edit-preview" class="preview list-preview">
            </div>
        </div>
    </div>
@endsection


@section('modals')
    @parent
    @if(auth()->user()->hasPermission('pages.create'))
        @include('cms::admin.pages.modals.addNewPage')
        @include('cms::admin.pages.modals.addLanguageVersion')
        @include('cms::admin.pages.modals.importExport')
    @endif
    @if(auth()->user()->hasPermission('pages.edit'))
        @include('cms::admin.pages.modals.editDetails')
        @include('cms::admin.pages.modals.toggleConfirmation')
    @endif
    @if(auth()->user()->hasPermission('sections.create'))
        @include('cms::admin.pages.modals.addSection')
    @endif
    @if(auth()->user()->hasPermission('sections.edit'))
        @include('cms::admin.pages.modals.editSection')
        @include('cms::admin.pages.modals.addContainer')
        @include('cms::admin.pages.modals.editContainer')
    @endif
    @if(auth()->user()->hasPermission('widgets.create'))
        @include('cms::admin.pages.modals.addWidget')
    @endif
    @include('cms::admin.pages.modals.publishPage')
    @if(auth()->user()->hasPermission('widgets.edit'))
        @if($widget_templates)
            @foreach($widget_templates as $widget)
                @if(view()->exists('admin.templates.widgets.modals.'.$widget->blade_name))
                    {!!  view('admin.templates.widgets.modals.'.$widget->blade_name,['widget' => $widget]) !!}
                @else
                    {!!  view('cms::admin.templates.widgets.modals.'.$widget->blade_name,['widget' => $widget]) !!}
                @endif
            @endforeach
        @endif
        @include('cms::admin.pages.modals.moveWidget')
    @endif
@endsection
@section('js')
    @parent
    @include('cms::admin.pages.partials.js.indexJs')
    @include('cms::admin.pages.partials.js.sectionsJs')
    @include('cms::admin.pages.partials.js.widgetsJs')
    @include('cms::admin.pages.partials.js.containersJs')
    @if(auth()->user()->hasPermission('pages.import') || auth()->user()->hasPermission('pages.export'))
        @include('cms::admin.pages.partials.js.importExportJs')
    @endif
@endsection
