@component('cms::admin.templates.widgetModal',get_defined_vars())
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.buttonText')}}</label>
        <textarea id="text" name="text" class="form-control data short"></textarea>
    </div>
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.buttonTitle')}}</label>
        <input id="title" name="title" class="form-control data">
    </div>
    {!! RouteSelector::build(__('app.widgets.labels.buttonLink'),'link') !!}
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.class')}}</label>
        <select id="class" name="class" class="form-control data" multiple>
            <option value="btn-primary"
                    class="front-bg-primary">{{__('app.widgets.classes.primary')}}</option>
            <option value="btn-secondary"
                    class="front-bg-secondary">{{__('app.widgets.classes.secondary')}}</option>
            <option value="btn-big" class="front-bg-primary">{{__('app.widgets.classes.big')}}</option>
            <option value="btn-border"
                    class="front-bg-secondary">{{__('app.widgets.classes.border')}}</option>
        </select>
    </div>
    @include('cms::admin.filemanager.manager',[
    'name' => 'button_icon',
    'label' => __('app.widgets.labels.icon'),
    'type' => 'single',
    'fileTypes' => ['icon']
    ])
@endcomponent
