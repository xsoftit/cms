@section('content')
    <main class="app-content">
        <div class="app-title">
            @section('title-content')
                <div>
                    <h1>@section('title') <i class="af af-dashboard"></i> Title @show</h1>
                    <p> @section('subtitle') Subtitle @show</p>
                </div>
            @show
            @section('breadcrumb')
                {!! Breadcrumb::make(['Home']) !!}
            @show
        </div>
        @yield('page-content')
    </main>
@show
