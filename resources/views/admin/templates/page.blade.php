<div class="mb15 preview-buttons">
    <div class="col-12 buttons" style="padding:0">
        @if(auth()->user()->hasPermission('pages.edit'))
            <span data-id="{{$pageLanguage->id}}" data-lang="{{$pageLanguage->language}}" class="btn btn-outline-info"
                  id="edit-details-button">{{__('app.pages.buttons.editDetails')}}</span>

            <span data-id="{{$pageLanguage->id}}" data-lang="{{$pageLanguage->language}}"
                  data-active="{{$pageLanguage->active}}" @if ($pageLanguage->active)class="btn btn-outline-danger"
                  id="deactivate-button" @else class="btn btn-outline-success"
                  id="activate-button"@endif>{{__('app.pages.buttons.active'.$pageLanguage->active)}}</span>
        @endif
        @if(auth()->user()->hasPermission('pages.publish') && !$pageLanguage->source_model_id)
            <span data-id="{{$pageLanguage->id}}" data-lang="{{$pageLanguage->language}}"
                  class="btn btn-outline-warning publish-version">{{__('app.pages.buttons.publish')}}</span>
        @endif
        @if(auth()->user()->hasPermission('pages.publish'))
            <span data-id="{{$pageLanguage->id}}" data-lang="{{$pageLanguage->language}}"
                  class="btn btn-outline-secondary preview-page">{{__('app.pages.buttons.previewPage')}}</span>
        @endif
    </div>
</div>
<div class="col-12">
    <ul class="sections-sortable">
        @foreach($pageLanguage->sections as $pageSection)
            <li data-id="{{$pageSection->id}}"
                @if(!$pageSection->pivot->active) class="element-no-active" @endif>{!! $pageSection->preview($editable,$pageLanguage) !!}</li>
        @endforeach
    </ul>
</div>
@if(auth()->user()->hasPermission('sections.create'))
    <div class="add-new-section" id="add-new-section">
        {{__('app.pages.addNewSection')}}
    </div>
@endif

