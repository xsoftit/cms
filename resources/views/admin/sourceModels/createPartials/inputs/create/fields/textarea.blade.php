<div class="form-group">
    <label>{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</label>
    <textarea name="{{$field->getName()}}" class="form-control">{\{old('{{$field->getName()}}')}}</textarea>
</div>
