<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = [
        'name', 'label', 'value', 'category'
    ];

    static public function getCategories()
    {
        return self::distinct('category')->pluck('category');
    }

    static public function SystemNameFields($name)
    {
        return self::where('category', $name)->pluck('name');
    }

    public function getImageAttribute()
    {
        if ($this->type == 'image' && $this->value != '') {
            $value = json_decode($this->value, true);
            if(key_exists(0,$value)){
                $file = File::find($value[0]);
                return $file;
            }
        }
        return null;
    }

}
