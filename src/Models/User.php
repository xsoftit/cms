<?php

namespace Xsoft\Cms\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guard_name = 'web';

    //REALATIONS

    public function lang()
    {
        return $this->belongsTo('Xsoft\Cms\Models\Language', 'language', 'code');
    }

    // METHODS

    public function role()
    {
        return $this->roles[0];
    }

//    public static function all($columns = [])
//    {
//        return User::leftJoin('model_has_roles as mhr', 'model_id', '=', 'users.id')
//            ->leftJoin('roles', 'roles.id', '=', 'mhr.role_id')
//            ->where('roles.name', '!=', 'superadmin')
//            ->get();
//    }

    public function hasPermission($permission){
        if($this->hasPermissionTo($permission) || $this->role()->name == 'superadmin'){
            return true;
        }
        return false;
    }
}
