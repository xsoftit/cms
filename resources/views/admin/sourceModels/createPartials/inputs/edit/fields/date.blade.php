<div class="form-group">
    <label>{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</label>
    <input type="date" name="{{$field->getName()}}" value="{\{old('{{$field->getName()}}',$exampleModel->{{$field->getName()}})}}" class="form-control">
</div>
