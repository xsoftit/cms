<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class File extends Model
{
    const TYPES = [
        'directory',
        'image' => [
            'png', 'jpg', 'jpeg', 'gif'
        ],
        'icon' => [
          'svg'
        ],
        'video' => [
            'mp4', 'avi', 'wmv'
        ],
        'pdf' => [
            'pdf'
        ],
        'other'
    ];

    protected $fillable = [
        'name',
        'storage_path',
        'type',
        'parent_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];


    public function parent()
    {
        return $this->hasOne(File::class, 'id', 'parent_id');
    }

    public function getAltAttribute(){
        if($this->attributes['alt']){
            return $this->attributes['alt'];
        }else{
            return $this->name;
        }
    }

    public function getPathAttribute()
    {
        return Storage::url($this->storage_path);
    }

    public static function checkType($file)
    {
        $extension = Str::lower($file->getClientOriginalExtension());
        foreach(self::TYPES as $key => $type){
            if(is_array($type)){
                if(in_array($extension,$type)){
                    return $key;
                }
            }else{
                continue;
            }
        }
        return 'other';
    }

    public static function backTrace($id, &$backTrace)
    {
        $file = File::find($id);
        if ($file) {
            if ($file->parent_id) {
                File::backTrace($file->parent_id, $backTrace);
            } else {
                $backTrace[] = [0, __('filemanager.files')];
            }
            $backTrace[] = [$file->id, $file->name];
        } else {
            $backTrace[] = [0, __('filemanager.files')];
        }
    }

    public static function get($data){
        $files = json_decode($data,true);
        $newFiles = [];
        if($files){
            foreach($files as $file_id){
                $file = File::find($file_id);
                if($file){
                    $newFiles[] = $file;
                }
            }
        }
        return $newFiles;
    }

    public function thumb($force = false){
        if($this->thumb_path || $force){
            return Storage::url($this->thumb_path);
        }else{
            return $this->path;
        }
    }
}
