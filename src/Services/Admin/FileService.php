<?php

namespace Xsoft\Cms\Services\Admin;

use Illuminate\Support\Str;
use Xsoft\Cms\Models\File;
use Illuminate\Support\Facades\File as FileFasade;
use Illuminate\Support\Facades\Storage;

class FileService
{
    public function getFiles($request, &$backTrace)
    {
        if ($request->get('fileTypes')) {
            $fileTypes = json_decode($request->get('fileTypes', true));
            $fileTypes[] = 'directory';
            $query = File::whereIn('type', $fileTypes);
        } else {
            $query = File::select('*');
        }
        if ($request->get('parent_id')) {
            $query->where('parent_id', $request->get('parent_id'));
            File::backTrace($request->get('parent_id'), $backTrace);
        } else {
            $query->where('parent_id', null);
            File::backTrace(0, $backTrace);
        }
        $files = $query->get();
        $sortedFiles = $files->sortBy(function ($file, $key) {
            if ($file->type == 'directory') {
                return 0;
            }
            return 1;
        });
        return $this->previewFiles($sortedFiles->values()->all());
    }

    public function previewFiles($files)
    {
        $filesArray = [];
        foreach ($files as $key => $file) {
            $filesArray[$key] = $file->toArray();
            $filesArray[$key]['path'] = $file->path;
        }
        return $filesArray;
    }

    public function move($request){
        $file = File::find($request->get('file_id'));
        return $file->update([
            'parent_id' => $request->get('directory_id') ? $request->get('directory_id') : null
        ]);
    }

    public function createDirectory($request)
    {
        return File::create([
            'name' => $request->get('name'),
            'parent_id' => $request->get('parent_id') ? $request->get('parent_id') : null,
            'type' => 'directory'
        ]);
    }

    public function createFile($request, $file)
    {
        $extension = Str::lower($file->getClientOriginalExtension());
        $filename = uniqid() . '.' . $extension;
        $storage_path = Storage::disk(config('cms.FILES_DISK'))->putFileAs('files', $file, $filename);
        return File::create([
            'parent_id' => $request->get('parent_id') ? $request->get('parent_id') : null,
            'name' => $file->getClientOriginalName(),
            'storage_path' => $storage_path,
            'type' => File::checkType($file)
        ]);
    }
}
