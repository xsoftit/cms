<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('urls', function (Blueprint $table) {
            $table->dropUnique('urls_url_unique');
            $table->string('language',2)->change();
            $table->unique(['url','language']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('urls', function (Blueprint $table) {
            $table->dropUnique('urls_url_language_unique');
            $table->unique('url');
        });

    }
}
