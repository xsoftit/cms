@foreach($section->getColumnWidgets($column_order) as $widget )
    {!! $widget->preview($pageLanguage) !!}
@endforeach
@if(auth()->user()->hasPermission('widgets.create'))
    <div class="add-widget" data-section="{{$section->id}}"
         data-column="{{$column_order}}">{{__('app.pages.addWidget')}}</div>
@endif
