<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class PrepareResourcesDirectories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cms:directories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // WIDGETS
        $this->widgets();
    }

    private function widgets()
    {
        if (!File::exists(resource_path('views/front'))) {
            File::makeDirectory(resource_path('views/front'));
            echo "Created 'views/front' directory." . PHP_EOL;
        }
        if (!File::exists(resource_path('views/front/rendered'))) {
            File::makeDirectory(resource_path('views/front/rendered'));
            echo "Created 'views/front/rendered' directory." . PHP_EOL;
        }
        if (!File::exists(resource_path('views/admin'))) {
            File::makeDirectory(resource_path('views/admin'));
            echo "Created 'views/admin' directory." . PHP_EOL;
        }
        if (!File::exists(resource_path('views/admin/templates'))) {
            File::makeDirectory(resource_path('views/admin/templates'));
            echo "Created 'views/admin/templates' directory." . PHP_EOL;
        }
        if (!File::exists(resource_path('views/admin/templates/widgets'))) {
            File::makeDirectory(resource_path('views/admin/templates/widgets'));
            echo "Created 'views/admin/templates/widgets' directory." . PHP_EOL;
        }
        if (!File::exists(resource_path('views/admin/templates/widgets/modals'))) {
            File::makeDirectory(resource_path('views/admin/templates/widgets/modals'));
            echo "Created 'views/admin/templates/widgets/modals' directory." . PHP_EOL;
        }
        if (!File::exists(resource_path('lang/default'))) {
            File::makeDirectory(resource_path('lang/default'));
            echo "Created 'lang/default' directory." . PHP_EOL;
        }
        if (!File::exists(resource_path('lang/default/models'))) {
            File::makeDirectory(resource_path('lang/default/models'));
            echo "Created 'lang/default/models' directory." . PHP_EOL;
        }
        if (!File::exists(app_path('Http/Services'))) {
            File::makeDirectory(app_path('Http/Services'));
            echo "Created 'Http/Services' directory." . PHP_EOL;
        }
        if (!File::exists(app_path('Http/Services/Front'))) {
            File::makeDirectory(app_path('Http/Services/Front'));
            echo "Created 'Http/Services/Front' directory." . PHP_EOL;
        }
        if (!File::exists(resource_path('CreatedSourceModels'))) {
            File::makeDirectory(resource_path('CreatedSourceModels'));
            echo "Created 'resources/CreatedSourceModels' directory." . PHP_EOL;
        }
    }
}
