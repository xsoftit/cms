<?php

namespace Xsoft\Cms\Observers;

use Xsoft\Cms\Models\PageLanguage;

class PageLanguageObserver
{
    /**
     * Handle the page language "created" event.
     *
     * @param  \Xsoft\Cms\Models\PageLanguage $pageLanguage
     * @return void
     */
    public function created(PageLanguage $pageLanguage)
    {
        //
    }

    /**
     * Handle the page language "updated" event.
     *
     * @param  \Xsoft\Cms\Models\PageLanguage $pageLanguage
     * @return void
     */
    public function updated(PageLanguage $pageLanguage)
    {
        //
    }

    /**
     * Handle the page language "deleted" event.
     *
     * @param  \Xsoft\Cms\Models\PageLanguage $pageLanguage
     * @return void
     */
    public function deleted(PageLanguage $pageLanguage)
    {
        //
    }

    public function deleting(PageLanguage $pageLanguage)
    {
        $sections = $pageLanguage->sections;
        foreach ($sections as $section) {
            $widgets = $section->widgets;
            foreach ($widgets as $widget) {
                $contents = $widget->contents()->where('language', $pageLanguage->language)->get();
                foreach ($contents as $content) {
                    $content->delete();
                }
            }
        }
        $pageLanguage->versions()->delete();
        if ($pageLanguage->page->pageLanguages->count() < 2) {
            $sourceModel = $pageLanguage->sourceModel;
            if($sourceModel){
                $sourceModel->update([
                    'in_use' => 0
                ]);
            }
            $pageLanguage->page->delete();
        }
    }

    /**
     * Handle the page language "restored" event.
     *
     * @param  \Xsoft\Cms\Models\PageLanguage $pageLanguage
     * @return void
     */
    public function restored(PageLanguage $pageLanguage)
    {
        //
    }

    /**
     * Handle the page language "force deleted" event.
     *
     * @param  \Xsoft\Cms\Models\PageLanguage $pageLanguage
     * @return void
     */
    public function forceDeleted(PageLanguage $pageLanguage)
    {
        //
    }
}
