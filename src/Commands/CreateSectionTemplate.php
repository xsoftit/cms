<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Xsoft\Cms\Helpers\SeederHelper;
use Xsoft\Cms\Models\PageSectionTemplate;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateSectionTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cms:section';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('make:cms:directories');
        $name = "";
        $sections = PageSectionTemplate::all();
        $names = $sections->pluck('name')->toArray();
        $slugs = $sections->pluck('slug')->toArray();
        $correct = false;
        while (!$correct) {
            $name = $this->ask('Section name: ');
            if (!$name) {
                continue;
            }
            if (in_array($name, $names)) {
                echo 'Section with that name already exists.' . PHP_EOL;
                $name = "";
                continue;
            }
            $slug = Str::slug($name);
            if (in_array($slug, $slugs)) {
                echo 'Section with that slug already exists.' . PHP_EOL;
                $name = "";
                continue;
            }
            $blade_name = $this->ask('Blade name (default: ' . Str::camel($slug) . '): ');
            if (!$blade_name) {
                $blade_name = Str::camel($slug);
            }
            $correct = true;
        }
        $tag = $this->confirm('Generate tag for this section?',true);
        $columns = $this->ask('Input bootstrap column numbers separated by comma (eg. "2, 10" | default: 12): ');
        $columns = explode(',', trim($columns));
        if (count($columns) == 0) {
            $columns[] = 12;
        }
        $seeder = file_get_contents(database_path('seeds/') . 'ElementsSeeder.php');
        $seeder = str_replace('//SEED_HERE', 'if (!PageSectionTemplate::where(\'slug\', \'' . $slug . '\')->first()) {
            $sectionTemplate = PageSectionTemplate::create([
                \'name\' => \'' . $name . '\',
                \'slug\' => \'' . $slug . '\',
                \'blade_name\' => \'' . $blade_name . '\',
                \'columns\' => json_encode(' . json_encode($columns) . '),
                \'tag\' => \'' . ($tag ? 1 : 0) . '\',
                
            ]);
            echo \'Seeded Section \' . $sectionTemplate->name . PHP_EOL;
        }
        //SEED_HERE', $seeder);

        file_put_contents(database_path('seeds/') . 'ElementsSeeder.php', $seeder);

        if (!File::exists(resource_path('views/front/templates/sections/' . $blade_name . '.blade.php'))) {
            copy(__DIR__ . '/../../resources/views/front/starters/section.blade.php', resource_path('views/front/templates/sections/' . $blade_name . '.blade.php'));
        }

        echo 'Created Section:' . PHP_EOL . "\tname: " . $name . PHP_EOL . "\tslug: " . $slug . PHP_EOL . "\tblade_name: " . $blade_name . PHP_EOL;
    }
}
