<div class="modal fade" id="language-create" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('app.languages.title.create')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" class="forms-sample" action="{{route('admin.config.languages.store')}}">
                <div class="modal-body">
                    @csrf
                    @method('POST')
                    <h4>{{__('app.languages.data')}}</h4>
                    <div class="form-group row">
                        <div class="col-6">
                            <label for="name">{{__('app.languages.fields.name')}} *</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
                        </div>
                        <div class="col-6">
                            <label for="name">{{__('app.languages.fields.code')}}</label>
                            <input type="text" class="form-control" id="code" name="code" value="{{old('code')}}">
                        </div>
                    </div>
                    <h4>{{__('app.languages.fields.fields')}}</h4>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="name">{{__('app.search')}} *</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            @include('cms::admin.config.languages.partials.fields',['template' => $template, 'parentKey' => ''])
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success">{{__('app.save')}}</button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                        {{__('app.close')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script src="/js/hoverable-collapse.js"></script>
@endsection
