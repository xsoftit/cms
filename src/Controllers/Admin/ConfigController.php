<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Xsoft\Cms\Models\Config;
use Xsoft\Cms\Models\Redirect;
use Xsoft\Cms\Models\Url;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $urls = [];
        if (auth()->user()->hasPermission('urls.show')) {
            foreach (Url::all() as $url) {
                if ($url->model == 'PageLanguage') {
                    $model = 'Xsoft\Cms\Models\PageLanguage';
                } else {
                    $model = 'App\\' . $url->model;
                }
                if($model::find($url->model_id)) {
                    $newUrl = [
                        'name' => $model::find($url->model_id)->name,
                        'language' => $url->language,
                        'id' => $url->model_id,
                        'url_id' => $url->id,
                        'url' => $url->url,
                        'hostWithPrefix' => $url->hostWithPrefix
                    ];
                }else{
                    $url->delete();
                }
                $urls[$url->model][] = $newUrl;
            }
        }
        $redirects = Redirect::all();
        return view('cms::admin.config.edit', compact('urls', 'redirects'));
    }

    public function update(Request $request)
    {
        if ($request->get('url')) {
            foreach ($request->get('url') as $id => $data) {
                $url = Url::find($id);
                Url::handleUpdate($data, $url);
            }
        }
        if ($request->get('redirect')) {
            Redirect::truncate();
            foreach ($request->get('redirect') as $id => $data) {
                Redirect::create([
                    'from' => $data['from'],
                    'to' => $data['to']
                ]);
            }
        }
        if ($request->get('domains')) {
            $domains = [];
            foreach ($request->get('domains') as $data) {
                if (key_exists('language', $data) && key_exists('name', $data)) {
                    if ($data['name'] && $data['language']) {
                        $domains[$data['name']] = $data['language'];
                    }
                }
            }
            $request->merge(['domains' => json_encode($domains)]);
        }
        foreach (Config::all() as $config) {
            if ($config->type == 'checkbox') {
                if ($config->name == 'multi_domains') {
                    if (!$request->get($config->name)) {
                        if (!Url::checkAll()) {
                            return redirect()->route('admin.config.edit');
                        }
                    }
                }
                $config->value = $request->get($config->name) ? 1 : 0;
            } else {
                $config->value = $request->get($config->name) ? $request->get($config->name) : $config->value;
            }
            $config->save();
        }
        alert(__('app.config.alert.success'), 'success');
        return redirect()->route('admin.config.edit');
    }

    public function setPrefix(Request $request)
    {
        $className = $request->get('class_name');
        if (defined("App\\" . $className . '::PARENT_MODEL')) {
            $model = "App\\" . $className;
            $className = $model::PARENT_MODEL;
        }
        $urls = Url::where('model', $className)->get();
        foreach ($urls as $url) {
            Url::updatePrefix($url, $request->get('prefix'));
        }
        return redirect()->back();
    }
}
