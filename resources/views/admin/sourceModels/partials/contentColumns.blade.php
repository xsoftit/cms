<label>{{__('app.sourceModels.fields.contentColumns')}}</label>
<ul class="form-group list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <label>{{__('app.sourceModels.fields.column.type')}}</label>
            </div>
            <div class="col-md-3 col-sm-12">
                <label>{{__('app.sourceModels.fields.column.name')}}</label>
            </div>
            <div class="col-md-3 col-sm-12">
                <label>{{__('app.sourceModels.fields.column.default')}}</label>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[0][type]" value="string" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[0][name]" value="title" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[0][modifiers][default]" value="" readonly>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[1][type]" value="string" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[1][name]" value="slug" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[1][modifiers][default]" value="" readonly>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[2][type]" value="text"
                       readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[2][name]" value="content" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[2][modifiers][default]" value="" readonly>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[3][type]" value="string"
                       readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[3][name]" value="language" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="contentFields[3][modifiers][default]" value="" readonly>
            </div>
        </div>
    </li>
</ul>
<div id="content-additional-columns">
    <ul class="form-group list-group">

        <li class="list-group-item">
            <button type="button" id="addContentColumn"
                    class="btn btn-secondary">{{__('app.sourceModels.fields.addColumn')}}</button>
        </li>
    </ul>
</div>
