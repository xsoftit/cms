@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.roles.title.index')}} | {{__('app.roles.title.edit',['name' => $role->name])}}
@endsection

@section('title')
    {{__('app.roles.title.edit',['name' => $role->name])}}
@endsection

@section('subtitle')
    {{__('app.roles.subtitle.edit',['name' => $role->name])}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make(
        [__('app.dashboard.title'),route('admin.dashboard')],
        [__('app.roles.title.index'),route('admin.roles.index')],
        [__('app.roles.title.edit',['name' => $role->name])]
    ) !!}
@endsection

@section('page-content')
    <form action="{{route('admin.roles.update',[$role->id])}}" method="POST">
        @csrf
        <div class="row">
            @foreach($permissions as $key => $permission)
                <div class="col-4 mb30">
                    <h3 class="text-capitalize mb15">{{str_replace('_',' ',$key)}}</h3>
                    <ul class="list-group">
                        @foreach($permission as $p)
                            <li class="list-group-item">
                                <div class="animated-checkbox">
                                    <label>
                                        <input type="checkbox" name="permissions[{{$key.'.'.$p['name']}}]"
                                               @if($role->hasPermissionTo($key.'.'.$p['name'])) checked
                                               @endif data-dependencies="{{ $p['dependencies'] }}"
                                               data-name="{{$key.'.'.$p['name']}}">
                                        <span
                                            class="label-text text-capitalize">{{str_replace('_',' ',$p['name'])}}</span>
                                    </label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
        @if(auth()->user()->hasPermission('roles.delete'))
            @deleteButton(route('admin.roles.destroy',[$role->id]))
        @endif
        <input type="submit" class="btn btn-outline-success float-right" value="{{__('app.save')}}" name="save">
        <input type="submit" class="btn btn-outline-secondary float-right" value="{{__('app.saveAndBack')}}" name="back">
    </form>
@endsection

@section('js')
    @parent
    <script>
        $('input[type="checkbox"]').on('change', function () {
            let input = $(this);
            if (input.is(':checked')) {
                let data = JSON.parse(input.attr('data-dependencies'));
                $.each(data, function (i, v) {
                    let otherInput = $('input[data-name="' + v + '"]');
                    if (!otherInput.is(':checked')) {
                        otherInput.click();
                    }
                });
            } else {
                let inputs = $('input[type="checkbox"]');
                $.each(inputs, function (i, v) {
                    let otherInput = $(this);
                    let data = JSON.parse(otherInput.attr('data-dependencies'));
                    $.each(data, function (i, v) {
                        if (v === input.attr('data-name')) {
                            if (otherInput.is(':checked')) {
                                otherInput.click();
                            }
                        }
                    });
                });
            }
        });
    </script>
@endsection
