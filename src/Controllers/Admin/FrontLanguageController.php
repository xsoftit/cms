<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Hermit\Logs\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Xsoft\Cms\Helpers\CmsConfig;
use Xsoft\Cms\Helpers\LanguageHelper;
use Xsoft\Cms\Models\Config;
use Xsoft\Cms\Models\Language;
use Xsoft\Cms\Models\Menu;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Requests\FrontLanguage\StoreRequest;
use Xsoft\Cms\Requests\FrontLanguage\UpdateRequest;
use Xsoft\Cms\Services\Admin\FrontLanguageService;

class FrontLanguageController extends Controller
{
    protected $languageService;

    public function __construct()
    {
        $this->languageService = new FrontLanguageService();
    }

    public function indexAjax(Request $request)
    {
        $table = $this->languageService->getTable($request);
        $output = $table->output($request);
        $this->languageService->prepareOutput($output);
        return $output;
    }

    public function index()
    {
        $table = $this->languageService->getTable();
        $template = include(resource_path() . '/lang/default/front.php');
        return response()->view('cms::admin.frontLanguages.index', compact('table', 'template'));
    }


    public function store(StoreRequest $request)
    {
        $language = $this->languageService->create($request);

        alert()->success(__('app.languages.alerts.saved', ['name' => $language->name]));
        Log::write('Language', 'Added new front language', 1, $language->id . ': ' . $language->name . ' [' . $language->code . ']');
        return redirect()->route('admin.frontLanguages.edit', [$language]);
    }

    public function edit(Language $language)
    {
        if ($language->front) {
            $template = include(resource_path() . '/lang/default/front.php');
            return response()->view('cms::admin.frontLanguages.edit', compact('template', 'language'));
        }
        return redirect()->route('admin.frontLanguages.index');
    }

    public function update(UpdateRequest $request, Language $language)
    {
        if ($language->front) {
            $language = $this->languageService->update($request, $language);

            alert()->success(__('app.languages.alerts.saved', ['name' => $language->name]));
            Log::write('Language', 'Updated front language', 1, $language->id . ': ' . $language->name . ' [' . $language->code . ']');
            if ($request->has('back')) {
                return redirect()->route('admin.frontLanguages.index');
            }
            return redirect()->route('admin.frontLanguages.edit', [$language]);
        }
        return redirect()->route('admin.frontLanguages.index');
    }

    public function stateChange(Language $language)
    {
        if ($language->front) {
            if (CmsConfig::get('default_front_language')->value == $language->code) {
                alert()->warning(__('app.frontLanguages.alerts.isMainLanguage'));
                return redirect()->route('admin.frontLanguages.index');
            }
            if ($language->active) {
                $language->active = 0;
                alert()->success(__('app.frontLanguages.alerts.deactivated', ['name' => $language->name]));
                Log::write('Language', 'Front language deactivated', 1, $language->id . ': ' . $language->name . ' [' . $language->code . ']');
            } else {
                $language->active = 1;
                alert()->success(__('app.frontLanguages.alerts.activated', ['name' => $language->name]));
                Log::write('Language', 'Front language activated', 1, $language->id . ': ' . $language->name . ' [' . $language->code . ']');
            }
            $language->save();

            return redirect()->route('admin.frontLanguages.edit', compact('language'));
        }
        return redirect()->route('admin.frontLanguages.index');
    }

    public function delete(Language $language)
    {
        $page_languages = PageLanguage::where('language', $language->code)->get();
        $error = false;
        if ($page_languages->count() > 0) {
            alert()->error(__('app.frontLanguages.alerts.cantDelete.pages', ['name' => $language->name]));
            $error = true;
        }
        $configs = Config::where('value', $language->code)->get();
        if ($configs->count() > 0) {
            alert()->error(__('app.frontLanguages.alerts.cantDelete.config', ['name' => $language->name]));
            $error = true;
        }
        if (!$error) {
            $name = $language->name;
            Menu::where('lang', $language->code)->delete();
            $language->delete();
            alert()->success(__('app.frontLanguages.alerts.deleted', ['name' => $name]));
        }
        return redirect()->route('admin.frontLanguages.index');
    }

    public function export(Language $language)
    {
        if (!$language) {
            return response()->json('Language not found', 400);
        }
        $output = $this->languageService->prepareArrayForExport($language);
        $output = json_encode($output);
        $file_name = now()->format('Y_m_d') . '_'. Str::slug(config('app.name')) .'_' . $language->id . '_' . Str::slug($language->name).'_front' . '.json';
        if (!Storage::exists('public/exportFiles')) {
            mkdir(Storage::path('public/exportFiles'), 0775, true);
            chmod(Storage::path('public/exportFiles'), 0775);
        }
        file_put_contents(storage_path('app/public/exportFiles/' . $file_name), $output);
        sleep(1);
        return Storage::download('public/exportFiles/' . $file_name);
    }

    public function import(Request $request)
    {
        $file = $request->file('import_file');
        $data = file_get_contents($file->getRealPath());
        $data = json_decode($data, true);
        if (key_exists('code', $data) && key_exists('name', $data) && key_exists('fields', $data)) {
            LanguageHelper::languageFrontCreate($data['code'], $data['name'], json_encode($data['fields']));
        }else{
            Log::write('ERROR', 'Invalid JSON', 1, 'Language Import');
        }

        return redirect()->route('admin.frontLanguages.index');
    }
}
