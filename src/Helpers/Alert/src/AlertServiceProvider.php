<?php

/*
 * This file is part of Laravel Alert.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Xsoft\Cms\Helpers\Alert;

use Illuminate\Contracts\Container\Container;
/**
 * This is the alert service provider class.
 *
 * @author Vincent Klaiber <hello@vinkla.com>
 */
class AlertServiceProvider extends \Vinkla\Alert\AlertServiceProvider
{

    public function register(): void
    {
        $this->app->singleton('alert', function (Container $app) {
            $session = $app['session.store'];

            return new Alert($session);
        });

        $this->app->alias('alert', Alert::class);
    }

}
