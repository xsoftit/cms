<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@section('head')
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('page-title')</title>
        @include('cms::admin.layouts.assets.css')
    </head>
@show
@section('body')
    <body class="app sidebar-mini rtl pace-done @if(auth()->user()->sidebar_toggle) sidenav-toggled @endif">
    <div class="loader main-loader"><img src="/admin/images/ajax.gif"/></div>
    <div class="alerts">@include('cms::admin.alert.bootstrap')</div>
    @include('cms::admin.layouts.partials.header')
    @include('cms::admin.layouts.partials.aside')
    @include('cms::admin.layouts.partials.content')
    @include('cms::admin.layouts.partials.modals')
    @include('cms::admin.layouts.assets.js')
    </body>
@show
</html>
