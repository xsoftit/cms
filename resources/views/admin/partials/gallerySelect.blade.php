<div class="form-group">
    <label>{{isset($label) ? $label : __('app.galleries.name')}}</label>
    <div class="form-control" style="padding: 0;">
        <input type="hidden" class="data no-clear" name="is_gallery" value="{{isset($name) ? $name : 'gallery'}}">
        <select class="form-control select-data-ajax data gallery-select" data-clear="1"
                data-placeholder="{{__('app.galleries.chooseGallery')}}"
                data-url="{{route('admin.galleries.getForSelect')}}" name="{{isset($name) ? $name : 'gallery'}}">
            @isset($value)
                @php($gallery = \Xsoft\Cms\Models\Gallery::find($value))
                @if($gallery)
                    <option value="{{$gallery->id}}" selected>{{$gallery->name}}</option>
                @endif
            @else
            @endisset
        </select>
    </div>
</div>
