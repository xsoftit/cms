<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Xsoft\Cms\Models\PageSectionTemplate;

class PageSectionTemplateController extends Controller
{
    public function preview(PageSectionTemplate $pageSectionTemplate)
    {
        $preview = $pageSectionTemplate->preview();
        return response()->json($preview);
    }
}
