<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUrlsTableAddPrefix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('urls', function (Blueprint $table) {
            $table->dropUnique('urls_url_language_unique');
            $table->string('prefix')->nullable();
            $table->string('fullUrl')->nullable();
            $table->unique(['language','fullUrl']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('urls', function (Blueprint $table) {
            $table->dropUnique('urls_language_fullurl_unique');
            $table->dropColumn('prefix');
            $table->dropColumn('fullUrl');
            $table->unique(['url','language']);
        });

    }
}
