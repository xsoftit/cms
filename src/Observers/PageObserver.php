<?php

namespace Xsoft\Cms\Observers;

use Illuminate\Support\Facades\DB;
use Xsoft\Cms\Models\Page;

class PageObserver
{
    /**
     * Handle the page "created" event.
     *
     * @param  \Xsoft\Cms\Models\Page $page
     * @return void
     */
    public function created(Page $page)
    {
        //
    }

    /**
     * Handle the page "updated" event.
     *
     * @param  \Xsoft\Cms\Models\Page $page
     * @return void
     */
    public function updated(Page $page)
    {
        //
    }

    /**
     * Handle the page "deleted" event.
     *
     * @param  \Xsoft\Cms\Models\Page $page
     * @return void
     */
    public function deleted(Page $page)
    {
        //
    }

    public function deleting(Page $page)
    {
        $sections = $page->sections;
        foreach ($sections as $section) {
            DB::table('page_page_section')
                ->where('page_id', $page->id)
                ->where('page_section_id', $section->id)
                ->delete();
            $connections = DB::table('page_page_section')->where('page_section_id', $section->id)->count();
            if (!$connections) {
                $section->delete();
            }
        }
    }

    /**
     * Handle the page "restored" event.
     *
     * @param  \Xsoft\Cms\Models\Page $page
     * @return void
     */
    public function restored(Page $page)
    {
        //
    }

    /**
     * Handle the page "force deleted" event.
     *
     * @param  \Xsoft\Cms\Models\Page $page
     * @return void
     */
    public function forceDeleted(Page $page)
    {
        //
    }
}
