<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Xsoft\Cms\CmsServiceProvider;
use Xsoft\Cms\Helpers\LanguageHelper;

class CmsLanguageUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:language:update {--override}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $languages = CmsServiceProvider::LANGUAGES;
        $languageCode = $this->choice(
            'Pick language you want to update',
            $languages
        );
        if ($this->option('override')) {
            LanguageHelper::languageUpdate($languageCode,true);
        }else{
            LanguageHelper::languageUpdate($languageCode);
        }
        echo "Language $languages[$languageCode] [$languageCode] updated successfully!".PHP_EOL;
    }

}
