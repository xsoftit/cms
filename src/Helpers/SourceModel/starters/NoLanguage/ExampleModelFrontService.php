<?php

namespace App\Services\Front;

use App\ExampleModel;
use Xsoft\Cms\Services\Front\WidgetService;

class ExampleModelService extends WidgetService
{
    static public function list($widget, $data, $pageLanguage)
    {
        $where = [];
        $where[] = ['active', 1];
        if (key_exists('amount', $data)) {
            $data['data'] = ExampleModel::where($where)->limit($data['amount'])->get();
        } else {
            $data['data'] = ExampleModel::where($where)->get();
        }
        return [
            'data' => $data,
            'blade' => $widget->template->blade_name,
        ];
    }

    static public function content($widget, $data, $pageLanguage)
    {
        if ($data['source_model'] == $pageLanguage->source_model) {
            $data['data'] = $pageLanguage->object;
        }
        return [
            'data' => $data,
            'blade' => $widget->template->blade_name,
        ];
    }

}
