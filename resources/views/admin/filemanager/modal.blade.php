<div class="modal fade filemanager-modal" id="filemanager-modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="loader"><img src="/admin/images/ajax.gif"/></div>
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('filemanager.title')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-7">
                        <ul class="breadcrumb backTrace">

                        </ul>
                    </div>
                    <div class="col-5">
                        <div class="float-right">
                            <div class="search-box" style="display: none">
                                <input class="form-control" id="search-input">
                            </div>
                            @if(auth()->user()->hasPermission('files.exportImport'))
                                <span class="btn btn-outline-warning" id="export">Export data</span>
                                <span class="btn btn-outline-danger" id="import">Import data</span>
                            @endif
                            <span class="btn btn-outline-info" id="search"><i class="af af-search"></i></span>
                            @if(auth()->user()->hasPermission('files.foldersManage'))
                                <span class="btn btn-outline-light" id="newFolder"><i
                                        class="af af-folder-plus"></i></span>
                            @endif
                            @if(auth()->user()->hasPermission('files.manage'))
                                <span class="btn btn-outline-secondary" id="newFile"><i class="af af-upload"></i></span>
                            @endif
                        </div>
                    </div>
                </div>
                @if(auth()->user()->hasPermission('files.manage'))
                    <span class="select-all">{{__('filemanager.selectAll')}}</span>
                @endif
                <div class="add-section">
                    <div class="new-folder" style="display: none">
                        <label>{{__('filemanager.newFolder')}}</label>
                        <div class="input-group">
                            <input type="text" class="form-control folder-name" name="name">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-light folder-add"
                                        style="margin: 0">{{__('app.create')}}</button>
                            </div>
                        </div>
                    </div>
                    <div class="new-file" style="display: none">
                        <div class="input-group">
                            <div class="file-input">
                                <label>{{__('filemanager.newFile')}}</label>
                                <p></p>
                                <input type="file" class="form-control files-input" name="files[]" multiple>
                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-light file-add"
                                        style="margin: 0">{{__('app.add')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="files row">

                </div>
                <div class="picked-files row">
                    <h3 style="display: none">{{__('filemanager.chosenFiles')}}</h3>
                </div>
            </div>
            <div class="modal-footer">
                <button id="add-files" type="submit" class="btn btn-outline-success">
                    {{__('app.save')}}
                </button>
                <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                    {{__('app.close')}}
                </button>
            </div>
            <div class="delete-content">
                <h3>{{__('filemanager.deleteQuestion')}} <b></b>?<br>
                    <small>({{__('filemanager.deleteWarning')}})</small>
                </h3>
                <div class="row">
                    <div class="col-12">
                        <button type="button" id="deleteFileConfirm"
                                class="btn btn-outline-danger">{{__('app.deleteButton.confirm')}}</button>
                        <button type="button" id="deleteFileCancel"
                                class="btn btn-outline-light">{{__('app.deleteButton.cancel')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(auth()->user()->hasPermission('files.manage') || auth()->user()->hasPermission('files.foldersManage'))
    <span class="filemanager-open"><i class="af af-folder-open"></i></span>
@endif
<ul id="filemanager-file-custom-menu" class="custom-menu">
    <li data-action="copyToClipboard"><i class="af af-copy"></i>{{__('filemanager.copyToClipboard')}}</li>
    @if(auth()->user()->hasPermission('files.manage'))
        <li data-action="renameFile"><i class="af af-edit"></i>{{__('filemanager.rename')}}</li>
    @endif
    @if(auth()->user()->hasPermission('files.delete'))
        <li data-action="deleteFile"><i class="af af-trash"></i>{{__('app.delete')}}</li>
    @endif
</ul>
@if(auth()->user()->hasPermission('files.foldersManage'))
    <ul id="filemanager-directory-custom-menu" class="custom-menu">
        <li data-action="renameDirectory"><i class="af af-edit"></i>{{__('filemanager.rename')}}</li>
        <li data-action="deleteDirectory"><i class="af af-trash"></i>{{__('app.delete')}}</li>
    </ul>
@endif
@if(auth()->user()->hasPermission('files.exportImport'))
    <div class="modal fade" id="filemanager-import-files-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{route('admin.files.import')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="modal-header">
                        <h5 class="modal-title">{{__('filemanager.import.title')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>{{__('filemanager.import.description')}}</label>
                            <input class="form-control" type="file" name="import_file">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit"
                                class="btn btn-outline-success">{{__('filemanager.import.button')}}</button>
                        <button type="reset" class="btn btn-outline-light"
                                data-dismiss="modal">{{__('app.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif
@section('js')
    @parent
    @include('cms::admin.filemanager.js')
@endsection

