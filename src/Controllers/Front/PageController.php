<?php

namespace Xsoft\Cms\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Xsoft\Cms\Helpers\CmsConfig;
use Xsoft\Cms\Helpers\FrontProcessors\DataProcessor;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\Url;

class PageController extends Controller
{
    public function index(Request $request, $url = null)
    {
        $multiDomains = CmsConfig::get('multi_domains')->value;
        $languageFallback = CmsConfig::get('default_front_language')->value;
        $language = $request->get('language');
        if ($language == 404) {
            $page = PageLanguage::where('is_main', 1)->where('language', $languageFallback)->first();
            if ($page) {
                $dp = new DataProcessor($page);
                $request->merge(['dataProcessor' => $dp]);
            }
            abort(404);
        }
        if (!$multiDomains) {
            $params = explode('/', $url, 2);
            if (key_exists(1, $params)) {
                $url = $params[1];
            }else{
                $url = '';
            }
        }
        $pageLanguage = null;
        if (!$url) {
            $pageLanguage = PageLanguage::where('is_main', 1)->where('language', $language)->first();
        } else {
            $urlObject = Url::findObject($url, $language);
            if ($urlObject) {
                $pageLanguage = $urlObject->getPageLanguage();
            }
        }
        if (!$pageLanguage) {
            $page = PageLanguage::where('is_main', 1)->where('language', $language)->first();
            if ($page) {
                $dp = new DataProcessor($page);
                $request->merge(['dataProcessor' => $dp]);
            }
            abort(404);
        }
        Session::put('frontLocale', $pageLanguage->language);
        if (config('app.env') == "local" || $pageLanguage->sourceModel) {
            $pageLanguage->local = true;
        } else {
            if (!$pageLanguage->activeVersion) {
                $page = PageLanguage::where('is_main', 1)->where('language', $language)->first();
                if ($page) {
                    $dp = new DataProcessor($page);
                    $request->merge(['dataProcessor' => $dp]);
                }
                abort(404);
            }
        }
        $dp = new DataProcessor($pageLanguage);
        $html = view('front.index', [
            'dataProcessor' => $dp
        ]);
        return $html;
    }
}
