<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CmsConfigSeeder::class);
        $this->call(CmsPermissionsSeeder::class);
        $this->call(CmsElementsSeeder::class);
        $this->call(ElementsSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(LanguageSeeder::class);
        if (!\App\User::where('name', 'Superadmin')->first()) {
            $this->call(CmsInitSeeder::class);
            $user = \App\User::create(['name' => 'Superadmin',
                'email' => 'superadmin@example.com',
                'password' => \Illuminate\Support\Facades\Hash::make('secret'),
                'active' => 1,
                'language' => \Xsoft\Cms\Models\Config::where('name', 'default_lang')->first()->value
            ]);
            $user->assignRole('superadmin');
        }
    }
}
