@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.sourceModels.title.index')}}
@endsection

@section('title')
    {{__('app.sourceModels.title.index')}} <a href="{{route('admin.sourceModels.create')}}"><i class="af af-plus-circle color-success"></i></a>
@endsection
@section('subtitle')
    {{__('app.sourceModels.subtitle.index')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.sourceModels.breadcrumbs.index')]) !!}
@endsection

@section('page-content')
    @dataTable($table)
@endsection
