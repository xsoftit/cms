@component('cms::admin.templates.widgetModal',get_defined_vars())
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.source')}}</label>
        <input id="source" name="source" class="form-control data">
    </div>
@endcomponent
