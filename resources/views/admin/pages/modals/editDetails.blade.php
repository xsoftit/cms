@section('modals')
    @parent
    <div class="modal fade" id="edit-details" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.editDetails')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="page_language_id">
                    <div class="form-group">
                        <label>{{__('app.pages.form.name')}}</label>
                        <input class="form-control" type="text" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>{{__('app.pages.form.metaTitle')}}</label>
                        <input class="form-control" type="text" name="meta_title" required>
                    </div>
                    <div class="form-group">
                        <label>{{__('app.pages.form.description')}}</label>
                        <textarea class="form-control no-jodit" name="meta_description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>{{__('app.pages.form.url')}}</label>
                        <div class="row url-input-row">
                            <div class="text-right">
                                <span class="url-span">
                                    {{config('cms.FRONT_DOMAIN').'/'}}
                                </span>
                            </div>
                            <div class="col" style="padding-left: 0">
                                <input class="form-control" type="text" name="url" required>
                            </div>
                        </div>
                        <div class="url-info" style="display: none;">{{__('app.pages.form.urlInfo')}}</div>
                    </div>
                    <div class="animated-checkbox is-main-checkbox">
                        <label>
                            <input type="checkbox" name="is_main" id="page-lang-is-main">
                            <span
                                class="label-text text-capitalize">{{__('app.pages.form.isMain')}}</span>
                        </label>
                    </div>
                    {{--<h4 class="mt30">{{__('app.pages.options')}}</h4>--}}
                    {{--<div class="options">--}}

                    {{--</div>--}}
                </div>
                <div class="modal-footer">
                    @if(auth()->user()->hasPermission('pages.delete'))
                        <button type="button" id="deletePage"
                                class="btn btn-outline-danger margin-right-auto"
                                style="display:none;">{{__('app.deleteButton.title')}}</button>
                    @endif
                    <button type="button" id="submit-details" class="btn btn-outline-success">{{__('app.update')}}</button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">{{__('app.close')}}</button>
                </div>
                @if(auth()->user()->hasPermission('pages.delete'))
                    <div class="delete-content">
                        <h3>{{__('app.pages.questions.delete')}}<br>
                            <small>({{__('app.pages.deleteWarning')}})</small>
                        </h3>
                        <div class="row">
                            <form action="{{route('admin.pages.delete')}}" method="POST">
                                <input type="hidden" name="page_language_id">
                                @csrf
                                <div class="col-12">
                                    <button type="submit" id="deletePageConfirm"
                                            class="btn btn-outline-danger">{{__('app.deleteButton.confirm')}}</button>
                                    <button type="reset" id="deletePageCancel"
                                            class="btn btn-outline-light">{{__('app.deleteButton.cancel')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
