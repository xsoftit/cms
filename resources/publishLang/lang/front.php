<?php

$locale = \Illuminate\Support\Facades\Session::get('frontLocale');

$langKey = 'front-language-fields-'.$locale;
if (Illuminate\Support\Facades\Cache::has($langKey)) {
    $fields = Illuminate\Support\Facades\Cache::get($langKey);
        return $fields;
}
