@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.exampleModels.title.edit')}}
@endsection

@section('title')
    {{__('app.exampleModels.title.edit')}}
@endsection
@section('subtitle')
    {{__('app.exampleModels.subtitle.edit')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.exampleModels.breadcrumbs.index'),route('admin.exampleModels.index')],[__('app.exampleModels.breadcrumbs.edit')]) !!}
@endsection

@section('page-content')
    <form action="{{route('admin.exampleModels.update',['exampleModel'=>$exampleModel->id])}}" method="post">
        @csrf
        @method('POST')
        FIELD_INPUTS
        @php($languages = LanguageHelper::getAllFront())
        <ul class="nav nav-tabs mt20">
            @foreach($languages as $language)
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab-{{$language}}">{{$language}}</a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content" id="myTabContent">
            @foreach($languages as $language)
                @php($content = $exampleModel->getContent($language))
                <div class="tab-pane fade" id='tab-{{$language}}'>
                    @if($content->slug)
                        @include('cms::admin.partials.url',[
                            'label' => __('app.exampleModels.fields.url'),
                            'item' => $content,
                            'name' => 'contents['.$language.'][url]'
                        ])
                    @endif
                    CONTENT_FIELD_INPUTS
                </div>
            @endforeach
        </div>
        <div class="form-group h-20">
            @if(auth()->user()->hasPermission('exampleModels.delete'))
                @deleteButton(route('admin.exampleModels.destroy',[$exampleModel->id]))
            @endif
            <input type="submit" class="btn btn-success float-right" value="{{__('app.save')}}" name="save">
            <input type="submit" class="btn btn-secondary float-right" value="{{__('app.saveAndBack')}}" name="back">
        </div>
    </form>
@endsection
