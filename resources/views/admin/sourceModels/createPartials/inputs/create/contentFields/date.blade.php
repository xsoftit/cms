<div class="form-group">
    <label>{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</label>
    <input type="date" name="contents[{\{$language}}][{{$field->getName()}}]" value="{\{old('contents[{\{$language}}][{{$field->getName()}}]')}}" class="form-control">
</div>
