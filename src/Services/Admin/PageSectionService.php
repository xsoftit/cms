<?php

namespace Xsoft\Cms\Services\Admin;


use Xsoft\Cms\Models\PageSection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PageSectionService
{
    public static function create($section_template_id, $name, $page)
    {
        $page_section = PageSection::create([
            'section_template_id' => $section_template_id,
            'name' => $name,
            'slug' => Str::slug($name),
        ]);
        DB::table('page_page_section')->insert([
            'page_section_id' => $page_section->id,
            'page_id' => $page->id,
            'order' => count($page->sections)
        ]);
        return $page_section;
    }

    public static function copy($section_id, $page, $with_content){
        $original_section = PageSection::find($section_id);
        $page_section = $original_section->replicate();
        $page_section->save();
        DB::table('page_page_section')->insert([
            'page_section_id' => $page_section->id,
            'page_id' => $page->id,
            'order' => count($page->sections)
        ]);
        foreach ($original_section->widgets as $widget){
            $newWidget = $widget->replicate();
            $newWidget->save();
            DB::table('page_section_widget')->insert([
                'page_section_id' => $page_section->id,
                'widget_id' => $newWidget->id,
                'placement_column' => $widget->pivot->placement_column,
                'placement_order' => $widget->pivot->placement_order
            ]);
            if($with_content){
                foreach($widget->contents as $content){
                    $newContent = $content->replicate();
                    $newContent->widget_id = $newWidget->id;
                    $newContent->save();
                }
            }
        }
        return $page_section;
    }
}
