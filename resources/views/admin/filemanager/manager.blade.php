@if(!isset($deletable))
    @php($deletable = false)
@endif
@if(!isset($data))
    @php($data = null)
@endif
@if(!isset($label))
    @php($label = '')
@endif
@if(!isset($sublabel))
    @php($sublabel = '')
@endif
@if(!isset($thumbs))
    @php($thumbs = false)
@endif
@if(!isset($required))
    @php($required = false)
@endif
<div class="row filemanager-row">
    <div class="col-12">
        <input type="hidden" name="fileInputType.{{$name}}" class="data no-clear" value="{{$type}}">
        <input type="hidden" name="files.{{$name}}" class="data no-clear" value="{{$name}}">
        <div class="form-group">
            @if($label && $data)
                @if(is_array(json_decode($data,true)))
                    @if(count(json_decode($data,true)) > 0)
                        <label class="label-text">{{__('app.widgets.labels.chosen').' '.$label}}</label>
                    @endif
                @endif
            @endif
            <ul id="{{str_replace('.','-',$name)}}-preview-modal"
                class="current-images @if($type == 'single') image-preview-modal @else gallery-images-sortable list-group @endif"
                data-name="{{$name}}" data-deletable="{{$deletable}}">
                @if($data)
                    @foreach(\Xsoft\Cms\Models\File::get($data) as $file)
                        <li class="ui-state-default list-group-item" data-id="{{$file->id}}">
                            <div class="gallery-image-preview-modal">
                                @if($file->type == 'image' || $file->type == 'icon')
                                    <img src="{{$file->path}}">
                                @elseif($file->type == 'video')
                                    <video width="100%" height="100%">
                                        <source src="{{$file->path}}" type="video/mp4"/>
                                    </video>
                                @else
                                    <img src="/admin/images/file.png"/>
                                @endif
                            </div>
                            <span> {{$file->name}} </span>
                            @if($deletable)
                                <div class="remove_item float-right"><i class="afs af-times"></i></div>
                            @endif
                            @if($thumbs)
                                <div class="add_thumb float-right" title="{{__('filemanager.uploadThumb')}}"><i class="afs af-image"></i></div>
                                <input type="file" name="{{$name}}" class="data filemanager-input thumb-input" data-id="{{$file->id}}">
                                @if($file->thumb_path)
                                    <img class="thumb-preview" src="{{$file->thumb(true)}}">
                                @endif
                            @endif
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
        @if($type == 'single')
            <div class="form-group">
                @if($label)
                    <label class="label-text">{{$label}}@if($sublabel)
                            <small>({{$sublabel}})</small> @endif</label>
                @endif
                <div class="row">
                    <div class="col-12">
                        <button type='button' data-type="single" class="btn btn-outline-secondary chooseFile"
                                data-name="{{$name}}"
                                data-filetypes='{!! json_encode($fileTypes) !!}'>{{__('filemanager.buttonTextSingle')}}</button>
                    </div>
                </div>
                <input id="singleInput" type="text" name="{{$name}}" class="data filemanager-input"
                       @if($data) value="{{$data}}" @endif @if($required) required @endif>
            </div>
        @elseif($type == 'multi')
            <div class="form-group">
                @if($label)
                    <label class="label-text">{{$label}}@if($sublabel)
                            <small>({{$sublabel}})</small> @endif</label>
                @endif
                <div class="row">
                    <div class="col-12">
                        <button type='button' data-type="multi" class="btn btn-outline-secondary chooseFile"
                                data-name="{{$name}}"
                                data-filetypes='{!! json_encode($fileTypes) !!}'>{{__('filemanager.buttonTextMulti')}}</button>
                    </div>
                </div>
                <input id="multiInput" type="text" name="{{$name}}" class="data filemanager-input"
                       @if($data) value="{{$data}}" @endif @if($required) required @endif>
            </div>
        @endif
    </div>
    <div class="col-12">
        <div class="manager-preview row" name="{{$name}}">
        </div>
    </div>
</div>

