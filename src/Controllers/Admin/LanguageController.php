<?php

namespace Xsoft\Cms\Controllers\Admin;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Xsoft\Cms\Helpers\CmsConfig;
use App\Http\Controllers\Controller;
use Xsoft\Cms\Helpers\LanguageHelper;
use Xsoft\Cms\Requests\Language\StoreRequest;
use Xsoft\Cms\Requests\Language\UpdateRequest;
use Xsoft\Cms\Models\Language;
use Xsoft\Cms\Services\Admin\LanguageService;
use App\User;
use Hermit\Logs\Log;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    protected $languageService;

    public function __construct()
    {
        $this->languageService = new LanguageService();
    }

    public function indexAjax(Request $request)
    {
        $table = $this->languageService->getTable($request);
        $output = $table->output($request);
        $this->languageService->prepareOutput($output);
        return $output;
    }

    public function index()
    {
        $table = $this->languageService->getTable();
        $template = include(resource_path() . '/lang/default/template.php');
        return response()->view('cms::admin.config.languages.index', compact('table', 'template'));
    }

    public function store(StoreRequest $request)
    {
        $language = $this->languageService->create($request);

        alert()->success(__('app.languages.alerts.saved', ['name' => $language->name]));
        Log::write('Language', 'Added new admin language', 1, $language->id . ': ' . $language->name . ' [' . $language->code . ']');
        return redirect()->route('admin.config.languages.edit', [$language]);
    }

    public function edit(Language $language)
    {
        if (!$language->front) {
            $template = include(resource_path() . '/lang/default/template.php');
            return response()->view('cms::admin.config.languages.edit', compact('template', 'language'));
        }
        return redirect()->route('admin.config.languages.index');
    }

    public function update(UpdateRequest $request, Language $language)
    {
        if (!$language->front) {
            $language = $this->languageService->update($request, $language);

            alert()->success(__('app.languages.alerts.saved', ['name' => $language->name]));
            Log::write('Language', 'Updated admin language', 1, $language->id . ': ' . $language->name . ' [' . $language->code . ']');
            if ($request->has('back')) {
                return redirect()->route('admin.config.languages.index');
            }
            return redirect()->route('admin.config.languages.edit', [$language]);
        }
        return redirect()->route('admin.config.languages.index');
    }

    public function stateChange(Language $language)
    {
        if (!$language->front) {
            $defaultLang = CmsConfig::get('default_lang')->value;
            if ($defaultLang == $language->code) {
                alert()->warning(__('app.languages.alerts.isMainLanguage'));
                return redirect()->route('admin.config.languages.index');
            }
            if ($language->active) {
                $users = User::where('language', $language->code)->get();
                foreach ($users as $user) {
                    $user->language = $defaultLang;
                    $user->save();
                }
                $language->active = 0;
                alert()->success(__('app.languages.alerts.deactivated', ['name' => $language->name]));
                Log::write('Language', 'Front language deactivated', 1, $language->id . ': ' . $language->name . ' [' . $language->code . ']');
            } else {
                $language->active = 1;
                alert()->success(__('app.languages.alerts.activated', ['name' => $language->name]));
                Log::write('Language', 'Front language activated', 1, $language->id . ': ' . $language->name . ' [' . $language->code . ']');
            }
            $language->save();

            return redirect()->route('admin.config.languages.edit', compact('language'));
        }
        return redirect()->route('admin.config.languages.index');
    }

    public function export(Language $language)
    {
        if (!$language) {
            return response()->json('Language not found', 400);
        }
        $output = $this->languageService->prepareArrayForExport($language);
        $output = json_encode($output);
        $file_name = now()->format('Y_m_d') . '_' . Str::slug(config('app.name')) . '_' . $language->id . '_' . Str::slug($language->name) . '.json';
        if (!Storage::exists('public/exportFiles')) {
            mkdir(Storage::path('public/exportFiles'), 0775, true);
            chmod(Storage::path('public/exportFiles'), 0775);
        }
        file_put_contents(storage_path('app/public/exportFiles/' . $file_name), $output);
        sleep(1);
        return Storage::download('public/exportFiles/' . $file_name);
    }

    public function import(Request $request)
    {
        $file = $request->file('import_file');
        $data = file_get_contents($file->getRealPath());
        $data = json_decode($data, true);
        if (key_exists('code', $data) && key_exists('name', $data) && key_exists('fields', $data)) {
            LanguageHelper::languageAdminCreate($data['code'], $data['name'], json_encode($data['fields']));
        }else{
            Log::write('ERROR', 'Invalid JSON', 1, 'Language Import');
        }

        return redirect()->route('admin.config.languages.index');
    }
}
