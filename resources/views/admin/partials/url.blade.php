<div class="form-group">
    <label>{{$label}}</label>
    <div class="row">
        <div class="text-right">
        <span class="url-span">
            {{$item->hostWithPrefix}}
        </span>
        </div>
        <div class="col" style="padding-left: 0">
            <input type="text" name="{{$name}}" class="form-control"
                   value="{{old($name,$item->url)}}">
        </div>
    </div>
</div>
