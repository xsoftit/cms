<?php

namespace Xsoft\Cms\Requests\Pages;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                Rule::unique('page_languages')->where(function ($query) {
                    return $query->where('language','=',$this->language)->where('source_model_id','=',$this->source_model_id);
                })
            ],
            'meta_title' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('validation.custom.name.required'),
            'name.unique' => __('validation.custom.name.unique',['object' => __('app.pages.name')]),
            'meta_title.required' => __('validation.custom.meta_title.required'),
        ];
    }
}
