<?php


return [

    // DOMAINS
    'FRONT_DOMAIN' => env('FRONT_DOMAIN', 'cms.localhost'),
    'ADMIN_DOMAIN' => env('ADMIN_DOMAIN', 'admin.cms.localhost'),
    'ADMIN_PREFIX' => env('ADMIN_PREFIX', 'admin.'),

    //Files
    'FILES_DISK' => env('FILES_DISK','public')
];
