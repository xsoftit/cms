<li><b>{{__('app.widgets.labels.type')}}:</b> <span id="type">{{$widget->getContent('type', $pageLanguage)}}</span></li>
<li><b>{{__('app.widgets.labels.header')}}:</b> <span id="header">{{$widget->getContent('header', $pageLanguage)}}</span></li>
<li><b>{{__('app.widgets.labels.text')}}:</b> <span id="text">{{$widget->getContent('text', $pageLanguage)}}</span></li>
