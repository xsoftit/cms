<?php

namespace Xsoft\Cms\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Email extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $blade_name;
    public $plain_blade_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($email, $blade_name = 'cms::emails.email', $plain_blade_name = 'cms::emails.email_plain')
    {
        $this->email = $email;
        $this->blade_name = $blade_name;
        $this->plain_blade_name = $plain_blade_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view($this->blade_name)
            ->text($this->plain_blade_name)
            ->replyTo($this->email->replyTo, $this->email->person)
            ->subject($this->email->title);
    }
}
