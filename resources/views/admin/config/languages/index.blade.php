@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.languages.title.index')}}
@endsection

@section('title')
    {{__('app.languages.title.index')}}
@endsection

@section('subtitle')
    {{__('app.languages.subtitle.index')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.config.title')]) !!}
@endsection

@section('page-content')
    @include('cms::admin.config.partials.pills',['active' => 'language'])
    <div class="row">
        <div class="col-md-12">
            @if(auth()->user()->hasPermission('admin_languages.create'))
                <span class="btn btn-outline-success float-right" data-toggle="modal"
                      data-target="#language-create">{{__('app.add')}}</span>
            @endif
            @if(auth()->user()->hasPermission('admin_languages.exportImport'))
                <span class="btn btn-outline-secondary float-right" data-toggle="modal"
                      data-target="#language-import">{{__('app.import')}}</span>
            @endif
        </div>
    </div>
    @dataTable($table)
@endsection

@section('modals')
    @if(auth()->user()->hasPermission('admin_languages.create'))
        @include('cms::admin.config.languages.modals.create')
    @endif
    @if(auth()->user()->hasPermission('admin_languages.exportImport'))
        @include('cms::admin.config.languages.modals.import')
    @endif
@endsection

