@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.exampleModels.title.edit')}}
@endsection

@section('title')
    {{__('app.exampleModels.title.edit')}}
@endsection
@section('subtitle')
    {{__('app.exampleModels.subtitle.edit')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.exampleModels.breadcrumbs.index'),route('admin.exampleModels.index')],[__('app.exampleModels.breadcrumbs.edit')]) !!}
@endsection

@section('page-content')
    <form action="{{route('admin.exampleModels.update',['exampleModel'=>$exampleModel->id])}}" method="post">
        @csrf
        @method('POST')
        FIELD_INPUTS
        @include('cms::admin.partials.url',[
            'label' => __('app.exampleModels.fields.url'),
            'item' => $exampleModel,
            'name' => 'url'
        ])
        <div class="form-group">
            @if(auth()->user()->hasPermission('exampleModels.delete'))
                @deleteButton(route('admin.exampleModels.destroy',[$exampleModel->id]))
            @endif
            <input type="submit" class="btn btn-success float-right" value="{{__('app.save')}}" name="save">
            <input type="submit" class="btn btn-secondary float-right" value="{{__('app.saveAndBack')}}" name="back">
        </div>
    </form>
@endsection
