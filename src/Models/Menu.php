<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'lang',
        'items',
        'type'
    ];
}
