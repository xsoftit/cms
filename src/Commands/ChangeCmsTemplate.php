<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class ChangeCmsTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:changeTemplate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Changing adminPanel template';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $template = $this->choice('Pick template', ['Default', 'AdminPro'], 'Default');
        $variables = $this->confirm('Change variables file?',false);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cms".$template."Layout", '--force' => true]);
        if($variables){
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "cms".$template."LayoutVariables", '--force' => true]);
        }
        echo 'Template changed to '.$template;
    }
}
