@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.users.title.index')}} | {{__('app.users.title.edit',['name' => $user->name])}}
@endsection

@section('title')
    {{__('app.users.title.edit',['name' => $user->name])}} @if(!$user->active)
        <small class="text-danger">({{__('app.users.fields.inactive')}})</small> @endif
@endsection

@section('subtitle')
    {{__('app.users.subtitle.edit',['name' => $user->name])}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make(
        [__('app.dashboard.title'),route('admin.dashboard')],
        [__('app.users.title.index'),route('admin.users.index')],
        [__('app.users.title.edit',['name' => $user->name])]
    ) !!}
@endsection

@section('page-content')
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-pills float-right">
                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle btn btn-outline-light" data-toggle="dropdown" href="#"
                                                 role="button" aria-haspopup="true"
                                                 aria-expanded="false">{{__('app.users.actions')}}</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" data-toggle="modal"
                           data-target="#user-password">{{__('app.users.changePassword')}}</a>
                        @if($user->id != auth()->id())
                            <a class="dropdown-item" data-toggle="modal"
                               data-target="#user-role">{{__('app.users.changeRole')}}</a>
                            <div class="dropdown-divider"></div>
                            @if($user->active)
                                <a class="dropdown-item" data-toggle="modal"
                                   data-target="#user-confirmation">{{__('app.users.deactivate')}}</a>
                            @else
                                <a class="dropdown-item" data-toggle="modal"
                                   data-target="#user-confirmation">{{__('app.users.activate')}}</a>
                            @endif
                        @endif
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <form action="{{route('admin.users.update',[$user->id])}}" method="POST">
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="name">{{__('app.users.fields.name')}}*</label>
                    <input class="form-control" name="name" required value="{{old('name',$user->name)}}">
                </div>
                <div class="form-group">
                    <label for="email">{{__('app.users.fields.email')}}*</label>
                    <input class="form-control" name="email" required value="{{old('email',$user->email)}}">
                </div>
            </div>
        </div>
        @if(auth()->user()->hasPermission('users.delete') && $user->id != auth()->id())
            @deleteButton(route('admin.users.destroy',[$user->id]))
        @endif
        <input type="submit" class="btn btn-outline-success float-right" value="{{__('app.save')}}" name="save">
        <input type="submit" class="btn btn-outline-secondary float-right" value="{{__('app.saveAndBack')}}" name="back">
    </form>
@endsection

@section('modals')
    @include('cms::admin.users.modals.changePassword')
    @if($user->id != auth()->id())
        @include('cms::admin.users.modals.confirmation')
        @include('cms::admin.users.modals.changeRole')
    @endif
@endsection
