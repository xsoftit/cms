<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Xsoft\Cms\CmsServiceProvider;
use Xsoft\Cms\Helpers\LanguageHelper;

class CmsLanguageCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:language:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $choiceLanguages = [];
        $installedLanguages = LanguageHelper::getAll();
        foreach (CmsServiceProvider::LANGUAGES as $code => $lang) {
            if (!in_array($code, $installedLanguages)) {
                $choiceLanguages[$code] = $lang;
            }
        }
        if (count($choiceLanguages)) {
            $languageCode = $this->choice(
                'Pick language you want to install',
                $choiceLanguages
            );
            LanguageHelper::languageCreate($languageCode, $choiceLanguages[$languageCode]);
            echo "Language $choiceLanguages[$languageCode] [$languageCode] installed successfully!".PHP_EOL;
        }else{
            echo 'No languages available';
        }
    }

}
