<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Xsoft\Cms\Traits\UrlTrait;

class ExampleModel extends Model
{
    use SoftDeletes,UrlTrait;

    protected $fillable = [
//FILLABLE
    ];

    static public function fields($string = false)
    {
        $fields = [
//FILLABLE
        ];
        if ($string) {
            $output = '';
            foreach ($fields as $key => $field) {
                $output .= $field;
                if ($key + 1 != count($field)) {
                    $output .= '; ';
                }
            }
        } else {
            $output = $fields;
        }
        return $output;
    }
}
