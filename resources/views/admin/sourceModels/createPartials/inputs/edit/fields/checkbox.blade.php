<div class="animated-checkbox">
    <label>
        <input type="checkbox" name="{{$field->getName()}}"{\{$exampleModel->{{$field->getName()}}?' checked="true"':''}}>
        <span
            class="label-text text-capitalize">{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</span>
    </label>
</div>
