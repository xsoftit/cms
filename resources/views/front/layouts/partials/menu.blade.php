@if($type == 'main')
    <ul class="navbar-nav">
        @foreach($menu as $item)
            <li class="nav-item{{$item['active'] ? ' active' : ''}}"><a class="nav-link"
                                                                               href="{{$item['route']}}">{{$item['name']}}</a>
            </li>
        @endforeach
    </ul>
@elseif($type == 'footer')
    @foreach($menu as $item)
        <a href="{{$item['route']}}">{{$item['name']}}</a>
    @endforeach
@endif
