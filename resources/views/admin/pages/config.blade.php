@extends('cms::admin.layouts.app')

@section('page-title')
    {{__('app.pageConfig.title')}}
@endsection

@section('title')
    {{__('app.pageConfig.title')}}
@endsection
@section('subtitle')
    {{__('app.pageConfig.subtitle')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.pageConfig.title')]) !!}
@endsection

@section('page-content')
    <ul id="langTabs" class="nav nav-tabs">
        @foreach($menuLangs as $lang => $menus)
            <li class="nav-item">
                <a class="nav-link text-uppercase" data-code="{{$lang}}" data-toggle="tab"
                   href="#{{$lang}}">{{$lang}}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($menuLangs as $lang => $menus)
            <div id="{{$lang}}" class="tab-pane fade">
                <form action="{{route('admin.pageConfig.update')}}" method="POST">
                    @csrf
                    <h3 class="mb30">{{__('app.pageConfig.manage.menu')}}</h3>
                    <input name="lang" value="{{$lang}}" hidden>
                    @foreach($menus as $menu)
                        @include('cms::admin.pages.partials.menu',['lang' => $lang, 'menu' => $menu])
                        @if (!$loop->last)
                            <hr>
                        @endif
                    @endforeach
                    <div class="form-group">
                        <div class="row">
                            <div class="col-12">
                                @if(auth()->user()->hasPermission('page_config.edit'))
                                    <button type="submit"
                                            class="btn btn-outline-success float-right">{{__('app.save')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(!$globalWidgets->isEmpty())
                        <h3>{{__('app.pageConfig.globalWidgets')}}</h3>
                        <div class="form-group mt30">
                            <div
                                class="preview @if(auth()->user()->hasPermission('page_config.edit')) config-preview @endif">
                                @foreach($globalWidgets as $globalWidget)
                                    @include('cms::admin.templates.widgets.widgetLayout',['widget'=>$globalWidget,'pageLanguage'=>$lang])
                                @endforeach
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        @endforeach
    </div>
@endsection

@section('modals')
    @parent
    @if(auth()->user()->hasPermission('page_config.edit'))
        @foreach($widget_templates as $widget_template)
            @if(view()->exists('admin.templates.widgets.modals.'.$widget_template->blade_name))
                {!!  view('admin.templates.widgets.modals.'.$widget_template->blade_name,['widget' => $widget_template]) !!}
            @else
                @include('cms::admin.templates.widgets.modals.'.$widget_template->blade_name,['widget'=>$widget_template])
            @endif
        @endforeach
    @endif
@endsection

@section('js')
    @parent
    <script>
        var currentPageLangId;
        $('#langTabs').on('click','.nav-link', function () {
            currentPageLangId = $(this).attr('data-code');
        });
    </script>
    @include('cms::admin.pages.partials.js.widgetsJs')
    @include('cms::admin.pages.partials.js.menuJs')
@endsection
