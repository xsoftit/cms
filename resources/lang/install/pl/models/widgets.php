<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    "layout" => "Szablon",
    "content" => "Zawartość",
    "noImages" => "Brak obrazów",
    "deleteWarning" => "Zostanie on usunięty ze wszystkich powiązanych stron",
    "deleteQuestion" => "Czy na pewno chcesz usunąć ten widżet?",
    'labels' => [
        "tag" => "Tag otaczający",
        "icon" => "Ikona",
        "loop" => "Zapętlony",
        "text" => "Tekst",
        "type" => "Typ",
        "class" => "Styl",
        "image" => "Obraz",
        "muted" => "Wyciszony",
        "video" => "Video",
        "width" => "Szerokość",
        "chosen" => "Wybrany",
        "header" => "Nagłowke",
        "height" => "Wysokość",
        "number" => "Numer",
        "onPage" => "Elementów na stronie",
        "source" => "Źródło",
        "styles" => "Style tagu",
        "classes" => "Klasa tagu",
        "gallery" => "Galeria",
        "autoplay" => "Autoodtwarzanie",
        "latitude" => "Szerokość geograficzna",
        "textarea" => "Tekst",
        "addImages" => "Dodaj obraz",
        "extraInfo" => "Dodatkowe informacje",
        "longitude" => "Długość geograficzna",
        "shortText" => "Tekst",
        "buttonLink" => "Link przycisku",
        "buttonText" => "Tekst przycisku",
        "personName" => "Imię osoby",
        "projectsLink" => "Link do projektów",
        "replaceImage" => "Zastąp obraz nowym",
        "existingImage" => "Istniejący obraz",
        "galleryImages" => "Obrazy w galerii",
        "backgroundText" => "Tekst tła",
        "elementsAmount" => "Ilość elemntów",
        "existingImages" => "Obrazy w galerii",
        "personPosition" => "Stanowisko osoby",
        "namePlaceholder" => "Placeholder nazwy",
        "emailPlaceholder" => "Placeholder emaila",
        "descriptionPlaceholder" => "Placeholder opisu",
    ],
    'tags' => [
        "div" => "Div",
        "none" => "Brak",
        "span" => "Span",
        "label" => "Label",
        "header1" => "Nagłowek 1",
        "header2" => "Nagłowek 2",
        "header3" => "Nagłowek 3",
        "header4" => "Nagłowek 4",
        "header5" => "Nagłowek 5",
        "listItem" => "Element listy",
        "paragraph" => "Paragraf",
    ],
    'classes' => [
        "big" => "Big",
        "border" => "Border",
        "primary" => "Primary",
        "secondary" => "Secondary",
    ],
    'texticonTypes' => [
        "number" => "Numer",
        "freeIcon" => "Ikona",
        "headerIcon" => "Ikona w nagłówku",
    ],
    'url' => [
        "enterUrl" => "Wpisz URL",
        "selectPage" => "Wybierz stronę dla której zostanie wygenerownay URL",
    ],
    'alerts' => [
        "added" => "Widżet dodano pomyślnie",
        "updated" => "Widżet zaktulizowano pomyslnie"
    ],
];
