@if($editable)
    <h3 class="section-preview-header{{$section->template->slug == 'footer'?' footer-preview':''}}">
        <span>{{$section->name}}
            <small>({{$section->template->name}})</small></span>
        @if(auth()->user()->hasPermission('sections.edit'))
            <div class="margin-left-auto">
                <i class="af af-chevron-up collapse-trigger"></i>
                <i class="af af-arrows-alt ml10"></i>
                <i class="af af-edit ml10 edit-section"></i>
                <div class="active-badge ml10"></div>
            </div>
        @endif
    </h3>
    <div class="row section-preview">
        {!! $section->generatePreview($pageLanguage) !!}
    </div>
@else
    <span>{{$section->name}}</span>
    <div class="row section-preview">
        @foreach($section->getColumns() as $column_order => $column)
            @if($column)
                <div class="col-{{$column}}">
                    <div class="widget-preview new-preview ">
                        {{__('app.pages.column').' '.($column_order+1)}}
                    </div>
                </div>
            @endif
        @endforeach
    </div>
@endif
