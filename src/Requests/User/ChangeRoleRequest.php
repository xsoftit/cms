<?php

namespace Xsoft\Cms\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ChangeRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role' => 'required|exists:roles,name'
        ];
    }

    public function messages()
    {
        return [
            'role.required' => __('validation.custom.role.required'),
            'role.exists' => __('validation.custom.role.exists'),
        ];
    }
}
