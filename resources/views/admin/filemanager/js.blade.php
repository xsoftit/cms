<script>
    let fileManagerModal = $('#filemanager-modal');
    let loader = fileManagerModal.find('.loader');
    let images = $('body').find(".gallery-images-sortable");
    let remove_item = $('body').find('.remove_item');
    let dataType, fileTypes, name, lastFolder = null;

    $('body').on('click', '.chooseFile', function () {
        dataType = $(this).attr('data-type');
        fileTypes = $(this).attr('data-filetypes');
        name = $(this).attr('data-name');
        getFiles(lastFolder);
    });

    $('body').on('click', '.filemanager-open', function () {
        dataType = 'global';
        fileTypes = null;
        name = null;
        getFiles(lastFolder);
    });

    $('body').on('click', '.remove_item', function () {
        let name = $(this).parents('ul').attr('data-name');
        let id = $(this).parent().attr('data-id');
        let files = JSON.parse($('input[name="' + name + '"]').val());
        $(this).parent().remove();
        files = $.grep(files, function (value) {
            return value != id;
        });
        if (files.length > 0) {
            $('input[name="' + name + '"]').val(JSON.stringify(files));
        } else {
            $('input[name="' + name + '"]').val('');
        }

    });

    $('body').on('click', '.add_thumb', function () {
        $(this).parent().find('.thumb-input').click();
    });

    $('body').on('change','.thumb-input',function(){
        let li = $(this).parent();
        let id = $(this).attr('data-id');
        let file = this.files[0];
        let formData = new FormData();
        formData.append('thumb', file);
        formData.append('id', id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            data: formData,
            contentType: false,
            processData: false,
            url: '{{route('admin.files.uploadThumb')}}',
            success: function (response) {
                if(li.find('.thumb-preview').length > 0){
                    li.find('.thumb-preview').attr('src',response.thumb);
                }else{
                    li.append('<img class="thumb-preview" src="'+response.thumb+'">');
                }
            },
        });
    });

    images.sortable({
        axis: "y",
        placeholder: "list-group-item",
        forcePlaceholderSize: true,
    });
    images.on('sortupdate', function () {
        let name = $(this).attr('data-name');
        let files = $(this).sortable('toArray', {attribute: 'data-id'});
        $('#multiInput[name="' + name + '"]').val(JSON.stringify(files));
    });
    fileManagerModal.on('hide.bs.modal', function () {
        let pickedFilesContent = fileManagerModal.find('.picked-files');
        let backdrop = $('.modal-backdrop');
        backdrop.css('z-index', '1040');
        backdrop.css('opacity', '0.50');
        pickedFilesContent.find('h3').hide();
    });

    @if(auth()->user()->hasPermission('files.manage'))
    fileManagerModal.on('click', '.files .file-box,.files .directory-box', function (e) {
        if (!$(e.target).hasClass('pick') && !fileManagerModal.hasClass('rename')) {
            $(this).toggleClass('active');
        }
    });
    @endif

    fileManagerModal.on('click', '.pick', function (e) {
        let file = $(this).parent().parent();
        let pickedFile = file.clone();
        let pickedFilesContent = fileManagerModal.find('.picked-files');
        pickedFile.removeAttr('data-toggle');
        let icon = pickedFile.find('i');
        icon.removeClass('pick');
        icon.addClass('remove');
        icon.removeClass('af-plus-circle');
        icon.addClass('af-minus-circle');
        if (dataType === 'single') {
            pickedFilesContent.find('.col-file').remove();
        }
        pickedFile.find('.file-box').addClass('picked');
        pickedFilesContent.append(pickedFile);
        pickedFilesContent.find('h3').show();
    });

    fileManagerModal.on('click', '.remove', function (e) {
        let file = $(this).parent().parent();
        let pickedFilesContent = fileManagerModal.find('.picked-files');
        file.remove();
        if (!pickedFilesContent.find('.col-file').length > 0) {
            pickedFilesContent.find('h3').hide();
        }
    });

    @if(auth()->user()->hasPermission('files.manage'))
    fileManagerModal.on('click', '.select-all', function () {
        fileManagerModal.find('.file-box,.directory-box').not('.parent-directory').addClass('active');
    });
    @endif

    fileManagerModal.on('click', '.breadcrumb-item', function () {
        getFiles($(this).attr('data-id'));
    });

    fileManagerModal.on('dblclick', '.directory-box', function () {
        getFiles($(this).attr('data-id'));
    });

    @if(auth()->user()->hasPermission('files.manage'))
    fileManagerModal.on('click', '#add-files', function () {
        let files = [];
        let newFiles = [];
        if (dataType === 'multi') {
            if ($('#multiInput[name="' + name + '"]').val()) {
                files = JSON.parse($('#multiInput[name="' + name + '"]').val());
            }
        }
        $.each(fileManagerModal.find('.picked'), function () {
            newFiles.push($(this).attr('data-id'));
        });
        if (dataType === 'single') {
            $('#singleInput[name="' + name + '"]').val(JSON.stringify(newFiles));
        } else if (dataType === 'multi') {
            $('#multiInput[name="' + name + '"]').val(JSON.stringify(files.concat(newFiles)));
        } else {
            currentEditor.fileManager.insertFiles(fileManagerModal.find('.picked'));
        }
        generatePreview(newFiles);
    });

    fileManagerModal.on('click', '#newFile', function () {
        let newFolder = $('.new-folder');
        let newFile = $('.new-file');
        newFile.toggle();
        newFolder.hide();
        if (newFile.css('display') === 'none') {
            fileManagerModal.find('.files-input').val('');
            fileManagerModal.find('.files-input').trigger('change');
        }
    });

    fileManagerModal.on('click', '.file-add', function () {
        addFile();
    });
    @endif

    @if(auth()->user()->hasPermission('files.foldersManage'))
    fileManagerModal.on('click', '#newFolder', function () {
        let newFolder = $('.new-folder');
        let newFile = $('.new-file');
        newFolder.toggle();
        newFile.hide();
        if (newFolder.css('display') === 'none') {
            fileManagerModal.find('.folder-name').val('');
        }
    });

    fileManagerModal.on('click', '.folder-add', function () {
        addFolder();
    });
    @endif

    fileManagerModal.on('click', '#search', function () {
        $('.search-box').toggle();
    });

    fileManagerModal.on('click', function (e) {
        if ((!$(e.target).hasClass('file-box') && !$(e.target).parents('.file-box').length) && (!$(e.target).hasClass('directory-box') && !$(e.target).parents('.directory-box').length) && !$(e.target).hasClass('select-all')) {
            fileManagerModal.find('.file-box,.directory-box').removeClass('active');
        }
    });

    fileManagerModal.on('keyup', '#search-input', function () {
        let searchText = $(this).val();
        let files = fileManagerModal.find('.files div');
        files.each(function () {
            if (searchText) {
                let fileName = $(this).find('span').text().toLowerCase();
                if (fileName.includes(searchText)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            } else {
                $(this).show();
            }
        });
    });

    fileManagerModal.on('change', '.files-input', function (event) {
        let input = $(this);
        let html = '';
        if (input.prop('files').length > 0) {
            input.siblings('label').hide();
            $.each(event.target.files, function (index, file) {
                html += '<span>' + file.name + '</span>';
            });
            input.siblings('p').html(html);
        } else {
            input.siblings('label').show();
            input.siblings('p').html('');
        }
    });

    @if(auth()->user()->hasPermission('files.foldersManage'))
    function addFolder() {
        loader.show();
        let parent_id = fileManagerModal.find('.backTrace li:last-child').attr('data-id');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            data: {
                parent_id: parent_id,
                name: fileManagerModal.find('.folder-name').val()
            },
            url: '{{route('admin.files.create.folder')}}',
            success: function () {
                getFiles(parent_id);
            },
            error: function () {
                alert("{{__('app.alerts.ajaxError')}}");
            }
        });
    }

    @endif

    @if(auth()->user()->hasPermission('files.manage'))
    function addFile() {
        loader.show();
        let parent_id = $('.backTrace li:last-child').attr('data-id');
        let files = fileManagerModal.find('.files-input').prop('files');
        let filesData = new FormData();
        $.each(files, function (index, file) {
            filesData.append('files[' + index + ']', file);
        });
        filesData.append('parent_id', parent_id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            data: filesData,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            type: "POST",
            dataType: 'json',
            url: '{{route('admin.files.create.files')}}',
            success: function () {
                getFiles(parent_id);
            },
            error: function () {
                alert("{{__('app.alerts.ajaxError')}}");
            }
        });
    }

    @endif

    function generatePreview(files) {
        let ids = JSON.stringify(files);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            data: {
                ids: ids
            },
            url: '{{route('admin.files.preview')}}',
            success: function (response) {
                $('.manager-preview[name="' + name + '"]').html(drawData(response, true));
                fileManagerModal.modal('hide');
            },
            error: function (data) {
                alert("{{__('app.alerts.ajaxError')}}");
            }
        });
    }

    function getFiles(parent_id) {
        let data = {
            fileTypes: fileTypes
        };
        if (parent_id > 0) {
            data.parent_id = parent_id;
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            data: data,
            url: '{{route('admin.files.get')}}',
            success: function (response) {
                lastFolder = parent_id;
                fileManagerModal.find('.modal-body .files').html(drawData(response.data));
                if (parent_id > 0) {
                    insertParent(parent_id, response.backTrace);
                }
                fileManagerModal.find('.modal-body .backTrace').html(drawBackTrace(response.backTrace));
                fileManagerModal.find('.picked-files').find('.col-file,.col-video').remove();
                fileManagerModal.find('.folder-name').val('');
                fileManagerModal.find('.files-input').val('');
                fileManagerModal.find('.files-input').trigger('change');
                fileManagerModal.attr('data-type', dataType);
                $('.new-file').hide();
                $('.new-folder').hide();
                $('.search-box').hide();
                $('#search-input').val('');
                loader.hide();
                if (dataType === 'global') {
                    fileManagerModal.find('#add-files').hide();
                    fileManagerModal.addClass('global');
                } else {
                    let backdrop = $('.modal-backdrop');
                    backdrop.css('z-index', '1060');
                    backdrop.css('opacity', '0.25');
                    fileManagerModal.find('#add-files').show();
                    fileManagerModal.removeClass('global');
                }
                let files = fileManagerModal.find('.file-box, .directory-box');
                let directories = fileManagerModal.find('.directory-box');
                @if(auth()->user()->hasPermission('files.manage'))
                initDropable(files, directories);
                @endif
                hideRenameBox();
                fileManagerModal.modal('show');
            },
            error: function (data) {
                alert("{{__('app.alerts.ajaxError')}}");
            }
        });
    }

    function insertParent(parent_id, backTrace) {
        let files = fileManagerModal.find('.modal-body .files');
        let html = files.html();
        let id = 0;
        $.each(backTrace, function (i, v) {
            if (v[0] == parent_id) {
                id = backTrace[i - 1][0];
            }
        });
        let block = '<div class="col-file"><div class="directory-box parent-directory" data-id="' + id + '"><img src="/admin/images/folder.png" class="img-responsive"><span> .. </span></div></div>';
        files.html(block + html);
    }

    function drawBackTrace(data) {
        let content = '';
        $.each(data, function (i, v) {
            if (i < data.length - 1) {
                content += '<li class="breadcrumb-item" data-id="' + v[0] + '"><span>' + v[1] + '</span></li>'
            } else {
                content += '<li class="breadcrumb-item disabled" data-id="' + v[0] + '"><span>' + v[1] + '</span></li>'
            }
        });
        return content;
    }

    function drawData(data, preview) {
        let content = '';
        $.each(data, function (i, v) {
            if (v.type === 'directory') {
                content += createDirectory(v);
            }
            else if (v.type === 'image' || v.type === 'icon') {
                content += createImage(v, preview);
            }
            else if (v.type === 'video') {
                content += createVideo(v, preview);
            }
            else {
                content += createOther(v, preview);
            }
        });
        return content;
    }

    function createImage(item, preview) {
        let content = '<div class="col-file" data-toggle="custom-menu" data-target="#filemanager-file-custom-menu" data-path="' + item.path + '">';
        content += '<div class="file-box" data-id="' + item.id + '">';
        if (dataType !== 'global' && !preview) {
            content += '<i class="af af-plus-circle pick filemanager-icon"></i>';
        }
        content += '<img src="' + item.path + '" class="img-responsive"/>';
        content += '<span data-alt=' + item.alt + '>' + item.name + '</span>';
        content += '</div>';
        content += '</div>';
        return content;
    }

    function createDirectory(item) {
        let content = '<div class="col-file" data-toggle="custom-menu" data-target="#filemanager-directory-custom-menu">';
        content += '<div class="directory-box" data-id="' + item.id + '">';
        content += '<img src="/admin/images/folder.png" class="img-responsive"/>';
        content += '<span>' + item.name + '</span>';
        content += '</div>';
        content += '</div>';
        return content;
    }

    function createVideo(item, preview) {
        let content = '<div class="col-video" data-toggle="custom-menu" data-target="#filemanager-file-custom-menu" data-path="' + item.path + '">';
        content += '<div class="file-box" data-id="' + item.id + '">';
        if (dataType !== 'global' && !preview) {
            content += '<i class="af af-plus-circle pick filemanager-icon"></i>';
        }
        content += '<video width="100%" height="100%" controls="true"><source src="' + item.path + '" type="video/mp4"/></video>';
        content += '<span data-alt=' + item.alt + '>' + item.name + '</span>';
        content += '</div>';
        content += '</div>';
        return content;
    }

    function createOther(item, preview) {
        let content = '<div class="col-file" data-toggle="custom-menu" data-target="#filemanager-file-custom-menu" data-path="' + item.path + '">';
        content += '<div class="file-box" data-id="' + item.id + '">';
        if (dataType !== 'global' && !preview) {
            content += '<i class="af af-plus-circle pick filemanager-icon"></i>';
        }
        content += '<img src="/admin/images/file.png" class="img-responsive"/>';
        content += '<span data-alt=' + item.alt + '>' + item.name + '</span>';
        content += '</div>';
        content += '</div>';
        return content;
    }

    @if(auth()->user()->hasPermission('files.manage'))
    function initDropable(files, directories) {
        let objects;
        files.draggable({
            delay: 200,
            snapTolerance: 300,
            revert: "invalid",
            start: function (event, ui) {
                let item = ui.helper;
                if (!item.hasClass('active')) {
                    fileManagerModal.find('.file-box,.directory-box').removeClass('active');
                } else {
                    objects = fileManagerModal.find('.file-box.active,.directory-box.active');
                }
            },
            drag: function (event, ui) {
                var currentLoc = $(this).position();
                var orig = ui.originalPosition;
                moveSelected(objects, currentLoc, orig);
            },
            stop: function (event, ui) {
                moveSelected(objects, 0, 0);
                objects = null;
            }
        });
        directories.droppable({
            classes: {
                "ui-droppable-hover": "active"
            },
            tolerance: "pointer",
            deactivate: function (event, ui) {
                var currentLoc = $(this).position();
                var orig = ui.originalPosition;
                moveSelected(objects, currentLoc, orig);
            },
            drop: function (event, ui) {
                let file = ui.draggable;
                if (!objects) {
                    move(file, $(this));
                } else {
                    move(objects, $(this));
                }
            }
        });
    }

    function move(objects, directory) {
        objects.each(function () {
            let file = $(this);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                data: {
                    file_id: file.attr('data-id'),
                    directory_id: directory.attr('data-id')
                },
                url: '{{route('admin.files.move')}}',
                success: function (response) {
                    file.parent().remove();
                },
                error: function (data) {
                    alert("{{__('app.alerts.ajaxError')}}");
                }
            });
        });
    }

    function moveSelected(objects, currentLoc, orig) {
        var offsetLeft = 0;
        var offsetTop = 0;
        if (currentLoc && orig) {
            offsetLeft = currentLoc.left - orig.left;
            offsetTop = currentLoc.top - orig.top;
        }
        if (objects) {
            objects.each(function () {
                $this = $(this);
                $this.css('left', offsetLeft);
                $this.css('top', offsetTop);
            })
        }
    }
    @endif

    // nie dotykać

    function copyToClipboard() {
        var $temp = $("<div style='width: 0px; height: 0px; opacity: 0'>");
        customMenuParent.append($temp);
        $temp.attr("contenteditable", true)
            .html(customMenuParent.attr('data-path')).select()
            .on("focus", function () {
                document.execCommand('selectAll', false, null)
            })
            .focus();
        document.execCommand("copy");
        $temp.remove();
    }

    fileManagerModal.on("contextmenu", '.rename-box', function () {
        return false;
    });

    @if(auth()->user()->hasPermission('files.delete'))
    function deleteFile() {
        let deleteContent = fileManagerModal.find('.delete-content');
        deleteContent.find('h3 b').html(customMenuParent.find('.file-box span').html());
        deleteContent.find('h3 small').show();
        deleteContent.css('display', 'flex');
    }

    @endif

    @if(auth()->user()->hasPermission('files.foldersManage'))
    function deleteDirectory() {
        let directoryId = customMenuParent.find('.directory-box').attr('data-id');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            data: {
                file_id: directoryId,
            },
            url: '{{route('admin.files.checkIfEmpty')}}',
            success: function (data) {
                if (data.empty != 0) {
                    let deleteContent = fileManagerModal.find('.delete-content');
                    deleteContent.find('h3 b').html(customMenuParent.find('.directory-box span').html());
                    deleteContent.find('h3 small').hide();
                    deleteContent.css('display', 'flex');
                } else {
                    showAlert("{{__('filemanager.directoryNotEmpty')}}", 'warning');
                }
            },
            error: function (data) {
                alert('"{{__('app.alerts.ajaxError')}}"');
            },
        });
    }

    function renameDirectory() {
        let directoryBox = customMenuParent.find('.directory-box');
        let name = directoryBox.find('span');
        hideRenameBox();
        fileManagerModal.addClass('rename');
        let box = '<div class="rename-box">' +
            '<input class="form-control" id="directory-name" value="' + name.text() + '">' +
            '<button class="btn btn-success" id="rename-directory"><i class="af af-check"></i></button>' +
            '<button class="btn btn-danger" id="close-rename"><i class="af af-times"></i></button>';
        name.after(box);
        name.hide();
    }

    fileManagerModal.on('click', '#rename-directory', function () {
        let box = customMenuParent.find('.directory-box');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            data: {
                file_id: box.attr('data-id'),
                name: box.find('#directory-name').val(),
            },
            url: '{{route('admin.files.rename')}}',
            success: function (data) {
                let span = box.find('span');
                span.text(data.name);
                hideRenameBox();
            },
            error: function (data) {
                alert('"{{__('app.alerts.ajaxError')}}"');
            },
        });
    });
    @endif

    @if(auth()->user()->hasPermission('files.manage'))
    function renameFile() {
        let fileBox = customMenuParent.find('.file-box');
        let name = fileBox.find('span');
        hideRenameBox();
        fileManagerModal.addClass('rename');
        let box = '<div class="rename-box">' +
            '<input class="form-control" id="file-name" value="' + name.text() + '">' +
            '<label>{{__("filemanager.alt")}}</label>' +
            '<input class="form-control" id="file-alt" value="' + name.attr("data-alt") + '">' +
            '<button class="btn btn-success" id="rename-file"><i class="af af-check"></i></button>' +
            '<button class="btn btn-danger" id="close-rename"><i class="af af-times"></i></button>';
        name.after(box);
        name.hide();
    }

    fileManagerModal.on('click', '#rename-file', function () {
        let box = customMenuParent.find('.file-box');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            data: {
                file_id: box.attr('data-id'),
                name: box.find('#file-name').val(),
                alt: box.find('#file-alt').val(),
            },
            url: '{{route('admin.files.rename')}}',
            success: function (data) {
                let span = box.find('span');
                span.attr('data-alt', data.alt);
                span.text(data.name);
                hideRenameBox();
            },
            error: function (data) {
                alert('"{{__('app.alerts.ajaxError')}}"');
            },
        });
    });
    @endif

    @if(auth()->user()->hasPermission('files.foldersManage') ||  auth()->user()->hasPermission('files.manage'))
    fileManagerModal.on('click', '#deleteFileCancel', function () {
        let deleteContent = fileManagerModal.find('.delete-content');
        deleteContent.css('display', 'none');
    });

    fileManagerModal.on('click', '#deleteFileConfirm', function () {
        let deleteContent = fileManagerModal.find('.delete-content');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            data: {
                file_id: customMenuParent.find('.file-box,.directory-box').attr('data-id'),
            },
            url: '{{route('admin.files.delete')}}',
            success: function (data) {
                customMenuParent.remove();
            },
            error: function (data) {
                alert('"{{__('app.alerts.ajaxError')}}"');
            },
            complete: function () {
                deleteContent.css('display', 'none');
            }
        });
    });
    @endif

    fileManagerModal.on('click', '#close-rename', function () {
        hideRenameBox();
    });

    function hideRenameBox() {
        fileManagerModal.find('.file-box,.directory-box').removeClass('active');
        fileManagerModal.find('.rename-box').each(function () {
            $(this).siblings('span').show();
            $(this).remove();
        });
        fileManagerModal.removeClass('rename');
    }

    @if(auth()->user()->hasPermission('files.exportImport'))
    fileManagerModal.on('click', '#export', function () {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            method: "POST",
            url: '{{route('admin.files.export')}}',
            success: function (data) {
                if (data['success']) {
                    let downloadUrl = '{{route('admin.files.downloadExportFile',['filename'=>'FILENAME_PLACEHOLDER']) }}';
                    downloadUrl = downloadUrl.replace('FILENAME_PLACEHOLDER', data['filename']);
                    window.location.href = downloadUrl;
                } else {
                    alert('"{{__('app.alerts.ajaxError')}}"');
                }
            },
            error: function (data) {
                alert('"{{__('app.alerts.ajaxError')}}"');
            },
        });
    });

    fileManagerModal.on('click', '#import', function () {
        fileManagerModal.modal('hide');
        $('#filemanager-import-files-modal').modal('show');
    });
    @endif
</script>

