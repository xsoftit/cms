<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryItem extends Model
{
    protected $fillable = [
        'photo',
        'gallery_id',
        'order',
        'options'
    ];

    public function contents()
    {
        return $this->hasMany(GalleryItemContent::class);
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    public function getContent($language, $new_object = true)
    {
        $content = $this->contents->where('language', $language)->first();
        if (!$content && $new_object) {
            $content = new GalleryItemContent();
        }
        return $content;
    }
}
