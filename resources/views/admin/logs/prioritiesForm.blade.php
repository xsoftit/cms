<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#priorities-modal">PRIORITIES</button>
<div class="modal fade" role="dialog" id="priorities-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('app.pages.title.create')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="log-priority-form" action="{{route('logs.savePriorities')}}" method="POST"
                  id="log-priority-form">
                <div class="modal-body">
                    <div class="row">
                        @foreach(\Hermit\Logs\LogType::all() as $logType)
                            <div class="form-group col-6">
                                <label for="{{$logType->name}}">{{$logType->name}}</label>
                                <input class="form-control" id="log-priority-{{$logType->name}}"
                                       name="{{$logType->name}}"
                                       value="{{$logType->priority}}" type="number" min="1">
                            </div>
                        @endforeach
                    </div>
                    <hr>
                </div>
                <div class="modal-footer">
                    <div class="log-priority-form-actions">
                        <div class="log-priority-button-container">
                            <button type="submit" class="btn btn-outline-light" id="log-priority-button-save">
                                {{__('app.save')}}
                            </button>
                        </div>
                    </div>
                </div>
                @csrf
                @method("POST")
            </form>
        </div>
    </div>
</div>
