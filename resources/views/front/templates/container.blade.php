<{{$container->tag}}{!! ($container->classes) ? ' class="'.$container->classes.'"' :''!!}{!! ($container->style) ? ' style="'.$container->style.'"' :''!!}>
{!! $container->view !!}
</{{$container->tag}}>
