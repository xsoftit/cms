<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageSectionWidgetContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_section_widget', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('page_section_id');
            $table->bigInteger('widget_id');
            $table->integer('placement_column')->default(0);
            $table->integer('placement_order')->default(0);
            $table->timestamps();
            $table->index('page_section_id');
            $table->index('widget_id');
            $table->index('placement_column');
            $table->index('placement_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_section_widget');
    }
}
