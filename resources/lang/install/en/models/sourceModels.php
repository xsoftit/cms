<?php

return [

    'menu' => 'Source Models',
    'yes' => 'Yes',
    'no' => 'No',
    'title' => [
        'index' => 'Source Models',
        'edit' => 'Edit Source Model',
        'create' => 'Create new Source Model',
        'summary' => 'Summary'
    ],
    'subtitle' => [
        'index' => 'List of Source Models',
        'edit' => 'Edit form for Source Model',
        'create' => 'Create form for Source Model',
        'summary' => 'Summary of created source model'
    ],
    'breadcrumbs' => [
        'index' => 'Source Models',
        'edit' => 'Edit',
        'create' => 'Creating new Source Model',
        'summary' => 'Summary'
    ],
    'fields' => [
        'model_name' => 'Model name',
        'in_use' => 'In use',
        'in_use0' => 'Not used',
        'fields' => 'Accessible fields',
        'singleObject' => 'Single Object',
        'className' => 'Class Name',
        'sourceTable' => 'Source Table entry - enables creating pages associated with this model',
        'languageContent' => 'Model with content - for multiple languages',
        'columns' => 'Columns',
        'contentColumns' => 'Content Columns (column with main model id will be generated automatically)',
        'column' => [
            'type' => 'Column type',
            'name' => 'Column name',
            'default' => 'Column default value',
            'nullable' => 'Nullable',
            'unique' => 'unique',
        ],
        'addColumn' => 'Add column'
    ],
    'summary' => 'Summary',
    'generateNames' => 'Preview names',
    'names' => [
        'class_plural' => 'Plural for class name',
        'nice' => 'Nice name - example: menu',
        'table' => 'Database table name',
        'floor' => 'Floor name - example: migration name',
        'camel' => 'Camel name',
        'camel_plural' => 'Plural for camel name - example: routes/web.php'
    ]

];
