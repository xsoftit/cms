<ul class="form-group list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <label>{{__('app.sourceModels.fields.column.type')}}</label>
            </div>
            <div class="col-md-3 col-sm-12">
                <label>{{__('app.sourceModels.fields.column.name')}}</label>
            </div>
            <div class="col-md-3 col-sm-12">
                <label>{{__('app.sourceModels.fields.column.default')}}</label>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[0][type]" value="boolean"
                       readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[0][name]" value="active" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[0][modifiers][default]" value="1" readonly>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[1][type]" value="string" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[1][name]" value="name" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[1][modifiers][default]" value="" readonly>
            </div>
        </div>
    </li>
    <li class="list-group-item" id="main-slug">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[2][type]" value="string" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[2][name]" value="slug" readonly>
            </div>
            <div class="col-md-3 col-sm-12">
                <input class="form-control" type="text" name="fields[2][modifiers][default]" value="" readonly>
            </div>
        </div>
    </li>
</ul>
<div id="main-additional-columns">
    <ul class="form-group list-group">

        <li class="list-group-item">
            <button type="button" id="addColumn"
                    class="btn btn-secondary">{{__('app.sourceModels.fields.addColumn')}}</button>
        </li>
    </ul>
</div>
