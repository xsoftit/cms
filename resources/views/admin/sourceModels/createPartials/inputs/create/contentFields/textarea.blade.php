<div class="form-group">
    <label>{\{__('app.exampleModels.fields.{{$field->getName()}}')}}</label>
    <textarea name="contents[{\{$language}}][{{$field->getName()}}]" class="form-control">{\{old('contents['.$language.'][{{$field->getName()}}]')}}</textarea>
</div>
