<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\PageSection;
use Xsoft\Cms\Models\PageSectionTemplate;
use Xsoft\Cms\Services\Admin\PageSectionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PageSectionController extends Controller
{

    public function addSection(Request $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        $page = $pageLanguage->page;
        if($request->get('page_section_template_id')){
            $template = PageSectionTemplate::find($request->get('page_section_template_id'));
            $page_section = PageSectionService::create($template->id, $request->get('name'), $page);
        }else{
            $page_section = PageSectionService::copy($request->get('page_section_id'), $page, $request->get('with_content'));
        }
        $preview = $page_section->preview(true,$pageLanguage);
        return response()->json(['preview' => $preview, 'section_id' => $page_section->id]);
    }

    public function sort(Request $request)
    {
        $page_id = PageLanguage::find($request->get('page_language_id'))->page_id;
        foreach ($request->get('sort') as $key => $id) {
            DB::table('page_page_section')->where('page_id', $page_id)->where('page_section_id', $id)->update(['order' => $key]);
        }
    }

    public function edit(Request $request)
    {
        $pageLanguage = PageLanguage::find($request->get('page_language_id'));
        $sections = $pageLanguage->sections;
        $section = $sections->where('id', $request->get('section_id'))->first();
        return response()->json(['section' => $section]);
    }

    public function update(Request $request)
    {
        $pageSection = PageSection::find($request->get('section_id'));
        $pageSection->update([
            'name' => $request->get('name'),
            'slug' => Str::slug($request->get('name')),
            'classes' => $request->get('classes'),
            'styles' => $request->get('styles'),
        ]);
        DB::table('page_page_section')->where('page_section_id', $pageSection->id)->where('page_id', $request->get('page_id'))->update([
            'active' => ($request->get('active') ? 1 : 0)
        ]);
        return response()->json(['section' => $pageSection]);
    }

    public function delete(Request $request)
    {
        DB::table('page_page_section')
            ->where('page_section_id', $request->get('section_id'))
            ->where('page_id', $request->get('page_id'))
            ->delete();
        $connections = DB::table('page_page_section')->where('page_section_id', $request->get('section_id'))->get();

        if (!$connections->count()) {
            $pageSection = PageSection::find($request->get('section_id'));
            $pageSection->delete();
        }
        return response()->json(['status' => 'success'], 200);

    }
}
