<?php

namespace Xsoft\Cms\Helpers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Xsoft\Cms\Models\Language;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Services\Admin\LanguageService;

class LanguageHelper
{
    const SEEDER_TEMPLATE = 'if(!Language::where("code","CODE_REPLACE")->first()){
            $CODE_REPLACE_fields = include(base_path("/vendor/xsoft/cms/resources/lang/install/CODE_REPLACE/template.php"));
            $language = Language::create([
                "name" => "NAME_REPLACE",
                "code" => "CODE_REPLACE",
                "fields" => json_encode($CODE_REPLACE_fields),
            ]);
            Language::create([
                "name" => "NAME_REPLACE",
                "code" => "CODE_REPLACE",
                "fields" => json_encode([]),
                "front" => 1
            ]);
            Menu::create([
                "lang" => "CODE_REPLACE",
                "type" => "main",
                "items" => json_encode([])
            ]);
            Menu::create([
                "lang" => "CODE_REPLACE",
                "type" => "footer",
                "items" => json_encode([])
            ]);
            LanguageService::cacheSave($language);
        }
        //SEED_HERE';

    public static function parseKey($key)
    {
        $arrayKey = explode('-', $key);
        $newKey = '';
        foreach ($arrayKey as $aKey) {
            if ($aKey) {
                $newKey .= '[' . $aKey . ']';
            }
        }
        return $newKey;
    }

    public static function getAll()
    {
        $data = [];
        foreach (Language::adminAll() as $lang) {
            if ($lang->active) {
                $data[] = $lang->code;
            }
        }
        return $data;
    }

    public static function getAllFront()
    {
        $data = [];
        foreach (Language::frontAll() as $lang) {
            $data[] = $lang->code;
        }
        return $data;
    }

    public static function getAllActiveFront()
    {
        $data = [];
        foreach (Language::activeFrontAll() as $lang) {
            $data[] = $lang->code;
        }
        return $data;
    }

    static public function getMainPageUrl($language){
        $mainPage = PageLanguage::where('is_main', 1)->where('language', $language)->first();
        $url = route('front.index');
        if ($mainPage) {
            $url = $mainPage->url(true);
        }
        return $url;
    }

    static public function handleMainPage($pageLanguage)
    {
        $language = Language::frontAll()->where('code', $pageLanguage->language)->first();
        if ($pageLanguage->is_main) {
            $another_main = PageLanguage::where('language', $pageLanguage->language)->where('id', '!=', $pageLanguage->id)->get();
            if ($another_main->count() > 0) {
                foreach ($another_main as $am) {
                    $am->update(['is_main' => false]);
                }
            }
            $hasMain = 1;
        } else {
            $hasMain = 0;
        }
        $language->update([
            'hasMain' => $hasMain,
        ]);
    }

    static public function languageCreate($code, $name, $fields = null)
    {
        if (!Language::where("code", $code)->first()) {
            $fields = self::handleFields($fields, $code);
            if (!$fields) {
                return false;
            }
            self::languageAdminCreate($code, $name, $fields);
            self::languageFrontCreate($code, $name, json_encode([]));

            $file = File::get(database_path('/seeds/LanguageSeeder.php'));
            $data = self::SEEDER_TEMPLATE;
            $data = str_replace('CODE_REPLACE', $code, $data);
            $data = str_replace('NAME_REPLACE', $name, $data);
            $file = Str::replaceFirst('//SEED_HERE', $data, $file);
            File::put(database_path('/seeds/LanguageSeeder.php'), $file);

            return true;
        }
    }

    static public function languageAdminCreate($code, $name, $fields)
    {
        if (!Language::where("code", $code)->where('front', 0)->first()) {
            $language = Language::create([
                "name" => $name,
                "code" => $code,
                "fields" => $fields,
            ]);
            LanguageService::cacheSave($language);
            alert()->success(__('app.languages.alerts.saved', ['name' => $language->name]));
        } else {
            alert()->warning(__('validation.custom.code.unique', ['object' => __('app.languages.name')]));
        }
    }

    static public function languageFrontCreate($code, $name, $fields)
    {
        if (!Language::where("code", $code)->where('front', 1)->first()) {
            $language = Language::create([
                "name" => $name,
                "code" => $code,
                "fields" => $fields,
                "front" => 1
            ]);
            FrontMenuHelper::createMenu($language->code, 'main');
            FrontMenuHelper::createMenu($language->code, 'footer');
            LanguageService::cacheSave($language);
            alert()->success(__('app.languages.alerts.saved', ['name' => $language->name]));
        } else {
            alert()->warning(__('validation.custom.code.unique', ['object' => __('app.frontLanguages.name')]));
        }
    }

    static public function languageUpdate($code, $override = false, $fields = null, $front = 0)
    {
        $language = Language::where('code', $code)->where('front', $front)->first();
        $fields = self::handleFields($fields, $code);
        if (!$fields) {
            return false;
        }
        if (!$override) {
            $fields = json_encode(array_replace_recursive(json_decode($fields, true), json_decode($language->fields, true)));
        }
        $language->fields = $fields;
        $language->update();
        LanguageService::cacheSave($language);

        return true;
    }

    static private function handleFields($fields, $code)
    {
        if (!$fields) {
            if (File::exists(base_path("/vendor/xsoft/cms/resources/lang/install/$code/template.php"))) {
                $fields = include(base_path("/vendor/xsoft/cms/resources/lang/install/$code/template.php"));
            }
        }
        if (is_array($fields)) {
            $fields = json_encode($fields);
        }
        return $fields;
    }

}
