@section('modals')
    @parent
    <div class="modal fade" id="add-section-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.sections.title.create')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add-section-form">
                    <div class="modal-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a id="section-new-link" class="nav-link text-uppercase no-save" data-toggle="tab"
                                   href="#section-new">{{__('app.sections.new')}}</a>
                            </li>
                            <li>
                                <a id="section-copy-link" class="nav-link text-uppercase no-save" data-toggle="tab"
                                   href="#section-copy">{{__('app.sections.copy')}}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <input hidden type="submit">
                            <input hidden name="page_language_id" id="page_language_id">
                            <input hidden name="action_type" id="action_type">
                            <div id="section-new" class="tab-pane fade">
                                <div class="form-group">
                                    <label>{{__('app.sections.form.name')}}</label>
                                    <input name="name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="page_template_id"
                                           class="label-text">{{__('app.sections.form.pageSectionTemplateChoice')}}</label>
                                    <select class="form-control" name="page_section_template_id"
                                            id="page_section_template_select"
                                            required>
                                        <option value="" selected
                                                disabled>{{__('app.sections.form.pageSectionTemplateChoice')}}</option>
                                        @foreach(\Xsoft\Cms\Models\PageSectionTemplate::orderBy('name','asc')->get() as $page_section_template)
                                            <option value="{{$page_section_template->id}}"
                                                    @if(old('page_section_template_id') == $page_section_template->id) selected @endif>{{$page_section_template->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="preview" id="page-section-preview">
                                </div>
                            </div>
                            <div id="section-copy" class="tab-pane fade">
                                <div class="form-group">
                                    <label for="page-select"
                                           class="label-text">{{__('app.sections.form.pageChoice')}}</label>
                                    <select class="form-control" name="page_id" id="page-select"
                                            required>
                                        <option value="" selected
                                                disabled>{{__('app.sections.form.pageChoice')}}</option>
                                        @foreach(\Xsoft\Cms\Models\Page::orderBy('name','asc')->get() as $page)
                                            <option value="{{$page->id}}"
                                                    @if(old('selected_page_language_id') == $page->id) selected @endif>{{$page->name}}                                         @if($page->source_model_id)
                                                    <small> ({{__('app.'.$page->sourceModel->display_name)}})
                                                    </small>
                                                @endif</option>
                                        @endforeach
                                    </select>
                                    <div id="section-select-box" style="display: none">
                                        <label for="section-select"
                                               class="label-text">{{__('app.sections.form.sectionChoice')}}</label>
                                        <select class="form-control" name="page_section_id" id="section-select"
                                                required>
                                            <option class="no-delete" value="" selected
                                                    disabled>{{__('app.sections.form.pageChoice')}}</option>
                                        </select>
                                        <div class="animated-checkbox mt15">
                                            <label class="no-before">
                                                <input type="checkbox" name="with_content" id="with-content"
                                                       checked="checked">
                                                <span class="label-text text-capitalize">{{__('app.sections.form.withContent')}} <small class="color-warning">({{__('app.sections.form.withContentDescription')}})</small></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="add-section-button" class="btn btn-outline-success"
                                style="margin: 0">{{__('app.create')}}</button>
                        <button type="reset" class="btn btn-outline-light"
                                data-dismiss="modal">{{__('app.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
