<?php

return [
    "alt" => "Alt",
    "files" => "Pliki",
    "title" => "Manadżer plików",
    "rename" => "Zmień nazwę",
    "newFile" => "Opuśc pliki lub kliknij tutaj",
    "fileName" => "Nazwa pliku",
    "newFolder" => "Nowy folder",
    "selectAll" => "Wybierz wszystkie",
    "chosenFiles" => "Wybrane pliki",
    "deleteWarning" => "Zostanie on usunięty ze wszystkich widżetów",
    "directoryName" => "Nazwa folderu",
    "deleteQuestion" => "CZy na pewno chcesz usunać",
    "buttonTextMulti" => "Wybierz pliki",
    "copyToClipboard" => "Kopiuj do schowka",
    "buttonTextSingle" => "Wybierz plik",
    "directoryNotEmpty" => "Folder nie jest pusty",
    'import' => [
        "title" => "Importuj pliki",
        "button" => "Import",
        "description" => "Ta akcja spowoduje zresetowanie tabeli plików. Pamiętaj, że musisz też przenieść pliki manualnie do folderu public/storage/files."
    ]
];
