<div class="modal fade" id="language-confirmation" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('app.frontLanguages.title.confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.frontLanguages.change.state',['language' => $language->id])}}" method="POST"
                  enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <p class="modal-text">
                                @if($language->active)
                                    {!! __('app.frontLanguages.confirmationQuestion',['action' => __('app.frontLanguages.deactivate'),'user' => $language->name]) !!}
                                @else
                                    {!! __('app.frontLanguages.confirmationQuestion',['action' =>  __('app.frontLanguages.activate'),'user' => $language->name]) !!}
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success">
                        {{__('app.confirm')}}
                    </button>
                    <button type="reset" class="btn btn-outline-light" data-dismiss="modal">
                        {{__('app.discard')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="delete-front-language" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=>{{__('app.frontLanguages.title.confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">{{__('app.frontLanguages.questions.delete')}}</div>
            <div class="modal-footer">
                <form action="{{route('admin.frontLanguages.delete',['language'=>$language->id])}}" method="POST">
                    @csrf
                    @method('POST')
                    <button type="submit" class="btn btn-danger">
                        {{__('app.confirm')}}
                    </button>
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">
                        {{__('app.discard')}}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
