<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logout' => 'Logout',
    'profile' => 'Profile',
    'edit' => 'Edit',
    'add' => 'Add',
    'update' => 'Update',
    'create' => 'Create',
    'delete' => 'Delete',
    'save' => 'Save',
    'close' => 'Close',
    'saveAndBack' => 'Save and go back',
    'search' => 'Search',
    'confirm' => 'Yes',
    'discard' => 'No',
    'move' => 'Move',
    'copy' => 'Copy',
    'cancel' => 'Cancel',
    'instruction' => 'Instruction',
    'routeSelect' => [
        'choose' => 'Choose URL',
        'chooseType' => 'Choose URL type',
    ],
    'datatables' => [
        'emptyTable' => 'No records found',
        'loading' => 'Loading...',
        'searchPlaceholder' => 'Search'
    ],
    'deleteButton' => [
        'title' => 'Delete',
        'question' => 'Delete?',
        'confirm' => 'Yes',
        'cancel' => 'No'
    ],
    'dashboard' => [
        'menu' => 'Dashboard',
        'title' => 'Dashboard',
        'subtitle' => 'Administration panel main page'
    ],
    'alerts' => [
        'ajaxError' => 'Something went wrong',
        'urlExist' => "Url ':url' exist",
        'urlExistWithLanguage' => "Url ':url' with language ':language' exist"
    ],
    'galleries' => include('models/galleries.php'),
    'config' => include('models/config.php'),
    'roles' => include('models/roles.php'),
    'pages' => include('models/pages.php'),
    'languages' => include('models/languages.php'),
    'frontLanguages' => include('models/frontLanguages.php'),
    'users' => include('models/users.php'),
    'pageConfig' => include('models/pageConfig.php'),
    'widgets' => include('models/widgets.php'),
    'sections' => include('models/sections.php'),
    'containers' => include('models/containers.php'),
    'sourceModels' => include('models/sourceModels.php'),
];
