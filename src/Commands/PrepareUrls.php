<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Xsoft\Cms\Helpers\CmsConfig;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Models\SourceModel;
use Xsoft\Cms\Models\Url;

class PrepareUrls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:prepareUrls {--reset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears URL`s table and generate new one';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('reset')) {
            DB::table('urls')->truncate();
        }
        foreach(PageLanguage::all() as $pageLanguage){
            $url = Url::getUrl($pageLanguage,'PageLanguage');
            if(!$url){
                Url::handlePageLanguage($pageLanguage,'PageLanguage');
            }
        }
        foreach (SourceModel::all() as $sourceModel){
            $class = 'App\\'.$sourceModel->model_name;
            foreach ($class::all() as $model){
                if (defined($class . '::CONTENT_MODEL')) {
                    foreach ($model->contents as $content){
                        $url = Url::getUrl($content,$sourceModel->model_name);
                        if(!$url) {
                            Url::handleSourceModel($content, $sourceModel->model_name);
                        }
                    }
                }else{
                    $url = Url::getUrl($model,$sourceModel->model_name);
                    if(!$url) {
                        Url::handleSourceModel($model, $sourceModel->model_name);
                    }
                }
            }
        }
        echo 'Urls generated!'.PHP_EOL;
    }
}
