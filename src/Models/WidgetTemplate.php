<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class WidgetTemplate extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'blade_name',
        'generate'
    ];

}
