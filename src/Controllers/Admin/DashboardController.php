<?php

namespace Xsoft\Cms\Controllers\Admin;

use App\Http\Controllers\Controller;
use Xsoft\Cms\Helpers\LanguageHelper;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   LanguageHelper::languageUpdate('pl',true);
        return response()->view('cms::admin.dashboard');
    }

    public function instruction()
    {
        return view('cms::admin.instruction');
    }
}
