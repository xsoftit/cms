<?php

namespace App;

use App\ExampleModelContent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Xsoft\Cms\Models\PageLanguage;
use Xsoft\Cms\Traits\UrlTrait;

class ExampleModel extends Model
{
    use SoftDeletes;

    protected $fillable = [
//FILLABLE
    ];

    const CONTENT_MODEL = 'ExampleModelContent';

    //RELATIONS
    public function contents()
    {
        return $this->hasMany('App\ExampleModelContent');
    }

    //METHODS
    static public function fields($string = false)
    {
        $fields = [
//FILLABLE
        ];
        if ($string) {
            $output = '';
            foreach ($fields as $key => $field) {
                $output .= $field;
                if ($key + 1 != count($field)) {
                    $output .= '; ';
                }
            }
        } else {
            $output = $fields;
        }
        return $output;
    }

    public function getContent($language, $new_object = true)
    {
        $content = $this->contents->where('language', $language)->first();
        if (!$content && $new_object) {
            $content = new ExampleModelContent();
        }
        return $content;
    }

    public function getPageLanguageSlug($language)
    {
        $pageLanguage = PageLanguage::where('source_model', 'ExampleModel')->where('language', $language)->first();
        if ($pageLanguage) {
            return $pageLanguage->slug;
        }
        return 'slugModel';
    }
}
