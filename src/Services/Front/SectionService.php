<?php

namespace Xsoft\Cms\Services\Front;

use Illuminate\Support\Collection;
use Xsoft\Cms\Helpers\FrontProcessors\Column;
use Xsoft\Cms\Helpers\FrontProcessors\Section;
use Xsoft\Cms\Models\PageLanguage;

class SectionService
{
    static public function makeSections(PageLanguage $pageLanguage)
    {
        $sections = new Collection();
        foreach ($pageLanguage->activeSections as $key => $pageSection) {
            $section = new Section($pageLanguage, $pageSection);
            $sections->add($section);
        }
        return $sections;
    }

    static public function getSectionColumns(PageLanguage $pageLanguage, Section $section)
    {
        $columns = new Collection();
        foreach ($section->getPageSection()->getColumns() as $column_order => $column) {
            $columns->add(new Column($pageLanguage, $section, $column_order, $column));
        }
        return $columns;
    }
}
