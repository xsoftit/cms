@foreach( $data->get('gallery') as $item)
    <img src="{{$item->photo->path}}" alt="{{$item->photo->alt}}">
    @if($item->content)
        <h3><a href="{{$item->content->url}}">{{$item->content->title}}</a></h3>
        <p>{{$item->content->description}}</p>
    @endif
@endforeach
