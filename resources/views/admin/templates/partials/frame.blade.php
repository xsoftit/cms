@isset($column_order)
    <div class="section-column-frame new-container">
        <i class="af af-plus-square add-container" data-section="{{$id}}" data-column="{{$column_order}}"
           data-type="widget"></i>
        {*REPLACE*}
    </div>
@else
    <div class="section-frame new-container">
        <i class="af af-plus-square add-container" data-section="{{$id}}" data-column="-1"
           data-type="section"></i>
        {*REPLACE*}
    </div>
@endisset
