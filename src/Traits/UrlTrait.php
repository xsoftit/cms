<?php
/**
 * Created by PhpStorm.
 * User: czc13
 * Date: 2019-07-04
 * Time: 14:19
 */

namespace Xsoft\Cms\Traits;

use Xsoft\Cms\Helpers\CmsConfig;
use Xsoft\Cms\Models\Url;

trait UrlTrait
{
    public static function bootUrlTrait()
    {
        static::created(function ($model) {
            $className = self::getClassName($model);
            if ($className == 'PageLanguage') {
                Url::handlePageLanguage($model, $className, $model);
            } else {
                Url::handleSourceModel($model, $className, $model);
            }
        });
        static::saving(function ($model) {
            $className = self::getClassName($model);
            if ($model->parent) {
                $request = 'contents.' . $model->language . '.url';
            } else {
                $request = 'url';
            }
            $url = Url::getUrl($model, $className);
            if (request()->has($request)) {
                $urlString = request()->input($request);
                return Url::handleUpdate($urlString, $url, $model);
            }
            return true;
        });
    }

    private static function getClassName($model)
    {
        $reflect = new \ReflectionClass($model);
        if (defined($reflect->getName() . '::PARENT_MODEL')) {
            return $model::PARENT_MODEL;
        } else {
            return $reflect->getShortName();
        }
    }

    public function url($domain = false)
    {
        $className = self::getClassName($this);
        $url = Url::getUrl($this, $className);
        $return = '';
        if ($url) {
            if ($domain) {
                $return .= $_SERVER['REQUEST_SCHEME'] . '://' . $url->host;
            } else {
                $return .= '/';
                $multiDomains = CmsConfig::get('multi_domains')->value;
                if (!$multiDomains) {
                    $return .= $url->language . '/';
                }
            }
            $return .= $url->fullUrl;
        }
        return $return;
    }

    public function getUrlAttribute()
    {
        $className = self::getClassName($this);
        $url = Url::getUrl($this, $className);
        if ($url) {
            return $url->url;
        }
        return '';
    }

    public function getFullUrlAttribute()
    {
        $className = self::getClassName($this);
        $url = Url::getUrl($this, $className);
        if ($url) {
            return $url->fullUrl ? $url->fullUrl : $url->url;
        }
        return '';
    }

    public function getPrefixAttribute()
    {
        $className = self::getClassName($this);
        $url = Url::getUrl($this, $className);
        if ($url) {
            return $url->prefix . '/';
        }
        return '';
    }


    public function getRouteSelectLabelAttribute()
    {
        $className = self::getClassName($this);
        $name = '';
        if ($this->language) {
            $name = '[' . $this->language . '] ';
        }
        if ($className == 'PageLanguage') {
            $name .= $this->name;
        } else {
            $name .= $this->title ? $this->title : $this->name;
        }
        return $name;
    }

    public static function prefixes()
    {
        $return = [];
        if (defined('self::PREFIXES')) {
            if (is_array(self::PREFIXES)) {
                $return = self::PREFIXES;;
            }
        }
        return $return;
    }

    public static function getPrefix()
    {
        $prefix = '';
        $object = self::first();
        if ($object) {
            $prefix = $object->prefix;
        }
        return $prefix;
    }

    public function getHostAttribute()
    {
        $className = self::getClassName($this);
        $url = Url::getUrl($this, $className);
        if ($url) {
            return $url->host;
        }
        return '';
    }

    public function getHostWithPrefixAttribute()
    {
        $className = self::getClassName($this);
        $url = Url::getUrl($this, $className);
        if ($url) {
            return $url->hostWithPrefix;
        }
        return '';
    }
}
