@section('js')
    @parent
    @if(auth()->user()->hasPermission('page_config.edit'))
        <script>
            $(function () {
                initSortable();
            });
            body.on('click', '.menu-delete', function () {
                $(this).parent().parent().remove();
            });
            $('.item-name').on('click', function () {
                $(this).removeAttr('style');
            });
            $('.add-menu-item').on('click', function () {
                let lang = $(this).attr('data-lang');
                let type = $(this).attr('data-type');
                let fieldsBox = $('.add-menu-fields[data-lang="' + lang + '"][data-type="' + type + '"]');
                let name = fieldsBox.find('.item-name').val();
                if (!name) {
                    fieldsBox.find('.item-name').css('border-color', '#b71c1c');
                    return false;
                }
                let link = fieldsBox.find('.route-select-link').val();
                createMenuItem(lang, name, link, type);
                fieldsBox.find('.route-select-link').val('');
                fieldsBox.find('.item-name').val('');

            });

            function createMenuItem(lang, name, link, type) {
                let tab = $('#' + lang + '');
                let list = tab.find('.front-menu-sortable[data-type="' + type + '"]');
                let key = getKey(list);
                let item = '<li class="ui-state-default list-group-item" data-key="' + key + '">\n' +
                    '<div class="animated-checkbox">\n' +
                    '<label>\n' +
                    '<input type="checkbox" name="' + type + '[items][' + key + '][show]">\n' +
                    '<span class="label-text"></span>\n' +
                    '</label>\n' +
                    '<span class="menu-text">' + name + '</span> <small class="menu-text">(' + link + ')</small>' +
                    '<input class="form-control menu-name" hidden name="' + type + '[items][' + key + '][name]" value="' + name + '">\n' +
                    '<input class="form-control menu-link" hidden name="' + type + '[items][' + key + '][link]" value="' + link + '">\n' +
                    '<input class="form-control menu-parent" hidden name="' + type + '[items][' + key + '][parent]" value="">' +
                    '<input class="form-control menu-key" hidden name="' + type + '[items][' + key + '][key]" value="' + key + '">' +
                    '<i class="af af-edit menu-edit"></i>\n' +
                    '<i class="af af-times menu-delete"></i>\n' +
                    '</div>\n' +
                    ' <ul class="front-menu-sortable list-group">\n' +
                    '</ul>' +
                    '</li>';
                list.append(item);
                initSortable();
            }

            function getKey(list) {
                let key = 0;
                list.find('li').each(function () {
                    let newKey = parseInt($(this).attr('data-key'));
                    if (newKey > key) {
                        key = newKey;
                    }
                });
                return key + 1;
            }

            $('body').on('click', '.menu-edit', function () {
                let parent = $(this).parents('.list-group-item').first();
                if (parent.hasClass('edit')) {
                    parent.removeClass('edit');
                    parent.find('.menu-text:eq(0)').show();
                    parent.find('.menu-text:eq(1)').show();
                    parent.find('.menu-name:eq(0)').attr('hidden', 'hidden');
                    parent.find('.menu-link:eq(0)').attr('hidden', 'hidden');
                    parent.find('.menu-text:eq(0)').html(parent.find('.menu-name').val());
                    parent.find('.menu-text:eq(1)').html('(' + parent.find('.menu-link').val() + ')');
                } else {
                    parent.addClass('edit');
                    parent.find('.menu-text:eq(0)').hide();
                    parent.find('.menu-text:eq(1)').hide();
                    parent.find('.menu-name:eq(0)').removeAttr('hidden');
                    parent.find('.menu-link:eq(0)').removeAttr('hidden');
                }
            });

            function initSortable() {
                let block = false;
                $(".front-menu-sortable").sortable({
                    connectWith: '.front-menu-sortable',
                    axis: 'y',
                    placeholder: "list-group-item",
                    forcePlaceholderSize: true,
                    over: function (event, ui) {
                        block = false;
                        let item = ui.item;
                        let receiver = $(event.target);
                        //console.log(receiver.html());
                        if (item.find('li').length > 0 && receiver.parents('li').length > 0) {
                            block = true;
                        }
                    },
                    beforeStop: function () {
                        if (block) {
                            $(this).sortable('cancel');
                        }
                    },
                    stop: function (event, ui) {
                        let item = ui.item;
                        item.find('.menu-parent').first().val(item.parents('li').attr('data-key'));
                        console.log(item.parents('li').attr('data-key'));
                    }
                });
                $(".front-menu-sortable").disableSelection();
            }
        </script>
    @endif
@endsection
