<?php

namespace Xsoft\Cms\Requests\FrontLanguage;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'code' => [
              'required',
                Rule::unique('languages')->where(function ($query) {
                    return $query->where('id', '!=',$this->language->id)->where('front','=',1);
                })
            ],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('validation.custom.name.required'),
            'code.required' => __('validation.custom.code.required'),
            'code.unique' => __('validation.custom.code.unique',['object' => __('app.frontLanguages.name')]),
        ];
    }
}
