# 2.1.3.9 => 2.1.3.10
fixed display of pre-rendered widgets

`resources/views/front/templates/column`

from:

    @foreach($frontWidgets as $frontWidget)
            @include('front.templates.widget',['frontWidget'=>$frontWidget])
    @endforeach

to:

    @foreach($frontWidgets as $frontWidget)
        @if($frontWidget->getBladeName())
            @include('front.templates.widget',['frontWidget'=>$frontWidget])
        @else
            {!! $frontWidget->getRenderedView() !!}
        @endif
    @endforeach
