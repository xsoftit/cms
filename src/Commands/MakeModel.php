<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\Artisan;
use Xsoft\Cms\Helpers\SourceModel\Field;
use Xsoft\Cms\Helpers\SourceModel\SourceModel;

class MakeModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cms:model {model_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $composer;

    public function __construct(Composer $composer)
    {
        $this->composer = $composer;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('make:cms:directories');
        $inSourceTable = $this->confirm('Add model to Source Models table and generate widgets?: ', true);
        $isSingleObject = $this->confirm('Should this model have only one object?: ', false);
        $hasLanguageVersions = $this->confirm('Should this model have different language versions?', true);
        $class_name = $this->argument('model_name');
        $sourceModel = new SourceModel($class_name, $inSourceTable, $isSingleObject, $hasLanguageVersions);
        $sourceModel->addField(new Field('boolean', 'active', ['default' => 1]));
        $sourceModel->addField(new Field('string', 'name'));
        if ($hasLanguageVersions) {
            $sourceModel->addContentField(new Field('string', 'title'));
            $sourceModel->addContentField(new Field('string', 'slug'));
            $sourceModel->addContentField(new Field('text', 'content'));
            $sourceModel->addContentField(new Field('string', 'language'));
        } else {
            $sourceModel->addField(new Field('string', 'slug'));
        }
        $sourceModel->handle();
        $this->composer->dumpAutoloads();
        echo 'DONE' . PHP_EOL;
    }


}
