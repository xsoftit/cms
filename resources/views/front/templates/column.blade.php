@foreach($frontWidgets as $frontWidget)
    @if($frontWidget->getBladeName())
        @include('front.templates.widget',['frontWidget'=>$frontWidget])
    @else
        {!! $frontWidget->getRenderedView() !!}
    @endif
@endforeach
