<?php

namespace Xsoft\Cms\Observers;

use Xsoft\Cms\Models\Widget;

class WidgetObserver
{
    /**
     * Handle the widget "created" event.
     *
     * @param  \Xsoft\Cms\Models\Widget $widget
     * @return void
     */
    public function created(Widget $widget)
    {
        //
    }

    /**
     * Handle the widget "updated" event.
     *
     * @param  \Xsoft\Cms\Models\Widget $widget
     * @return void
     */
    public function updated(Widget $widget)
    {
        //
    }

    /**
     * Handle the widget "deleted" event.
     *
     * @param  \Xsoft\Cms\Models\Widget $widget
     * @return void
     */
    public function deleted(Widget $widget)
    {
        //
    }

    public function deleting(Widget $widget)
    {
        $contents = $widget->contents;
        foreach ($contents as $content) {
            $content->delete();
        }
    }

    /**
     * Handle the widget "restored" event.
     *
     * @param  \Xsoft\Cms\Models\Widget $widget
     * @return void
     */
    public function restored(Widget $widget)
    {
        //
    }

    /**
     * Handle the widget "force deleted" event.
     *
     * @param  \Xsoft\Cms\Models\Widget $widget
     * @return void
     */
    public function forceDeleted(Widget $widget)
    {
        //
    }
}
