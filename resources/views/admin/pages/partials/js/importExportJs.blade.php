@section('js')
    @parent
    <script>
        body = $('body');

        let page_id;

        $(document).ready(function () {
            $('.export-page').on('click', function () {
                page_id = $(this).attr('data-id');
            });

            $('#confirm-export-page').on('click', function (e) {
                $('#export-page').modal('hide');
                if (page_id !== undefined) {
                    let url = '{{route('admin.pages.export',['page'=>'PAGE_ID_PLACEHOLDER'])}}';
                    url = url.replace('PAGE_ID_PLACEHOLDER', page_id);
                    window.location.href = url;
                }
            })

        });

        //----------------------FUNCTIONS----------------------


    </script>
@endsection
