<?php

namespace Xsoft\Cms\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Xsoft\Cms\Helpers\SeederHelper;
use Xsoft\Cms\Models\WidgetTemplate;

class CreateWidgetTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cms:widgetTemplate {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('make:cms:directories');
        $name = "";
        $widgets = WidgetTemplate::all();
        $names = $widgets->pluck('name')->toArray();
        $slugs = $widgets->pluck('slug')->toArray();
        $correct = false;
        while (!$correct) {
            $name = $this->ask('Widget template name: ');
            if (!$name) {
                continue;
            }
            if (in_array($name, $names)) {
                echo 'Widget template with that name already exists.' . PHP_EOL;
                $name = "";
                continue;
            }
            $slug = Str::slug($name);
            if (in_array($slug, $slugs)) {
                echo 'Widget template with that slug already exists.' . PHP_EOL;
                $name = "";
                continue;
            }
            $blade_name = $this->ask('Blade name (default: ' . Str::camel($slug) . '): ');
            if (!$blade_name) {
                $blade_name = Str::camel($slug);
            }
            $correct = true;
        }
        $generate = $this->confirm('Should view for this template widgets be rendered on publish?', true);

        $seeder = file_get_contents(database_path('seeds/') . 'ElementsSeeder.php');
        $seeder = str_replace('//SEED_HERE', 'if (!WidgetTemplate::where(\'slug\', \'' . $slug . '\')->first()) {
            $widget = WidgetTemplate::create([
                \'name\' => \'' . $name . '\',
                \'slug\' => \'' . $slug . '\',
                \'blade_name\' => \'' . $blade_name . '\',
                \'generate\' => ' . ($generate ? 1 : 0) . ',
            ]);
            echo \'Seeded WidgetTemplate \' . $widget->name . PHP_EOL;
        }
        //SEED_HERE', $seeder);
        file_put_contents(database_path('seeds/') . 'ElementsSeeder.php', $seeder);

        if (!File::exists(resource_path('views/admin/templates/widgets/' . $blade_name . '.blade.php'))) {
            copy(__DIR__ . '/../../resources/views/admin/starters/widget.blade.php', resource_path('views/admin/templates/widgets/' . $blade_name . '.blade.php'));
        }
        if (!File::exists(resource_path('views/admin/templates/widgets/modals' . $blade_name . '.blade.php'))) {
            $source_model = $this->confirm('Will this widget use data from source model?', false);
            $widgetModal = file_get_contents(__DIR__ . '/../../resources/views/admin/starters/widgetModal.blade.php');
            if ($source_model) {
                $widgetModal = str_replace('{*{SOURCE-MODEL}*}', "<input id=\"source_model\" name=\"source_model\" type=\"text\" value=\"\" class=\"data\" hidden>", $widgetModal);
            } else {
                $widgetModal = str_replace('{*{SOURCE-MODEL}*}', "", $widgetModal);
            }

            file_put_contents(resource_path('views/admin/templates/widgets/modals/' . $blade_name . '.blade.php'), $widgetModal);
        }
        if (!File::exists(resource_path('views/front/templates/widgets/' . $blade_name . '.blade.php'))) {
            copy(__DIR__ . '/../../resources/views/front/starters/widget.blade.php', resource_path('views/front/templates/widgets/' . $blade_name . '.blade.php'));
        }
        echo 'Created WidgetTemplate :' . PHP_EOL . "\tname: " . $name . PHP_EOL . "\tslug: " . $slug . PHP_EOL . "\tblade_name: " . $blade_name;
    }
}
