<?php

namespace Xsoft\Cms\Helpers;

use Illuminate\Support\Str;
use Xsoft\AdminPanel\Menu;
use Xsoft\AdminPanel\MenuItem;
use Xsoft\Cms\Models\SourceModel;

class MenuHelper
{
    public static function prepare($items)
    {
        $sourceModelsPermissions = [];
        foreach (SourceModel::all() as $model) {
            $sourceModelsPermissions[] = Str::lower($model->model_name) . 's.show';
        }
        $menu = Menu::make(
            MenuItem::create('CMS'),
            MenuItem::create(__("app.dashboard.menu"), route('admin.dashboard'), 'af-home'),
            MenuItem::create(__("app.pages.menu"), route('admin.pages.index'), 'af-file')->activeOn([
                'pages/',
            ]),
            MenuItem::create(__("app.pageConfig.menu"), route('admin.pageConfig.index'), 'af-wrench'),
            MenuItem::create(__("app.frontLanguages.menu"), route('admin.frontLanguages.index'), 'af-language')->activeOn([
                'site/languages',
            ]),
            MenuItem::create('hr')->permissions($sourceModelsPermissions),
            MenuItem::create(__("app.sourceModels.menu"), route("admin.sourceModels.index"), 'af-list'),
            MenuItem::create(__("app.galleries.menu"), route('admin.galleries.index'), 'af-image')->activeOn([
                'galleries/+.+/edit',
                'galleries/create',
            ])
        );
        call_user_func_array([$menu, 'addItems'], $items);
        $menu->addItems(
            MenuItem::create('hr')->permissions(['users.show', 'config.show', 'roles.show']),
            MenuItem::create(__("app.users.menu"), route('admin.users.index'), 'af-users')->activeOn([
                'users/+.+/edit',
            ]),
            MenuItem::create(__("app.config.menu"), route('admin.config.edit'), 'af-cog')->activeOn([
                'config/languages',
            ]),
            MenuItem::create(__("app.roles.menu"), route('admin.roles.index'), 'af-user-check')->activeOn([
                'roles/+.+/edit',
            ]),
            MenuItem::create('hr')->permissions(['logs.show']),
            //MenuItem::create(__("app.instruction"), route('admin.instruction'), 'af-question'),
            MenuItem::create('Logs', route('logs.index'), 'af-list')
        );
        return $menu->output();
    }
}
