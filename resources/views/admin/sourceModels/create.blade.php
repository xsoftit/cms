@extends('cms::admin.layouts.app')

@section('css')
    @parent
    <style>
        i.remove-column {
            font-size: 20px;
        }

        i.remove-column:hover {
            cursor: pointer;
        }

        body.app .list-group-item .animated-checkbox {
            display: inline-block;
            margin-right: 25px;
        }
    </style>
@endsection

@section('page-title')
    {{__('app.sourceModels.title.create')}}
@endsection

@section('title')
    {{__('app.sourceModels.title.create')}}
@endsection
@section('subtitle')
    {{__('app.sourceModels.subtitle.create')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title'),route('admin.dashboard')],[__('app.sourceModels.breadcrumbs.index'),route('admin.sourceModels.index')],[__('app.sourceModels.breadcrumbs.create')]) !!}
@endsection

@section('page-content')
    <form action="{{route('admin.sourceModels.store')}}" method="POST">
        @csrf
        @method('POST')
        <div class="form-group">
            <label>{{__('app.sourceModels.fields.className')}}</label>
            <div class="input-group">
                <input id="class-name" type="text" name="className" value="{{old('className')}}" class="form-control"
                       required>
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="button"
                            id="generate-names-btn">{{__('app.sourceModels.generateNames')}}</button>
                </div>
            </div>
        </div>
        <div id="generated-names">
        </div>
        <div class="animated-checkbox">
            <label>
                <input type="checkbox" name="sourceTable" checked="true">
                <span
                    class="label-text text-capitalize">{{__('app.sourceModels.fields.sourceTable')}}</span>
            </label>
        </div>
        <div class="animated-checkbox">
            <label>
                <input type="checkbox" id="languageContent"
                       name="languageContent"{{old('languageContent')?" checked='true'":""}}>
                <span
                    class="label-text text-capitalize">{{__('app.sourceModels.fields.languageContent')}}</span>
            </label>
        </div>
        <div class="animated-checkbox">
            <label>
                <input type="checkbox" name="singleObject"{{old('singleObject')?" checked='true'":""}}>
                <span
                    class="label-text text-capitalize">{{__('app.sourceModels.fields.singleObject')}}</span>
            </label>
        </div>
        <hr>
        <label>{{__('app.sourceModels.fields.columns')}}</label>
        @include('cms::admin.sourceModels.partials.mainColumns')
        <div id="language-content-columns" style="display: none">
            <hr>
            @include('cms::admin.sourceModels.partials.contentColumns')
        </div>
        <div class="form-group h-20">
            <button type="submit" class="btn btn-success form float-right">{{__('app.create')}}</button>
        </div>
    </form>
    <template id="columnTemplate">
        @include('cms::admin.sourceModels.partials.column',['class'=>'column','name'=>'fields'])
    </template>
    <template id="contentColumnTemplate">
        @include('cms::admin.sourceModels.partials.column',['class'=>'contentColumn','name'=>'contentFields'])
    </template>
@endsection

@section('js')
    @parent
    @include('cms::admin.sourceModels.partials.columnJs')
    @include('cms::admin.sourceModels.partials.generateNameInputs')
@endsection
