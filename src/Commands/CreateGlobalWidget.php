<?php

namespace Xsoft\Cms\Commands;

use Xsoft\Cms\Models\Widget;
use Xsoft\Cms\Models\WidgetTemplate;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CreateGlobalWidget extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cms:globalWidget';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = "";
        $correct = false;
        $globalWidgets = Widget::where('is_global', 1)->get();
        $names = $globalWidgets->pluck('name')->toArray();
        $slugs = $globalWidgets->pluck('slug')->toArray();
        while (!$correct) {
            $name = $this->ask('Widget name: ');
            if (!$name) {
                continue;
            }
            if (in_array($name, $names)) {
                echo 'Widget with that name already exists.' . PHP_EOL;
                $name = "";
                continue;
            }
            $slug = Str::slug($name);
            if (in_array($slug, $slugs)) {
                echo 'Widget with that slug already exists.' . PHP_EOL;
                $name = "";
                continue;
            }
            $correct = true;
        }
        $correct = false;
        while (!$correct) {
            $template_slug = $this->ask('Enter template slug: ');
            $widgetTemplate = WidgetTemplate::where('slug', $template_slug)->first();
            if (!$widgetTemplate) {
                echo 'Widget template not found!' . PHP_EOL;
                continue;
            }
            $correct = true;
        }
        $seeder = file_get_contents(database_path('seeds/') . 'ElementsSeeder.php');
        $seeder = str_replace('//SEED_HERE', 'if (!Widget::where(\'slug\', \'' . $slug . '\')->where(\'name\',\'' . $name . '\')->first()) {
            $widget = Widget::create([
                \'widget_template_id\' => \'' . $widgetTemplate->id . '\',
                \'name\' => \'' . $name . '\',
                \'slug\' => \'' . $slug . '\',
                \'is_global\' => \'' . 1 . '\',
                \'class\' => \'\',
            ]);
            echo \'Seeded GlobalWidget \' . $widget->name . PHP_EOL;
        }
        //SEED_HERE', $seeder);
        file_put_contents(database_path('seeds/') . 'ElementsSeeder.php', $seeder);

        Artisan::call('db:seed --class=ElementsSeeder');
        echo 'Created Widget :' . PHP_EOL . "\tname: " . $name . PHP_EOL . "\tslug: " . $slug . PHP_EOL . "\twidget_template_id: " . $widgetTemplate->id . PHP_EOL . "\twidget_template_slug: " . $widgetTemplate->slug;
    }
}
