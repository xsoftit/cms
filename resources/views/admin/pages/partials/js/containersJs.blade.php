@section('js')
    @parent
    <script>
        body = $('body');
        let cointanerModals = {
            'add': $('#add-container-modal'),
            'edit': $('#edit-container-modal'),
        };
        let containerPlacement,type;
        var section = {
            name: 'section',
            parent: '.section-preview',
            class: 'section-frame'
        };
        var widget = {
            name: 'widget',
            parent: '.section-col',
            class: 'section-column-frame'
        };
        $(document).ready(function () {
            body.on('click', '.add-container', function () {
                cointanerModals.add.find('#section_id').val($(this).attr('data-section'));
                cointanerModals.add.find('#column_id').val($(this).attr('data-column'));
                cointanerModals.add.modal('show');
                let data_type = $(this).attr('data-type')
                type = window[data_type];
                containerPlacement = $(this).parents(type.parent);
            });
            body.on('click', '.edit-container', function () {
                cointanerModals.edit.find('#container_id').val($(this).attr('data-container'));
                type = window[$(this).attr('data-type')];
                containerPlacement = $(this).parents(type.parent);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    data: {
                        container_id: $(this).attr('data-container')
                    },
                    url: '{{route('admin.containers.get')}}',
                    success: function (data) {
                        fillContainerEditModal(data.data);
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    }
                });
            });
            body.on('click', '.move-down-container', function () {
                type = window[$(this).attr('data-type')];
                containerPlacement = $(this).parents(type.parent);
                let data_container = $(this).attr('data-container');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    data: {
                        dir: 0,
                        container_id: data_container
                    },
                    url: '{{route('admin.containers.move')}}',
                    success: function (data) {
                        handleMove(0,data_container);
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    }
                });
            });
            body.on('click', '.move-up-container', function () {
                type = window[$(this).attr('data-type')];
                containerPlacement = $(this).parents(type.parent);
                let data_container = $(this).attr('data-container');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    data: {
                        dir: 1,
                        container_id: data_container
                    },
                    url: '{{route('admin.containers.move')}}',
                    success: function (data) {
                        handleMove(1,data_container);
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    }
                });
            });
            cointanerModals.add.on('click', '#add-container-button', function () {
                let data = {
                    page_section_id: cointanerModals.add.find('#section_id').val(),
                    placement_column: cointanerModals.add.find('#column_id').val(),
                    tag: cointanerModals.add.find('#container_tag').val(),
                    classes: cointanerModals.add.find('#container_classes').val(),
                    style: cointanerModals.add.find('#container_styles').val(),
                };
                if (validateForm($(this).parents('form'))) {
                    addContainer(data);
                }
            });
            cointanerModals.edit.on('click', '#edit-container-button', function () {
                let data = {
                    container_id: cointanerModals.edit.find('#container_id').val(),
                    tag: cointanerModals.edit.find('#container_tag').val(),
                    classes: cointanerModals.edit.find('#container_classes').val(),
                    style: cointanerModals.edit.find('#container_styles').val(),
                };
                if (validateForm($(this).parents('form'))) {
                    editContainer(data);
                }
            });

            cointanerModals.edit.on('click', '#deleteContainer', function () {
                let deleteContent = cointanerModals.edit.find('.delete-content');
                deleteContent.css('display', 'flex');
            });

            cointanerModals.edit.on('click', '#deleteContainerCancel', function () {
                let deleteContent = cointanerModals.edit.find('.delete-content');
                deleteContent.css('display', 'none');
            });

            cointanerModals.edit.on('click', '#deleteContainerConfirm', function () {
                let container_id = cointanerModals.edit.find('#container_id').val();
                let deleteContent = cointanerModals.edit.find('.delete-content');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    data: {
                        container_id: container_id
                    },
                    url: '{{route('admin.containers.delete')}}',
                    success: function (data) {
                        cointanerModals.edit.modal('hide');
                        handleDelete(container_id);
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    },
                    complete: function () {
                        deleteContent.css('display', 'none');
                    }
                });
            });

            function fillContainerEditModal(data) {
                cointanerModals.edit.find('#container_tag').val(data.tag);
                cointanerModals.edit.find('#container_classes').val(data.classes);
                cointanerModals.edit.find('#container_styles').val(data.style);
                cointanerModals.edit.modal('show');
            }

            function addContainer(data) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    data: data,
                    url: '{{route('admin.containers.add')}}',
                    success: function (data) {
                        insertContainer(data.data);
                        cointanerModals.add.modal('hide');
                        cointanerModals.add.find('input').val('');
                        showAlert("{{__('app.containers.alerts.added')}}", 'success');
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    }
                });
            }

            function insertContainer(data) {
                let frame = containerPlacement.find('.new-container');
                let block = '<div class="'+type.class+'">\n' +
                    '<h3 class="container-header"><span data-container="' + data.id + '">' + data.tag + ' <small>(' + (data.classes ? data.classes : '') + ')</small></span>\n' +
                    '<div class="float-right">\n' +
                    '<i class="af af-arrow-down move-down-container"  data-container="' + data.id + '" data-type="'+type.name+'"></i>\n' +
                    '<i class="af af-arrow-up move-up-container" data-container="' + data.id + '" data-type="'+type.name+'"></i>\n' +
                    '<i class="af af-edit edit-container" data-container="' + data.id + '" data-type="'+type.name+'"></i>\n' +
                    '</div>' +
                    '</h3>\n' +
                    '<div class="'+type.class+' new-container">\n' +
                    frame.html() +
                    '</div>' +
                    '</div>';
                // console.log($(block).html());
                // $(block).find('h3').after(frame);
                // console.log($(block).html());
                frame.replaceWith(block);
                containerPlacement.find('i[data-type="'+type.name+'"]').show();
                containerPlacement.find('.move-up-container[data-type="'+type.name+'"]').first().hide();
                containerPlacement.find('.move-down-container[data-type="'+type.name+'"]').last().hide();
            }

            function editContainer(data) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    method: "POST",
                    data: data,
                    url: '{{route('admin.containers.edit')}}',
                    success: function (data) {
                        editContainerHeader(data.data);
                        cointanerModals.edit.modal('hide');
                        cointanerModals.edit.find('input').val('');
                        showAlert("{{__('app.containers.alerts.updated')}}", 'success');
                    },
                    error: function (data) {
                        alert('"{{__('app.alerts.ajaxError')}}"');
                    }
                });
            }

            function editContainerHeader(data) {
                let span = containerPlacement.find('span[data-container="' + data.container_id + '"]');
                console.log(span.html());
                span.html(data.tag + ' <small>(' + data.classes + ')</small>');
            }

            function handleMove(dir,container_id){
                let header = containerPlacement.find('span[data-container="' + container_id + '"]').parents('h3');
                let secondHeader;
                if(dir === 1){
                    secondHeader = header.parent().siblings('h3');
                }else{
                    secondHeader = header.siblings('div').find('h3').first();
                }
                let secondHeaderHtml = secondHeader.html();
                secondHeader.html(header.html());
                header.html(secondHeaderHtml);
                containerPlacement.find('i[data-type="'+type.name+'"]').show();
                containerPlacement.find('.move-up-container[data-type="'+type.name+'"]').first().hide();
                containerPlacement.find('.move-down-container[data-type="'+type.name+'"]').last().hide();
            }

            function handleDelete(container_id){
                let container = containerPlacement.find('span[data-container="' + container_id + '"]').parents('.'+type.class).first();
                let html = container.find('.'+type.class).first();
                container.replaceWith(html);
                containerPlacement.find('i[data-type="'+type.name+'"]').show();
                containerPlacement.find('.move-up-container[data-type="'+type.name+'"]').first().hide();
                containerPlacement.find('.move-down-container[data-type="'+type.name+'"]').last().hide();
            }
        });
    </script>
@endsection
