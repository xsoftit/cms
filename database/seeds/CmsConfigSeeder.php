<?php

use Xsoft\Cms\Models\Config;
use Xsoft\Cms\Models\File;
use Illuminate\Database\Seeder;

class CmsConfigSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!File::where('name', 'default.jpg')->first()) {
             File::create([
                'name' => 'default.jpg',
                'storage_path' => 'public/default.jpg',
                'type' => 'image'
            ]);
        }
        if (!Config::where('name', 'default_lang')->first()) {
            Config::create([
                'name' => 'default_lang',
                'label' => 'defaultLang',
                'value' => '',
                'category' => 'general',
                'type' => 'lang'
            ]);
        }
        if (!Config::where('name', 'default_front_language')->first()) {
            Config::create([
                'name' => 'default_front_language',
                'label' => 'defaultFrontLanguage',
                'value' => '',
                'category' => 'general',
                'type' => 'front_lang'
            ]);
        }
        if (!Config::where('name', 'admin_logo')->first()) {
            Config::create([
                'name' => 'admin_logo',
                'label' => 'adminLogo',
                'category' => 'general',
                'value' => '',
                'type' => 'image',
                'description' => 'adminLogo'
            ]);
        }
        if (!Config::where('name', 'multi_domains')->first()) {
            Config::create([
                'name' => 'multi_domains',
                'label' => 'multiDomains',
                'category' => 'general',
                'value' => '0',
                'type' => 'checkbox',
            ]);
        }
        if (!Config::where('name', 'logo_1')->first()) {
            $file = File::where('name','default.jpg')->first();
            Config::create([
                'name' => 'logo_1',
                'label' => 'logo1',
                'value' => json_encode([$file->id]),
                'category' => 'page',
                'type' => 'image',
                'description' => 'mainLogo'
            ]);
        }
        if (!Config::where('name', 'logo_2')->first()) {
            $file = File::where('name','default.jpg')->first();
            Config::create([
                'name' => 'logo_2',
                'label' => 'logo2',
                'value' => json_encode([$file->id]),
                'category' => 'page',
                'type' => 'image',
                'description' => 'secondaryLogo'
            ]);
        }

        if (!Config::where('name', 'title_prefix')->first()) {
            Config::create([
                'name' => 'title_prefix',
                'label' => 'titlePrefix',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }

        if (!Config::where('name', 'company_name')->first()) {
            Config::create([
                'name' => 'company_name',
                'label' => 'companyName',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'company_address_1')->first()) {
            Config::create([
                'name' => 'company_address_1',
                'label' => 'companyAddress1',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'company_address_2')->first()) {
            Config::create([
                'name' => 'company_address_2',
                'label' => 'companyAddress2',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'company_phone')->first()) {
            Config::create([
                'name' => 'company_phone',
                'label' => 'companyPhone',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'company_email')->first()) {
            Config::create([
                'name' => 'company_email',
                'label' => 'companyEmail',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'facebook_link')->first()) {
            Config::create([
                'name' => 'facebook_link',
                'label' => 'facebookLink',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'twitter_link')->first()) {
            Config::create([
                'name' => 'twitter_link',
                'label' => 'twitterLink',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'youtube_link')->first()) {
            Config::create([
                'name' => 'youtube_link',
                'label' => 'youtubeLink',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'google_api_key')->first()) {
            Config::create([
                'name' => 'google_api_key',
                'label' => 'googleApiKey',
                'value' => '',
                'category' => 'page',
                'type' => 'info'
            ]);
        }
        if (!Config::where('name', 'domains')->first()) {
            Config::create([
                'name' => 'domains',
                'label' => 'domains',
                'value' => json_encode([config('cms.FRONT_DOMAIN') => 'en']),
                'category' => 'domains',
                'type' => 'domains'
            ]);
        }
    }
}
