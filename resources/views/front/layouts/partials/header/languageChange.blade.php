@foreach($languages as $lang => $url)
    @if($url=='current')
        <span class="text-white text-uppercase{{!$loop->last?' mr10':''}}" style="cursor: default">{{$lang}}</span>
    @else
        <a class="text-uppercase{{!$loop->last?' mr10':''}}"
           href="{{$url}}"
           style="color: gray">{{$lang}}</a>
    @endif
@endforeach
