@section('modals')
    @parent
    <div class="modal fade" id="add-language-version-modal" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{__('app.pages.title.addLanguageVersion')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.pages.addLanguageVersion')}}" method="POST">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="page_id">
                        {{--<input type="hidden" name="origin_lang">--}}
                        <div class="form-group">
                            <label>{{__('app.pages.form.chooseLanguageForPage')}}</label>
                            <div id="languages-select">
                                {{--SELECT INSERTED BY JS--}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label>{{__('app.pages.form.name')}}</label>
                            <input name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>{{__('app.pages.form.metaTitle')}}</label>
                            <input class="form-control" type="text" name="meta_title" required>
                        </div>
                        <div class="form-group">
                            <label>{{__('app.pages.form.description')}}</label>
                            <textarea class="form-control" name="meta_description"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-success">{{__('app.add')}}</button>
                        <button type="reset" class="btn btn-outline-light" data-dismiss="modal">{{__('app.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
