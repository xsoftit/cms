@component('cms::admin.templates.widgetModal',get_defined_vars())
    <div class="form-group">
        <label class="label-text">{{__('app.widgets.labels.shortText')}}</label>
        <textarea id="text" name="text" class="form-control data short"></textarea>
    </div>
@endcomponent
