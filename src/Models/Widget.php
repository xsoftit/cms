<?php

namespace Xsoft\Cms\Models;

use Illuminate\Database\Eloquent\Model;
use Xsoft\Cms\Helpers\FrontProcessors\FrontWidget;
use Xsoft\Cms\Services\Admin\WidgetService;

class Widget extends Model
{
    protected $widgetService;

    public function __construct(array $attributes = [])
    {
        $this->widgetService = new WidgetService();
        parent::__construct($attributes);
    }

    protected $fillable = [
        'widget_template_id',
        'name',
        'slug',
        'content',
        'class',
        'is_global'
    ];

    // RELATIONS

    public function template()
    {
        return $this->belongsTo('Xsoft\Cms\Models\WidgetTemplate', 'widget_template_id');
    }

    public function section()
    {
        return $this->belongsToMany('Xsoft\Cms\Models\PageSection');
    }

    public function contents()
    {
        return $this->hasMany('Xsoft\Cms\Models\WidgetContent');
    }

    // ATTRIBUTES

    public function getBladeNameAttribute()
    {
        return $this->template->blade_name;
    }

    public function getPageAttribute()
    {
        return $this->section->page;
    }


    // METHODS

    public function getWidgetService()
    {
        return $this->widgetService;
    }

    public function getContent($param, $pageLanguage)
    {
        $language = null;
        if (is_string($pageLanguage)) {
            $language = $pageLanguage;
        } else if ($pageLanguage) {
            $language = $pageLanguage->language;
        }
        $content = json_decode($this->getContents($language), true);
        if (is_array($content)) {
            if (key_exists($param, $content)) {
                if(key_exists('is_gallery',$content)){
                    if($param = $content['is_gallery']){
                        $gallery = $this->getGallery($content[$param]);
                        if($gallery){
                            return $gallery;
                        }
                    }
                }
                return $content[$param];
            }
        }
        return null;
    }

    public function getGallery($id){
        $gallery = Gallery::find($id);
        if($gallery){
            return $gallery->name;
        }
        return false;
    }


    public function getContents($language)
    {
        $content = '[]';
        if ($language) {
            $widgetContent = $this->contents->where('language', $language)->first();
            if ($widgetContent) {
                $content = $widgetContent->content;
            }
        }
        if (!$content) {
            $content = '[]';
        }
        return $content;
    }

    static public function getGlobals()
    {
        return Widget::where('is_global', 1)->get();
    }

    static public function getGlobal($slug)
    {
        return Widget::getGlobals()->where('slug', $slug)->first();
    }

    // FRONT

    public function preview($pageLanguage = null)
    {
        return view('cms::admin.templates.widgets.widgetLayout', ['widget' => $this, 'pageLanguage' => $pageLanguage, 'widgetService' => new WidgetService()])->render();
    }


    public function getFrontContent($pageLanguage)
    {
        $data = \Xsoft\Cms\Services\Front\WidgetService::handleWidget($pageLanguage, $this);
        $frontWidget = new FrontWidget($data);
        $view = view('front.templates.widget', ['frontWidget' => $frontWidget])->render();
        return $view;
    }

    static public function getFrontGlobal($slug, $pageLanguage)
    {
        return Widget::getGlobal($slug)->getFrontContent($pageLanguage);
    }
}
